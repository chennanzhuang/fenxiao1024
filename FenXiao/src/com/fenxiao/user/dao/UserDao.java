package com.fenxiao.user.dao;

import java.util.List;

import java.util.List;

import com.fenxiao.agent.entity.Agent;
import com.fenxiao.common.Pager;
import com.fenxiao.user.entity.User;

/**
 * 
 * @author Administrator
 * 
 */
public interface UserDao {
	public int updateUsergeren(User user);
	/**
	 * 审核失败后返还钱
	 * */
	public int updateBalanceUsermonye(Double money, long userId);
	
	public int updateBalanceOfUser(Double money, long userId);

	/**
	 * 根据登录名查询
	 */

	public User queryUser(String userLoginName);
	
	/**
	 * 分页查询直属团队
	 */
	
	public Pager queryZhishu(int pageSize, int page, String search,User user);
	/**
	 * 分页查询全部成员
	 */
	
	public Pager queryQuanbu(int pageSize, int page, String search,User user);

	/**
	 * 查询所有记录数
	 * 
	 * @param hql
	 *            查询的条件
	 * @return 总记录数
	 */
	public int getAllRowCount(String hql);

	/**
	 * 分页查询
	 * 
	 * @param hql
	 *            查询的条件
	 * @param offset
	 *            开始记录
	 * @param length
	 *            一次查询几条记录
	 * @return
	 */
	public List queryForPage(String hql, int offset, int length);

	/**
	 * 查询所有记录数(原生态sql)
	 * 
	 * @param hql
	 *            查询的条件
	 * @return 总记录数
	 */
	public int getAllSQLRowCount(String hql);

	/**
	 * 分页查询
	 * 
	 * @param sql
	 *            查询的条件
	 * @param offset
	 *            开始记录
	 * @param length
	 *            一次查询几条记录
	 * @return
	 */
	public List querySQLForPage(String hql, int offset, int length);

	// public int addUser(User user);

	public User queryMoneyOfUser(String openId);

	public User queryUserById(Long userId);

	// public int updateBalanceOfUser(Double money,long userId);

	public User queryUserUnSubscribe();

	public int updateUser(User user, long userId);

	public int updateUserByOpenId(User user);

	public int updateUserForCompleteRegisteration(User user);

	public User weixinLogin(User user);

	/**
	 * 分页查询员工佣金	
	 */
	public Pager queryPageForStaff(int pageSize, int page, String search,
			long agentId);

	/**
	 * 分页查询员工佣金	
	 */
	public Pager queryUserPage(int pageSize, int page, String search,
			long agentId);

	/**
	 * 添加员工
	 */
	public int addUser(User user);

	/**
	 * 批量删除员工
	 */
	public int deleteUser(String num);

	/**
	 * 批量删除员工
	 */
	public int deleteUser1(long id);

	/**
	 * 查询详情
	 */
	public User queryUserInfo(User user);

	/**
	 * 分页查询所有的会员
	 */
	public Pager queryAllUserPage(int pageSize, int page, String search);

	/**
	 * 查询所有的会员
	 */
	public List<User> queryUserList();

	/**
	 * 修改
	 */
	public int updateUser(User user);

	public String getRecommendCode();

	// 查询所有员工
	public List<User> queryGetAll(Long userid);

	public int updateUserAfterSubscribe(User user);

	public List<User> queryTeamOfTheUser(User user, Integer userLevel,
			String search);

	public int updateUpdateUnsubscribe(String openId);

	public int resetPassword(long userId, String password);

	public int updateUserByMobile(User user);

	public User checkMobile(String mobile);

	/**
	 * 查询user
	 */
	public User queryUserSadff(String recode);

	/**
	 * 查询员工
	 */
	public User queryUserSdf(long sadffId);
	/**
	 * 通过代理商查询员工
	 */
	public List queryUseragent(long agentId);
}
