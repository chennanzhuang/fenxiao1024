package com.fenxiao.user.service.impl;


import java.util.List;
import java.util.List;

import com.fenxiao.common.Pager;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fenxiao.user.dao.UserDao;
import com.fenxiao.user.dao.impl.UserDaoImpl;
import com.fenxiao.user.entity.User;
import com.fenxiao.user.service.UserService;
@Service
@Transactional
public class UserServiceImpl implements UserService {
	
@Resource
private UserDao userDao;

public UserDao getUserDao() {
	return userDao;
}


public void setUserDao(UserDao userDao) {
	this.userDao = userDao;
}
public int updateUsergeren(User user){
	userDao.updateUsergeren(user);
	return 1;
	
}
//检查登陆
public User checkUserLogin(User user) {
User item=userDao.queryUser(user.getUsername());
if(item!=null&&item.getUserPassword().equals(user.getUserPassword().trim())){
return item;//登陆成功
}else{
	return null;//登录失败
}
}
	@Override
		public int updateBalanceOfUser(Double money, long userId) {
			// TODO Auto-generated method stub
			return userDao.updateBalanceOfUser(money, userId);
		}
	
/*	@Override
	public int addUser(User user) {
		// TODO Auto-generated method stub
		return userDao.addUser(user);
	}
	
	@Override
	


@Override
	public int updateBalanceOfUser(Double money, long userId) {
		// TODO Auto-generated method stub
		return userDao.updateBalanceOfUser(money, userId);
	}



@Override
	public User queryMoneyOfUser(String openId) {
		// TODO Auto-generated method stub
		return userDao.queryMoneyOfUser(openId);
	}

*/
/**
* 分页查询员工佣金	
*/
public Pager queryPageForStaff(int pageSize,int page,String search, long agentId){
	
return userDao.queryPageForStaff(pageSize, page, search, agentId);		
}

/**
* 分页查询员工	
*/
public Pager queryUserPage(int pageSize,int page,String search, long agentId){
	return userDao.queryUserPage(pageSize, page, search, agentId);
}

/**
* 添加员工
*/
public int addUser(User user){
	return userDao.addUser(user);
}
/**
* 批量删除员工
*/
public int deleteUser(String num){
	return userDao.deleteUser(num);
}
/**
* 批量删除员工
*/
public int deleteUser1(long id){
	return userDao.deleteUser1(id);
}
/**
* 查询详情
*/
public User queryUserInfo(User user){
	return userDao.queryUserInfo(user);
}
/**
* 分页查询所有的会员
*/
public Pager queryAllUserPage(int pageSize,int page,String search){
	return userDao.queryAllUserPage(pageSize, page, search);
}
/**
* 查询所有的会员
*/
public List<User> queryUserList(){
	return userDao.queryUserList();
}
/**
 * 修改
 */
public int updateUser(User user){
	return userDao.updateUser(user);
}

public User queryuserById(long userId) {
	// TODO Auto-generated method stub
	return userDao.queryUserById(userId);
}

public User queryUserUnSubscribe() {
	// TODO Auto-generated method stub
	return userDao.queryUserUnSubscribe();
}

@Override
public int updateUser(User user,long userId) {
	// TODO Auto-generated method stub
	return userDao.updateUser(user,userId);
}

@Override
public int updateUSerByOpenId(User user) {
	// TODO Auto-generated method stub
	return userDao.updateUserByOpenId(user);
}

@Override
public int updateUserForCompleteRegisteration(User user) {
	// TODO Auto-generated method stub
	return userDao.updateUserForCompleteRegisteration(user);
}

@Override
public User weixinLogin(User user) {
	// TODO Auto-generated method stub
	return userDao.weixinLogin(user);
}


@Override
	public String getRecommendCode() {
		// TODO Auto-generated method stub
		return userDao.getRecommendCode();
	}

@Override
	public int updateUserAfterSubscribe(User user) {
		// TODO Auto-generated method stub
		return userDao.updateUserAfterSubscribe(user);
	}

@Override
	public List<User> queryTeamOfTheUser(User user,Integer userLevel,String search) {
		// TODO Auto-generated method stub
		return userDao.queryTeamOfTheUser(user,userLevel,search);
	}

@Override
	public int updateUpdateUnsubscribe(String openId) {
		// TODO Auto-generated method stub
		return userDao.updateUpdateUnsubscribe(openId);
	}

@Override
	public int resetPassword(long userId, String password) {
		// TODO Auto-generated method stub
		return userDao.resetPassword(userId, password);
	}

@Override
	public int updateUserByMobile(User user) {
		// TODO Auto-generated method stub
		return userDao.updateUserByMobile(user);
	}

public User queryMoneyOfUser(String openId) {
	// TODO Auto-generated method stub
	return userDao.queryMoneyOfUser(openId);
}

@Override
	public User checkMobile(String mobile) {
		// TODO Auto-generated method stub
		return userDao.checkMobile(mobile);
	}
/**
 * 查询user
 */
 public User queryUserSadff(String recode){
	 return userDao.queryUserSadff(recode);
 }
 /**
  * 查询员工
  */
 public User queryUserSdf(long sadffId){
	 return userDao.queryUserSdf(sadffId);
 }

@Override
public List<User> queryGetAll(Long userid) {
	
	return userDao.queryGetAll(userid);
}
//直属团队
@Override
public Pager queryZhishu(int pageSize, int page, String search, User user) {
	// TODO Auto-generated method stub
	return userDao.queryZhishu(pageSize, page, search, user);
}
//全部
@Override
public Pager queryQuanbu(int pageSize, int page, String search, User user) {
	// TODO Auto-generated method stub
	return userDao.queryQuanbu(pageSize, page, search, user);
}

@Override
public int updateBalanceUsermonye(Double money, long userId) {
	  
	return userDao.updateBalanceUsermonye(money, userId);
}


@Override
public List queryUseragent(long agentId) {
	
	return userDao.queryUseragent(agentId);
}
}
