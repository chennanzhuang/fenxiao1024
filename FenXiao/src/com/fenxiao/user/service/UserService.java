package com.fenxiao.user.service;

import java.util.List;
import java.util.List;

import com.fenxiao.common.Pager;
import com.fenxiao.user.entity.User;

public interface UserService {
	/**
	 * 通过代理商查询员工
	 */
	public List queryUseragent(long agentId);
	public int updateUsergeren(User user);
	/**
	 * 审核失败后返还钱
	 * */
	public int updateBalanceUsermonye(Double money, long userId);
	/**
	 * 分页查询直属团队
	 */
	
	public Pager queryZhishu(int pageSize, int page, String search,User user);
	/**
	 * 分页查询全部成员
	 */
	
	public Pager queryQuanbu(int pageSize, int page, String search,User user);


	// 查询所有员工
	public List<User> queryGetAll(Long userid);

	public User checkUserLogin(User user);

	public User queryMoneyOfUser(String openId);

	public User queryuserById(long userId);

	public int updateBalanceOfUser(Double money, long userId);

	public User queryUserUnSubscribe();

	public int updateUser(User user, long userId);

	public int updateUSerByOpenId(User user);

	public int updateUserForCompleteRegisteration(User user);

	public User weixinLogin(User user);

	public String getRecommendCode();

	public int updateUserAfterSubscribe(User user);

	public List<User> queryTeamOfTheUser(User user, Integer userLevel,
			String search);

	public int updateUpdateUnsubscribe(String openId);

	public int resetPassword(long userId, String password);

	public int updateUserByMobile(User user);

	/**
	 * 分页查询员工
	 */
	public Pager queryPageForStaff(int pageSize, int page, String search,
			long agentId);

	/**
	 * 分页查询员工
	 */
	public Pager queryUserPage(int pageSize, int page, String search,
			long agentId);

	/**
	 * 添加员工
	 */
	public int addUser(User user);

	/**
	 * 批量删除员工
	 */
	public int deleteUser(String num);

	/**
	 * 批量删除员工
	 */
	public int deleteUser1(long id);

	/**
	 * 查询详情
	 */
	public User queryUserInfo(User user);

	/**
	 * 分页查询所有的会员
	 */
	public Pager queryAllUserPage(int pageSize, int page, String search);

	/**
	 * 查询所有的会员
	 */
	public List<User> queryUserList();

	/**
	 * 修改
	 */
	public int updateUser(User user);

	public User checkMobile(String mobile);

	/**
	 * 查询user
	 */
	public User queryUserSadff(String recode);

	/**
	 * 查询员工
	 */
	public User queryUserSdf(long sadffId);
}
