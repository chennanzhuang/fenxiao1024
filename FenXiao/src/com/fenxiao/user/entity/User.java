package com.fenxiao.user.entity;

// default package

import java.util.HashSet;
import java.util.Set;

import com.fenxiao.agent.entity.Agent;

/**
 * 会员 User entity. @author MyEclipse Persistence Tools
 */

public class User implements java.io.Serializable {

	// Fields

	private Long id;
	private String area;
	private String province;
	private String city;
	private Integer subscribe;
//	private Agent agent;//代理商
//	private Agent agent;//代理商
	private Long usercode;// 编号
	private String nickName;// 昵称
	private String username;// 用户名
	private String userPassword;// 密码
	private String userPhoto;// 头像
	private String mobile;// 手机
	private Integer userLevel;// 等级
	private Integer userStatus;// 状态
	private String userQr;// 二维码
	private Integer taskNum;// 完成任务数
	private Integer sex;// 性别
	private String dress;// 地址
	private Double balance;// 余额
	private Double taskCommission;// 任务佣金
	private Double promotionCommission;// 推广佣金
	private Double teamCommissions;// 团队佣金
	private Double freezingAmount;//冻结金额
	private String recommendationCode;// 推荐码
	private Double totalMoney;// 总金额
	private String regTime;// 注册时间
	private String recomCode;// 图鉴人的推荐码
	private String weChatId;// 微信号
	private String presentPass;// 提现密码
	private Set userTeams = new HashSet(0);// 我的团队
	private Set taskTeams = new HashSet(0);// 团队任务
	private Set presentRecords = new HashSet(0);// 提现记录
	private Set userTasks = new HashSet(0);// 我的任务
	private Set bankcards = new HashSet(0);// 银行卡
	private Set tasks = new HashSet(0);// 任务
	private Set commissionrecords = new HashSet(0);// 佣金记录
	private Set payRecords = new HashSet(0);// 支付记录
	private Set incomeRecodes = new HashSet(0);// 收入记录
	private Set freeAmounts = new HashSet(0);
	private Long sadffId;// 员工Id
	private Long partnerId;// 合伙人id
	private Long agentsId;// 代理商id

	// Constructors

	/** default constructor */
	public User() {
	}

	/** full constructor */
	public User(Long usercode, String nickName, String username,
			String userPassword, String userPhoto, String mobile,
			Integer userLevel, Integer userStatus, String userQr,
			Integer taskNum, Integer sex, String dress, Double balance,
			Double taskCommission, Double promotionCommission,
			Double teamCommissions, String recommendationCode,
			Double totalMoney, String regTime, String recomCode,
			String weChatId, String presentPass, Set userTeams, Set taskTeams,
			Set presentRecords, Set userTasks, Set bankcards, Set tasks,
			Set commissionrecords, Set payRecords, Set incomeRecodes,
			Double freezingAmount, Set freeAmounts, Long sadffId,
			Long partnerId, Long agentsId) {
		this.usercode = usercode;
		this.nickName = nickName;
		this.username = username;
		this.userPassword = userPassword;
		this.userPhoto = userPhoto;
		this.mobile = mobile;
		this.userLevel = userLevel;
		this.userStatus = userStatus;
		this.userQr = userQr;
		this.taskNum = taskNum;
		this.sex = sex;
		this.dress = dress;
		this.balance = balance;
		this.taskCommission = taskCommission;
		this.promotionCommission = promotionCommission;
		this.teamCommissions = teamCommissions;
		this.recommendationCode = recommendationCode;
		this.totalMoney = totalMoney;
		this.regTime = regTime;
		this.recomCode = recomCode;
		this.weChatId = weChatId;
		this.presentPass = presentPass;
		this.userTeams = userTeams;
		this.taskTeams = taskTeams;
		this.presentRecords = presentRecords;
		this.userTasks = userTasks;
		this.bankcards = bankcards;
		this.tasks = tasks;
		this.commissionrecords = commissionrecords;
		this.payRecords = payRecords;
		this.incomeRecodes = incomeRecodes;
		this.freezingAmount = freezingAmount;
		this.freeAmounts = freeAmounts;
		this.sadffId = sadffId;
		this.partnerId = partnerId;
		this.agentsId = agentsId;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(Long partnerId) {
		this.partnerId = partnerId;
	}

	public Long getAgentsId() {
		return agentsId;
	}

	public void setAgentsId(Long agentsId) {
		this.agentsId = agentsId;
	}

	public Long getUsercode() {
		return this.usercode;
	}

	public void setUsercode(Long usercode) {
		this.usercode = usercode;
	}

	public String getNickName() {
		return this.nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserPassword() {
		return this.userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserPhoto() {
		return this.userPhoto;
	}

	public void setUserPhoto(String userPhoto) {
		this.userPhoto = userPhoto;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getUserLevel() {
		return this.userLevel;
	}

	public void setUserLevel(Integer userLevel) {
		this.userLevel = userLevel;
	}

	public Integer getUserStatus() {
		return this.userStatus;
	}

	public void setUserStatus(Integer userStatus) {
		this.userStatus = userStatus;
	}

	public String getUserQr() {
		return this.userQr;
	}

	public void setUserQr(String userQr) {
		this.userQr = userQr;
	}

	public Integer getTaskNum() {
		return this.taskNum;
	}

	public void setTaskNum(Integer taskNum) {
		this.taskNum = taskNum;
	}

	public Integer getSex() {
		return this.sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getDress() {
		return this.dress;
	}

	public void setDress(String dress) {
		this.dress = dress;
	}

	public Double getBalance() {
		return this.balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Double getTaskCommission() {
		return this.taskCommission;
	}

	public void setTaskCommission(Double taskCommission) {
		this.taskCommission = taskCommission;
	}

	public Double getPromotionCommission() {
		return this.promotionCommission;
	}

	public void setPromotionCommission(Double promotionCommission) {
		this.promotionCommission = promotionCommission;
	}

	public Double getTeamCommissions() {
		return this.teamCommissions;
	}

	public void setTeamCommissions(Double teamCommissions) {
		this.teamCommissions = teamCommissions;
	}

	public String getRecommendationCode() {
		return this.recommendationCode;
	}

	public void setRecommendationCode(String recommendationCode) {
		this.recommendationCode = recommendationCode;
	}

	public Double getTotalMoney() {
		return this.totalMoney;
	}

	public void setTotalMoney(Double totalMoney) {
		this.totalMoney = totalMoney;
	}

	public String getRegTime() {
		return this.regTime;
	}

	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}

	public String getRecomCode() {
		return this.recomCode;
	}

	public void setRecomCode(String recomCode) {
		this.recomCode = recomCode;
	}

	public String getWeChatId() {
		return this.weChatId;
	}

	public void setWeChatId(String weChatId) {
		this.weChatId = weChatId;
	}

	public String getPresentPass() {
		return this.presentPass;
	}

	public void setPresentPass(String presentPass) {
		this.presentPass = presentPass;
	}

	public Set getUserTeams() {
		return this.userTeams;
	}

	public void setUserTeams(Set userTeams) {
		this.userTeams = userTeams;
	}

	public Set getTaskTeams() {
		return this.taskTeams;
	}

	public void setTaskTeams(Set taskTeams) {
		this.taskTeams = taskTeams;
	}

	public Set getPresentRecords() {
		return this.presentRecords;
	}

	public void setPresentRecords(Set presentRecords) {
		this.presentRecords = presentRecords;
	}

	public Set getUserTasks() {
		return this.userTasks;
	}

	public void setUserTasks(Set userTasks) {
		this.userTasks = userTasks;
	}

	public Set getBankcards() {
		return this.bankcards;
	}

	public void setBankcards(Set bankcards) {
		this.bankcards = bankcards;
	}

	public Set getTasks() {
		return this.tasks;
	}

	public void setTasks(Set tasks) {
		this.tasks = tasks;
	}

	public Set getCommissionrecords() {
		return this.commissionrecords;
	}

	public void setCommissionrecords(Set commissionrecords) {
		this.commissionrecords = commissionrecords;
	}

	public Set getPayRecords() {
		return this.payRecords;
	}

	public void setPayRecords(Set payRecords) {
		this.payRecords = payRecords;
	}

	public Set getIncomeRecodes() {
		return this.incomeRecodes;
	}

	public void setIncomeRecodes(Set incomeRecodes) {
		this.incomeRecodes = incomeRecodes;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getSubscribe() {
		return subscribe;
	}

	public void setSubscribe(Integer subscribe) {
		this.subscribe = subscribe;
	}

	public Double getFreezingAmount() {
		return freezingAmount;
	}

	public void setFreezingAmount(Double freezingAmount) {
		this.freezingAmount = freezingAmount;
	}

	public Set getFreeAmounts() {
		return freeAmounts;
	}

	public void setFreeAmounts(Set freeAmounts) {
		this.freeAmounts = freeAmounts;
	}

	public Long getSadffId() {
		return sadffId;
	}

	public void setSadffId(Long sadffId) {
		this.sadffId = sadffId;
	}

}