package com.fenxiao.adminRole.entity;
// default package

import java.util.HashSet;
import java.util.Set;

/**
 * 总部管理员菜单
 * AdminMenu entity. @author MyEclipse Persistence Tools
 */

public class AdminMenu implements java.io.Serializable {

	// Fields


	private String menuCode;//编码
	private String menuTitle;//标题
	private String menuUrl;//路径
	private String menuParentCode;//父编码
	//private Set adminRoleMenus = new HashSet(0);//权限

	// Constructors

	/** default constructor */
	public AdminMenu() {
	}

	/** minimal constructor */
	public AdminMenu(String menuCode, String menuTitle, String menuUrl,
			String menuParentCode) {
		this.menuCode = menuCode;
		this.menuTitle = menuTitle;
		this.menuUrl = menuUrl;
		this.menuParentCode = menuParentCode;
	}


	// Property accessors

	

	public String getMenuCode() {
		return this.menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public String getMenuTitle() {
		return this.menuTitle;
	}

	public void setMenuTitle(String menuTitle) {
		this.menuTitle = menuTitle;
	}

	public String getMenuUrl() {
		return this.menuUrl;
	}

	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}

	public String getMenuParentCode() {
		return this.menuParentCode;
	}

	public void setMenuParentCode(String menuParentCode) {
		this.menuParentCode = menuParentCode;
	}

	/*public Set getAdminRoleMenus() {
		return this.adminRoleMenus;
	}

	public void setAdminRoleMenus(Set adminRoleMenus) {
		this.adminRoleMenus = adminRoleMenus;
	}
*/
}