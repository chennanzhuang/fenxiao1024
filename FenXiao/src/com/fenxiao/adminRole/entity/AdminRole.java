package com.fenxiao.adminRole.entity;
// default package

import java.util.HashSet;
import java.util.Set;


/**
 * 角色
 * AdminRole entity. @author MyEclipse Persistence Tools
 */

public class AdminRole implements java.io.Serializable {

	// Fields

		private Integer id;
		private String rolename;//角色名称
		private Set adminRoleMenus = new HashSet(0);//权限
		private Set admins = new HashSet(0);
	// Constructors

	/** default constructor */
	public AdminRole() {
	}

	/** minimal constructor */
	public AdminRole(String rolename) {
		this.rolename = rolename;
	}

	/** full constructor */
	public AdminRole(String rolename, Set adminRoleMenus, Set admins) {
		this.rolename = rolename;
		this.adminRoleMenus = adminRoleMenus;
		this.admins = admins;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRolename() {
		return this.rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public Set getAdminRoleMenus() {
		return this.adminRoleMenus;
	}

	public void setAdminRoleMenus(Set adminRoleMenus) {
		this.adminRoleMenus = adminRoleMenus;
	}

	public Set getAdmins() {
		return this.admins;
	}

	public void setAdmins(Set admins) {
		this.admins = admins;
	}

}