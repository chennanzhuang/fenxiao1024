package com.fenxiao.adminRole.entity;
// default package


/**
 * 管理权限表
 * AdminRoleMenu entity. @author MyEclipse Persistence Tools
 */

public class AdminRoleMenu implements java.io.Serializable {

	// Fields

	private Long id;
	private AdminRole adminRole;//角色
	private AdminMenu adminMenu;//编码
	// Constructors

	/** default constructor */
	public AdminRoleMenu() {
	}

	/** full constructor */
	public AdminRoleMenu(AdminRole adminRole, AdminMenu adminMenu) {
		this.adminRole = adminRole;
		this.adminMenu = adminMenu;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AdminRole getAdminRole() {
		return this.adminRole;
	}

	public void setAdminRole(AdminRole adminRole) {
		this.adminRole = adminRole;
	}

	public AdminMenu getAdminMenu() {
		return this.adminMenu;
	}

	public void setAdminMenu(AdminMenu adminMenu) {
		this.adminMenu = adminMenu;
	}

}