package com.fenxiao.adminRole.dao;

import java.util.List;

import com.fenxiao.adminRole.entity.AdminMenu;

public interface AdminRoleMenuDao {
	/**
	  * 根据roleId查询菜单
	  */
	public List<AdminMenu>queryRoleMenu(int roleId);
	
	/**
	  * 根据menuCode查询子菜单
	  */
	public List<AdminMenu>queryRoleMenuChildCode(String menuCode,int roleId);
	
	
}
