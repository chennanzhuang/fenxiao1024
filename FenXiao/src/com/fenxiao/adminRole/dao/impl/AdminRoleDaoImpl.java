package com.fenxiao.adminRole.dao.impl;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

import com.fenxiao.admin.dao.AdminDao;
import com.fenxiao.adminRole.dao.AdminRoleDao;
import com.fenxiao.adminRole.entity.AdminRole;

public class AdminRoleDaoImpl implements AdminRoleDao {
	
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public AdminRole queryRole(int roleId) {
		String hql="from AdminRole r where r.id="+roleId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		Object obj=query.uniqueResult();
		if(obj!=null){
			return (AdminRole) obj;
		}else{
			return null;
		}
	}
	
	
}
