package com.fenxiao.adminRole.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;

import com.fenxiao.adminRole.dao.AdminRoleMenuDao;
import com.fenxiao.adminRole.entity.AdminMenu;
import com.fenxiao.role.entity.Menu;



public class AdminRoleMenuDaoImpl implements AdminRoleMenuDao{
   
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<AdminMenu> queryRoleMenu(int roleId) {
      String hql="select r.adminMenu from AdminRoleMenu r where r.adminMenu.menuParentCode='ROOT_MENU'and r.adminRole.id="+roleId;
		
		List<AdminMenu> list=sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return list;
	}

	@Override
	public List<AdminMenu> queryRoleMenuChildCode(String menuCode, int roleId) {
		 String hql="select r.adminMenu from AdminRoleMenu r where r.adminMenu.menuParentCode='"+menuCode+"'and r.adminRole.id="+roleId;
			
			List<AdminMenu> list=sessionFactory.getCurrentSession().createQuery(hql).list();
			
			return list;
	}
}
