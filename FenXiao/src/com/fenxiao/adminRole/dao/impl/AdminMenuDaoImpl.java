package com.fenxiao.adminRole.dao.impl;

import org.hibernate.SessionFactory;

import com.fenxiao.adminRole.dao.AdminMenuDao;

public class AdminMenuDaoImpl implements AdminMenuDao {
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
