package com.fenxiao.adminRole.dao;

import com.fenxiao.adminRole.entity.AdminRole;

public interface AdminRoleDao {
	
	public  AdminRole queryRole(int roleId);
}
