package com.fenxiao.adminRole.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.fenxiao.adminRole.entity.AdminMenu;
import com.fenxiao.adminRole.service.AdminMenuService;
import com.fenxiao.adminRole.service.AdminRoleMenuService;
import com.fenxiao.adminRole.service.AdminRoleService;
import com.opensymphony.xwork2.ActionSupport;

public class AdminRoleAction extends ActionSupport {

	 private AdminRoleService adminRoleService;
	    private AdminRoleMenuService adminRoleMenuService;
	    private AdminMenuService adminMenuService;
	    
	    private HttpServletRequest request;
		private HttpServletResponse response;	
		private HttpSession session;
	
		private Map<String, Object> dataMap;
		private String msg;
		private List<AdminMenu> menuList;
		private List<AdminMenu> menuList1;
		private String menuCode;
		
		/**
		 * 查询父菜单
		 * @return
		 */
		public String queryMenu(){
			
			request = ServletActionContext.getRequest();
			response = ServletActionContext.getResponse();
			HttpSession session = request.getSession();
			dataMap=new HashMap<String, Object>();
			try {
				
				menuList=adminRoleMenuService.queryRoleMenu((Integer)session.getAttribute("roleId"));
				
				
				dataMap.put("menuList", menuList);
				dataMap.put("retcode", 0);
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				msg="菜单加载失败，请联系管理员！";
				dataMap.put("msg", msg);
			}
			
			return "dataMap";
			
		}
		/**
		 * 查询子菜单
		 * @return
		 */
		public String queryMenuChild(){
			
			request = ServletActionContext.getRequest();
			response = ServletActionContext.getResponse();
	//		HttpServletResponse response = ServletActionContext.getResponse();
			HttpSession session = request.getSession();
			dataMap=new HashMap<String, Object>();
			int roleId=(Integer)session.getAttribute("roleId");
			try {
				
				menuList1=adminRoleMenuService.queryRoleMenuChildCode(menuCode, roleId);
				
				
				dataMap.put("menuList1", menuList1);
	//			response.getWriter().println(menuList1);
	//			response.getWriter().println(menuList1);
				dataMap.put("retcode", 0);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				msg="菜单加载失败，请联系管理员！";
				dataMap.put("msg", msg);
			}
			
			return "dataMap";
			
		}
	
	    
	    
	    
		public AdminRoleService getAdminRoleService() {
			return adminRoleService;
		}
		public void setAdminRoleService(AdminRoleService adminRoleService) {
			this.adminRoleService = adminRoleService;
		}
		public AdminRoleMenuService getAdminRoleMenuService() {
			return adminRoleMenuService;
		}
		public void setAdminRoleMenuService(AdminRoleMenuService adminRoleMenuService) {
			this.adminRoleMenuService = adminRoleMenuService;
		}
		public AdminMenuService getAdminMenuService() {
			return adminMenuService;
		}
		public void setAdminMenuService(AdminMenuService adminMenuService) {
			this.adminMenuService = adminMenuService;
		}
		public HttpServletRequest getRequest() {
			return request;
		}
		public void setRequest(HttpServletRequest request) {
			this.request = request;
		}
		public HttpServletResponse getResponse() {
			return response;
		}
		public void setResponse(HttpServletResponse response) {
			this.response = response;
		}
		public HttpSession getSession() {
			return session;
		}
		public void setSession(HttpSession session) {
			this.session = session;
		}
		public Map<String, Object> getDataMap() {
			return dataMap;
		}
		public void setDataMap(Map<String, Object> dataMap) {
			this.dataMap = dataMap;
		}
		public String getMsg() {
			return msg;
		}
		public void setMsg(String msg) {
			this.msg = msg;
		}
		public List<AdminMenu> getMenuList() {
			return menuList;
		}
		public void setMenuList(List<AdminMenu> menuList) {
			this.menuList = menuList;
		}
		public List<AdminMenu> getMenuList1() {
			return menuList1;
		}
		public void setMenuList1(List<AdminMenu> menuList1) {
			this.menuList1 = menuList1;
		}
		public String getMenuCode() {
			return menuCode;
		}
		public void setMenuCode(String menuCode) {
			this.menuCode = menuCode;
		}
		
   
	
}
