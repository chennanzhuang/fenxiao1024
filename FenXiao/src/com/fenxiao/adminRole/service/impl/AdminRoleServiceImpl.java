package com.fenxiao.adminRole.service.impl;

import com.fenxiao.adminRole.dao.AdminRoleDao;
import com.fenxiao.adminRole.entity.AdminRole;
import com.fenxiao.adminRole.service.AdminRoleService;

public class AdminRoleServiceImpl implements AdminRoleService {
   private AdminRoleDao adminRoleDao;

   public AdminRoleDao getAdminRoleDao() {
	return adminRoleDao;
   }

   public void setAdminRoleDao(AdminRoleDao adminRoleDao) {
	this.adminRoleDao = adminRoleDao;
  }

   @Override
   public AdminRole queryRole(int roleId) {
  
	return adminRoleDao.queryRole(roleId);
  }
	
	
}
