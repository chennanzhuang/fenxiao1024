package com.fenxiao.adminRole.service.impl;

import java.util.List;

import com.fenxiao.adminRole.dao.AdminRoleMenuDao;
import com.fenxiao.adminRole.entity.AdminMenu;
import com.fenxiao.adminRole.service.AdminRoleMenuService;


public class AdminRoleMenuServiceImpl implements AdminRoleMenuService {
      
	private AdminRoleMenuDao adminRoleMenuDao;

	
	@Override
	public List<AdminMenu> queryRoleMenu(int roleId) {
		
		return adminRoleMenuDao.queryRoleMenu(roleId);
	}

	@Override
	public List<AdminMenu> queryRoleMenuChildCode(String menuCode, int roleId) {
	
		return adminRoleMenuDao.queryRoleMenuChildCode(menuCode, roleId);
	}

	public AdminRoleMenuDao getAdminRoleMenuDao() {
		return adminRoleMenuDao;
	}

	public void setAdminRoleMenuDao(AdminRoleMenuDao adminRoleMenuDao) {
		this.adminRoleMenuDao = adminRoleMenuDao;
	}
	
	
}
