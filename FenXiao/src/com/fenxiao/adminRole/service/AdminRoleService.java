package com.fenxiao.adminRole.service;

import com.fenxiao.adminRole.entity.AdminRole;

public interface AdminRoleService {
	public  AdminRole queryRole(int roleId);
}
