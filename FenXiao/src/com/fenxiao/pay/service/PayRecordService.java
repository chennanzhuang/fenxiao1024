package com.fenxiao.pay.service;

import com.fenxiao.pay.entity.PayRecord;

public interface PayRecordService {
  
	public int addPayRecord(PayRecord payRecord);
}
