package com.fenxiao.pay.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fenxiao.pay.dao.PayRecordDao;
import com.fenxiao.pay.entity.PayRecord;
import com.fenxiao.pay.service.PayRecordService;
import com.fenxiao.user.service.UserService;


@Service
@Transactional
public class PayRecordServiceImpl implements PayRecordService{
	@Resource
	private PayRecordDao payRecordDao;
	
	@Override
	public int addPayRecord(PayRecord payRecord) {
		// TODO Auto-generated method stub
		return payRecordDao.addPayRecord(payRecord);
	}
	

	public PayRecordDao getPayRecordDao() {
		return payRecordDao;
	}

	public void setPayRecordDao(PayRecordDao payRecordDao) {
		this.payRecordDao = payRecordDao;
	}


}
