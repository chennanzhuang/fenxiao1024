package com.fenxiao.pay.entity;

import com.fenxiao.user.entity.User;
// default package

/**
 * 支付记录
 * PayRecord entity. @author MyEclipse Persistence Tools
 */

public class PayRecord implements java.io.Serializable {

	// Fields

	private Long payId;
	private User user;//会员
	private String taskCode;//任务编号
	private String taskTitle;//标题
	private String taskBriefing;//简介
	private String taskOrderTime;//下单时间
	private String taskPayTime;//支付时间
	private Double taskMoney;//任务金额
	private Double poundage;//手续费
	private Double payMoney;//支付金额
	private Integer payStauts;//支付状态（0、未支付 1、支付成功）

	// Constructors

	/** default constructor */
	public PayRecord() {
	}

	/** full constructor */
	public PayRecord(User user, String taskCode, String taskTitle,
			String taskBriefing, String taskOrderTime, String taskPayTime,
			Double taskMoney, Double poundage, Double payMoney,
			Integer payStauts) {
		this.user = user;
		this.taskCode = taskCode;
		this.taskTitle = taskTitle;
		this.taskBriefing = taskBriefing;
		this.taskOrderTime = taskOrderTime;
		this.taskPayTime = taskPayTime;
		this.taskMoney = taskMoney;
		this.poundage = poundage;
		this.payMoney = payMoney;
		this.payStauts = payStauts;
	}

	// Property accessors

	public Long getPayId() {
		return this.payId;
	}

	public void setPayId(Long payId) {
		this.payId = payId;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTaskCode() {
		return this.taskCode;
	}

	public void setTaskCode(String taskCode) {
		this.taskCode = taskCode;
	}

	public String getTaskTitle() {
		return this.taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public String getTaskBriefing() {
		return this.taskBriefing;
	}

	public void setTaskBriefing(String taskBriefing) {
		this.taskBriefing = taskBriefing;
	}

	public String getTaskOrderTime() {
		return this.taskOrderTime;
	}

	public void setTaskOrderTime(String taskOrderTime) {
		this.taskOrderTime = taskOrderTime;
	}

	public String getTaskPayTime() {
		return this.taskPayTime;
	}

	public void setTaskPayTime(String taskPayTime) {
		this.taskPayTime = taskPayTime;
	}

	public Double getTaskMoney() {
		return this.taskMoney;
	}

	public void setTaskMoney(Double taskMoney) {
		this.taskMoney = taskMoney;
	}

	public Double getPoundage() {
		return this.poundage;
	}

	public void setPoundage(Double poundage) {
		this.poundage = poundage;
	}

	public Double getPayMoney() {
		return this.payMoney;
	}

	public void setPayMoney(Double payMoney) {
		this.payMoney = payMoney;
	}

	public Integer getPayStauts() {
		return this.payStauts;
	}

	public void setPayStauts(Integer payStauts) {
		this.payStauts = payStauts;
	}

}