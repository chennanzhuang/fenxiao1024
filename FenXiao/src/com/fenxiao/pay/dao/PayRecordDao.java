package com.fenxiao.pay.dao;

import com.fenxiao.pay.entity.PayRecord;

public interface PayRecordDao {

	public int addPayRecord(PayRecord payRecord);
}
