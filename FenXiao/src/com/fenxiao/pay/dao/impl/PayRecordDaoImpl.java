package com.fenxiao.pay.dao.impl;

import org.hibernate.SessionFactory;

import com.fenxiao.base.BaseDaoImpl;
import com.fenxiao.pay.dao.PayRecordDao;
import com.fenxiao.pay.entity.PayRecord;

public class PayRecordDaoImpl extends BaseDaoImpl<PayRecord> implements PayRecordDao {
	private  SessionFactory sessionFactory;
	
	@Override
	public int addPayRecord(PayRecord payRecord) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(payRecord);
		return 1;
	}
	

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
