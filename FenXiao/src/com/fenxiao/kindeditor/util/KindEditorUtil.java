package com.fenxiao.kindeditor.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
/**
 * 
 * KindEditor 工具类
 * 
 * @author ChenYong
 * 
 */
public class KindEditorUtil {
	/**
	 * 
	 * 获取新文件名称
	 * 
	 * @param oldFileName 旧文件名称
	 * @return 新文件名称
	 * 
	 */
	public static String getNewFileName(String oldFileName) {
		StringBuffer newFileName = new StringBuffer();
		UUID uuid = UUID.randomUUID();
		
		newFileName.append(uuid.toString());
		newFileName.append(".");
		newFileName.append(getExtensionName(oldFileName));
		
		return newFileName.toString();
	}
	
	/**
	 * 
	 * 获取文件的扩展名
	 * 
	 * @param fileName 文件名称
	 * @return 文件的扩展名
	 * 
	 */
	public static String getExtensionName(String fileName) {
		return fileName.substring(fileName.lastIndexOf(".") + 1);
	}
	
	/**
	 * 
	 * 获取目录名称
	 * 
	 * @return 目录名称组成的 List 集合
	 * 
	 */
	public static List<String> getDirName() {
		List<String> dirNames = new ArrayList<String>();

		dirNames.add("file");
		dirNames.add("image");
		dirNames.add("flash");
		dirNames.add("media");
		
		// 排序
		Collections.sort(dirNames);
		
		return dirNames;
	}

	/**
	 * 
	 * 获取文件类型
	 * 
	 * @return 文件类型组成的 List 集合
	 * 
	 */
	public static List<String> getFileType() {
		List<String> fileTypes = new ArrayList<String>();

		fileTypes.add("doc");
		fileTypes.add("docx");
		fileTypes.add("xls");
		fileTypes.add("xlsx");
		fileTypes.add("ppt");
		fileTypes.add("htm");
		fileTypes.add("html");
		fileTypes.add("txt");
		fileTypes.add("zip");
		fileTypes.add("rar");
		fileTypes.add("gz");
		fileTypes.add("bz2");
		
		// 排序
		Collections.sort(fileTypes);
		
		return fileTypes;
	}
	
	/**
	 * 
	 * 获取图片类型
	 * 
	 * @return 图片类型组成的 List 集合
	 * 
	 */
	public static List<String> getImageType() {
		List<String> imageTypes = new ArrayList<String>();

		imageTypes.add("gif");
		imageTypes.add("jpg");
		imageTypes.add("jpeg");
		imageTypes.add("png");
		imageTypes.add("bmp");
		
		// 排序
		Collections.sort(imageTypes);

		return imageTypes;
	}
	
	/**
	 * 
	 * 获取动画类型
	 * 
	 * @return 动画类型组成的 List 集合
	 * 
	 */
	public static List<String> getFlashType() {
		List<String> flashTypes = new ArrayList<String>();
		
		flashTypes.add("swf");
		flashTypes.add("flv");
		
		// 排序
		Collections.sort(flashTypes);
		
		return flashTypes;
	}
	
	/**
	 * 
	 * 获取视频类型
	 * 
	 * @return 视频类型组成的 List 集合
	 * 
	 */
	public static List<String> getMediaType() {
		List<String> mediaTypes = new ArrayList<String>();
		
		mediaTypes.add("swf");
		mediaTypes.add("flv");
		mediaTypes.add("mp3");
		mediaTypes.add("wav");
		mediaTypes.add("wma");
		mediaTypes.add("wmv");
		mediaTypes.add("mid");
		mediaTypes.add("avi");
		mediaTypes.add("mpg");
		mediaTypes.add("asf");
		mediaTypes.add("rm");
		mediaTypes.add("rmvb");
		
		// 排序
		Collections.sort(mediaTypes);
		
		return mediaTypes;
	}
}
