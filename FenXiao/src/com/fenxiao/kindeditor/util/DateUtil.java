package com.fenxiao.kindeditor.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * 日期工具类
 * 
 * @author ChenYong
 *
 */
public class DateUtil {
	/**
	 * 
	 * 将日期 date 按格式 format 转换为日期字符串
	 * 
	 * @param date 被转换的日期
	 * @param format 转换的格式
	 * @return 日期 date 按格式 format 转换的日期字符串
	 * 
	 */
	public static String format(Date date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		
		return sdf.format(date);
	}
	
	/**
	 * 
	 * 将日期毫秒数 millisecond 按格式 format 转换为日期字符串
	 * 
	 * @param millisecond 被转换的日期毫秒数
	 * @param format 转换的格式
	 * @return 日期毫秒数 millisecond 按格式 format 转换的日期字符串
	 * 
	 */
	public static String format(long millisecond, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		
		return sdf.format(millisecond);
	}
	
	/**
	 * 
	 * 将日期字符串 source 按格式 format 转换为日期
	 * 
	 * @param source 被转换的日期字符串
	 * @param format 转换的格式
	 * @return 日期字符串 source 按格式 format 转换的日期
	 * 
	 */
	public static Date parse(String source, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		
		try {
			return sdf.parse(source);
		} catch (ParseException e) {
			return null;
		}
	}
}

