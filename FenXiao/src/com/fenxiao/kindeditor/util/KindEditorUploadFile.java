package com.fenxiao.kindeditor.util;

import java.util.List;
import java.util.Map;
/**
 * 
 * KindEditor 上传的文件信息
 * 
 * @author ChenYong
 * 
 */
public class KindEditorUploadFile {
	// 移到上一级目录的路径
	private String moveup_dir_path;
	// 当前目录的路径
	private String current_dir_path;
	// 访问文件的 Url
	private String current_url;
	// 文件数量
	private int total_count;
	// 文件列表
	private List<Map<String, Object>> file_list;

	// 构造器
	public KindEditorUploadFile() {
	}

	public KindEditorUploadFile(String moveup_dir_path, String current_dir_path,
			String current_url, int total_count,
			List<Map<String, Object>> file_list) {
		this.moveup_dir_path = moveup_dir_path;
		this.current_dir_path = current_dir_path;
		this.current_url = current_url;
		this.total_count = total_count;
		this.file_list = file_list;
	}

	// Getters and Setters
	public String getMoveup_dir_path() {
		return moveup_dir_path;
	}

	public void setMoveup_dir_path(String moveup_dir_path) {
		this.moveup_dir_path = moveup_dir_path;
	}

	public String getCurrent_dir_path() {
		return current_dir_path;
	}

	public void setCurrent_dir_path(String current_dir_path) {
		this.current_dir_path = current_dir_path;
	}

	public String getCurrent_url() {
		return current_url;
	}

	public void setCurrent_url(String current_url) {
		this.current_url = current_url;
	}

	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	public List<Map<String, Object>> getFile_list() {
		return file_list;
	}

	public void setFile_list(List<Map<String, Object>> file_list) {
		this.file_list = file_list;
	}
}