package com.fenxiao.role.entity;
// default package

import java.util.HashSet;
import java.util.Set;

/**
 * 角色2
 * Role entity. @author MyEclipse Persistence Tools
 */

public class Role implements java.io.Serializable {

	// Fields

	private Integer id;
	private String rolename;//角色名称
	private Set roleMenus = new HashSet(0);//权限
	private Set agents = new HashSet(0);

	// Constructors

	/** default constructor */
	public Role() {
	}

	/** minimal constructor */
	public Role(String rolename) {
		this.rolename = rolename;
	}

	/** full constructor */
	public Role(String rolename, Set roleMenus, Set agents) {
		this.rolename = rolename;
		this.roleMenus = roleMenus;
		this.agents = agents;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRolename() {
		return this.rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public Set getRoleMenus() {
		return this.roleMenus;
	}

	public void setRoleMenus(Set roleMenus) {
		this.roleMenus = roleMenus;
	}

	public Set getAgents() {
		return this.agents;
	}

	public void setAgents(Set agents) {
		this.agents = agents;
	}

}