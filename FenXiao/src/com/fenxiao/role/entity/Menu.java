package com.fenxiao.role.entity;
// default package

import java.util.HashSet;
import java.util.Set;

/**
 * 合伙人菜单
 * Menu entity. @author MyEclipse Persistence Tools
 */

public class Menu implements java.io.Serializable {

	private String menuCode;
	private String menuTitle;
	private String menuUrl;
	private String menuParentCode;
//	private Set roleMenus = new HashSet(0);

	// Constructors

	/** default constructor */
	public Menu() {
	}

	/** full constructor */
	public Menu(String menuTitle, String menuUrl, String menuParentCode
			) {
		this.menuTitle = menuTitle;
		this.menuUrl = menuUrl;
		this.menuParentCode = menuParentCode;
		//this.roleMenus = roleMenus;
	}

	// Property accessors

	public String getMenuCode() {
		return this.menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public String getMenuTitle() {
		return this.menuTitle;
	}

	public void setMenuTitle(String menuTitle) {
		this.menuTitle = menuTitle;
	}

	public String getMenuUrl() {
		return this.menuUrl;
	}

	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}

	public String getMenuParentCode() {
		return this.menuParentCode;
	}

	public void setMenuParentCode(String menuParentCode) {
		this.menuParentCode = menuParentCode;
	}

//	public Set getRoleMenus() {
//		return this.roleMenus;
//	}
//
//	public void setRoleMenus(Set roleMenus) {
//		this.roleMenus = roleMenus;
//	}
}