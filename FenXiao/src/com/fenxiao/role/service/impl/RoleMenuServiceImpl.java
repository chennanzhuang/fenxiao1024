package com.fenxiao.role.service.impl;

import java.util.List;

import com.fenxiao.role.dao.RoleMenuDao;
import com.fenxiao.role.entity.Menu;
import com.fenxiao.role.service.RoleMenuService;

public class RoleMenuServiceImpl implements RoleMenuService {
 
	private RoleMenuDao roleMenuDao;

	public RoleMenuDao getRoleMenuDao() {
		return roleMenuDao;
	}

	public void setRoleMenuDao(RoleMenuDao roleMenuDao) {
		this.roleMenuDao = roleMenuDao;
	}
	
	
	
	public List<Menu>queryRoleMenu(int roleId){
		return roleMenuDao.queryRoleMenu(roleId);
	}
	
	public List<Menu>queryRoleMenuChildCode(String menuCode,int roleId){
		return roleMenuDao.queryRoleMenuChildCode(menuCode,roleId);
	}
}
