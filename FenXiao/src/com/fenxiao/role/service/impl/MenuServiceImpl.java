package com.fenxiao.role.service.impl;

import com.fenxiao.role.dao.MenuDao;
import com.fenxiao.role.service.MenuService;

public class MenuServiceImpl implements MenuService {
  
	private MenuDao menuDao;

	public MenuDao getMenuDao() {
		return menuDao;
	}

	public void setMenuDao(MenuDao menuDao) {
		this.menuDao = menuDao;
	}
	
}
