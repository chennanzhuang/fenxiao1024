package com.fenxiao.role.service.impl;

import com.fenxiao.role.dao.RoleDao;
import com.fenxiao.role.entity.Role;
import com.fenxiao.role.service.RoleService;

public class RoleServiceImpl implements RoleService {
 
	private RoleDao roleDao;

	public RoleDao getRoleDao() {
		return roleDao;
	}

	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}
	public  Role queryRole(int roleId){
		return roleDao.queryRole(roleId);
	}
}
