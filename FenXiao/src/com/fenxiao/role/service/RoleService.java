package com.fenxiao.role.service;

import com.fenxiao.role.entity.Role;

public interface RoleService {
	public  Role queryRole(int roleId);
}
