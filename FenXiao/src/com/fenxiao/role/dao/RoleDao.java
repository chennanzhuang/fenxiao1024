package com.fenxiao.role.dao;

import com.fenxiao.role.entity.Role;

public interface RoleDao {

	public  Role queryRole(int roleId);
}
