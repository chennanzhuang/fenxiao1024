package com.fenxiao.role.dao;

import java.util.List;

import com.fenxiao.role.entity.Menu;

public interface RoleMenuDao {

	 /**
	  * 根据roleId查询菜单
	  */
	public List<Menu>queryRoleMenu(int roleId);
	
	/**
	  * 根据menuCode查询子菜单
	  */
	public List<Menu>queryRoleMenuChildCode(String menuCode,int roleId);
	
}
