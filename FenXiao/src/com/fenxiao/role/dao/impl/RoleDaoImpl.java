package com.fenxiao.role.dao.impl;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

import com.fenxiao.role.dao.RoleDao;
import com.fenxiao.role.entity.Role;

public class RoleDaoImpl implements RoleDao{
	
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	public  Role queryRole(int roleId){
		String hql="from Role r where r.id="+roleId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		Object obj=query.uniqueResult();
		if(obj!=null){
			return (Role) obj;
		}else{
			return null;
		}
	}

}
