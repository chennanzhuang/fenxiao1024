package com.fenxiao.role.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;

import com.fenxiao.role.dao.RoleMenuDao;
import com.fenxiao.role.entity.Menu;

public class RoleMenuDaoImpl implements RoleMenuDao {

	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	 /**
	  * 根据roleId查询菜单
	  */
	public List<Menu>queryRoleMenu(int roleId){
		String hql="select r.menu from RoleMenu r where r.menu.menuParentCode='ROOT_MENU'and r.role.id="+roleId;
		
		List<Menu> list=sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return list;
	}
	/**
	  * 根据menuCode查询子菜单
	  */
	public List<Menu>queryRoleMenuChildCode(String menuCode,int roleId){
     String hql="select r.menu from RoleMenu r where r.menu.menuParentCode='"+menuCode+"'and r.role.id="+roleId;
		
		List<Menu> list=sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return list;

	}
	
}
