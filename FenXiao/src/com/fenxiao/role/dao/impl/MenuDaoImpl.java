package com.fenxiao.role.dao.impl;

import org.hibernate.SessionFactory;

import com.fenxiao.role.dao.MenuDao;

public class MenuDaoImpl implements MenuDao {
	
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
