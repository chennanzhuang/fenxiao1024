package com.fenxiao.role.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.fenxiao.role.entity.Menu;
import com.fenxiao.role.service.MenuService;
import com.fenxiao.role.service.RoleMenuService;
import com.fenxiao.role.service.RoleService;
import com.opensymphony.xwork2.ActionSupport;

public class RoleAction extends ActionSupport {

	private HttpServletRequest request;
	private HttpServletResponse response;	
	private HttpSession session;
	
	private RoleService roleService;
	private RoleMenuService roleMenuService;
	private MenuService menuService;
	
	private Map<String, Object> dataMap;
	private String msg;
	private List<Menu> menuList;
	private List<Menu> menuList1;
	private String menuCode;
	/**
	 * 查询父菜单
	 * @return
	 */
	public String queryMenu(){
		
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();
		dataMap=new HashMap<String, Object>();
		try {
			
			menuList=roleMenuService.queryRoleMenu((Integer)session.getAttribute("roleId"));
			
			
			dataMap.put("menuList", menuList);
			dataMap.put("retcode", 0);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			msg="菜单加载失败，请联系管理员！";
			dataMap.put("msg", msg);
		}
		
		return "dataMap";
		
	}
	/**
	 * 查询子菜单
	 * @return
	 */
	public String queryMenuChild(){
		
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
//		HttpServletResponse response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();
		dataMap=new HashMap<String, Object>();
		int roleId=(Integer)session.getAttribute("roleId");
		try {
			
			menuList1=roleMenuService.queryRoleMenuChildCode(menuCode, roleId);
			
			
			dataMap.put("menuList1", menuList1);
//			response.getWriter().println(menuList1);
//			response.getWriter().println(menuList1);
			dataMap.put("retcode", 0);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			msg="菜单加载失败，请联系管理员！";
			dataMap.put("msg", msg);
		}
		
		return "dataMap";
		
	}	

	
	
	public RoleService getRoleService() {
		return roleService;
	}
	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}
	public RoleMenuService getRoleMenuService() {
		return roleMenuService;
	}
	public void setRoleMenuService(RoleMenuService roleMenuService) {
		this.roleMenuService = roleMenuService;
	}
	public MenuService getMenuService() {
		return menuService;
	}
	public void setMenuService(MenuService menuService) {
		this.menuService = menuService;
	}
	public Map<String, Object> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, Object> dataMap) {
		this.dataMap = dataMap;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<Menu> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<Menu> menuList) {
		this.menuList = menuList;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	public List<Menu> getMenuList1() {
		return menuList1;
	}
	public void setMenuList1(List<Menu> menuList1) {
		this.menuList1 = menuList1;
	}
	public String getMenuCode() {
		return menuCode;
	}
	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

}
