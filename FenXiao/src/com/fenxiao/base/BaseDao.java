package com.fenxiao.base;

import java.util.List;

public interface BaseDao<T> {

	void save(T entity);

	void delete(Integer id);
	
	void update(T entity);

	T getById(Integer id);

	List<T> findAll();

	List<T> getByIds(Integer[] ids);
	
	/*PageBean<T> getPageBean(int page, int limit, String hql,List<Object> parameters);*/

}

