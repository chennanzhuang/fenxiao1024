package com.fenxiao.record.service.impl;

import com.fenxiao.record.dao.CommissionrecordDao;
import com.fenxiao.record.entity.Commissionrecord;
import com.fenxiao.record.service.CommissionrecordService;

public class CommissionrecordServiceImpl implements CommissionrecordService {
	private CommissionrecordDao commissionrecordDao;
	@Override
	public int addCommissionrecord(Commissionrecord commissionrecord) {
		// TODO Auto-generated method stub
		return commissionrecordDao.addCommissionrecord(commissionrecord);
	}
 
	

	public CommissionrecordDao getCommissionrecordDao() {
		return commissionrecordDao;
	}

	public void setCommissionrecordDao(CommissionrecordDao commissionrecordDao) {
		this.commissionrecordDao = commissionrecordDao;
	}
	
}
