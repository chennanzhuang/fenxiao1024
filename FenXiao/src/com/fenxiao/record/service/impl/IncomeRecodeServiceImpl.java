package com.fenxiao.record.service.impl;

import com.fenxiao.record.dao.IncomeRecodeDao;
import com.fenxiao.record.entity.IncomeRecode;
import com.fenxiao.record.service.IncomeRecodeService;

public class IncomeRecodeServiceImpl implements IncomeRecodeService {
  private IncomeRecodeDao incomeRecodeDao;
	
	public IncomeRecodeDao getIncomeRecodeDao() {
	return incomeRecodeDao;
}

public void setIncomeRecodeDao(IncomeRecodeDao incomeRecodeDao) {
	this.incomeRecodeDao = incomeRecodeDao;
}

	/**
	 * 添加
	 */
	public int addIncomeRecode(IncomeRecode incomeRecode){
		return incomeRecodeDao.addIncomeRecode(incomeRecode);
	}
}
