package com.fenxiao.record.service.impl;

import com.fenxiao.record.dao.FreeAmountDao;
import com.fenxiao.record.entity.FreeAmount;
import com.fenxiao.record.service.FreeAmountService;

public class FreeAmountServiceImpl implements FreeAmountService {
  private FreeAmountDao freeAmountDao;
  
  /**
	 * 添加
	 */
	public int addFreeAmount(FreeAmount freeAmount){
		return freeAmountDao.addFreeAmount(freeAmount);
	}

public FreeAmountDao getFreeAmountDao() {
	return freeAmountDao;
}

public void setFreeAmountDao(FreeAmountDao freeAmountDao) {
	this.freeAmountDao = freeAmountDao;
}
  
  
}
