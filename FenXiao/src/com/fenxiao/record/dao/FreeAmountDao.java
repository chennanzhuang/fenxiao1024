package com.fenxiao.record.dao;

import com.fenxiao.record.entity.FreeAmount;

public interface FreeAmountDao {

	/**
	 * 添加
	 */
	public int addFreeAmount(FreeAmount freeAmount);
}
