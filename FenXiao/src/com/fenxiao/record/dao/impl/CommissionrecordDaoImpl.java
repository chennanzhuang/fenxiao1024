package com.fenxiao.record.dao.impl;

import org.hibernate.SessionFactory;

import com.fenxiao.record.dao.CommissionrecordDao;
import com.fenxiao.record.entity.Commissionrecord;

public class CommissionrecordDaoImpl implements CommissionrecordDao {
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	/**
	 * 添加
	 */
	public int addCommissionrecord(Commissionrecord commissionrecord){
		this.sessionFactory.getCurrentSession().save(commissionrecord);
		return 1;
	}
}
