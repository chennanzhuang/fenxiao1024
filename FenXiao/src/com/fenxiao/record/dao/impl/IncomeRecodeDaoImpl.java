package com.fenxiao.record.dao.impl;

import org.hibernate.SessionFactory;

import com.fenxiao.record.dao.IncomeRecodeDao;
import com.fenxiao.record.entity.IncomeRecode;

public class IncomeRecodeDaoImpl implements IncomeRecodeDao {
	
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * 添加
	 */
	public int addIncomeRecode(IncomeRecode incomeRecode){
		this.sessionFactory.getCurrentSession().save(incomeRecode);
		return 1;
	}
}
