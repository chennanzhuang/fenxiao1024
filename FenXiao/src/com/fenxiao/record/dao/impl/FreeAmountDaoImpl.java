package com.fenxiao.record.dao.impl;

import org.hibernate.SessionFactory;

import com.fenxiao.record.dao.FreeAmountDao;
import com.fenxiao.record.entity.FreeAmount;

public class FreeAmountDaoImpl implements FreeAmountDao {
    
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	/**
	 * 添加
	 */
	public int addFreeAmount(FreeAmount freeAmount){
		
		this.sessionFactory.getCurrentSession().save(freeAmount);
		return 1;
	}
}
