package com.fenxiao.record.dao;

import com.fenxiao.record.entity.IncomeRecode;

public interface IncomeRecodeDao {

	/**
	 * 添加
	 */
	public int addIncomeRecode(IncomeRecode incomeRecode);
}
