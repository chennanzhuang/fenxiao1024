package com.fenxiao.record.entity;

import com.fenxiao.user.entity.User;
// default package

/**
 * 佣金记录
 * Commissionrecord entity. @author MyEclipse Persistence Tools
 */

public class Commissionrecord implements java.io.Serializable {

	// Fields

	private Long recordId;
	private User user;//会员
	private String taskTitle;//标题
	private Integer recordType;//类型（1、任务佣金 2、推广佣金 ）
	private Integer taskType;//任务类型
	private Double taskMoney;//任务金额
	private String recordTime;//时间

	// Constructors

	/** default constructor */
	public Commissionrecord() {
	}

	/** full constructor */
	public Commissionrecord(User user, String taskTitle, Integer recordType,
			Integer taskType, Double taskMoney, String recordTime) {
		this.user = user;
		this.taskTitle = taskTitle;
		this.recordType = recordType;
		this.taskType = taskType;
		this.taskMoney = taskMoney;
		this.recordTime = recordTime;
	}

	// Property accessors

	public Long getRecordId() {
		return this.recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTaskTitle() {
		return this.taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public Integer getRecordType() {
		return this.recordType;
	}

	public void setRecordType(Integer recordType) {
		this.recordType = recordType;
	}

	public Integer getTaskType() {
		return this.taskType;
	}

	public void setTaskType(Integer taskType) {
		this.taskType = taskType;
	}

	public Double getTaskMoney() {
		return this.taskMoney;
	}

	public void setTaskMoney(Double taskMoney) {
		this.taskMoney = taskMoney;
	}

	public String getRecordTime() {
		return this.recordTime;
	}

	public void setRecordTime(String recordTime) {
		this.recordTime = recordTime;
	}

}