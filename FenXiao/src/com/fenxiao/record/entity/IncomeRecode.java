package com.fenxiao.record.entity;

import com.fenxiao.admin.entity.Admin;
import com.fenxiao.agent.entity.Agent;
import com.fenxiao.user.entity.User;
// default package
/**
 * 收入记录
 * IncomeRecode entity. @author MyEclipse Persistence Tools
 */

public class IncomeRecode implements java.io.Serializable {

	// Fields

	private Long incomeId;
	private Agent agent;
	private Admin admin;
	private User user;
	private Integer incomeStatus;//状态
	private Double incomeMoney;//金额
	private String incomeTime;//时间

	// Constructors

	/** default constructor */
	public IncomeRecode() {
	}

	/** full constructor */
	public IncomeRecode(Agent agent, Admin admin, User user,
			Integer incomeStatus, Double incomeMoney, String incomeTime) {
		this.agent = agent;
		this.admin = admin;
		this.user = user;
		this.incomeStatus = incomeStatus;
		this.incomeMoney = incomeMoney;
		this.incomeTime = incomeTime;
	}

	// Property accessors

	public Long getIncomeId() {
		return this.incomeId;
	}

	public void setIncomeId(Long incomeId) {
		this.incomeId = incomeId;
	}

	public Agent getAgent() {
		return this.agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public Admin getAdmin() {
		return this.admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getIncomeStatus() {
		return this.incomeStatus;
	}

	public void setIncomeStatus(Integer incomeStatus) {
		this.incomeStatus = incomeStatus;
	}

	public Double getIncomeMoney() {
		return this.incomeMoney;
	}

	public void setIncomeMoney(Double incomeMoney) {
		this.incomeMoney = incomeMoney;
	}

	public String getIncomeTime() {
		return this.incomeTime;
	}

	public void setIncomeTime(String incomeTime) {
		this.incomeTime = incomeTime;
	}

}
