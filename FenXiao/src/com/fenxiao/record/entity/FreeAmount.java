package com.fenxiao.record.entity;

import com.fenxiao.user.entity.User;

/**
 * 冻结金额
 * @author Administrator
 *
 */
public class FreeAmount {
	private Long freeId;
	private User user;//会员
	private String taskTitle;//标题
	private Integer freeStatus;//状态
	private Integer taskType;//任务类型
	private Double taskMoney;//任务金额
	private String recordTime;//时间
	private String thawTime;//解冻时间
	public Long getFreeId() {
		return freeId;
	}
	public void setFreeId(Long freeId) {
		this.freeId = freeId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getTaskTitle() {
		return taskTitle;
	}
	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}
	public Integer getFreeStatus() {
		return freeStatus;
	}
	public void setFreeStatus(Integer freeStatus) {
		this.freeStatus = freeStatus;
	}
	public Integer getTaskType() {
		return taskType;
	}
	public void setTaskType(Integer taskType) {
		this.taskType = taskType;
	}
	public Double getTaskMoney() {
		return taskMoney;
	}
	public void setTaskMoney(Double taskMoney) {
		this.taskMoney = taskMoney;
	}
	public String getRecordTime() {
		return recordTime;
	}
	public void setRecordTime(String recordTime) {
		this.recordTime = recordTime;
	}
	public String getThawTime() {
		return thawTime;
	}
	public void setThawTime(String thawTime) {
		this.thawTime = thawTime;
	}
	
}
