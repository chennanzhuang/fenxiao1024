package com.fenxiao.present.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.fenxiao.base.BaseDaoImpl;
import com.fenxiao.common.Pager;
import com.fenxiao.pay.entity.PayRecord;
import com.fenxiao.present.dao.PresentDao;
import com.fenxiao.present.entity.PresentRecord;

public class PresentDaoImpl extends BaseDaoImpl<PresentRecord> implements PresentDao {
	private  SessionFactory sessionFactory;

	@Override
	public int addPresentRecord(PresentRecord presentRecord) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(presentRecord);
		return 1;
	}
	
	@Override
	public List<PresentRecord> queryAllPresentRecord(long userId) {
		// TODO Auto-generated method stub
		String hql="select p from PresentRecord p where p.user.id="+userId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PresentRecord> list=query.list();
		return list;
	}
	 /**
	    * 查询所有记录数
	    * @param hql 查询的条件
	    * @return 总记录数
	    */
	public int getAllRowCount(String hql){
		Session session=sessionFactory.openSession();
		List list=null;
		try{
			Query query=session.createQuery(hql);
		
			list=query.list();
			
		}catch (RuntimeException e) {
		   throw e;
		}
		session.close();
		return list.size();
	}
		/**
		    * 分页查询
		    * @param hql 查询的条件
		    * @param offset 开始记录
		    * @param length 一次查询几条记录
		    * @return
		    */
     public List  queryForPage(String hql, int offset, int length){
	  Session session=sessionFactory.openSession();
		List  list=null;
		try{
			Query query=session.createQuery(hql);
			query.setFirstResult(offset);
        query.setMaxResults(length);
        list=query.list();
			
		}catch (RuntimeException e) {
		   throw e;
		}
		session.close();
		return list;
}
      /**
       * 分页查询
       */
     public Pager queryPresent(int pageSize,int page,String search){
	  String hql="from PresentRecord pr left join pr.user where pr.presentStatus=0";
	  if(search!=null&&!"".equals(search.trim())){
			 hql+="and pr.user.username like'%"+search+"%'";
		 }
		 hql+=" order by pr.presentTime desc";
		 int allRow =this.getAllRowCount(hql);    
	       int totalPage = Pager.countTotalPage(pageSize, allRow);    
	       final int offset = Pager.countOffset(pageSize, page);    
	       final int length = pageSize;    
	       final int currentPage = Pager.countCurrentPage(page);
	       List list = this.queryForPage(hql,offset, length); 
	       
	       Pager pages = new Pager();
	       pages.setPageSize(pageSize);    
	       pages.setCurrentPage(currentPage);
	       pages.setAllRow(allRow);
	       pages.setTotalPage(totalPage);
	       pages.setList(list);
	       pages.init();
	       return pages; 
		 
		 
     }
     /**
      * 根据ID查询记录
      */
     public PresentRecord queryPresentRecordInfo(long presentId){
    	 String hql="from PresentRecord pr  where pr.presentId="+presentId;
    	 Query query=this.sessionFactory.getCurrentSession().createQuery(hql);
    	 PresentRecord obj=(PresentRecord) query.uniqueResult();
    	 if(obj!=null){
    		return obj; 
    	 }else{
    		return null; 
    	 }
     }
     /**
      * 修改记录
      */
     public int updatePresentRecord(PresentRecord presentRecord){
    	 this.sessionFactory.getCurrentSession().update(presentRecord);
    	 return 1;
     }
     /**
	   * 分页查询已审核记录
	   */
     public Pager queryPresentAuditS(int pageSize,int page,String search){
    	 String hql="from PresentRecord pr left join pr.user where pr.presentStatus<>0";
   	  if(search!=null&&!"".equals(search.trim())){
   			 hql+="and pr.user.username like'%"+search+"%'";
   		 }
   		 hql+=" order by pr.presentTime desc";
   		 int allRow =this.getAllRowCount(hql);    
   	       int totalPage = Pager.countTotalPage(pageSize, allRow);    
   	       final int offset = Pager.countOffset(pageSize, page);    
   	       final int length = pageSize;    
   	       final int currentPage = Pager.countCurrentPage(page);
   	       List list = this.queryForPage(hql,offset, length); 
   	       
   	       Pager pages = new Pager();
   	       pages.setPageSize(pageSize);    
   	       pages.setCurrentPage(currentPage);
   	       pages.setAllRow(allRow);
   	       pages.setTotalPage(totalPage);
   	       pages.setList(list);
   	       pages.init();
   	       return pages; 
     }
     /**
      * 批量删除
      */
     public int deleteAllPresentRecord(String numbers){
    	 String hql="delete PresentRecord pr where pr.presentId in ("+numbers+")";
    	 return this.sessionFactory.getCurrentSession().createQuery(hql).executeUpdate();
    	 
     }
     /**
      * 批量删除
      */
     public int deletePresentRecord(long presentId){
    	 String hql="delete PresentRecord pr where pr.presentId ="+presentId;
    	 return this.sessionFactory.getCurrentSession().createQuery(hql).executeUpdate();
     }
     //分页查询提现账单
     @Override
     public Pager queryPagetixian(int pageSize, int page, long user) {
    	String hql="select p from PresentRecord p where p.user.id="+user;
    	hql+=" order by p.presentTime desc";
  		 int allRow =this.getAllRowCount(hql);    
  	       int totalPage = Pager.countTotalPage(pageSize, allRow);    
  	       final int offset = Pager.countOffset(pageSize, page);    
  	       final int length = pageSize;    
  	       final int currentPage = Pager.countCurrentPage(page);
  	       List list = this.queryForPage(hql,offset, length); 
  	       
  	       Pager pages = new Pager();
  	       pages.setPageSize(pageSize);    
  	       pages.setCurrentPage(currentPage);
  	       pages.setAllRow(allRow);
  	       pages.setTotalPage(totalPage);
  	       pages.setList(list);
  	       pages.init();
  	       return pages; 
    	 
     }
     
     
     
     
     
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


}
