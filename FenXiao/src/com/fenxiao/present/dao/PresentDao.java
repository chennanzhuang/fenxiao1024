package com.fenxiao.present.dao;

import java.util.List;

import com.fenxiao.common.Pager;
import com.fenxiao.present.entity.PresentRecord;
import com.fenxiao.user.entity.User;

public interface PresentDao {
	//分页查询提现账单
	public Pager queryPagetixian(int pageSize,int page,long user);
	
	public int addPresentRecord(PresentRecord presentRecord);
	
	public List<PresentRecord> queryAllPresentRecord(long userId);

	 /**
	    * 查询所有记录数
	    * @param hql 查询的条件
	    * @return 总记录数
	    */
	public int getAllRowCount(String hql);
		/**
		    * 分页查询
		    * @param hql 查询的条件
		    * @param offset 开始记录
		    * @param length 一次查询几条记录
		    * @return
		    */
     public List  queryForPage(String hql, int offset, int length);

       /**
	   * 分页查询
	   */
      public Pager queryPresent(int pageSize,int page,String search);
      /**
	   * 分页查询已审核记录
	   */
      public Pager queryPresentAuditS(int pageSize,int page,String search);
      /**
       * 根据ID查询记录
       */
      public PresentRecord queryPresentRecordInfo(long presentId);
      /**
       * 修改记录
       */
      public int updatePresentRecord(PresentRecord presentRecord);
      /**
       * 批量删除
       */
      public int deleteAllPresentRecord(String numbers);
      /**
       * 批量删除
       */
      public int deletePresentRecord(long presentId);
}
