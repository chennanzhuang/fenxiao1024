package com.fenxiao.present.entity;

import com.fenxiao.admin.entity.Admin;
import com.fenxiao.agent.entity.Agent;
import com.fenxiao.user.entity.User;
// default package

/**
 * 提现记录
 * PresentRecord entity. @author MyEclipse Persistence Tools
 */

public class PresentRecord implements java.io.Serializable {

	// Fields

	private Long presentId;
	private Agent agent;//代理商
	private Admin admin;//管理员
	private User user;//会员
	private Double presentMoney;//提现金额
	private Double presentFee;//提现手续费
	private Double remainingSum;//此次提现后余额
	private String presentTime;//时间
    private int presentStatus;//提现状态
    private String presentReson;//提现失败原因
    private String auditTime;//审核时间
	// Constructors

	/** default constructor */
	public PresentRecord() {
	}

	/** full constructor */
	public PresentRecord(Agent agent, Admin admin, User user,
			Double presentMoney, Double presentFee, String presentTime,int presentStatus,
			String presentReson,String auditTime) {
		this.agent = agent;
		this.admin = admin;
		this.user = user;
		this.presentMoney = presentMoney;
		this.presentFee = presentFee;
		this.presentTime = presentTime;
		this.presentStatus=presentStatus;
		this.presentReson=presentReson;
		this.auditTime=auditTime;
	}

	// Property accessors

	public Long getPresentId() {
		return this.presentId;
	}

	public void setPresentId(Long presentId) {
		this.presentId = presentId;
	}

	public Agent getAgent() {
		return this.agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public Admin getAdmin() {
		return this.admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Double getRemainingSum() {
		return remainingSum;
	}

	public void setRemainingSum(Double remainingSum) {
		this.remainingSum = remainingSum;
	}

	public Double getPresentMoney() {
		return this.presentMoney;
	}

	public void setPresentMoney(Double presentMoney) {
		this.presentMoney = presentMoney;
	}

	public Double getPresentFee() {
		return this.presentFee;
	}

	public void setPresentFee(Double presentFee) {
		this.presentFee = presentFee;
	}

	public String getPresentTime() {
		return this.presentTime;
	}

	public void setPresentTime(String presentTime) {
		this.presentTime = presentTime;
	}

	public int getPresentStatus() {
		return presentStatus;
	}

	public void setPresentStatus(int presentStatus) {
		this.presentStatus = presentStatus;
	}

	public String getPresentReson() {
		return presentReson;
	}

	public void setPresentReson(String presentReson) {
		this.presentReson = presentReson;
	}

	public String getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(String auditTime) {
		this.auditTime = auditTime;
	}

}