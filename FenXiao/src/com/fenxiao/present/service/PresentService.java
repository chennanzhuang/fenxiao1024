package com.fenxiao.present.service;

import java.util.List;

import com.fenxiao.common.Pager;
import com.fenxiao.present.entity.PresentRecord;



public interface PresentService {
	//分页查询提现账单
		public Pager queryPagetixian(int pageSize,int page,long user);
	public int addPresentRecord(PresentRecord presentRecord);
	
	public List<PresentRecord> queryAllPresentRocord(long userId);
	
	 /**
     * 分页查询
     */
   public Pager queryPresent(int pageSize,int page,String search);
   /**
    * 根据ID查询记录
    */
   public PresentRecord queryPresentRecordInfo(long presentId);
   /**
    * 修改记录
    */
   public int updatePresentRecord(PresentRecord presentRecord);
   /**
	   * 分页查询已审核记录
	   */
   public Pager queryPresentAuditS(int pageSize,int page,String search);
   /**
    * 批量删除
    */
   public int deleteAllPresentRecord(String numbers);
   /**
    * 批量删除
    */
   public int deletePresentRecord(long presentId);
}
