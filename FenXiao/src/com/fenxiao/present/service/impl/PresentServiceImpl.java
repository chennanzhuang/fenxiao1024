package com.fenxiao.present.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fenxiao.common.Pager;
import com.fenxiao.present.dao.PresentDao;
import com.fenxiao.present.entity.PresentRecord;
import com.fenxiao.present.service.PresentService;
import com.fenxiao.user.entity.User;

@Service
@Transactional
public class PresentServiceImpl implements PresentService {
	@Resource
	private PresentDao presentDao;

	@Override
	public int addPresentRecord(PresentRecord presentRecord) {
		// TODO Auto-generated method stub
		return presentDao.addPresentRecord(presentRecord);
	}
	
	@Override
	public List<PresentRecord> queryAllPresentRocord(long userId) {
		// TODO Auto-generated method stub
		return presentDao.queryAllPresentRecord(userId);
	}

	 /**
     * 分页查询
     */
   public Pager queryPresent(int pageSize,int page,String search){
	   
	   return presentDao.queryPresent(pageSize, page, search);
   }
	
   /**
    * 根据ID查询记录
    */
   public PresentRecord queryPresentRecordInfo(long presentId){
	  return presentDao.queryPresentRecordInfo(presentId);
   }
   /**
    * 修改记录
    */
   public int updatePresentRecord(PresentRecord presentRecord){
	   return presentDao.updatePresentRecord(presentRecord);
   }
   /**
	   * 分页查询已审核记录
	   */
   public Pager queryPresentAuditS(int pageSize,int page,String search){
	   return presentDao.queryPresentAuditS(pageSize, page, search);
   }
   /**
    * 批量删除
    */
   public int deleteAllPresentRecord(String numbers){
	   return presentDao.deleteAllPresentRecord(numbers);
   }
   /**
    * 批量删除
    */
   public int deletePresentRecord(long presentId){
	   return presentDao.deletePresentRecord(presentId);
   }
   
   
   
   
	public PresentDao getPresentDao() {
		return presentDao;
	}

	public void setPresentDao(PresentDao presentDao) {
		this.presentDao = presentDao;
	}
	//分页查询提现账单
	@Override
	public Pager queryPagetixian(int pageSize, int page, long user) {
		// TODO Auto-generated method stub
		return presentDao.queryPagetixian(pageSize, page, user);
	}

}
