package com.fenxiao.present.action;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.fenxiao.admin.entity.Admin;
import com.fenxiao.admin.service.AdminService;
import com.fenxiao.card.entity.Bankcard;
import com.fenxiao.card.service.CardService;
import com.fenxiao.common.Pager;
import com.fenxiao.present.entity.PresentRecord;
import com.fenxiao.present.service.PresentService;
import com.fenxiao.user.entity.User;
import com.fenxiao.user.service.UserService;
import com.opensymphony.xwork2.ActionSupport;

public class PresentAction extends ActionSupport{
	private HttpServletRequest request;
	private HttpServletResponse response;
	private HttpSession session;
	private PresentService presentService;

	private int page=1;
	private List<PresentRecord> presentRecordList;
	private Pager pageBean;
	private String search;
	private User user;
	private PresentRecord presentRecord;
	private CardService cardService;
	private Bankcard bankcard;

	
	private String num;
	
	private Admin admin;
	private AdminService adminService;
	private UserService userService;
	//提现账单
	public String queryPresentList(){
		 request = ServletActionContext.getRequest();
		 response = ServletActionContext.getResponse();
	long userId=Long.parseLong(request.getParameter("userId"));
	   //分页查询
		Pager pagepresent = presentService.queryPagetixian(10, page, userId);
		int Total = pagepresent.getTotalPage();
	    List pagetixia = pagepresent.getList();
	
	
//		List<PresentRecord> list=presentService.queryAllPresentRocord(userId);
		User user=userService.queryuserById(userId);
		
//		request.setAttribute("list",list);
		request.setAttribute("openId",user.getWeChatId());
		request.setAttribute("pagetixia",pagetixia);
		request.setAttribute("Total",Total);
		request.setAttribute("page",page);
		return "queryPresentList";
	}
	/**
	 * 分页查询提现审核记录
	 * @return
	 */
     public String queryPresentPageList(){
    	request = ServletActionContext.getRequest();
 		response = ServletActionContext.getResponse();
     	pageBean=presentService.queryPresent(3, page, search);
     	presentRecordList=pageBean.getList();
     	request.setAttribute("pageBean", pageBean);
     	request.setAttribute("presentRecordList", presentRecordList);
     	return "queryPresentPageList";	 
    	 
    	 
     }
	/**
	 * 提现审核页面
	 * @return
	 */
	public String queryPresentPage(){
	  request = ServletActionContext.getRequest();
	 response = ServletActionContext.getResponse();
	 presentRecord=presentService.queryPresentRecordInfo(presentRecord.getPresentId());
	 bankcard=cardService.queryBankcardOfUser(user.getId());
	 //提现手续费
	 Double persentMoney = presentRecord.getPresentMoney();
	// user=userService.queryuserById(user.getId());
	 request.setAttribute("presentRecord", presentRecord);
  	request.setAttribute("bankcard", bankcard);
  //	request.setAttribute("user", user);
  	return "queryPresentPage";
		
	}
	
	/**
	 * 审核通过
	 * @return
	 */
	public String AuditPresentSuccess(){
		PresentRecord presentRecord1=presentService.queryPresentRecordInfo(presentRecord.getPresentId());
		presentRecord1.setPresentStatus(1);
		presentRecord1.setPresentReson(presentRecord.getPresentReson());
		admin=adminService.findAdmin(admin);
		presentRecord1.setAdmin(admin);
		Date date =new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String df=sdf.format(date);
		presentRecord1.setAuditTime(df);
		
		int i=presentService.updatePresentRecord(presentRecord1);
		return queryPresentPageList();
	} 
	/**
	 * 审核失败
	 * @return
	 */
	public String AuditPresentError(){
		PresentRecord presentRecord1=presentService.queryPresentRecordInfo(presentRecord.getPresentId());
		presentRecord1.setPresentStatus(2);
		presentRecord1.setPresentReson(presentRecord.getPresentReson());
		admin=adminService.findAdmin(admin);
		presentRecord1.setAdmin(admin);
		   Long userid = presentRecord1.getUser().getId();
		   //用户余额
		   NumberFormat formatter1 = new DecimalFormat("#0.00");
		    Double balance1 = presentRecord1.getRemainingSum();
		    double balance = Double.parseDouble(formatter1.format(balance1));//（四舍五入）     
		    //提现金额
		     Double presentMoney1 = presentRecord1.getPresentMoney();
		    double preMoney = Double.parseDouble(formatter1.format(presentMoney1));//（四舍五入）     
		    //手续费
		    Double presentFee = presentRecord1.getPresentFee();
	
		    double preFee = Double.parseDouble(formatter1.format(presentFee));//（四舍五入）  
		    double money = (balance+preMoney+preFee);
		   userService.updateBalanceUsermonye(money, userid);
		Date date =new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String df=sdf.format(date);
		presentRecord1.setAuditTime(df);
		
		int i=presentService.updatePresentRecord(presentRecord1);
		return queryPresentPageList();
	} 
	/**
	 * 分页查询已审核记录
	 * @return
	 */
	public String queryAuditPresent(){
		request = ServletActionContext.getRequest();
 		response = ServletActionContext.getResponse();
     	pageBean=presentService.queryPresentAuditS(3, page, search);
     	presentRecordList=pageBean.getList();
     	request.setAttribute("pageBean", pageBean);
     	request.setAttribute("presentRecordList", presentRecordList);
     	return "queryAuditPresent";	 
	}
	
	
	/**
	 * 批量删除
	 * @return
	 */
	public String deleteAllPresent(){
		int i=presentService.deleteAllPresentRecord(num);
		return queryAuditPresent();
		
	}
	/**
	 * 删除
	 * @return
	 */
	public String deletePresent(){
		int i=presentService.deletePresentRecord(presentRecord.getPresentId());
		return queryAuditPresent();
		
	}
	
	


	public PresentService getPresentService() {
		return presentService;
	}

	public void setPresentService(PresentService presentService) {
		this.presentService = presentService;
	}


	public HttpServletRequest getRequest() {
		return request;
	}


	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}


	public HttpServletResponse getResponse() {
		return response;
	}


	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}


	public HttpSession getSession() {
		return session;
	}


	public void setSession(HttpSession session) {
		this.session = session;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public List<PresentRecord> getPresentRecordList() {
		return presentRecordList;
	}
	public void setPresentRecordList(List<PresentRecord> presentRecordList) {
		this.presentRecordList = presentRecordList;
	}
	public Pager getPageBean() {
		return pageBean;
	}
	public void setPageBean(Pager pageBean) {
		this.pageBean = pageBean;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public PresentRecord getPresentRecord() {
		return presentRecord;
	}
	public void setPresentRecord(PresentRecord presentRecord) {
		this.presentRecord = presentRecord;
	}
	public CardService getCardService() {
		return cardService;
	}
	public void setCardService(CardService cardService) {
		this.cardService = cardService;
	}
	public Bankcard getBankcard() {
		return bankcard;
	}
	public void setBankcard(Bankcard bankcard) {
		this.bankcard = bankcard;
	}
	public Admin getAdmin() {
		return admin;
	}
	public void setAdmin(Admin admin) {
		this.admin = admin;
	}
	public AdminService getAdminService() {
		return adminService;
	}
	public void setAdminService(AdminService adminService) {
		this.adminService = adminService;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public UserService getUserService() {
		return userService;
	}
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
