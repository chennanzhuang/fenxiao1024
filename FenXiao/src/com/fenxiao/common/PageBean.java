package com.fenxiao.common;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
/**
 * ������
 * @author Administrator
 *
 */
public class PageBean {
	
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	/**
     * ��ѯ���м�¼��
     * @param hql ��ѯ������
     * @return �ܼ�¼��
     */
	public int getAllRowCount(String hql) {
		Session session=sessionFactory.openSession();
		List list=null;
		try{
			Query query=session.createQuery(hql);
		
			list=query.list();
			
		}catch (RuntimeException e) {
		   throw e;
		}
		session.close();
		return list.size();
		
	}
	/**
     * ��ҳ��ѯ
     * @param hql ��ѯ������
     * @param offset ��ʼ��¼
     * @param length һ�β�ѯ������¼
     * @return
     */
	public List  queryForPage(String hql, int offset, int length) {
		Session session=sessionFactory.openSession();
		List  list=null;
		try{
			Query query=session.createQuery(hql);
			query.setFirstResult(offset);
            query.setMaxResults(length);
            list=query.list();
			
		}catch (RuntimeException e) {
		   throw e;
		}
		session.close();
		return list;
	}
	/**
     * ��ѯ���м�¼��(ԭ��̬sql)
     * @param hql ��ѯ������
     * @return �ܼ�¼��
     */
	public int getAllSQLRowCount(String hql) {
		Session session=sessionFactory.openSession();
		List list=null;
		try{
			Query query=session.createSQLQuery(hql);
		
			list=query.list();
			
		}catch (RuntimeException e) {
		   throw e;
		}
		session.close();
		return list.size();
		
	}
	/**
     * ��ҳ��ѯ
     * @param sql ��ѯ������
     * @param offset ��ʼ��¼
     * @param length һ�β�ѯ������¼
     * @return
     */
	public List  querySQLForPage(String hql, int offset, int length) {
		Session session=sessionFactory.openSession();
		List  list=null;
		try{
			Query query=session.createSQLQuery(hql);
			query.setFirstResult(offset);
            query.setMaxResults(length);
            list=query.list();
			
		}catch (RuntimeException e) {
		   throw e;
		}
		session.close();
		return list;
	}
	

	
}
