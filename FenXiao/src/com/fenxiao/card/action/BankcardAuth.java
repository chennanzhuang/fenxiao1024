package com.fenxiao.card.action;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

//详情参考：http://www.apix.cn/services/show/159
public class BankcardAuth {
	
	public static String request(String httpUrl, String httpArg) {
	    BufferedReader reader = null;
	    String result = null;
	    StringBuffer sbf = new StringBuffer();
	    httpUrl = httpUrl + "?" + httpArg;

	    try {
	        URL url = new URL(httpUrl);
	        HttpURLConnection connection = (HttpURLConnection) url
	                .openConnection();
	        connection.setRequestMethod("GET");
			
	        // 填入apix-key到HTTP header
	        connection.setRequestProperty("apix-key", "sfjnjewfy34rwfsh7273fhfb23hbdsb");
	        connection.connect();
	        InputStream is = connection.getInputStream();
	        reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
	        String strRead = null;
	        while ((strRead = reader.readLine()) != null) {
	            sbf.append(strRead);
	            sbf.append("\r\n");
	        }
	        reader.close();
	        result = sbf.toString();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return result;
	}

	public static void main(String[] args) {
	    //发送 GET 请求
		String httpUrl = "http://v.apix.cn/apixcredit/bankcardauth/bankcardauth";
		String httpArg = "name=陈自伟&bankcardno=6212261302004911888";
		String jsonResult = request(httpUrl, httpArg);
		System.out.println(jsonResult);
	}
}
