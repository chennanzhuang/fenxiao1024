package com.fenxiao.card.action;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.components.Password;

import com.cloopen.rest.sdk.utils.encoder.BASE64Encoder;
import com.fenxiao.card.entity.Bankcard;
import com.fenxiao.card.service.CardService;
import com.fenxiao.user.entity.User;
import com.fenxiao.user.service.UserService;
import com.opensymphony.xwork2.ActionSupport;

public class BankcardAction extends ActionSupport{
	private HttpServletRequest request;
	private HttpServletResponse response;
	private HttpSession session;
	private Bankcard bankcard;
	private CardService cardService;
	private UserService userService;
	private Map<String, Object> dataMap;
	
	public Map<String, Object> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, Object> dataMap) {
		this.dataMap = dataMap;
	}

	//密码提现
	public String bindingBankcard() throws Exception{
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		User user=userService.queryuserById(bankcard.getUser().getId());
		bankcard.setUser(user);
		   user.setPresentPass(EncoderByMd5(bankcard.getCardPassword()));
		    int i = userService.updateUser(user);
		    bankcard.setCardPassword(EncoderByMd5(bankcard.getCardPassword()));
		cardService.addBankcard(bankcard);
		
		request.setAttribute("user",user);
		request.setAttribute("openId",user.getWeChatId());
		return "money";
	}
	
	public String updateBankcard(){
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		String userId=request.getParameter("userId");
		request.setAttribute("userId",userId);
		User user=userService.queryuserById(Long.parseLong(userId));
		Bankcard carduser = cardService.queryBankcardOfUser(Long.parseLong(userId));
		String modil = carduser.getCardMobile();
		String lastFour = modil.substring(0,3)+"****"+modil.substring(7,modil.length());
		request.setAttribute("lastFour", lastFour);
		request.setAttribute("cardModile", carduser.getCardMobile());
		request.setAttribute("openId",user.getWeChatId());
		return "updateBankcard";
	}
	//判断密码
		public String checkOwnMobile1() throws Exception {
			request = ServletActionContext.getRequest();
			response = ServletActionContext.getResponse();
			dataMap = new HashMap<String, Object>();
			String UserId = request.getParameter("userId");
			String Password = request.getParameter("password");
				long userId = Long.parseLong(UserId);
//				System.err.println("password+---------"+password);
			     User User = userService.queryuserById(userId);
			      Bankcard bansk = cardService.queryBankcardOfUser(userId);
			      if (User == null) {
				dataMap.put("code", 0);
			} else {
				if(bansk!=null){
					if (Password.equals(bansk.getCardPassword())) {
						dataMap.put("code", 1);
					} else if(EncoderByMd5(Password).equals(bansk.getCardPassword())){
						dataMap.put("code", 1);
					}else {
						dataMap.put("code", 0);
					}
				}
			}

			return "dataMap";
		}
		//MD5
		public String EncoderByMd5(String str) throws Exception{
	        //确定计算方法
	        MessageDigest md5=MessageDigest.getInstance("MD5");
	        BASE64Encoder base64en = new BASE64Encoder();
	        //加密后的字符串
	        String newstr=base64en.encode(md5.digest(str.getBytes("utf-8")));
	        return newstr;
	    }
	
	public String updateBankcard2() throws Exception{
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		cardService.updateBankcard(bankcard);
		User user=userService.queryuserById(bankcard.getUser().getId());
		request.setAttribute("openId",user.getWeChatId());
		request.setAttribute("user",user);
		return "money";
	}
	
	
	
	
	
	
	public CardService getCardService() {
		return cardService;
	}
	public void setCardService(CardService cardService) {
		this.cardService = cardService;
	}
	public Bankcard getBankcard() {
		return bankcard;
	}
	public void setBankcard(Bankcard bankcard) {
		this.bankcard = bankcard;
	}
	public HttpServletRequest getRequest() {
		return request;
	}
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	public HttpServletResponse getResponse() {
		return response;
	}
	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}
	public HttpSession getSession() {
		return session;
	}
	public void setSession(HttpSession session) {
		this.session = session;
	}






	public UserService getUserService() {
		return userService;
	}






	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
