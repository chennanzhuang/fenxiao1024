package com.fenxiao.card.service.impl;

import com.fenxiao.card.dao.CardDao;
import com.fenxiao.card.entity.Bankcard;
import com.fenxiao.card.service.CardService;

public class CardServiceImpl implements CardService {
	private CardDao cardDao;

	public CardDao getCardDao() {
		return cardDao;
	}

	public void setCardDao(CardDao cardDao) {
		this.cardDao = cardDao;
	}

	@Override
	public Bankcard queryBankcardOfUser(long userId) {
		// TODO Auto-generated method stub
		return cardDao.queryCardOfUser(userId);
	}
	
	@Override
	public int addBankcard(Bankcard bankcard) {
		// TODO Auto-generated method stub
		return cardDao.addBankcard(bankcard);
	}
	public int resetPassword(long userId, String password) {
		// TODO Auto-generated method stub
		return cardDao.resetPassword(userId, password);
	}
	@Override
	public int updateBankcard(Bankcard bankcard) {
		// TODO Auto-generated method stub
		return cardDao.updateBankcard(bankcard);
	}

}
