package com.fenxiao.card.service;

import com.fenxiao.card.entity.Bankcard;

public interface CardService {
	public int resetPassword(long userId, String password);
	/**
	 * 根据userId查询银行卡
	 * @param userId
	 * @return
	 */
	public Bankcard queryBankcardOfUser(long userId);
	
	public int addBankcard(Bankcard bankcard);
	
	public int updateBankcard(Bankcard bankcard);

}
