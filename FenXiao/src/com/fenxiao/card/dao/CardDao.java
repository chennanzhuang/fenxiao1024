package com.fenxiao.card.dao;

import com.fenxiao.card.entity.Bankcard;


public interface CardDao {
	//修改密码
	public int resetPassword(long userId, String password);
	//根据userId查询银行卡
  public Bankcard queryCardOfUser(long userId);
  
  public int addBankcard(Bankcard bankcard);
  
  public int updateBankcard(Bankcard bankcard);
}
