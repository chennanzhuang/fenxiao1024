package com.fenxiao.card.dao.impl;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

import com.fenxiao.base.BaseDaoImpl;
import com.fenxiao.card.dao.CardDao;
import com.fenxiao.card.entity.Bankcard;
import com.fenxiao.user.entity.User;

public class CardDaoImpl extends BaseDaoImpl<Bankcard> implements CardDao {
	
	private  SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	
	//根据userId查询银行卡
	public Bankcard queryCardOfUser(long userId) {
		// TODO Auto-generated method stub
		String hql="select b as balance from Bankcard b LEFT JOIN b.user as u where u.id="+userId;
		//"SELECT t FROM Task as t LEFT JOIN t.user as u WHERE u.weChatId='"+openId+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		Object obj=query.uniqueResult();
		if(obj!=null){
			return (Bankcard) obj;
		}else{
			return null;	
		}
	}
	
	@Override
	public int addBankcard(Bankcard bankcard) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(bankcard);
		return 1;
	}
	
	@Override
	public int updateBankcard(Bankcard bankcard) {
		// TODO Auto-generated method stub
		String hql="update Bankcard u set u.bankName='"+bankcard.getBankName()+"',u.partBankName='"+bankcard.getPartBankName()+
				"',u.cardCode='"+bankcard.getCardCode()+"',u.cardName='"+bankcard.getCardName()+"',u.cardMobile='"+bankcard.getCardMobile()+
				"' where u.user.id="+bankcard.getUser().getId();
		return sessionFactory.getCurrentSession().createQuery(hql).executeUpdate();
	}

	@Override
	public int resetPassword(long userId, String password) {
		String hql="Update Bankcard u set u.cardPassword='"+password+"' where u.user.id="+userId;
		return sessionFactory.getCurrentSession().createQuery(hql).executeUpdate();
	}

}
