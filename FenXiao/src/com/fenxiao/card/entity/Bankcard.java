package com.fenxiao.card.entity;

import com.fenxiao.admin.entity.Admin;
import com.fenxiao.agent.entity.Agent;
import com.fenxiao.user.entity.User;
// default package

/**
 * 银行
 * Bankcard entity. @author MyEclipse Persistence Tools
 */

public class Bankcard implements java.io.Serializable {

	// Fields

	private Long cardId;//ID
	private String partBankName;
	private Agent agent;//合伙人
	private Admin admin;//管理员
	private User user;//会员
	private String bankName;//开户银行
	private double balance;//
	private String bankLogo;//logo
	private String cardCode;//卡号
	private String cardName;//用户名
	private String cardMobile;//手机号
	private String cardPassword;//提现密码
	private String cardTime;//时间

	// Constructors

	/** default constructor */
	public Bankcard() {
	}

	/** full constructor */
	public Bankcard(Agent agent, Admin admin, User user, String bankName,
			String bankLogo, String cardCode, String cardName,
			String cardMobile, String cardPassword, String cardTime) {
		this.agent = agent;
		this.admin = admin;
		this.user = user;
		this.bankName = bankName;
		this.bankLogo = bankLogo;
		this.cardCode = cardCode;
		this.cardName = cardName;
		this.cardMobile = cardMobile;
		this.cardPassword = cardPassword;
		this.cardTime = cardTime;
	}

	// Property accessors

	public Long getCardId() {
		return this.cardId;
	}

	public void setCardId(Long cardId) {
		this.cardId = cardId;
	}

	public Agent getAgent() {
		return this.agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public Admin getAdmin() {
		return this.admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankLogo() {
		return this.bankLogo;
	}

	public void setBankLogo(String bankLogo) {
		this.bankLogo = bankLogo;
	}

	public String getCardCode() {
		return this.cardCode;
	}

	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

	public String getCardName() {
		return this.cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getCardMobile() {
		return this.cardMobile;
	}

	public void setCardMobile(String cardMobile) {
		this.cardMobile = cardMobile;
	}

	public String getCardPassword() {
		return this.cardPassword;
	}

	public void setCardPassword(String cardPassword) {
		this.cardPassword = cardPassword;
	}

	public String getPartBankName() {
		return partBankName;
	}

	public void setPartBankName(String partBankName) {
		this.partBankName = partBankName;
	}

	public String getCardTime() {
		return this.cardTime;
	}

	public void setCardTime(String cardTime) {
		this.cardTime = cardTime;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

}