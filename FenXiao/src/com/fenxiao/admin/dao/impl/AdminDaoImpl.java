package com.fenxiao.admin.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.fenxiao.admin.dao.AdminDao;
import com.fenxiao.admin.entity.Admin;
import com.fenxiao.common.Pager;

public class AdminDaoImpl implements AdminDao {
	private SessionFactory sessionFactory;
	
	   public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	/**
	    * 根据登陆账号查询admin
	    */
		public Admin queryAdmin(String adminAccount){
			String hql="from Admin a where a.adminAccount='"+adminAccount
					+"' or a.adminPhone='"+adminAccount+"' or a.adminName='"+adminAccount+"'";
			Query query=sessionFactory.getCurrentSession().createQuery(hql);
			Object obj=query.uniqueResult();
			if(obj!=null){
				return (Admin) obj;
			}else{
				return null;
			}
			
		}
		/**
		 * 更新账号
		 */
		public int updateAdmin(Admin admin){
			sessionFactory.getCurrentSession().update(admin);
			return 1;
			
		}
		/**
		 * 修改密码
		 */
		public int querypass(String adminAccount,Long id){
			String hql="UPDATE Admin a SET a.adminPass = '"+adminAccount+"' WHERE a.adminId ="+id;
			int result= sessionFactory.getCurrentSession().createQuery(hql).executeUpdate();
			return 1;
			
		}
		 /**
		    * 查询所有记录数
		    * @param hql 查询的条件
		    * @return 总记录数
		    */
		public int getAllRowCount(String hql){
			Session session=sessionFactory.openSession();
			List list=null;
			try{
				Query query=session.createQuery(hql);
			
				list=query.list();
				
			}catch (RuntimeException e) {
			   throw e;
			}
			session.close();
			return list.size();
		}
			/**
			    * 分页查询
			    * @param hql 查询的条件
			    * @param offset 开始记录
			    * @param length 一次查询几条记录
			    * @return
			    */
	  public List  queryForPage(String hql, int offset, int length){
		  Session session=sessionFactory.openSession();
			List  list=null;
			try{
				Query query=session.createQuery(hql);
				query.setFirstResult(offset);
	           query.setMaxResults(length);
	           list=query.list();
				
			}catch (RuntimeException e) {
			   throw e;
			}
			session.close();
			return list;
	  }
	 
		/**
		 * 分页查询
		 */
	  public Pager queryAdmin(int pageSize,int page,String search){
		  String hql="from Admin a where 1=1";
		  if(search!=null&&!"".equals(search.trim())){
			  hql+=" and (a.adminAccount like'%"+search+"%' or a.adminName like'%"+search+"%')";
		  }
		  hql+=" order by a.regTime desc";
		  
		   int allRow =this.getAllRowCount(hql);    //总记录数
	       int totalPage = Pager.countTotalPage(pageSize, allRow);    //总页数
	       final int offset = Pager.countOffset(pageSize, page);    //当前页开始记录
	       final int length = pageSize;    //每页记录数
	       final int currentPage = Pager.countCurrentPage(page);
	       List list = this.queryForPage(hql,offset, length); //"一页"的记录
	             
	     //把分页信息保存到Bean中
	       Pager pages = new Pager();
	       pages.setPageSize(pageSize);    
	       pages.setCurrentPage(currentPage);
	       pages.setAllRow(allRow);
	       pages.setTotalPage(totalPage);
	       pages.setList(list);
	       pages.init();
	       return pages;
		   
	  }
	  /**
	   * 添加管理员
	   * 
	   */
	   public int addAdmin(Admin admin){
		   sessionFactory.getCurrentSession().save(admin);
		   return 1;
	   }
	   /**
	      * 查询管理员
	      */
	   public Admin findAdmin(Admin admin){
	    String hql="from Admin a where a.adminId="+admin.getAdminId();
	    Query query=sessionFactory.getCurrentSession().createQuery(hql);
	    Object obj=query.uniqueResult();
	    if(obj!=null){
	    	return (Admin)obj;
	    }else{
	    	return null;
	    }
	    
	     }
	   /**
	    * 查询管理员
	    */
	   public Admin checkMobile(String mobile){
		   String hql="from Admin a where a.adminPhone="+mobile;
		   Query query=sessionFactory.getCurrentSession().createQuery(hql);
		   Object obj=query.uniqueResult();
		   if(obj!=null){
			   return (Admin)obj;
		   }else{
			   return null;
		   }
		   
	   }
	   /**
	      * 批量删除
	      */
	     public int deleteAdmin(String numbers){
	    	 String hql="delete Admin a where a.adminId in ("+numbers+")";
	    	 int result= sessionFactory.getCurrentSession().createQuery(hql).executeUpdate(); 
			 return result;
	     }
	     /**
	      * 根据ID 删除
	      */
	     public int deleteAdminById(long adminId){
	    	 String hql="delete Admin a where a.adminId="+adminId;
	    	 int result= sessionFactory.getCurrentSession().createQuery(hql).executeUpdate(); 
			 return result;
	     }

		@Override
		public Admin queryAdminid(Long id) {
			 String hql="from Admin a where a.adminId="+id;
			    Query query=sessionFactory.getCurrentSession().createQuery(hql);
			    Object obj=query.uniqueResult();
			    if(obj!=null){
			    	return (Admin)obj;
			    }else{
			    	return null;
			    }
		}
}
	

