package com.fenxiao.admin.dao;

import java.util.List;

import com.fenxiao.admin.entity.Admin;
import com.fenxiao.common.Pager;
import com.fenxiao.user.entity.User;

public interface AdminDao {
	//验证手机号码
	public Admin checkMobile(String mobile); 
	//修改密码
	public int querypass(String adminAccount,Long id);
	/**
	    * 根据登陆账号查询admin
	    */
		public Admin queryAdmin(String adminAccount);
		/**
		 * 根据id
		 */
		public Admin queryAdminid(Long id);
		/**
		 * 更新账号
		 */
		public int updateAdmin(Admin admin);
		 /**
		    * 查询所有记录数
		    * @param hql 查询的条件
		    * @return 总记录数
		    */
		public int getAllRowCount(String hql);
			/**
			    * 分页查询
			    * @param hql 查询的条件
			    * @param offset 开始记录
			    * @param length 一次查询几条记录
			    * @return
			    */
	    public List  queryForPage(String hql, int offset, int length);
	 
	      /**
		   * 分页查询
		   */
	    public Pager queryAdmin(int pageSize,int page,String search);
	    /**
	     * 添加管理员
	     * 
	     */
	     public int addAdmin(Admin admin);
	     /**
	      * 查询管理员
	      */
	     public Admin findAdmin(Admin admin);
	     /**
	      * 批量删除
	      */
	     public int deleteAdmin(String numbers);
	     /**
	      * 根据ID 删除
	      */
	     public int deleteAdminById(long adminId);	
}
