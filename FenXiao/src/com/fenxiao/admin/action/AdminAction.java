package com.fenxiao.admin.action;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import com.cloopen.rest.sdk.CCPRestSDK;
import com.cloopen.rest.sdk.utils.encoder.BASE64Encoder;
import com.fenxiao.admin.entity.Admin;
import com.fenxiao.admin.service.AdminService;
import com.fenxiao.adminRole.entity.AdminRole;
import com.fenxiao.adminRole.service.AdminMenuService;
import com.fenxiao.adminRole.service.AdminRoleMenuService;
import com.fenxiao.adminRole.service.AdminRoleService;
import com.fenxiao.agent.entity.Agent;
import com.fenxiao.agent.service.AgentService;
import com.fenxiao.common.Pager;
import com.fenxiao.mytask.entity.UserTask;
import com.fenxiao.mytask.service.UserTaskService;
import com.fenxiao.task.entity.Task;
import com.fenxiao.user.entity.User;
import com.fenxiao.util.Tools;
import com.opensymphony.xwork2.ActionSupport;

public class AdminAction extends ActionSupport {

	private HttpServletRequest request;
	private HttpServletResponse response;	
	private HttpSession session;
	private Map<String, Object> dataMap;
	private User user;
	private Task task;
	public Task getTask() {
		return task;
	}
	public void setTask(Task task) {
		this.task = task;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public UserTaskService getUserTaskService() {
		return userTaskService;
	}
	public void setUserTaskService(UserTaskService userTaskService) {
		this.userTaskService = userTaskService;
	}

	private Admin admin;
	private AdminService adminService;
	private AdminRoleService adminRoleService;
	private AdminRoleMenuService adminRoleMenuService;
	private AdminMenuService adminMenuService;
	private AgentService agentService;

	private String adminAccount;
	private String adminPass;
	private String adminPass2;
	private String seccode;

	private int page;
	private List<Admin> adminList;
	private Pager pageBean;
	private String search;
	private long adminId;
	private AdminRole role;
	private int roleId;

	private String msg;
	private String mesg;

	private File fileTest; // 接收这个上传的文件
	private String fileTestFileName;
	private File fileTest1; // 接收这个上传的文件
	private String fileTest1FileName;
	private String num;
	private UserTaskService userTaskService;
	//md5加密方法
	public String EncoderByMd5(String str) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		 //确定计算方法
		         MessageDigest md5=MessageDigest.getInstance("MD5");
		         BASE64Encoder base64en = new BASE64Encoder();
		 //加密后的字符串
		         String newstr=base64en.encode(md5.digest(str.getBytes("utf-8")));
		 return newstr;
		     }
	/**
	 * 登陆
	 * 
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws NoSuchAlgorithmException 
	 */
	public String queryLogin() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();
		String authCode = null;
		if (session.getAttribute("checkcode") != null) {
			authCode = session.getAttribute("checkcode").toString();
		}
		if (seccode != null) {
			seccode = seccode.toLowerCase();
			authCode = authCode.toLowerCase();
			if (seccode.equals(authCode)) {
				admin = adminService.checkAdminLogin(adminAccount, EncoderByMd5(adminPass));
				Admin admin1 = adminService.checkAdminLogin(adminAccount, adminPass);
				if (admin != null) {
					session.setAttribute("admin", admin);
					session.setAttribute("roleId", admin.getAdminRole().getId());
					return "loginSuccess";
				}else if (admin1!=null) {
					admin1 = adminService.checkAdminLogin(adminAccount, adminPass);
					session.setAttribute("admin", admin1);
					session.setAttribute("roleId", admin1.getAdminRole().getId());
					return "loginSuccess";
				} else {
					msg = "您输入的账号或密码错误！";
					request.setAttribute("msg", msg);
					return "loginError";
				}
			} else {
				msg = "您输入的验证码错误！";
				request.setAttribute("msg", msg);
				return "loginError";
			}
		} else {
			msg = "请输入验证码！";
			request.setAttribute("msg", msg);
			return "loginError";
		}
	}
	//退出session
	public String queryOut(){
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();
		session.invalidate();
		return "loginError";
		
	}
	/**
	 *修改口令页面
	 * 
	 * @return
	 */
	public String queryXiougai() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();
		return "adminAccount";
	}
	//收益统计
	public String queryshouyi(){
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		String renwuming = request.getParameter("renwuming");
		String renwumzt = request.getParameter("renwumzt");
		//手机号码
		
		//user等级
		String yunagong = request.getParameter("yunagong");
		//代理商id
		String dalishang = request.getParameter("dailishang");
		String hehuoren = request.getParameter("hehuoren");
		String huiyuan = request.getParameter("huiyuan");
		
		
		
		return adminAccount;
		
		
	}
	/**
	 *修改口令密码
	 * 
	 * @return
	 * @throws NoSuchAlgorithmException 
	 * @throws UnsupportedEncodingException 
	 */
	public String querykouling() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();
		 adminId = (Long) request.getAttribute("adminId");
		 String adminAccount1 = request.getParameter("adminAccount");
		 if(adminId!=0){ 
			 adminService.querypass(EncoderByMd5(adminAccount1), adminId);
		 }
		return "loginError";
	}
	//查询原密码是否真确
	public String querymima() throws NoSuchAlgorithmException, UnsupportedEncodingException{
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		adminId = (Long) request.getAttribute("adminId");
		System.err.println("adminId+-----------"+adminId);
		dataMap = new HashMap<String, Object>();
		 String adminyuan = request.getParameter("adminyuan");
		      Admin admin1 = new Admin();
		      admin1.setAdminId(adminId);
		 admin = adminService.findAdmin(admin1);
		 if(!admin.getAdminPass().equals(EncoderByMd5(adminyuan))&&!admin.getAdminPass().equals(adminyuan)){
			 dataMap.put("code", 0);	
		 }else {
			dataMap.put("code", 1);
		}
		 return "dataMap";
		
	}
	//查询手机号码
	public String checkMobile() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		dataMap = new HashMap<String, Object>();
		String adminPhone = request.getParameter("adminPhone");
		Admin admin1 = adminService.checkMobile(adminPhone);
		if (admin1 != null) {
			dataMap.put("code", 0);
		} else {
			dataMap.put("code", 1);
		}
		return "dataMap";
	}
	//获取验证码
	public String getSMSCheckCode() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		dataMap = new HashMap<String, Object>();
		HashMap<String, Object> result = null;
		CCPRestSDK restAPI = new CCPRestSDK();
		String adminPhone = request.getParameter("adminPhone");
		/*
		 * String userId=request.getParameter("userId"); User
		 * user=userService.queryuserById(Long.parseLong(userId));
		 */
		restAPI.init("app.cloopen.com", "8883");
		// 初始化服务器地址和端口，生产环境配置成app.cloopen.com，端口是8883.
		restAPI.setAccount("8a48b5514a61a814014a7faa434e11bb",
				"4aeffdba4df3435aa816609728dd5052");
		// 初始化主账号名称和主账号令牌，登陆云通讯网站后，可在"控制台-应用"中看到开发者主账号ACCOUNT SID和
		// 主账号令牌AUTH TOKEN。
		restAPI.setAppId("aaf98f894a70a61d014a7fe71da8099d");
		// 初始化应用ID，如果是在沙盒环境开发，请配置"控制台-应用-测试DEMO"中的APPID。
		// 如切换到生产环境，请使用自己创建应用的APPID
		String checkCode = getSix();
		System.out.println(checkCode);
		dataMap.put("code", checkCode);
		result = restAPI.sendTemplateSMS(adminPhone, "111684",
				new String[] { checkCode });
		System.out.println("SDKTestGetSubAccounts result=" + result);
		if ("000000".equals(result.get("statusCode"))) {
			// 正常返回输出data包体信息（map）
			HashMap<String, Object> data = (HashMap<String, Object>) result
					.get("data");
			Set<String> keySet = data.keySet();
			for (String key : keySet) {
				Object object = data.get(key);
				System.out.println(key + " = " + object);
				dataMap.put("codeT", 1);
			}
		} else {
			// 异常返回输出错误码和错误信息
			System.out.println("错误码=" + result.get("statusCode") + " 错误信息= "
					+ result.get("statusMsg"));
			dataMap.put("codeT", 0);
			dataMap.put("msg", result.get("statusMsg"));
		}
		response.setHeader("Access-Control-Allow-Origin", "*");
		return "dataMap";
	}

	public String getSix() {
		Random rad = new Random();
		String result = rad.nextInt(1000000) + "";
		if (result.length() != 6) {
			return getSix();
		}
		return result;
	}
	/**
	 * 验证账号是否存在
	 * 
	 * @return
	 */
	public String queryAccount() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();
		dataMap = new HashMap<String, Object>();
		admin = adminService.queryAdmin(adminAccount);
		if (admin != null) {
			dataMap.put("mesg", "账号可以使用");
		} else {
			dataMap.put("mesg", "账号不存在，请重新输入！");
		}
		return "dataMap";
	}

	/**
	 * 修改密码
	 * 
	 * @return
	 * @throws NoSuchAlgorithmException 
	 * @throws UnsupportedEncodingException 
	 */
	public String updatePass() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();

		String authCode = session.getAttribute("checkcode").toString();
		if (seccode != null) {
			seccode = seccode.toLowerCase();

			authCode = authCode.toLowerCase();
			System.out.println(seccode);
			if (seccode.equals(authCode)) {

				admin = adminService.queryAdmin(adminAccount);
				if (admin != null) {
					if (adminPass != null && adminPass.equals(adminPass2)) {
						
						admin.setAdminPass(EncoderByMd5(adminPass));
						Date date = new Date();
						SimpleDateFormat sdf = new SimpleDateFormat(
								"yyyy-MM-dd hh:mm:ss");
						String dateTime = sdf.format(date);
						admin.setAdminloginTime(dateTime);
						adminService.updateAdmin(admin);

					} else {
						msg = "两次密码输入不一致！";
						request.setAttribute("msg", msg);
						return "updateError";
					}
					session.setAttribute("admin", admin);
					session.setAttribute("roleId", admin.getAdminRole().getId());
					return "loginError";
				} else {
					msg = "账号不存在！";
					request.setAttribute("msg", msg);
					return "updateError";
				}

			} else {
				msg = "输入验证码错误！";
				request.setAttribute("msg", msg);
				return "updateError";
			}

		} else {
			msg = "请输入验证码！";
			request.setAttribute("msg", msg);
			return "updateError";
		}
	}

	/**
	 * 分页管理员列表
	 * 
	 * @return
	 */
	public String queryAdmin() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		pageBean = adminService.queryAdmin(3, page, search);
		adminList = pageBean.getList();
		request.setAttribute("pageBean", pageBean);
		request.setAttribute("adminList", adminList);
		return "queryAdmin";
	}

	/**
	 * 添加管理员
	 * 
	 * @return
	 */
	public String addAdmin() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();
		int type = 0;
		String[] str = { ".jpg", ".jpeg", ".bmp", ".png", ".gif" };
		// 限定文件大小是4MB
		if (fileTest != null) {
			if (fileTest.length() > 2097152) {
				msg = "图片大小超出了2M";
				return "imageError";
			} else {

				for (String s : str) {
					if (fileTestFileName.endsWith(s)) {
						type = 1;
						String realPath = ServletActionContext
								.getServletContext().getRealPath("/faceimages");// 实际路径
						// String realPath ="d:/upload/faceimages";
						System.out.println(realPath);
						fileTestFileName = Tools.getRandomFileName() + s;
						File saveFile = new File(new File(realPath),
								fileTestFileName); // 在该实际路径下实例化一个文件
						// 判断父目录是否存在
						if (!saveFile.getParentFile().exists()) {
							saveFile.getParentFile().mkdirs();
						}
						try {
							// 执行文件上传
							// FileUtils 类名 org.apache.commons.io.FileUtils;
							// 是commons-io包中的，commons-fileupload 必须依赖
							// commons-io包实现文件上次，实际上就是将一个文件转换成流文件进行读写
							FileUtils.copyFile(fileTest, saveFile);
							admin.setAdminPhoto("faceimages/"
									+ fileTestFileName);

						} catch (IOException e) {
							msg = "图片上传失败";
							return "imageError";
						}
					}
				}
				if (type == 0) {
					msg = "上传的图片类型错误";
					return "imageError";
				}
			}
		}
		if (fileTest1 != null) {
			if (fileTest1.length() > 2097152) {
				msg = "图片未上传或者图片大小超出了2M";
				return "imageError";
			} else {

				for (String s : str) {
					if (fileTest1FileName.endsWith(s)) {
						type = 1;
						String realPath = ServletActionContext
								.getServletContext().getRealPath("/faceimages");// 实际路径
						// String realPath ="d:/upload/faceimages";
						System.out.println(realPath);
						fileTest1FileName = Tools.getRandomFileName() + s;
						File saveFile = new File(new File(realPath),
								fileTest1FileName); // 在该实际路径下实例化一个文件
						// 判断父目录是否存在
						if (!saveFile.getParentFile().exists()) {
							saveFile.getParentFile().mkdirs();
						}
						try {
							// 执行文件上传
							// FileUtils 类名 org.apache.commons.io.FileUtils;
							// 是commons-io包中的，commons-fileupload 必须依赖
							// commons-io包实现文件上次，实际上就是将一个文件转换成流文件进行读写
							FileUtils.copyFile(fileTest1, saveFile);
							admin.setAdminOr("faceimages/" + fileTest1FileName);

						} catch (IOException e) {
							msg = "图片上传失败";
							return "imageError";
						}
					}
				}
				if (type == 0) {
					msg = "上传的图片类型错误";
					return "imageError";
				}
			}
		}

		role = adminRoleService.queryRole(roleId);
		admin.setAdminRole(role);
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(date);
		admin.setRegTime(dateString);
		int i = adminService.addAdmin(admin);
		return queryAdmin();
	}

	/**
	 * 查看管理员详情
	 * 
	 * @return
	 */
	public String queryAdminInfo() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();
		admin = adminService.findAdmin(admin);
		request.setAttribute("admin", admin);
		return "queryAdminInfo";
	}

	/**
	 * 批量删除
	 * 
	 * @return
	 */
	public String deleteAllAdmin() {
		int i = adminService.deleteAdmin(num);
		if (i > 0) {
			msg = "删除成功！";

		} else {
			msg = "删除失败！";
		}
		return queryAdmin();
	}

	/**
	 * 修改页面
	 */
	public String updateAdminPage() {

		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();

		admin = adminService.findAdmin(admin);
		request.setAttribute("admin", admin);

		return "updateAdminPage";

	}

	/**
	 * 修改admin
	 * 
	 * @return
	 */
	public String updateAdmin() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();
		Admin ag = adminService.findAdmin(admin);
		System.err.println("fileTest+----------"+fileTest);
		System.err.println("fileTestFileName+----------"+fileTestFileName);
		int type = 0;
		String[] str = { ".jpg", ".jpeg", ".bmp", ".png", ".gif" };
		// 限定文件大小是4MB
		if (fileTest != null) {
			if (fileTest.length() > 2097152) {
				msg = "图片大小超出了2M";
				return "imageError";
			} else {

				for (String s : str) {
					if (fileTestFileName.endsWith(s)) {
						type = 1;
						String realPath = ServletActionContext
								.getServletContext().getRealPath("/faceimages");// 实际路径
						// String realPath ="d:/upload/faceimages";
						System.out.println(realPath);
						fileTestFileName = Tools.getRandomFileName() + s;
						File saveFile = new File(new File(realPath),
								fileTestFileName); // 在该实际路径下实例化一个文件
						// 判断父目录是否存在
						if (!saveFile.getParentFile().exists()) {
							saveFile.getParentFile().mkdirs();
						}
						try {
							// 执行文件上传
							// FileUtils 类名 org.apache.commons.io.FileUtils;
							// 是commons-io包中的，commons-fileupload 必须依赖
							// commons-io包实现文件上次，实际上就是将一个文件转换成流文件进行读写
							FileUtils.copyFile(fileTest, saveFile);
							ag.setAdminPhoto("faceimages/" + fileTestFileName);

						} catch (IOException e) {
							msg = "图片上传失败";
							return "imageError";
						}
					}
				}
				if (type == 0) {
					msg = "上传的图片类型错误";
					return "imageError";
				}
			}
		} else {
			ag.setAdminPhoto(admin.getAdminPhoto());
		}

		if (fileTest1 != null) {
			if (fileTest1.length() > 2097152) {
				msg = "图片未上传或者图片大小超出了2M";
				return "imageError";
			} else {

				for (String s : str) {
					if (fileTest1FileName.endsWith(s)) {
						type = 1;
						String realPath = ServletActionContext
								.getServletContext().getRealPath("/faceimages");// 实际路径
						// String realPath ="d:/upload/faceimages";
						System.out.println(realPath);
						fileTest1FileName = Tools.getRandomFileName() + s;
						File saveFile = new File(new File(realPath),
								fileTest1FileName); // 在该实际路径下实例化一个文件
						// 判断父目录是否存在
						if (!saveFile.getParentFile().exists()) {
							saveFile.getParentFile().mkdirs();
						}
						try {
							// 执行文件上传
							// FileUtils 类名 org.apache.commons.io.FileUtils;
							// 是commons-io包中的，commons-fileupload 必须依赖
							// commons-io包实现文件上次，实际上就是将一个文件转换成流文件进行读写
							FileUtils.copyFile(fileTest1, saveFile);
							ag.setAdminOr("faceimages/" + fileTest1FileName);

						} catch (IOException e) {
							msg = "图片上传失败";
							return "imageError";
						}
					}
				}
				if (type == 0) {
					msg = "上传的图片类型错误";
					return "imageError";
				}
			}
		} else {
			ag.setAdminOr(admin.getAdminOr());
		}
		role = adminRoleService.queryRole(roleId);
		ag.setAdminRole(role);
		ag.setRegTime(admin.getRegTime());
		ag.setAdminAccount(admin.getAdminAccount());
		ag.setAdminBalance(admin.getAdminBalance());
		ag.setAdminMoney(admin.getAdminMoney());
		ag.setAdminName(admin.getAdminName());
		ag.setAdminPass(admin.getAdminPass());
		ag.setAdminPhone(admin.getAdminPhone());
		ag.setAdminWeChatId(admin.getAdminWeChatId());
		ag.setRecommendationCode(admin.getRecommendationCode());

		int i = adminService.updateAdmin(ag);
		if (i > 0) {
			return queryAdmin();
		} else {
			msg = "修改失败！";
			adminId = adminId;
			return updateAdminPage();
		}
	}
	/**
	 * 分页查询已审核的任务
	 * 
	 * @return
	 */
	public String queryAuditUserTask() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();
		String taskType1 = request.getParameter("taskType");
		String modile = request.getParameter("modile");
		String agentHehuo = request.getParameter("agentHehuo");
		String daili = request.getParameter("agentdaili");
		String Useryuan = request.getParameter("Useryuan");
		System.err.println("agentHehuo+-----------"+agentHehuo);
		System.err.println("daili+-----------"+daili);
		System.err.println("Useryuan+-----------"+Useryuan);
		//查询合伙人
		 List<Agent> agentlist = agentService.findPartnerList();
		 if(agentlist!=null){
			 request.setAttribute("agentlist", agentlist);
		 }
		int taskType=0;
		long hehuoId=0;
		long dailiId=0;
		long yuanId=0;
		if(taskType1!=null&&!"".equals(taskType1)){
			taskType  = Integer.parseInt(taskType1);
			request.setAttribute("taskType", taskType);
		}
		//按会员搜索
		if(search!=null&&!"".equals(search)){
			request.setAttribute("search", search);
		}
		if(modile!=null&&!"".equals(modile)){
			request.setAttribute("modile", modile);
		}
		//按合伙人查询
		if(agentHehuo!=null&&!"".equals(agentHehuo)){
			System.err.println("我进来了");
			 hehuoId = Long.parseLong(agentHehuo);
			 
		}
		request.setAttribute("agentHehuo", hehuoId);
		//按代理商查询
		if(daili!=null&&!"".equals(daili)){
			dailiId = Long.parseLong(daili);
		
		
		}
		request.setAttribute("daili", dailiId);
		//按员工查询
		if(Useryuan!=null&&!"".equals(Useryuan)){
			yuanId = Long.parseLong(Useryuan);
		}
		request.setAttribute("Useryuan", yuanId);
		double hehuoshu = 0;
		pageBean = userTaskService.queryAllTaskPageChengong(3, page, search, taskType, modile,hehuoId,dailiId,yuanId);
		List usertaskList = pageBean.getList();
		request.setAttribute("Tola",pageBean.getTotalPage());
	    Double shouyi = userTaskService.queryAllTaskshouyi(search, taskType, modile, hehuoId, dailiId, yuanId);
	    //任务收益
	    if(shouyi!=null&&!"".equals(shouyi)){
	    	BigDecimal   b   =   new   BigDecimal(shouyi);  
	    	double   f1   =   b.setScale(2,   BigDecimal.ROUND_HALF_UP).doubleValue();  
	    	if(hehuoId!=-1&&dailiId==-1&&yuanId==-1){
	    		     double hehuoren = (shouyi*1/3);
	    		BigDecimal   b1   =   new   BigDecimal(hehuoren);  
		    	double   f2   =   b1.setScale(2,   BigDecimal.ROUND_HALF_UP).doubleValue();  
		    	System.err.println("f2+-------------------"+f2);
		    	request.setAttribute("hehuoshouyi", f2);
	    	}else if (hehuoId!=-1&&dailiId!=-1&&yuanId==-1) {
	    		   double dailisahng = (shouyi*1/3);
		    		BigDecimal   b2   =   new   BigDecimal(dailisahng);  
			    	double   f3   =   b2.setScale(2,   BigDecimal.ROUND_HALF_UP).doubleValue();  
			    	System.err.println("f3+-------------------"+f3 );
			    	request.setAttribute("dailisahng", f3);
			}else if(hehuoId!=-1&&dailiId!=-1&&yuanId!=-1&&agentHehuo!=null&&daili!=null&&Useryuan!=null){
				double yuangong = (shouyi*1/6);
	    		BigDecimal   b3   =   new   BigDecimal(yuangong);  
		    	double   f4   =   b3.setScale(2,   BigDecimal.ROUND_HALF_UP).doubleValue();  
		    	System.err.println("f4+-------------------"+f4 );
		    	request.setAttribute("yuangong", f4);
			}
	    	request.setAttribute("hehuoshu", f1);
	    }else {
	    	if(hehuoId!=-1&&dailiId==-1&&yuanId==-1){
   		
	    	request.setAttribute("hehuoshouyi", 0.00);
   	}else if (hehuoId!=-1&&dailiId!=-1&&yuanId==-1) {
   		  
		    	request.setAttribute("dailisahng", 0.00);
		}else if(hehuoId!=-1&&dailiId!=-1&&yuanId!=-1&&agentHehuo!=null&&daili!=null&&Useryuan!=null){
		
	    	request.setAttribute("yuangong", 0.00);
		}
	    	request.setAttribute("hehuoshu", 0.00);
		}
	     request.setAttribute("usertaskList", usertaskList);
		request.setAttribute("pageBean", pageBean);
		return "queryAuditUserTask";
	}

	/**
	 * 根据ID删除admin
	 * 
	 * @return
	 */
	public String deleteAdmin() {
		int i = adminService.deleteAdminById(admin.getAdminId());
		if (i > 0) {
			msg = "删除成功！";

		} else {
			msg = "删除失败！";
		}
		return queryAdmin();
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public Admin getAdmin() {
		return admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public AdminService getAdminService() {
		return adminService;
	}

	public void setAdminService(AdminService adminService) {
		this.adminService = adminService;
	}

	public String getAdminAccount() {
		return adminAccount;
	}

	public void setAdminAccount(String adminAccount) {
		this.adminAccount = adminAccount;
	}

	public String getAdminPass() {
		return adminPass;
	}

	public void setAdminPass(String adminPass) {
		this.adminPass = adminPass;
	}

	public String getSeccode() {
		return seccode;
	}

	public void setSeccode(String seccode) {
		this.seccode = seccode;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Map<String, Object> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, Object> dataMap) {
		this.dataMap = dataMap;
	}

	public String getMesg() {
		return mesg;
	}

	public void setMesg(String mesg) {
		this.mesg = mesg;
	}

	public String getAdminPass2() {
		return adminPass2;
	}

	public void setAdminPass2(String adminPass2) {
		this.adminPass2 = adminPass2;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public List<Admin> getAdminList() {
		return adminList;
	}

	public void setAdminList(List<Admin> adminList) {
		this.adminList = adminList;
	}

	public Pager getPageBean() {
		return pageBean;
	}

	public void setPageBean(Pager pageBean) {
		this.pageBean = pageBean;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public long getAdminId() {
		return adminId;
	}

	public void setAdminId(long adminId) {
		this.adminId = adminId;
	}

	public AdminRoleService getAdminRoleService() {
		return adminRoleService;
	}

	public void setAdminRoleService(AdminRoleService adminRoleService) {
		this.adminRoleService = adminRoleService;
	}

	public AdminRoleMenuService getAdminRoleMenuService() {
		return adminRoleMenuService;
	}

	public void setAdminRoleMenuService(
			AdminRoleMenuService adminRoleMenuService) {
		this.adminRoleMenuService = adminRoleMenuService;
	}

	public AdminMenuService getAdminMenuService() {
		return adminMenuService;
	}

	public void setAdminMenuService(AdminMenuService adminMenuService) {
		this.adminMenuService = adminMenuService;
	}

	public AdminRole getRole() {
		return role;
	}

	public void setRole(AdminRole role) {
		this.role = role;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public File getFileTest() {
		return fileTest;
	}

	public void setFileTest(File fileTest) {
		this.fileTest = fileTest;
	}

	public String getFileTestFileName() {
		return fileTestFileName;
	}

	public void setFileTestFileName(String fileTestFileName) {
		this.fileTestFileName = fileTestFileName;
	}

	public File getFileTest1() {
		return fileTest1;
	}

	public void setFileTest1(File fileTest1) {
		this.fileTest1 = fileTest1;
	}

	public String getFileTest1FileName() {
		return fileTest1FileName;
	}

	public void setFileTest1FileName(String fileTest1FileName) {
		this.fileTest1FileName = fileTest1FileName;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}
	public AgentService getAgentService() {
		return agentService;
	}
	public void setAgentService(AgentService agentService) {
		this.agentService = agentService;
	}

}
