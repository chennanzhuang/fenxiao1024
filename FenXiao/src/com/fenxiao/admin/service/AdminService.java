package com.fenxiao.admin.service;

import com.fenxiao.admin.entity.Admin;
import com.fenxiao.common.Pager;
import com.fenxiao.user.entity.User;

public interface AdminService {
	/**
	 * 根据id
	 */
	public Admin queryAdminid(Long id);
	//验证手机号码
	public Admin checkMobile(String mobile);
	//修改密码
	public int querypass(String adminAccount,Long id);
	  /**
	    * ����½
	    */
	public Admin checkAdminLogin(String adminAccount,String adminPass);
	
	 /**
	    * ��ݵ�½�˺Ų�ѯadmin
	    */
	public Admin queryAdmin(String adminAccount);
	/**
	 * �����˺�
	 */
	public int updateAdmin(Admin admin);
	/**
	 * ��ҳ��ѯ
	 */
public Pager queryAdmin(int pageSize,int page,String search);
/**
* ��ӹ���Ա 
*/
public int addAdmin(Admin admin);
/**
* ��ѯ����Ա
*/
public Admin findAdmin(Admin admin);
/**
* ����ɾ��
*/
public int deleteAdmin(String numbers);
/**
* ���ID ɾ��
*/
public int deleteAdminById(long adminId); 
}
