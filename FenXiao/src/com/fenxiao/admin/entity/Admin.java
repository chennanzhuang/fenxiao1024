package com.fenxiao.admin.entity;
// default package

import java.util.HashSet;
import java.util.Set;

import com.fenxiao.adminRole.entity.AdminRole;
/**
 * ����Ա
 * Admin entity. @author MyEclipse Persistence Tools
 */

public class Admin implements java.io.Serializable {

	// Fields

	private Long adminId;//����ԱID
	private AdminRole adminRole;//��ɫ
	private String adminName;//�û���
	private String adminPass;//����
	private String adminAccount;//�˺�
	private Double adminMoney;//���
	private Integer sex;//�Ա�
	private String adminOr;//��ά��
	private Double adminBalance;//���
	private String adminloginTime;//��½ʱ��
	private String adminPhoto;//ͷ��
	private String recommendationCode;//�Ƽ���
	private String adminPhone;//��ϵ�绰
	private String adminWeChatId;//΢�ź�
	private String adminPresentPass;//��������
	private String regTime;//ע��ʱ��
	private Set bankcards = new HashSet(0);//���п�
	private Set incomeRecodes = new HashSet(0);//�����¼
	private Set presentRecords = new HashSet(0);//���ּ�¼
/*=======
	private Long adminId;//����ԱID
	private AdminRole adminRole;//��ɫ
	private String adminName;//�û���
	private String adminPass;//����
	private String adminAccount;//�˺�
	private Double adminMoney;//���
	private Integer sex;//�Ա�
	private String adminOr;//��ά��
	private Double adminBalance;//���
	private String adminloginTime;//��½ʱ��
	private String adminPhoto;//ͷ��
	private String recommendationCode;//�Ƽ���
	private String adminPhone;//��ϵ�绰
	private String adminWeChatId;//΢�ź�
	private String adminPresentPass;//��������
	private String regTime;//ע��ʱ��
	private Set bankcards = new HashSet(0);//���п�
	private Set incomeRecodes = new HashSet(0);//�����¼
	private Set presentRecords = new HashSet(0);//���ּ�¼
>>>>>>> .r903*/

	// Constructors

	/** default constructor */
	public Admin() {
	}

	/** minimal constructor */
	public Admin(AdminRole adminRole, String adminAccount) {
		this.adminRole = adminRole;
		this.adminAccount = adminAccount;
	}

	/** full constructor */
	public Admin(AdminRole adminRole, String adminName, String adminPass,
			String adminAccount, Double adminMoney, Integer sex,
			String adminOr, Double adminBalance, String adminloginTime,
			String adminPhoto, String recommendationCode, String adminPhone,
			String adminWeChatId, String adminPresentPass,String regTime,Set bankcards,
			Set incomeRecodes, Set presentRecords) {
		this.adminRole = adminRole;
		this.adminName = adminName;
		this.adminPass = adminPass;
		this.adminAccount = adminAccount;
		this.adminMoney = adminMoney;
		this.sex = sex;
		this.adminOr = adminOr;
		this.adminBalance = adminBalance;
		this.adminloginTime = adminloginTime;
		this.adminPhoto = adminPhoto;
		this.recommendationCode = recommendationCode;
		this.adminPhone = adminPhone;
		this.adminWeChatId = adminWeChatId;
		this.adminPresentPass = adminPresentPass;
		this.regTime=regTime;
		this.bankcards = bankcards;
		this.incomeRecodes = incomeRecodes;
		this.presentRecords = presentRecords;
	}

	// Property accessors

	public Long getAdminId() {
		return this.adminId;
	}

	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}

	public AdminRole getAdminRole() {
		return this.adminRole;
	}

	public void setAdminRole(AdminRole adminRole) {
		this.adminRole = adminRole;
	}

	public String getAdminName() {
		return this.adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public String getAdminPass() {
		return this.adminPass;
	}

	public void setAdminPass(String adminPass) {
		this.adminPass = adminPass;
	}

	public String getAdminAccount() {
		return this.adminAccount;
	}

	public void setAdminAccount(String adminAccount) {
		this.adminAccount = adminAccount;
	}

	public Double getAdminMoney() {
		return this.adminMoney;
	}

	public void setAdminMoney(Double adminMoney) {
		this.adminMoney = adminMoney;
	}

	public Integer getSex() {
		return this.sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getAdminOr() {
		return this.adminOr;
	}

	public void setAdminOr(String adminOr) {
		this.adminOr = adminOr;
	}

	public Double getAdminBalance() {
		return this.adminBalance;
	}

	public void setAdminBalance(Double adminBalance) {
		this.adminBalance = adminBalance;
	}

	public String getAdminloginTime() {
		return this.adminloginTime;
	}

	public void setAdminloginTime(String adminloginTime) {
		this.adminloginTime = adminloginTime;
	}

	public String getAdminPhoto() {
		return this.adminPhoto;
	}

	public void setAdminPhoto(String adminPhoto) {
		this.adminPhoto = adminPhoto;
	}

	public String getRecommendationCode() {
		return this.recommendationCode;
	}

	public void setRecommendationCode(String recommendationCode) {
		this.recommendationCode = recommendationCode;
	}

	public String getAdminPhone() {
		return this.adminPhone;
	}

	public void setAdminPhone(String adminPhone) {
		this.adminPhone = adminPhone;
	}

	public String getAdminWeChatId() {
		return this.adminWeChatId;
	}

	public void setAdminWeChatId(String adminWeChatId) {
		this.adminWeChatId = adminWeChatId;
	}

	public String getAdminPresentPass() {
		return this.adminPresentPass;
	}

	public void setAdminPresentPass(String adminPresentPass) {
		this.adminPresentPass = adminPresentPass;
	}

	public Set getBankcards() {
		return this.bankcards;
	}

	public void setBankcards(Set bankcards) {
		this.bankcards = bankcards;
	}

	public Set getIncomeRecodes() {
		return this.incomeRecodes;
	}

	public void setIncomeRecodes(Set incomeRecodes) {
		this.incomeRecodes = incomeRecodes;
	}

	public Set getPresentRecords() {
		return this.presentRecords;
	}

	public void setPresentRecords(Set presentRecords) {
		this.presentRecords = presentRecords;
	}

	public String getRegTime() {
		return regTime;
	}

	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}


}