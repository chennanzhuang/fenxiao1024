package com.fenxiao.json.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import com.fenxiao.agent.entity.Agent;
import com.fenxiao.agent.service.AgentService;
import com.fenxiao.common.Pager;
import com.fenxiao.mytask.entity.UserTask;
import com.fenxiao.mytask.service.UserTaskService;
import com.fenxiao.present.entity.PresentRecord;
import com.fenxiao.present.service.PresentService;
import com.fenxiao.task.entity.Task;
import com.fenxiao.task.service.TaskService;
import com.fenxiao.user.entity.User;
import com.fenxiao.user.service.UserService;

public class JsonAction {
	private HttpServletRequest request;
	private HttpServletResponse response;
	private HttpSession session;
	private TaskService taskService;
	private String result;
	private String search;
	private UserService userService;
	private UserTaskService userTaskService;
	private PresentService presentService;
	private AgentService agentService;

	public AgentService getAgentService() {
		return agentService;
	}

	public void setAgentService(AgentService agentService) {
		this.agentService = agentService;
	}

	public PresentService getPresentService() {
		return presentService;
	}

	public void setPresentService(PresentService presentService) {
		this.presentService = presentService;
	}

	public UserTaskService getUserTaskService() {
		return userTaskService;
	}

	public void setUserTaskService(UserTaskService userTaskService) {
		this.userTaskService = userTaskService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	private int page;

	// 查询代理商Id查询员工
	public String queryAllyuan() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		String dailiId1 = request.getParameter("dailiId");
		int dailiId = Integer.parseInt(dailiId1);
		
		if (dailiId!=-1) {
			 List<User> usergent = userService.queryUseragent(dailiId);
			
			JSONArray jArray = new JSONArray();
			
			
			if (usergent != null && usergent.size() > 0) {
				try {
					for (int i = 0; i < usergent.size(); i++) {
//						System.err.println("task+----------"
//								+ pageli3.get(i).getTaskId());
						
//						Object[] ob=(Object[]) pageli3.get(i);
//						Task task =(Task) ob[0];
						JSONObject object = new JSONObject();
						
						object.put("UserId", usergent.get(i).getId());
						object.put("UserName", usergent.get(i).getUsername());
						jArray.add(object);
						
					}
					result = jArray.toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		} else {
			result = null;
		}
		// 总页数
		// int Total = pagelist.getTotalPage();
		return "SUCCESS";
		
	}
	// 查询合伙人Id查询代理商
	public String queryAlldaili() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		String agentId1 = request.getParameter("agentId");
		int agentId = Integer.parseInt(agentId1);

		if (agentId!=-1) {
			    List<Agent> agent = agentService.queryallAgent(agentId);
			
			JSONArray jArray = new JSONArray();
			
			
			if (agent != null && agent.size() > 0) {
				System.err.println("fsfsfdsfdsfsdfsd");
				try {
					for (int i = 0; i < agent.size(); i++) {
//						System.err.println("task+----------"
//								+ pageli3.get(i).getTaskId());
						
//						Object[] ob=(Object[]) pageli3.get(i);
//						Task task =(Task) ob[0];
						JSONObject object = new JSONObject();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						
						
						object.put("agentId", agent.get(i).getAgentId());
						object.put("agentName", agent.get(i).getAgentName());
						jArray.add(object);
						
					}
					result = jArray.toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		} else {
			result = null;
		}
		// 总页数
		// int Total = pagelist.getTotalPage();
		return "SUCCESS";
		
	}
	// 分页查询
	public String queryTaskAll() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		String Total = request.getParameter("Total");
		String opneId = request.getParameter("openId");
		int Total1 = Integer.parseInt(Total);
		page = Integer.parseInt(request.getParameter("page"));
		if (Total1 >= page) {
			  User user = userService.queryMoneyOfUser(opneId);
			Pager pagelist = taskService.queryTaskwc(user,20, page, search);
			JSONArray jArray = new JSONArray();
			List<Task> task =pagelist.getList();
			
			
			if (task != null && task.size() > 0) {
				System.err.println("fsfsfdsfdsfsdfsd");
				try {
					for (int i = 0; i < task.size(); i++) {
//						System.err.println("task+----------"
//								+ pageli3.get(i).getTaskId());
						
//						Object[] ob=(Object[]) pageli3.get(i);
//						Task task =(Task) ob[0];
						JSONObject object = new JSONObject();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						
							
							object.put("id",task.get(i).getTaskId());
							object.put("EndTime", task.get(i).getTaskEndTime());
							object.put("Remainder", task.get(i)
									.getTaskRemainderNumber());
							object.put("Logo", task.get(i).getTaskLogo());
							object.put("Title", task.get(i).getTaskTitle());
							object.put("Bonus", task.get(i).getTaskBonus());
							object.put("Briefing", task.get(i).getTaskBriefing());
							object.put("Type", task.get(i).getTaskType());
							object.put("Code", task.get(i).getTaskCode());
							object.put("City", task.get(i).getCity());
							object.put("City", task.get(i).getCity());
							object.put("Area", task.get(i).getArea());
							jArray.add(object);
					
					}
					result = jArray.toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}

		} else {
			result = null;
		}
		// 总页数
		// int Total = pagelist.getTotalPage();
		return "SUCCESS";

	}
	// 分页点亮查询查询
	public String queryTaskAlldl() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		String Total = request.getParameter("Total");
		String opneId = request.getParameter("openId");
		int Total1 = Integer.parseInt(Total);
		page = Integer.parseInt(request.getParameter("page"));
		if (Total1 >= page) {
			User user = userService.queryMoneyOfUser(opneId);
			Pager pagelist = taskService.queryTaskdl(user,20, page, search);
			JSONArray jArray = new JSONArray();
			List<Task> task =pagelist.getList();
			
			
			if (task != null && task.size() > 0) {
				try {
					for (int i = 0; i < task.size(); i++) {
//						System.err.println("task+----------"
//								+ pageli3.get(i).getTaskId());
						
//						Object[] ob=(Object[]) pageli3.get(i);
//						Task task =(Task) ob[0];
						JSONObject object = new JSONObject();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						
						
						object.put("id",task.get(i).getTaskId());
						object.put("EndTime", task.get(i).getTaskEndTime());
						object.put("Remainder", task.get(i)
								.getTaskRemainderNumber());
						object.put("Logo", task.get(i).getTaskLogo());
						object.put("Title", task.get(i).getTaskTitle());
						object.put("Bonus", task.get(i).getTaskBonus());
						object.put("Briefing", task.get(i).getTaskBriefing());
						object.put("Type", task.get(i).getTaskType());
						object.put("Code", task.get(i).getTaskCode());
						object.put("City", task.get(i).getCity());
						object.put("City", task.get(i).getCity());
						object.put("Area", task.get(i).getArea());
						jArray.add(object);	
					}
					result = jArray.toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		} else {
			result = null;
		}
		// 总页数
		// int Total = pagelist.getTotalPage();
		return "SUCCESS";
		
	}
	// 分页查询任务
	public String queryTask() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		String Total = request.getParameter("Total");
		String opneId = request.getParameter("openId");
		int Total1 = Integer.parseInt(Total);
		String bounts1=request.getParameter("bounts");
		String type=request.getParameter("taskType");
		String area1=request.getParameter("area");
		page = Integer.parseInt(request.getParameter("page"));
		int taskType=0;
		int bounts=0;
	   String area=null;
		if(type!=null&&!"".equals(type)){
	    	taskType = Integer.parseInt(type);
	    	request.setAttribute("taskType", taskType);
	    }
		if(area1!=null&&!"".equals(area1)){	
	     area=area1;
	    	request.setAttribute("area", area);
	    }
		if(bounts1!=null&&!"".equals(bounts1)){
	    	bounts=Integer.parseInt(bounts1);
	    	request.setAttribute("bounts", bounts);
	    }
		if (Total1 >= page) {
			User user = userService.queryMoneyOfUser(opneId);
			Pager pagelist = taskService.queryTaskStar(2, page, search, taskType, bounts, area, user);
			JSONArray jArray = new JSONArray();
			List<Task> task =pagelist.getList();
			if (task != null && task.size() > 0) {
				try {
					for (int i = 0; i < task.size(); i++) {
//						System.err.println("task+----------"
//								+ pageli3.get(i).getTaskId());
						
//						Object[] ob=(Object[]) pageli3.get(i);
//						Task task =(Task) ob[0];
						JSONObject object = new JSONObject();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");		
						object.put("id",task.get(i).getTaskId());
						object.put("EndTime", task.get(i).getTaskEndTime());
						object.put("Remainder", task.get(i)
								.getTaskRemainderNumber());
						object.put("Logo", task.get(i).getTaskLogo());
						object.put("Title", task.get(i).getTaskTitle());
						object.put("Bonus", task.get(i).getTaskBonus());
						object.put("Briefing", task.get(i).getTaskBriefing());
						object.put("Type", task.get(i).getTaskType());
						object.put("Code", task.get(i).getTaskCode());
						object.put("City", task.get(i).getCity());
						object.put("City", task.get(i).getCity());
						object.put("Area", task.get(i).getArea());
						jArray.add(object);
					}
					result = jArray.toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		} else {
			result = null;
		}
		// 总页数
		// int Total = pagelist.getTotalPage();
		return "SUCCESS";
		
	}
	// 分页查询我的需求
	public String queryTaskxq() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		String Total = request.getParameter("Total");
		String opneId = request.getParameter("openId");
		int Total1 = Integer.parseInt(Total);
		page = Integer.parseInt(request.getParameter("page"));
		if (Total1 >= page) {
			User user = userService.queryMoneyOfUser(opneId);
			Pager pagelist = taskService.queryTaskxq(10, page, user.getId());
			JSONArray jArray = new JSONArray();
			List<Task> task =pagelist.getList();
			if (task != null && task.size() > 0) {
				try {
					for (int i = 0; i < task.size(); i++) {
//						System.err.println("task+----------"
//								+ pageli3.get(i).getTaskId());
						
//						Object[] ob=(Object[]) pageli3.get(i);
//						Task task =(Task) ob[0];
						JSONObject object = new JSONObject();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						
						
						object.put("id",task.get(i).getTaskId());
						object.put("userid",task.get(i).getUser().getId());
						object.put("PubTime",task.get(i).getPubTime());
						object.put("TotalNumber",task.get(i).getTaskTotalNumber());
						object.put("Status",task.get(i).getTaskStatus());
						object.put("TotalBonus",task.get(i).getTaskTotalBonus());
						object.put("SBonus",task.get(i).getTaskSBonus());
						object.put("heji",task.get(i).getHeji());
						object.put("Shouxu",task.get(i).getShouxu());
						object.put("userid",task.get(i).getUser().getId());
						object.put("EndTime", task.get(i).getTaskEndTime());
						object.put("Remainder", task.get(i)
								.getTaskRemainderNumber());
						object.put("Logo", task.get(i).getTaskLogo());
						object.put("Title", task.get(i).getTaskTitle());
						object.put("Bonus", task.get(i).getTaskBonus());
						object.put("Briefing", task.get(i).getTaskBriefing());
						object.put("Type", task.get(i).getTaskType());
						object.put("Code", task.get(i).getTaskCode());
						object.put("City", task.get(i).getCity());
						object.put("Area", task.get(i).getArea());
						jArray.add(object);
						
					}
					result = jArray.toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		} else {
			result = null;
		}
		// 总页数
		// int Total = pagelist.getTotalPage();
		return "SUCCESS";
		
	}
	// 分页查询自己的任务
	public String queryMyTask() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		String Total = request.getParameter("Total");
		String opneId = request.getParameter("openId");
		int Total1 = Integer.parseInt(Total);
		String taskStatus1=request.getParameter("taskStatus");
		String taskPrice1=request.getParameter("taskPrice");
		String taskType1=request.getParameter("taskType");
		page = Integer.parseInt(request.getParameter("page"));
		int taskStatus=0;
		int taskPrice=0;
		int taskType=0;
		if(taskStatus1!=null&&!"".equals(taskStatus1)){
			taskStatus = Integer.parseInt(taskStatus1);
//			request.setAttribute("taskStatus", taskStatus);
		}
		if(taskPrice1!=null&&!"".equals(taskPrice1)){	
			taskPrice=Integer.parseInt(taskPrice1);
//			request.setAttribute("taskPrice", taskPrice);
		}
		if(taskType1!=null&&!"".equals(taskType1)){
			taskType=Integer.parseInt(taskType1);
//			request.setAttribute("taskType", taskType);
		}
		if (Total1 >= page) {
			User user = userService.queryMoneyOfUser(opneId);
			    Long userId = user.getId();
			Pager pagelist = userTaskService.queryMyTaskfy(10, page, userId, taskStatus, taskType, taskPrice);
			JSONArray jArray = new JSONArray();
			List<UserTask> Usertask =pagelist.getList();
			if (Usertask != null && Usertask.size() > 0) {
				try {
					for (int i = 0; i < Usertask.size(); i++) {
//						System.err.println("task+----------"
//								+ pageli3.get(i).getTaskId());
						
//						Object[] ob=(Object[]) pageli3.get(i);
//						Task task =(Task) ob[0];
						JSONObject object = new JSONObject();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						
						
						object.put("id",Usertask.get(i).getUserTaskId());
						object.put("UserId", Usertask.get(i)
								.getUserTaskId());
						object.put("Logo", Usertask.get(i).getTaskLogo());
						object.put("Title", Usertask.get(i).getTaskTitle());
						object.put("Bonus", Usertask.get(i).getTaskBonus());
						object.put("Briefing", Usertask.get(i).getTaskBriefing());
						object.put("Type", Usertask.get(i).getTaskType());
						object.put("CompTime", Usertask.get(i).getTaskCompletionTime());
						object.put("Status", Usertask.get(i).getTaskStatus());
						jArray.add(object);
						
					}
					result = jArray.toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		} else {
			result = null;
		}
		// 总页数
		// int Total = pagelist.getTotalPage();
		return "SUCCESS";
		
	}
	// 分页推广佣金
	public String queryMyTasktuiguang() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		String Total = request.getParameter("Total");
		String opneId = request.getParameter("openId");
		int Total1 = Integer.parseInt(Total);
		page = Integer.parseInt(request.getParameter("page"));
		if (Total1 >= page) {
			User user = userService.queryMoneyOfUser(opneId);
			Long userId = user.getId();
			Pager pagelist = userTaskService.queryTaskPagetuiguan(10, page, user);
			JSONArray jArray = new JSONArray();
			List<UserTask> Usertask =pagelist.getList();
			if (Usertask != null && Usertask.size() > 0) {
				try {
					for (int i = 0; i < Usertask.size(); i++) {
						JSONObject object = new JSONObject();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				
						object.put("Title", Usertask.get(i).getTaskTitle());
						object.put("OrderTime", Usertask.get(i).getTaskOrderTime());
						object.put("nickName", Usertask.get(i).getUser().getNickName());
						object.put("Type", Usertask.get(i).getTuiguang());
						jArray.add(object);
						
					}
					result = jArray.toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		} else {
			result = null;
		}
		// 总页数
		// int Total = pagelist.getTotalPage();
		return "SUCCESS";
		
	}
	// 分页团队佣金
	public String queryMyTasktuandui() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		String Total = request.getParameter("Total");
		String opneId = request.getParameter("openId");
		int Total1 = Integer.parseInt(Total);
		page = Integer.parseInt(request.getParameter("page"));
		if (Total1 >= page) {
			User user = userService.queryMoneyOfUser(opneId);
			Long userId = user.getId();
			Pager pagelist = userTaskService.queryTaskPagetuangdui(10, page, user);
			JSONArray jArray = new JSONArray();
			List<UserTask> Usertask =pagelist.getList();
			if (Usertask != null && Usertask.size() > 0) {
				try {
					for (int i = 0; i < Usertask.size(); i++) {
						JSONObject object = new JSONObject();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						
						object.put("Title", Usertask.get(i).getTaskTitle());
						object.put("OrderTime", Usertask.get(i).getTaskOrderTime());
						object.put("nickName", Usertask.get(i).getUser().getNickName());
						object.put("Yongjing", Usertask.get(i).getYongjing());
						jArray.add(object);
						
					}
					result = jArray.toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		} else {
			result = null;
		}
		// 总页数
		// int Total = pagelist.getTotalPage();
		return "SUCCESS";
		
	}
	// 分页任务佣金
	public String queryMyTaskrenwu() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		String Total = request.getParameter("Total");
		String opneId = request.getParameter("openId");
		int Total1 = Integer.parseInt(Total);
		page = Integer.parseInt(request.getParameter("page"));
		if (Total1 >= page) {
			User user = userService.queryMoneyOfUser(opneId);
			Long userId = user.getId();
			Pager pagelist = userTaskService.queryTaskMyyj(10, page, userId);
			JSONArray jArray = new JSONArray();
			List<UserTask> Usertask =pagelist.getList();
			if (Usertask != null && Usertask.size() > 0) {
				try {
					for (int i = 0; i < Usertask.size(); i++) {
						JSONObject object = new JSONObject();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						
						object.put("Title", Usertask.get(i).getTaskTitle());
						object.put("Type", Usertask.get(i).getTaskType());
						object.put("CompletionTime", Usertask.get(i).getTaskCompletionTime());
						object.put("Bonus", Usertask.get(i).getTaskBonus());
						jArray.add(object);
						
					}
					result = jArray.toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		} else {
			result = null;
		}
		// 总页数
		// int Total = pagelist.getTotalPage();
		return "SUCCESS";
		
	}
	// 分页查询直属团队
	public String queryUserZhishu() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		String Total = request.getParameter("Total");
		String opneId = request.getParameter("openId");
		search = request.getParameter("search");
		User user = userService.queryMoneyOfUser(opneId);
		int Total1 = Integer.parseInt(Total);
		page = Integer.parseInt(request.getParameter("page"));
		if (Total1 >= page) {
			Pager pager = userService.queryZhishu(10, page, search, user);
			JSONArray jArray = new JSONArray();
		 List<User> userzhshu = pager.getList();
			if (userzhshu != null && userzhshu.size() > 0) {
				try {
					for (int i = 0; i < userzhshu.size(); i++) {
						JSONObject object = new JSONObject();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						
						object.put("Photo", userzhshu.get(i).getUserPhoto());
						object.put("NickName", userzhshu.get(i).getNickName());
						object.put("RegTime", userzhshu.get(i).getRegTime());
						object.put("Level", userzhshu.get(i).getUserLevel());
						jArray.add(object);
						
					}
					result = jArray.toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		} else {
			result = null;
		}
		// 总页数
		// int Total = pagelist.getTotalPage();
		return "SUCCESS";
		
	}
	// 分页查询全部成员
	public String queryUserquanbu() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		String Total = request.getParameter("Total");
		String opneId = request.getParameter("openId");
		search = request.getParameter("search");
		User user = userService.queryMoneyOfUser(opneId);
		int Total1 = Integer.parseInt(Total);
		page = Integer.parseInt(request.getParameter("page"));
		if (Total1 >= page) {
			Pager pager = userService.queryQuanbu(10, page, search, user);
			JSONArray jArray = new JSONArray();
			List<User> userquanbu= pager.getList();
			if (userquanbu != null && userquanbu.size() > 0) {
				try {
					for (int i = 0; i < userquanbu.size(); i++) {
//						System.err.println("task+----------"
//								+ pageli3.get(i).getTaskId());
						
//						Object[] ob=(Object[]) pageli3.get(i);
//						Task task =(Task) ob[0];
						JSONObject object = new JSONObject();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						
						object.put("Photo", userquanbu.get(i).getUserPhoto());
						object.put("NickName", userquanbu.get(i).getNickName());
						object.put("RegTime", userquanbu.get(i).getRegTime());
						object.put("Level", userquanbu.get(i).getUserLevel());
						jArray.add(object);
						
					}
					result = jArray.toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		} else {
			result = null;
		}
		// 总页数
		// int Total = pagelist.getTotalPage();
		return "SUCCESS";
		
	}
	// 分页提现账单
	public String queryUsertixian() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		String Total = request.getParameter("Total");
		String opneId = request.getParameter("openId");
		search = request.getParameter("search");
		User user = userService.queryMoneyOfUser(opneId);
		int Total1 = Integer.parseInt(Total);
		page = Integer.parseInt(request.getParameter("page"));
		if (Total1 >= page) {
			  Pager pager = presentService.queryPagetixian(10, page, user.getId());
			JSONArray jArray = new JSONArray();
			List<PresentRecord> tixian= pager.getList();
			if (tixian != null && tixian.size() > 0) {
				try {
					for (int i = 0; i < tixian.size(); i++) {
//						System.err.println("task+----------"
//								+ pageli3.get(i).getTaskId());
						
//						Object[] ob=(Object[]) pageli3.get(i);
//						Task task =(Task) ob[0];
						JSONObject object = new JSONObject();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						
						object.put("PresentTime", tixian.get(i).getPresentTime());
						object.put("Fee", tixian.get(i).getPresentFee());
						object.put("Sum", tixian.get(i).getRemainingSum());
						object.put("Status", tixian.get(i).getPresentStatus());
						object.put("Money", tixian.get(i).getPresentMoney());
						jArray.add(object);
						
					}
					result = jArray.toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		} else {
			result = null;
		}
		// 总页数
		// int Total = pagelist.getTotalPage();
		return "SUCCESS";
		
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

}
