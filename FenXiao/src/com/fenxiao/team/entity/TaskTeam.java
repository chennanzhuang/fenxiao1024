package com.fenxiao.team.entity;

import com.fenxiao.user.entity.User;
// default package

/**
 * 团队任务
 * TaskTeam entity. @author MyEclipse Persistence Tools
 */

public class TaskTeam implements java.io.Serializable {

	// Fields

	private Long taskTeamId;
	private UserTeam userTeam;//团队
	private User user;//会员
	private String taskTitle;//标题
	private String taskLogo;//logo
	private String taskBriefing;//简介
	private Double taskBonus;//奖金
	private Integer taskStatus;//状态
	private String taskOrderCode;//订单号
	private String taskOrderTime;//下单时间
	private String taskCompletionTime;//完成时间
	private Integer taskType;//类型
	private String taskName;//app名

	// Constructors

	/** default constructor */
	public TaskTeam() {
	}

	/** full constructor */
	public TaskTeam(UserTeam userTeam, User user, String taskTitle,
			String taskLogo, String taskBriefing, Double taskBonus,
			Integer taskStatus, String taskOrderCode, String taskOrderTime,
			String taskCompletionTime, Integer taskType, String taskName) {
		this.userTeam = userTeam;
		this.user = user;
		this.taskTitle = taskTitle;
		this.taskLogo = taskLogo;
		this.taskBriefing = taskBriefing;
		this.taskBonus = taskBonus;
		this.taskStatus = taskStatus;
		this.taskOrderCode = taskOrderCode;
		this.taskOrderTime = taskOrderTime;
		this.taskCompletionTime = taskCompletionTime;
		this.taskType = taskType;
		this.taskName = taskName;
	}

	// Property accessors

	public Long getTaskTeamId() {
		return this.taskTeamId;
	}

	public void setTaskTeamId(Long taskTeamId) {
		this.taskTeamId = taskTeamId;
	}

	public UserTeam getUserTeam() {
		return this.userTeam;
	}

	public void setUserTeam(UserTeam userTeam) {
		this.userTeam = userTeam;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTaskTitle() {
		return this.taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public String getTaskLogo() {
		return this.taskLogo;
	}

	public void setTaskLogo(String taskLogo) {
		this.taskLogo = taskLogo;
	}

	public String getTaskBriefing() {
		return this.taskBriefing;
	}

	public void setTaskBriefing(String taskBriefing) {
		this.taskBriefing = taskBriefing;
	}

	public Double getTaskBonus() {
		return this.taskBonus;
	}

	public void setTaskBonus(Double taskBonus) {
		this.taskBonus = taskBonus;
	}

	public Integer getTaskStatus() {
		return this.taskStatus;
	}

	public void setTaskStatus(Integer taskStatus) {
		this.taskStatus = taskStatus;
	}

	public String getTaskOrderCode() {
		return this.taskOrderCode;
	}

	public void setTaskOrderCode(String taskOrderCode) {
		this.taskOrderCode = taskOrderCode;
	}

	public String getTaskOrderTime() {
		return this.taskOrderTime;
	}

	public void setTaskOrderTime(String taskOrderTime) {
		this.taskOrderTime = taskOrderTime;
	}

	public String getTaskCompletionTime() {
		return this.taskCompletionTime;
	}

	public void setTaskCompletionTime(String taskCompletionTime) {
		this.taskCompletionTime = taskCompletionTime;
	}

	public Integer getTaskType() {
		return this.taskType;
	}

	public void setTaskType(Integer taskType) {
		this.taskType = taskType;
	}

	public String getTaskName() {
		return this.taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

}