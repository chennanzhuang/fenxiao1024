package com.fenxiao.team.entity;
// default package

import java.util.HashSet;
import java.util.Set;

import com.fenxiao.user.entity.User;
/**
 * 我的团队
 * UserTeam entity. @author MyEclipse Persistence Tools
 */

public class UserTeam implements java.io.Serializable {

	// Fields

	private Long teamId;
	private User user;
	private Long usercode;//编码
	private String nickName;//昵称
	private String username;//名称
	private String userPhoto;//图像
	private String mobile;//手机
	private Integer userLevel;//等级（0，普通会员 1、VIP 会员 2、员工）
	private Integer userStatus;//状态（0、关闭 1、在线）
	private String userQr;//二维码
	private Integer taskNum;//完成数
	private Integer sex;//性别
	private String dress;//地址
	private Double balance;//余额
	private Double taskCommission;//任务佣金
	private Double promotionCommission;//推广佣金
	private Double teamCommissions;//团队佣金
	private String recommendationCode;//推荐码
	private Double totalMoney;//总金额
	private Long agentId;//
	private String regTime;//注册时间
	private String recomCode;//推荐人的推荐码
	private Integer userUnder;//直属

	private Set taskTeams = new HashSet(0);//

	// Constructors

	/** default constructor */
	public UserTeam() {
	}

	/** full constructor */
	public UserTeam(User user, Long usercode, String nickName, String username,
			String userPhoto, String mobile, Integer userLevel,
			Integer userStatus, String userQr, Integer taskNum, Integer sex,
			String dress, Double balance, Double taskCommission,
			Double promotionCommission, Double teamCommissions,
			String recommendationCode, Double totalMoney, Long agentId,
			String regTime, String recomCode, Integer userUnder, Set taskTeams) {
		this.user = user;
		this.usercode = usercode;
		this.nickName = nickName;
		this.username = username;
		this.userPhoto = userPhoto;
		this.mobile = mobile;
		this.userLevel = userLevel;
		this.userStatus = userStatus;
		this.userQr = userQr;
		this.taskNum = taskNum;
		this.sex = sex;
		this.dress = dress;
		this.balance = balance;
		this.taskCommission = taskCommission;
		this.promotionCommission = promotionCommission;
		this.teamCommissions = teamCommissions;
		this.recommendationCode = recommendationCode;
		this.totalMoney = totalMoney;
		this.agentId = agentId;
		this.regTime = regTime;
		this.recomCode = recomCode;
		this.userUnder = userUnder;
		this.taskTeams = taskTeams;
	}

	// Property accessors

	public Long getTeamId() {
		return this.teamId;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getUsercode() {
		return this.usercode;
	}

	public void setUsercode(Long usercode) {
		this.usercode = usercode;
	}

	public String getNickName() {
		return this.nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserPhoto() {
		return this.userPhoto;
	}

	public void setUserPhoto(String userPhoto) {
		this.userPhoto = userPhoto;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getUserLevel() {
		return this.userLevel;
	}

	public void setUserLevel(Integer userLevel) {
		this.userLevel = userLevel;
	}

	public Integer getUserStatus() {
		return this.userStatus;
	}

	public void setUserStatus(Integer userStatus) {
		this.userStatus = userStatus;
	}

	public String getUserQr() {
		return this.userQr;
	}

	public void setUserQr(String userQr) {
		this.userQr = userQr;
	}

	public Integer getTaskNum() {
		return this.taskNum;
	}

	public void setTaskNum(Integer taskNum) {
		this.taskNum = taskNum;
	}

	public Integer getSex() {
		return this.sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getDress() {
		return this.dress;
	}

	public void setDress(String dress) {
		this.dress = dress;
	}

	public Double getBalance() {
		return this.balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Double getTaskCommission() {
		return this.taskCommission;
	}

	public void setTaskCommission(Double taskCommission) {
		this.taskCommission = taskCommission;
	}

	public Double getPromotionCommission() {
		return this.promotionCommission;
	}

	public void setPromotionCommission(Double promotionCommission) {
		this.promotionCommission = promotionCommission;
	}

	public Double getTeamCommissions() {
		return this.teamCommissions;
	}

	public void setTeamCommissions(Double teamCommissions) {
		this.teamCommissions = teamCommissions;
	}

	public String getRecommendationCode() {
		return this.recommendationCode;
	}

	public void setRecommendationCode(String recommendationCode) {
		this.recommendationCode = recommendationCode;
	}

	public Double getTotalMoney() {
		return this.totalMoney;
	}

	public void setTotalMoney(Double totalMoney) {
		this.totalMoney = totalMoney;
	}

	public Long getAgentId() {
		return this.agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public String getRegTime() {
		return this.regTime;
	}

	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}

	public String getRecomCode() {
		return this.recomCode;
	}

	public void setRecomCode(String recomCode) {
		this.recomCode = recomCode;
	}

	public Integer getUserUnder() {
		return this.userUnder;
	}

	public void setUserUnder(Integer userUnder) {
		this.userUnder = userUnder;
	}

	public Set getTaskTeams() {
		return this.taskTeams;
	}

	public void setTaskTeams(Set taskTeams) {
		this.taskTeams = taskTeams;
	}

}