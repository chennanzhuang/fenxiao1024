package com.fenxiao.agent.dao;

import java.util.List;

import com.fenxiao.agent.entity.Agent;
import com.fenxiao.common.Pager;
import com.fenxiao.user.entity.User;
/**
* dao层
* @author Administrator
*
*/
public interface AgentDao {
	/**
	 * 根据用户名查询代理商或合伙人
	 */
	public Agent queryAgent(String agentAccount)throws Exception;
	 /**
	  * 分页查询代理商佣金
	  */
	public Pager queryPage(int pageSize,int page,String search,long agentId);
	
	/**
	  * 分页查询代理商
	  */
	public Pager findPage(int pageSize,int page,String search,long agentId);
	 /**
	    * 查询所有记录数
	    * @param hql 查询的条件
	    * @return 总记录数
	    */
	public int getAllRowCount(String hql);
		/**
		    * 分页查询
		    * @param hql 查询的条件
		    * @param offset 开始记录
		    * @param length 一次查询几条记录
		    * @return
		    */
    public List  queryForPage(String hql, int offset, int length);
    /**
     * 查询所有记录数(原生态sql)
     * @param hql 查询的条件
     * @return 总记录数
     */
 	public int getAllSQLRowCount(String hql);
 	/**
    * 分页查询
    * @param sql 查询的条件
    * @param offset 开始记录
    * @param length 一次查询几条记录
    * @return
    */
	public List  querySQLForPage(String hql, int offset, int length);  	
	/**
	 * 添加代理商
	 */
	public int addAgent(Agent agent);
	/**
	 * 修改代理商
	 */
	public int updateAgent(Agent agent);
	/**
	 * 查询代理商
	 */
	public Agent queryAgentInfo(long agentId);
	/**
	 * 查询多个代理三
	 */
	public List<Agent> queryallAgent(long agentId);
	/**
	  * 分页查询代理商
	  */
	public Pager findAgentPage(int pageSize,int page,String search);
	/**
	  * 分页查询代理商
	  */
	public Pager selectAgentPage(int pageSize,int page,String search);
	/**
	  * 分页查询合伙人
	  */
	public Pager findPartnerPage(int pageSize,int page,String search);
	/**
	  * 分页查询合伙人集合
	  */
	public List<Agent> findPartnerList();
	/**
	 * 批量删除
	 */
	public int deleteAllAgent(String number);
	/**
	 * 删除
	 */
	public int deleteAgent(long agentId); 
	/**
	 * 根据合伙人id查询
	 * */
	public Agent queryHehuo(Long agentId);
	//查询合伙人和代理商
	public Agent quertwo(Long pranId);

}
