package com.fenxiao.agent.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.fenxiao.agent.dao.AgentDao;
import com.fenxiao.agent.entity.Agent;
import com.fenxiao.common.PageBean;
import com.fenxiao.common.Pager;
import com.fenxiao.user.entity.User;

public class AgentDaoImpl implements AgentDao {
	
	private SessionFactory sessionFactory;
	
   public SessionFactory getSessionFactory() {
		return sessionFactory;
	}


   public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


 /**
    * ��ѯ���м�¼��
    * @param hql ��ѯ������
    * @return �ܼ�¼��
    */
	public int getAllRowCount(String hql) {
		Session session=sessionFactory.openSession();
		List list=null;
		try{
			Query query=session.createQuery(hql);
		
			list=query.list();
			
		}catch (RuntimeException e) {
		   throw e;
		}
		session.close();
		return list.size();
		
	}
	/**
    * ��ҳ��ѯ
    * @param hql ��ѯ������
    * @param offset ��ʼ��¼
    * @param length һ�β�ѯ������¼
    * @return
    */
	public List  queryForPage(String hql, int offset, int length) {
		Session session=sessionFactory.openSession();
		List  list=null;
		try{
			Query query=session.createQuery(hql);
			query.setFirstResult(offset);
           query.setMaxResults(length);
           list=query.list();
			
		}catch (RuntimeException e) {
		   throw e;
		}
		session.close();
		return list;
	}
	/**
    * ��ѯ���м�¼��(ԭ��̬sql)
    * @param hql ��ѯ������
    * @return �ܼ�¼��
    */
	public int getAllSQLRowCount(String hql) {
		Session session=sessionFactory.openSession();
		List list=null;
		try{
			Query query=session.createSQLQuery(hql);
		
			list=query.list();
			
		}catch (RuntimeException e) {
		   throw e;
		}
		session.close();
		return list.size();
		
	}
	/**
    * ��ҳ��ѯ
    * @param sql ��ѯ������
    * @param offset ��ʼ��¼
    * @param length һ�β�ѯ������¼
    * @return
    */
	public List  querySQLForPage(String hql, int offset, int length) {
		Session session=sessionFactory.openSession();
		List  list=null;
		try{
			Query query=session.createSQLQuery(hql);
			query.setFirstResult(offset);
           query.setMaxResults(length);
           list=query.list();
			
		}catch (RuntimeException e) {
		   throw e;
		}
		session.close();
		return list;
	}
	

/**
   * ����û����ѯ
   */
	public Agent queryAgent(String agentAccount)throws Exception{
		String hql="from Agent a where a.agentAccount= '"+agentAccount+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		Object obj=query.uniqueResult();
		if(obj!=null){
			return (Agent) obj;
		}else{
			return null;
		}
		
	}
	
	 /**
	  * ��ҳ��ѯ
	  */
	public Pager queryPage(int pageSize,int page,String search,long agentId){
		String hql="from Agent a where a.agentStatus in(1,4) and a.partnerId="+agentId;
		 if(search!=null&&!"".equals(search.trim())){
			 hql+="and (a.agentAccount like'%"+search+"%' or a.agentName like'%"+search+"%' or a.agentPhone like'%"+search+"%')";
		 }
		 hql+=" order by a.agentRegTime desc";
		   int allRow =this.getAllRowCount(hql);    //�ܼ�¼��
	       int totalPage = Pager.countTotalPage(pageSize, allRow);    //��ҳ��
	       final int offset = Pager.countOffset(pageSize, page);    //��ǰҳ��ʼ��¼
	       final int length = pageSize;    //ÿҳ��¼��
	       final int currentPage = Pager.countCurrentPage(page);
	       List list = this.queryForPage(hql,offset, length); //"һҳ"�ļ�¼
	       
	       
	     //�ѷ�ҳ��Ϣ���浽Bean��
	       Pager pages = new Pager();
	       pages.setPageSize(pageSize);    
	       pages.setCurrentPage(currentPage);
	       pages.setAllRow(allRow);
	       pages.setTotalPage(totalPage);
	       pages.setList(list);
	       pages.init();
	       return pages;
		   
	}
	/**
	  * ��ҳ��ѯ������
	  */
	public Pager findPage(int pageSize,int page,String search,long agentId){
		String hql="from Agent a where a.partnerId="+agentId;
		 if(search!=null&&!"".equals(search.trim())){
			 hql+="and (a.agentAccount like'%"+search+"%' or a.agentName like'%"+search+"%' or a.agentPhone like'%"+search+"%')";
		 }
		 hql+=" order by a.agentRegTime desc";
		int allRow =this.getAllRowCount(hql);    //�ܼ�¼��
	       int totalPage = Pager.countTotalPage(pageSize, allRow);    //��ҳ��
	       final int offset = Pager.countOffset(pageSize, page);    //��ǰҳ��ʼ��¼
	       final int length = pageSize;    //ÿҳ��¼��
	       final int currentPage = Pager.countCurrentPage(page);
	       List list = this.queryForPage(hql,offset, length); //"һҳ"�ļ�¼
	       
	       
	     //�ѷ�ҳ��Ϣ���浽Bean��
	       Pager pages = new Pager();
	       pages.setPageSize(pageSize);    
	       pages.setCurrentPage(currentPage);
	       pages.setAllRow(allRow);
	       pages.setTotalPage(totalPage);
	       pages.setList(list);
	       pages.init();
	       return pages;
	}
 	/**
 	 * ��Ӵ�����
 	 */
 	public int addAgent(Agent agent){
 		this.sessionFactory.getCurrentSession().save(agent);
 		return 1;
 	}
 	/**
 	 * �޸Ĵ�����
 	 */
 	public int updateAgent(Agent agent){
 		sessionFactory.getCurrentSession().update(agent);
 		return 1;
 	}
 	/**
 	 * ��ѯ������
 	 */
 	public Agent queryAgentInfo(long agentId){
 		String hql="from Agent a where a.agentId="+agentId;
 		Query query=sessionFactory.getCurrentSession().createQuery(hql);
 		Object obj=query.uniqueResult();
 		if(obj!=null){
 			return (Agent)obj;
 		}else{
 			return null;
 		}
 		
 		
 	}
 	/**
	  * ��ҳ��ѯ������
	  */
	public Pager findAgentPage(int pageSize,int page,String search){
		String hql="from Agent a WHERE a.role.id=1";
		 if(search!=null&&!"".equals(search.trim())){
			 hql+="and (a.agentAccount like'%"+search+"%' or a.agentName like'%"+search+"%' or a.agentPhone like'%"+search+"%')";
		 }
		 hql+=" order by a.agentRegTime desc";
		int allRow =this.getAllRowCount(hql);    //�ܼ�¼��
	       int totalPage = Pager.countTotalPage(pageSize, allRow);    //��ҳ��
	       final int offset = Pager.countOffset(pageSize, page);    //��ǰҳ��ʼ��¼
	       final int length = pageSize;    //ÿҳ��¼��
	       final int currentPage = Pager.countCurrentPage(page);
	       List list = this.queryForPage(hql,offset, length); //"һҳ"�ļ�¼
	       
	       
	     //�ѷ�ҳ��Ϣ���浽Bean��
	       Pager pages = new Pager();
	       pages.setPageSize(pageSize);    
	       pages.setCurrentPage(currentPage);
	       pages.setAllRow(allRow);
	       pages.setTotalPage(totalPage);
	       pages.setList(list);
	       pages.init();
	       return pages;

	}
	/**
	  * ��ҳ��ѯ������
	  */
	public Pager selectAgentPage(int pageSize,int page,String search){
		String hql="from Agent a WHERE a.role.id=1 and a.agentStatus=1";
		 if(search!=null&&!"".equals(search.trim())){
			 hql+="and (a.agentAccount like'%"+search+"%' or a.agentName like'%"+search+"%' or a.agentPhone like'%"+search+"%')";
		 }
		 hql+=" order by a.agentRegTime desc";
		int allRow =this.getAllRowCount(hql);    //�ܼ�¼��
	       int totalPage = Pager.countTotalPage(pageSize, allRow);    //��ҳ��
	       final int offset = Pager.countOffset(pageSize, page);    //��ǰҳ��ʼ��¼
	       final int length = pageSize;    //ÿҳ��¼��
	       final int currentPage = Pager.countCurrentPage(page);
	       List list = this.queryForPage(hql,offset, length); //"һҳ"�ļ�¼
	       
	       
	     //�ѷ�ҳ��Ϣ���浽Bean��
	       Pager pages = new Pager();
	       pages.setPageSize(pageSize);    
	       pages.setCurrentPage(currentPage);
	       pages.setAllRow(allRow);
	       pages.setTotalPage(totalPage);
	       pages.setList(list);
	       pages.init();
	       return pages;
	}
	
	/**
	  * ��ҳ��ѯ�ϻ���
	  */
	public Pager findPartnerPage(int pageSize,int page,String search){
		String hql="from Agent a where a.role.id=2";
		 if(search!=null&&!"".equals(search.trim())){
			 hql+="and (a.agentAccount like'%"+search+"%' or a.agentName like'%"+search+"%' or a.agentPhone like'%"+search+"%')";
		 }
		 hql+=" order by a.agentRegTime desc";
		int allRow =this.getAllRowCount(hql);    //�ܼ�¼��
	       int totalPage = Pager.countTotalPage(pageSize, allRow);    //��ҳ��
	       final int offset = Pager.countOffset(pageSize, page);    //��ǰҳ��ʼ��¼
	       final int length = pageSize;    //ÿҳ��¼��
	       final int currentPage = Pager.countCurrentPage(page);
	       List list = this.queryForPage(hql,offset, length); //"һҳ"�ļ�¼
	       
	       
	     //�ѷ�ҳ��Ϣ���浽Bean��
	       Pager pages = new Pager();
	       pages.setPageSize(pageSize);    
	       pages.setCurrentPage(currentPage);
	       pages.setAllRow(allRow);
	       pages.setTotalPage(totalPage);
	       pages.setList(list);
	       pages.init();
	       return pages;
	} 	
	/**
	  * ��ҳ��ѯ�ϻ��˼���
	  */
	public List<Agent> findPartnerList(){
		String hql="from Agent a where a.role.id=2";
		Query query=this.sessionFactory.getCurrentSession().createQuery(hql);
		List<Agent> list=query.list();
		if(list!=null){
			return list;
		}else{
			return null;
		}
	}
	/**
	 * 查询多个代理商
	 */
	public List<Agent> queryallAgent(long agentId){
		String hql="from Agent a where a.partnerId="+agentId;
		Query query=this.sessionFactory.getCurrentSession().createQuery(hql);
		List<Agent> list=query.list();
		if(list!=null){
			return list;
		}else{
			return null;
		}
	}
	/**
	 * ����ɾ��
	 */
	public int deleteAllAgent(String number){
		String hql="delete Agent a where a.agentId in ("+number+")";
	    Query query=this.sessionFactory.getCurrentSession().createQuery(hql);
	   int result=query.executeUpdate();
	   return result;
	}
	
	/**
	 * ɾ��
	 */
	public int deleteAgent(long agentId){
		String hql="delete Agent a where a.agentId="+agentId;
	    Query query=this.sessionFactory.getCurrentSession().createQuery(hql);
	   int result=query.executeUpdate();
	   return result;
	}


	@Override
	public Agent queryHehuo(Long agentId) {
		String hql="select a from Agent a where a.agentId="+agentId;
		Query query=this.sessionFactory.getCurrentSession().createQuery(hql);
		if(query!=null){	
			Object obj=query.uniqueResult();
			return (Agent) obj;
		}else {
			return null;
		}
	}


	@Override
	public Agent quertwo(Long pranId) {
	String hql="select a from Agent a where a.agentId="+pranId;
	Query query=this.sessionFactory.getCurrentSession().createQuery(hql);
	if(query!=null){
		Object obj=query.uniqueResult();
		return (Agent) obj;
	}else {
		return null;
	}
	}




  
  
}
