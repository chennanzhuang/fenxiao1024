package com.fenxiao.agent.action;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import com.fenxiao.agent.entity.Agent;
import com.fenxiao.agent.service.AgentService;
import com.fenxiao.common.Pager;
import com.fenxiao.role.entity.Role;
import com.fenxiao.role.service.MenuService;
import com.fenxiao.role.service.RoleMenuService;
import com.fenxiao.role.service.RoleService;
import com.fenxiao.user.entity.User;
import com.fenxiao.user.service.UserService;
import com.fenxiao.util.Tools;
import com.opensymphony.xwork2.ActionSupport;

public class AgentAction extends ActionSupport {

	private HttpServletRequest request;
	private HttpServletResponse response;
	private HttpSession session;

	private AgentService agentService;
	private RoleService roleService;
	private RoleMenuService roleMenuService;
	private MenuService menuService;
	private UserService userService;

	private Agent agent;
	private String seccode;
	private String msg;
	private String imgcode;
	private String num;

	private int page;
	private Pager pageBean;
	private List<Agent> agentList;
	private List<User> userList;
	private String search;
	private long agentId;
	private Role role;
	private int roleId;
	private String agentPass;
	private int agentStatus;
	private File fileTest; // 接收这个上传的文件
	private String fileTestFileName;
	private File fileTest1; // 接收这个上传的文件
	private String fileTest1FileName;

	// 登陆
	public String agentlogin() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();
		try {
			String authCode = session.getAttribute("checkcode").toString();

			seccode = seccode.toLowerCase();
			authCode = authCode.toLowerCase();
			System.out.println(seccode);
			if (seccode.equals(authCode)) {
				agent = agentService.checkLogin(agent);
				if (agent == null) {
					msg = "您输入的账号或密码错误！";
					return "loginerro";

				} else {
					session.setAttribute("agent", agent);
					session.setAttribute("roleId", agent.getRole().getId());
					return "loginSuccess";
				}
			} else {
				msg = "验证码错误，请重新输入！";
				return "loginerro";
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			msg = "您输入的账号或密码错误！";
			return "loginerro";
		}

	}

	/**
	 * 分页查询代理商佣金
	 * 
	 * @return
	 */
	public String queryPageForAgent() {
		pageBean = agentService.queryPage(3, page, search, agentId);
		agentList = pageBean.getList();
		search = search;
		return "queryPageForAgent";
	}

	/**
	 * 分页查询会员佣金
	 * 
	 * @return
	 */
	public String queryPageForUser() {
		pageBean = userService.queryPageForStaff(3, page, search, agentId);
		userList = pageBean.getList();
		search = search;
		return "queryPageForUser";
	}

	/**
	 * 分页查询代理商
	 * 
	 * @return
	 */
	public String findPageForAgent() {
		pageBean = agentService.findPage(3, page, search, agentId);
		agentList = pageBean.getList();
		search = search;
		return "findPageForAgent";
	}

	/**
	 * 
	 * 提交代理商资料
	 * 
	 * @return
	 */
	public String addAgent() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();
		int type = 0;
		String[] str = { ".jpg", ".jpeg", ".bmp", ".png", ".gif" };
		// 限定文件大小是4MB
		if (fileTest != null) {
			if (fileTest.length() > 2097152) {
				msg = "图片大小超出了2M";
				return "imageError";
			} else {

				for (String s : str) {
					if (fileTestFileName.endsWith(s)) {
						type = 1;
						String realPath = ServletActionContext
								.getServletContext().getRealPath("/faceimages");// 实际路径
						// String realPath ="d:/upload/faceimages";
						System.out.println(realPath);
						fileTestFileName = Tools.getRandomFileName() + s;
						File saveFile = new File(new File(realPath),
								fileTestFileName); // 在该实际路径下实例化一个文件
						// 判断父目录是否存在
						if (!saveFile.getParentFile().exists()) {
							saveFile.getParentFile().mkdirs();
						}
						try {
							// 执行文件上传
							// FileUtils 类名 org.apache.commons.io.FileUtils;
							// 是commons-io包中的，commons-fileupload 必须依赖
							// commons-io包实现文件上次，实际上就是将一个文件转换成流文件进行读写
							FileUtils.copyFile(fileTest, saveFile);
							agent.setAgentPhoto("faceimages/"
									+ fileTestFileName);

						} catch (IOException e) {
							msg = "图片上传失败";
							return "imageError";
						}
					}
				}
				if (type == 0) {
					msg = "上传的图片类型错误";
					return "imageError";
				}
			}
		}
		if (fileTest1 != null) {
			if (fileTest1.length() > 2097152) {
				msg = "图片未上传或者图片大小超出了2M";
				return "imageError";
			} else {

				for (String s : str) {
					if (fileTest1FileName.endsWith(s)) {
						type = 1;
						String realPath = ServletActionContext
								.getServletContext().getRealPath("/faceimages");// 实际路径
						// String realPath ="d:/upload/faceimages";
						System.out.println(realPath);
						fileTest1FileName = Tools.getRandomFileName() + s;
						File saveFile = new File(new File(realPath),
								fileTest1FileName); // 在该实际路径下实例化一个文件
						// 判断父目录是否存在
						if (!saveFile.getParentFile().exists()) {
							saveFile.getParentFile().mkdirs();
						}
						try {
							// 执行文件上传
							// FileUtils 类名 org.apache.commons.io.FileUtils;
							// 是commons-io包中的，commons-fileupload 必须依赖
							// commons-io包实现文件上次，实际上就是将一个文件转换成流文件进行读写
							FileUtils.copyFile(fileTest1, saveFile);
							agent.setAgentOr("faceimages/" + fileTest1FileName);

						} catch (IOException e) {
							msg = "图片上传失败";
							return "imageError";
						}
					}
				}
				if (type == 0) {
					msg = "上传的图片类型错误";
					return "imageError";
				}
			}
		}
		// msg = "图片上传成功";

		agent.setPartnerId(agentId);
		role = roleService.queryRole(roleId);
		agent.setRole(role);
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(date);
		agent.setAgentRegTime(dateString);
		int i = agentService.addAgent(agent);
		agentId = agentId;
		return findPageForAgent();
	}

	/**
	 * 查看详情
	 * 
	 * @return
	 */
	public String queryAgentInfo() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		agent = agentService.queryAgentInfo(agent.getAgentId());
		request.setAttribute("agent", agent);
		return "queryAgentInfo";

	}

	/**
	 * 关闭代理商
	 * 
	 * @return
	 */
	public String closeAgent() {
		Agent ag = agentService.queryAgentInfo(agent.getAgentId());
		ag.setAgentStatus(agent.getAgentStatus());
		agentService.updateAgent(ag);
		agentId = agentId;
		return findPageForAgent();
	}

	/**
	 * 更新页面
	 */
	public String updateAgentOfPage() {

		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		agent = agentService.queryAgentInfo(agent.getAgentId());
		request.setAttribute("agent", agent);

		return "updateAgentOfPage";

	}

	/**
	 * 更新代理商
	 * 
	 * @return
	 */
	public String updateAgent() {

		Agent ag = agentService.queryAgentInfo(agent.getAgentId());
		int type = 0;
		String[] str = { ".jpg", ".jpeg", ".bmp", ".png", ".gif" };
		// 限定文件大小是4MB

		if (fileTest != null) {
			if (fileTest.length() > 2097152) {
				msg = "图片大小超出了2M";
				return updateAgentOfPage();
			} else {

				for (String s : str) {
					if (fileTestFileName.endsWith(s)) {
						type = 1;
						String realPath = ServletActionContext
								.getServletContext().getRealPath("/faceimages");// 实际路径
						// String realPath ="d:/upload/faceimages";
						System.out.println(realPath);
						fileTestFileName = Tools.getRandomFileName() + s;
						File saveFile = new File(new File(realPath),
								fileTestFileName); // 在该实际路径下实例化一个文件
						// 判断父目录是否存在
						if (!saveFile.getParentFile().exists()) {
							saveFile.getParentFile().mkdirs();
						}
						try {
							// 执行文件上传
							// FileUtils 类名 org.apache.commons.io.FileUtils;
							// 是commons-io包中的，commons-fileupload 必须依赖
							// commons-io包实现文件上次，实际上就是将一个文件转换成流文件进行读写
							FileUtils.copyFile(fileTest, saveFile);
							ag.setAgentPhoto("faceimages/" + fileTestFileName);

						} catch (IOException e) {
							msg = "图片上传失败";
							return updateAgentOfPage();
						}
					}
				}
				if (type == 0) {
					msg = "上传的图片类型错误";
					return updateAgentOfPage();
				}
			}
		}
		if (fileTest1 != null) {
			if (fileTest1.length() > 2097152) {
				msg = "图片未上传或者图片大小超出了2M";
				return updateAgentOfPage();
			} else {

				for (String s : str) {
					if (fileTest1FileName.endsWith(s)) {
						type = 1;
						String realPath = ServletActionContext
								.getServletContext().getRealPath("/faceimages");// 实际路径
						// String realPath ="d:/upload/faceimages";
						System.out.println(realPath);
						fileTest1FileName = Tools.getRandomFileName() + s;
						File saveFile = new File(new File(realPath),
								fileTest1FileName); // 在该实际路径下实例化一个文件
						// 判断父目录是否存在
						if (!saveFile.getParentFile().exists()) {
							saveFile.getParentFile().mkdirs();
						}
						try {
							// 执行文件上传
							// FileUtils 类名 org.apache.commons.io.FileUtils;
							// 是commons-io包中的，commons-fileupload 必须依赖
							// commons-io包实现文件上次，实际上就是将一个文件转换成流文件进行读写
							FileUtils.copyFile(fileTest1, saveFile);
							ag.setAgentOr("faceimages/" + fileTest1FileName);

						} catch (IOException e) {
							msg = "图片上传失败";
							return updateAgentOfPage();
						}
					}
				}
				if (type == 0) {
					msg = "上传的图片类型错误";
					return updateAgentOfPage();
				}
			}
		}
		// msg = "图片上传成功";

		ag.setAgentAccount(agent.getAgentAccount());
		ag.setAgentName(agent.getAgentName());
		ag.setAgentPhone(agent.getAgentPhone());
		ag.setAgentWeChatId(agent.getAgentWeChatId());
		ag.setAgentRegTime(agent.getAgentRegTime());
		ag.setAgentStatus(agent.getAgentStatus());
		ag.setSex(agent.getSex());

		agentService.updateAgent(ag);
		agentId = agentId;
		return findPageForAgent();

	}

	/**
	 * 分页查询代理商
	 * 
	 * @return
	 */
	public String queryAgentByPage() {

		pageBean = agentService.findAgentPage(3, page, search);
		agentList = pageBean.getList();
		search = search;
		return "queryAgentByPage";

	}

	/**
	 * 分页查询合伙人
	 * 
	 * @return
	 */
	public String queryPartnerByPage() {

		pageBean = agentService.findPartnerPage(3, page, search);
		agentList = pageBean.getList();
		search = search;
		return "queryPartnerByPage";

	}

	/**
	 * 管理员添加代理商页面
	 * 
	 * @return
	 */
	public String addAgentPage() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		agentList = agentService.findPartnerList();
		request.setAttribute("agentList", agentList);
		return "addAgentPage";
	}

	/**
	 * 管理员添加代理商
	 * 
	 * @return
	 */
	public String addAllAgent() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();
		int type = 0;
		String[] str = { ".jpg", ".jpeg", ".bmp", ".png", ".gif" };
		// 限定文件大小是4MB
		if (fileTest != null) {
			if (fileTest.length() > 2097152) {
				msg = "图片大小超出了2M";
				return addAgentPage();
			} else {

				for (String s : str) {
					if (fileTestFileName.endsWith(s)) {
						type = 1;
						String realPath = ServletActionContext
								.getServletContext().getRealPath("/faceimages");// 实际路径
						// String realPath ="d:/upload/faceimages";
						System.out.println(realPath);
						fileTestFileName = Tools.getRandomFileName() + s;
						File saveFile = new File(new File(realPath),
								fileTestFileName); // 在该实际路径下实例化一个文件
						// 判断父目录是否存在
						if (!saveFile.getParentFile().exists()) {
							saveFile.getParentFile().mkdirs();
						}
						try {
							// 执行文件上传
							// FileUtils 类名 org.apache.commons.io.FileUtils;
							// 是commons-io包中的，commons-fileupload 必须依赖
							// commons-io包实现文件上次，实际上就是将一个文件转换成流文件进行读写
							FileUtils.copyFile(fileTest, saveFile);
							agent.setAgentPhoto("faceimages/"
									+ fileTestFileName);

						} catch (IOException e) {
							msg = "图片上传失败";
							return addAgentPage();
						}
					}
				}
				if (type == 0) {
					msg = "上传的图片类型错误";
					return addAgentPage();
				}
			}
		}
		if (fileTest1 != null) {
			if (fileTest1.length() > 2097152) {
				msg = "图片未上传或者图片大小超出了2M";
				return addAgentPage();
			} else {

				for (String s : str) {
					if (fileTest1FileName.endsWith(s)) {
						type = 1;
						String realPath = ServletActionContext
								.getServletContext().getRealPath("/faceimages");// 实际路径
						// String realPath ="d:/upload/faceimages";
						System.out.println(realPath);
						fileTest1FileName = Tools.getRandomFileName() + s;
						File saveFile = new File(new File(realPath),
								fileTest1FileName); // 在该实际路径下实例化一个文件
						// 判断父目录是否存在
						if (!saveFile.getParentFile().exists()) {
							saveFile.getParentFile().mkdirs();
						}
						try {
							// 执行文件上传
							// FileUtils 类名 org.apache.commons.io.FileUtils;
							// 是commons-io包中的，commons-fileupload 必须依赖
							// commons-io包实现文件上次，实际上就是将一个文件转换成流文件进行读写
							FileUtils.copyFile(fileTest1, saveFile);
							agent.setAgentOr("faceimages/" + fileTest1FileName);

						} catch (IOException e) {
							msg = "图片上传失败";
							return addAgentPage();
						}
					}
				}
				if (type == 0) {
					msg = "上传的图片类型错误";
					return addAgentPage();
				}
			}
		}
		// msg = "图片上传成功";
		role = roleService.queryRole(roleId);
		agent.setRole(role);
		if (agent.getAgentPass() == null) {
			msg = "密码不能为空！";
			return addAgentPage();
		} else {
			if (!agentPass.trim().equals(agent.getAgentPass().trim())) {
				msg = "两次密码输入不一致！";
				return addAgentPage();
			} else {
				agent.setAgentPass(agentPass);
			}
		}
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(date);
		agent.setAgentRegTime(dateString);
		agent.setRecommendationCode(showSmsCode() + "");
		int i = agentService.addAgent(agent);

		return queryAgentByPage();

	}

	/**
	 * 管理员关闭代理商
	 * 
	 * @return
	 */
	public String closeAllAgent() {
		Agent ag = agentService.queryAgentInfo(agent.getAgentId());
		ag.setAgentStatus(agent.getAgentStatus());
		agentService.updateAgent(ag);

		return queryAgentByPage();

	}

	/**
	 * 批量删除
	 * 
	 * @return
	 */
	public String deleteAllAgent() {
		int result = agentService.deleteAllAgent(num);
		if (result > 0) {
			msg = "删除成功！";

		} else {
			msg = "删除失败！";
		}
		return queryAgentByPage();
	}

	/**
	 * 删除
	 * 
	 * @return
	 */
	public String deleteAgent() {

		int result = agentService.deleteAgent(agent.getAgentId());
		if (result > 0) {
			msg = "删除成功！";

		} else {
			msg = "删除失败！";
		}
		return queryAgentByPage();
	}

	/**
	 * 审核
	 * 
	 * @return
	 */
	public String auditAgentPage() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		agent = agentService.queryAgentInfo(agent.getAgentId());
		request.setAttribute("agent", agent);
		return "auditAgentPage";
	}

	/**
	 * 审核
	 * 
	 * @return
	 */
	public String auditAgent() {
		Agent ag = agentService.queryAgentInfo(agent.getAgentId());
		ag.setReason(agent.getReason());
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String time = sdf.format(date);
		ag.setAuditTime(time);
		ag.setAgentStatus(1);

		int i = agentService.updateAgent(ag);
		if (i > 0) {
			msg = "提交成功！";

		} else {
			msg = "提交失败！";

			return auditAgentPage();
		}

		return queryAgentByPage();
	}

	/**
	 * 审核
	 * 
	 * @return
	 */
	public String auditAgentFail() {
		Agent ag = agentService.queryAgentInfo(agent.getAgentId());
		ag.setReason(agent.getReason());
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String time = sdf.format(date);
		ag.setAuditTime(time);
		ag.setAgentStatus(3);

		int i = agentService.updateAgent(ag);
		if (i > 0) {
			msg = "提交成功！";

		} else {
			msg = "提交失败！";
			return auditAgentPage();
		}
		return queryAgentByPage();
	}

	/**
	 * 关闭合伙人
	 * 
	 * @return
	 */
	public String closePartner() {
		Agent ag = agentService.queryAgentInfo(agent.getAgentId());
		ag.setAgentStatus(agent.getAgentStatus());
		agentService.updateAgent(ag);

		return queryPartnerByPage();
	}

	/**
	 * 修改合伙人页面
	 * 
	 * @return
	 */
	public String updatePartnerOfPage() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		agent = agentService.queryAgentInfo(agent.getAgentId());
		request.setAttribute("agent", agent);

		return "updatePartnerOfPage";
	}

	/**
	 * 修改合伙人
	 * 
	 * @return
	 */
	public String updatePartner() {
		Agent ag = agentService.queryAgentInfo(agent.getAgentId());
		int type = 0;
		String[] str = { ".jpg", ".jpeg", ".bmp", ".png", ".gif" };
		// 限定文件大小是4MB

		if (fileTest != null) {
			if (fileTest.length() > 2097152) {
				msg = "图片大小超出了2M";
				return "imageError2";
			} else {

				for (String s : str) {
					if (fileTestFileName.endsWith(s)) {
						type = 1;
						String realPath = ServletActionContext
								.getServletContext().getRealPath("/faceimages");// 实际路径
						// String realPath ="d:/upload/faceimages";
						System.out.println(realPath);
						fileTestFileName = Tools.getRandomFileName() + s;
						File saveFile = new File(new File(realPath),
								fileTestFileName); // 在该实际路径下实例化一个文件
						// 判断父目录是否存在
						if (!saveFile.getParentFile().exists()) {
							saveFile.getParentFile().mkdirs();
						}
						try {
							// 执行文件上传
							// FileUtils 类名 org.apache.commons.io.FileUtils;
							// 是commons-io包中的，commons-fileupload 必须依赖
							// commons-io包实现文件上次，实际上就是将一个文件转换成流文件进行读写
							FileUtils.copyFile(fileTest, saveFile);
							ag.setAgentPhoto("faceimages/" + fileTestFileName);

						} catch (IOException e) {
							msg = "图片上传失败";
							return updatePartnerOfPage();
						}
					}
				}
				if (type == 0) {
					msg = "上传的图片类型错误";
					return updatePartnerOfPage();
				}
			}
		}
		if (fileTest1 != null) {
			if (fileTest1.length() > 2097152) {
				msg = "图片未上传或者图片大小超出了2M";
				return updatePartnerOfPage();
			} else {

				for (String s : str) {
					if (fileTest1FileName.endsWith(s)) {
						type = 1;
						String realPath = ServletActionContext
								.getServletContext().getRealPath("/faceimages");// 实际路径
						// String realPath ="d:/upload/faceimages";
						System.out.println(realPath);
						fileTest1FileName = Tools.getRandomFileName() + s;
						File saveFile = new File(new File(realPath),
								fileTest1FileName); // 在该实际路径下实例化一个文件
						// 判断父目录是否存在
						if (!saveFile.getParentFile().exists()) {
							saveFile.getParentFile().mkdirs();
						}
						try {
							// 执行文件上传
							// FileUtils 类名 org.apache.commons.io.FileUtils;
							// 是commons-io包中的，commons-fileupload 必须依赖
							// commons-io包实现文件上次，实际上就是将一个文件转换成流文件进行读写
							FileUtils.copyFile(fileTest1, saveFile);
							ag.setAgentOr("faceimages/" + fileTest1FileName);

						} catch (IOException e) {
							msg = "图片上传失败";
							return updatePartnerOfPage();
						}
					}
				}
				if (type == 0) {
					msg = "上传的图片类型错误";
					return updatePartnerOfPage();
				}
			}
		}
		// msg = "图片上传成功";

		ag.setAgentAccount(agent.getAgentAccount());
		ag.setAgentName(agent.getAgentName());
		ag.setAgentPhone(agent.getAgentPhone());
		ag.setAgentWeChatId(agent.getAgentWeChatId());
		ag.setAgentRegTime(agent.getAgentRegTime());
		ag.setAgentStatus(agent.getAgentStatus());
		ag.setSex(agent.getSex());

		agentService.updateAgent(ag);

		return queryPartnerByPage();
	}

	/**
	 * 添加合伙人
	 * 
	 * @return
	 */
	public String addAllPartner() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();
		int type = 0;
		String[] str = { ".jpg", ".jpeg", ".bmp", ".png", ".gif" };
		// 限定文件大小是4MB
		if (fileTest != null) {
			if (fileTest.length() > 2097152) {
				msg = "图片大小超出了2M";
				return "addAllPartner";
			} else {

				for (String s : str) {
					if (fileTestFileName.endsWith(s)) {
						type = 1;
						String realPath = ServletActionContext
								.getServletContext().getRealPath("/faceimages");// 实际路径
						// String realPath ="d:/upload/faceimages";
						System.out.println(realPath);
						fileTestFileName = Tools.getRandomFileName() + s;
						File saveFile = new File(new File(realPath),
								fileTestFileName); // 在该实际路径下实例化一个文件
						// 判断父目录是否存在
						if (!saveFile.getParentFile().exists()) {
							saveFile.getParentFile().mkdirs();
						}
						try {
							// 执行文件上传
							// FileUtils 类名 org.apache.commons.io.FileUtils;
							// 是commons-io包中的，commons-fileupload 必须依赖
							// commons-io包实现文件上次，实际上就是将一个文件转换成流文件进行读写
							FileUtils.copyFile(fileTest, saveFile);
							agent.setAgentPhoto("faceimages/"
									+ fileTestFileName);

						} catch (IOException e) {
							msg = "图片上传失败";
							return "addAllPartner";
						}
					}
				}
				if (type == 0) {
					msg = "上传的图片类型错误";
					return "addAllPartner";
				}
			}
		}
		if (fileTest1 != null) {
			if (fileTest1.length() > 2097152) {
				msg = "图片未上传或者图片大小超出了2M";
				return "addAllPartner";
			} else {

				for (String s : str) {
					if (fileTest1FileName.endsWith(s)) {
						type = 1;
						String realPath = ServletActionContext
								.getServletContext().getRealPath("/faceimages");// 实际路径
						// String realPath ="d:/upload/faceimages";
						System.out.println(realPath);
						fileTest1FileName = Tools.getRandomFileName() + s;
						File saveFile = new File(new File(realPath),
								fileTest1FileName); // 在该实际路径下实例化一个文件
						// 判断父目录是否存在
						if (!saveFile.getParentFile().exists()) {
							saveFile.getParentFile().mkdirs();
						}
						try {
							// 执行文件上传
							// FileUtils 类名 org.apache.commons.io.FileUtils;
							// 是commons-io包中的，commons-fileupload 必须依赖
							// commons-io包实现文件上次，实际上就是将一个文件转换成流文件进行读写
							FileUtils.copyFile(fileTest1, saveFile);
							agent.setAgentOr("faceimages/" + fileTest1FileName);

						} catch (IOException e) {
							msg = "图片上传失败";
							return "addAllPartner";
						}
					}
				}
				if (type == 0) {
					msg = "上传的图片类型错误";
					return "addAllPartner";
				}
			}
		}
		// msg = "图片上传成功";
		role = roleService.queryRole(roleId);
		agent.setRole(role);
		if (agent.getAgentPass() == null) {
			msg = "密码不能为空！";
			return "addAllPartner";
		} else {
			if (!agentPass.trim().equals(agent.getAgentPass().trim())) {
				msg = "两次密码输入不一致！";
				return "";
			} else {
				agent.setAgentPass(agentPass);
			}
		}
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(date);
		agent.setAgentRegTime(dateString);
		agent.setRecommendationCode(showSmsCode() + "");

		int i = agentService.addAgent(agent);

		return queryPartnerByPage();
	}

	/**
	 * 批量删除合伙人
	 * 
	 * @return
	 */
	public String deleteAllPartner() {
		int result = agentService.deleteAllAgent(num);
		if (result > 0) {
			msg = "删除成功！";

		} else {
			msg = "删除失败！";
		}
		return queryPartnerByPage();

	}

	/**
	 * 删除合伙人
	 * 
	 * @return
	 */
	public String deletePartner() {
		int result = agentService.deleteAgent(agent.getAgentId());
		if (result > 0) {
			msg = "删除成功！";

		} else {
			msg = "删除失败！";
		}
		return queryPartnerByPage();

	}

	/**
	 * 通过代理商查询员工的代理商的列表
	 * 
	 * @return
	 */
	public String selectAgent() {
		pageBean = agentService.selectAgentPage(3, page, search);
		agentList = pageBean.getList();
		search = search;
		return "selectAgent";
	}

	/**
	 * 合伙人详情
	 * 
	 * @return
	 */
	public String partnerInfo() {
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		agent = agentService.queryAgentInfo(agent.getAgentId());
		request.setAttribute("agent", agent);
		return "partnerInfo";

	}

	// 随机生成 6 位数推荐码
	public int showSmsCode() {
		return (int) ((Math.random() * 9 + 1) * 100000);
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public AgentService getAgentService() {
		return agentService;
	}

	public void setAgentService(AgentService agentService) {
		this.agentService = agentService;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public String getSeccode() {
		return seccode;
	}

	public void setSeccode(String seccode) {
		this.seccode = seccode;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getImgcode() {
		return imgcode;
	}

	public void setImgcode(String imgcode) {
		this.imgcode = imgcode;
	}

	public RoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	public RoleMenuService getRoleMenuService() {
		return roleMenuService;
	}

	public void setRoleMenuService(RoleMenuService roleMenuService) {
		this.roleMenuService = roleMenuService;
	}

	public MenuService getMenuService() {
		return menuService;
	}

	public void setMenuService(MenuService menuService) {
		this.menuService = menuService;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public Pager getPageBean() {
		return pageBean;
	}

	public void setPageBean(Pager pageBean) {
		this.pageBean = pageBean;
	}

	public List<Agent> getAgentList() {
		return agentList;
	}

	public void setAgentList(List<Agent> agentList) {
		this.agentList = agentList;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public long getAgentId() {
		return agentId;
	}

	public void setAgentId(long agentId) {
		this.agentId = agentId;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public File getFileTest() {
		return fileTest;
	}

	public void setFileTest(File fileTest) {
		this.fileTest = fileTest;
	}

	public String getFileTestFileName() {
		return fileTestFileName;
	}

	public void setFileTestFileName(String fileTestFileName) {
		this.fileTestFileName = fileTestFileName;
	}

	public File getFileTest1() {
		return fileTest1;
	}

	public void setFileTest1(File fileTest1) {
		this.fileTest1 = fileTest1;
	}

	public String getFileTest1FileName() {
		return fileTest1FileName;
	}

	public void setFileTest1FileName(String fileTest1FileName) {
		this.fileTest1FileName = fileTest1FileName;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getAgentPass() {
		return agentPass;
	}

	public void setAgentPass(String agentPass) {
		this.agentPass = agentPass;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public int getAgentStatus() {
		return agentStatus;
	}

	public void setAgentStatus(int agentStatus) {
		this.agentStatus = agentStatus;
	}

}
