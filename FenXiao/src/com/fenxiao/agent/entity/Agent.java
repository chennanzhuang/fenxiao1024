package com.fenxiao.agent.entity;
// default package

import java.util.HashSet;
import java.util.Set;


import com.fenxiao.role.entity.Role;

/**
 * 合伙人、代理商表
 * Agent entity. @author MyEclipse Persistence Tools
 */

public class Agent implements java.io.Serializable {

	// Fields

	private Long agentId;
	private Role role;//角色
	private String agentAccount;//账号
	private String agentName;//名称
	private String agentPass;//密码
	private String agentPhone;//联系方式
	private Double agentMoney;//佣金
	private Double promotionCommission;//推广佣金
	private Double teamCommissions;//团队佣金
	private Double taskCommissions;//任务佣金
	private Integer sex;//性别
	private String agentOr;//二维码
	private String agentNickName;//昵称
	private String agentAdress;//地址
	private Double agentBalance;//余额
	private String agentPhoto;//头像
	private String recommendationCode;//推荐码
	private String agentRegTime;//注册时间
	private Long partnerId;//合伙人
	private String agentWeChatId;//微信
	private String agentPresentPass;//提现密码
	private int agentStatus;//状态(1,可用，2，正在审核，3 审核失败 4 禁用)
	//private int agentExamine;//审核状态
	private Set incomeRecodes = new HashSet(0);//收入记录
	private Set presentRecords = new HashSet(0);//提现记录
	private Set bankcards = new HashSet(0);//银行卡
	private String reason;//审核失败原因
	private String auditTime;//审核时间
	// Constructors

	/** default constructor */
	public Agent() {
	}

	/** full constructor */
	public Agent(Role role, String agentName, String agentPass,
			String agentPhone, Double agentMoney, Integer sex, String agentOr,
			String agentNickName, String agentAdress, Double agentBalance,
			String agentPhoto, String recommendationCode, String agentRegTime,Long partnerId,
			String agentWeChatId, String agentPresentPass, String agentAccount,
			Double promotionCommission, Double teamCommissions,
			Double taskCommissions, Integer agentStatus, Set incomeRecodes,
		    Set presentRecords, Set bankcards,String reason,String auditTime) {
		this.role = role;
		this.agentName = agentName;
		this.agentPass = agentPass;
		this.agentPhone = agentPhone;
		this.agentMoney = agentMoney;
		this.sex = sex;
		this.agentOr = agentOr;
		this.agentNickName = agentNickName;
		this.agentAdress = agentAdress;
		this.agentBalance = agentBalance;
		this.agentPhoto = agentPhoto;
		this.recommendationCode = recommendationCode;
		this.agentRegTime = agentRegTime;
		this.agentWeChatId = agentWeChatId;
		this.agentPresentPass = agentPresentPass;
		this.agentAccount = agentAccount;
		this.promotionCommission = promotionCommission;
		this.teamCommissions = teamCommissions;
		this.taskCommissions = taskCommissions;
		this.agentStatus = agentStatus;
		this.incomeRecodes = incomeRecodes;
		this.presentRecords = presentRecords;
		this.partnerId=partnerId;
		this.bankcards = bankcards;
		this.reason=reason;
		this.auditTime=auditTime;
	}

	// Property accessors

	public Long getAgentId() {
		return this.agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Long getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(Long partnerId) {
		this.partnerId = partnerId;
	}

	public String getAgentName() {
		return this.agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentPass() {
		return this.agentPass;
	}

	public void setAgentPass(String agentPass) {
		this.agentPass = agentPass;
	}

	public String getAgentPhone() {
		return this.agentPhone;
	}

	public void setAgentPhone(String agentPhone) {
		this.agentPhone = agentPhone;
	}

	public Double getAgentMoney() {
		return this.agentMoney;
	}

	public void setAgentMoney(Double agentMoney) {
		this.agentMoney = agentMoney;
	}

	public Integer getSex() {
		return this.sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getAgentOr() {
		return this.agentOr;
	}

	public void setAgentOr(String agentOr) {
		this.agentOr = agentOr;
	}

	public String getAgentNickName() {
		return this.agentNickName;
	}

	public void setAgentNickName(String agentNickName) {
		this.agentNickName = agentNickName;
	}

	public String getAgentAdress() {
		return this.agentAdress;
	}

	public void setAgentAdress(String agentAdress) {
		this.agentAdress = agentAdress;
	}

	public Double getAgentBalance() {
		return this.agentBalance;
	}

	public void setAgentBalance(Double agentBalance) {
		this.agentBalance = agentBalance;
	}

	public String getAgentPhoto() {
		return this.agentPhoto;
	}

	public void setAgentPhoto(String agentPhoto) {
		this.agentPhoto = agentPhoto;
	}

	public String getRecommendationCode() {
		return this.recommendationCode;
	}

	public void setRecommendationCode(String recommendationCode) {
		this.recommendationCode = recommendationCode;
	}

	public String getAgentRegTime() {
		return this.agentRegTime;
	}

	public void setAgentRegTime(String agentRegTime) {
		this.agentRegTime = agentRegTime;
	}

	public String getAgentWeChatId() {
		return this.agentWeChatId;
	}

	public void setAgentWeChatId(String agentWeChatId) {
		this.agentWeChatId = agentWeChatId;
	}

	public String getAgentPresentPass() {
		return this.agentPresentPass;
	}

	public void setAgentPresentPass(String agentPresentPass) {
		this.agentPresentPass = agentPresentPass;
	}

	public String getAgentAccount() {
		return this.agentAccount;
	}

	public void setAgentAccount(String agentAccount) {
		this.agentAccount = agentAccount;
	}

	public Double getPromotionCommission() {
		return this.promotionCommission;
	}

	public void setPromotionCommission(Double promotionCommission) {
		this.promotionCommission = promotionCommission;
	}

	public Double getTeamCommissions() {
		return this.teamCommissions;
	}

	public void setTeamCommissions(Double teamCommissions) {
		this.teamCommissions = teamCommissions;
	}

	public Double getTaskCommissions() {
		return this.taskCommissions;
	}

	public void setTaskCommissions(Double taskCommissions) {
		this.taskCommissions = taskCommissions;
	}

	public Integer getAgentStatus() {
		return this.agentStatus;
	}

	public void setAgentStatus(Integer agentStatus) {
		this.agentStatus = agentStatus;
	}

	public Set getIncomeRecodes() {
		return this.incomeRecodes;
	}

	public void setIncomeRecodes(Set incomeRecodes) {
		this.incomeRecodes = incomeRecodes;
	}

	public Set getPresentRecords() {
		return this.presentRecords;
	}

	public void setPresentRecords(Set presentRecords) {
		this.presentRecords = presentRecords;
	}

	public Set getBankcards() {
		return this.bankcards;
	}

	public void setBankcards(Set bankcards) {
		this.bankcards = bankcards;
	}

	public void setAgentStatus(int agentStatus) {
		this.agentStatus = agentStatus;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(String auditTime) {
		this.auditTime = auditTime;
	}


}