package com.fenxiao.mytask.entity;
// default package

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.fenxiao.user.entity.User;


/**

 * 我的任务
 * UserTask entity. @author MyEclipse Persistence Tools
 */

public class UserTask implements java.io.Serializable {

	// Fields

	private Long userTaskId;

	private User user;//会员
	private String taskTitle;//标题
	private String taskLogo;//logo
	private String taskEndTime;
	private String appRegisterName;
	private String weChatOfficialAccountReply;
	private String taskBriefing;//简介
	private Double taskBonus;//任务奖金
	private Double taskBonus1;//实际奖金
	private Integer taskStatus;//任务状态（1、进行中 2、完成、5正在审核.3审核失败）
	private String taskOrderCode;//订单号
	private String taskOrderTime;//下单时间
	private String taskCompletionTime;//完成时间
	private Integer taskType;//任务类型（0、关注公众号 1、广告宣传 2、app下载）
	private String taskName;//app注册名（店铺名）
	private String taskInfo;//任务详情
	private String taskQr;//二维码
	private int uploadTime;//上传次数（最多上传两次）
	private Set taskScreenshots = new HashSet(0);//截图
	private Long taskId;//taskId
	private String randomCode;
	private String taskReson;//审核结果
	private String originalID;
	private Double yongjing;//团队佣金
	private Double tuiguang;//推广佣金
	// Constructors

	/** default constructor */
	public UserTask() {
	}

	public Double getYongjing() {
		return yongjing;
	}

	public void setYongjing(Double yongjing) {
		this.yongjing = yongjing;
	}

	public Double getTuiguang() {
		return tuiguang;
	}

	public void setTuiguang(Double tuiguang) {
		this.tuiguang = tuiguang;
	}

	/** full constructor */
	public UserTask(User user, String taskTitle, String taskLogo,
			String taskBriefing, Double taskBonus, Integer taskStatus,
			String taskOrderCode, String taskOrderTime,
			String taskCompletionTime, Integer taskType, String taskName,
			String taskInfo, String taskQr, Set taskScreenshots,Long taskId,
			Double taskBonus1,String taskReson,Double yongjing,
			Double tuiguang) {
		this.user = user;
		this.taskTitle = taskTitle;
		this.taskLogo = taskLogo;
		this.taskBriefing = taskBriefing;
		this.taskBonus = taskBonus;
		this.taskStatus = taskStatus;
		this.taskOrderCode = taskOrderCode;
		this.taskOrderTime = taskOrderTime;
		this.taskCompletionTime = taskCompletionTime;
		this.taskType = taskType;
		this.taskName = taskName;
		this.taskInfo = taskInfo;
		this.taskQr = taskQr;
		this.taskScreenshots = taskScreenshots;
		this.taskId=taskId;
		this.taskBonus1=taskBonus1;
		this.taskReson=taskReson;
		this.yongjing=yongjing;
		this.tuiguang=tuiguang;
	}

	// Property accessors

	public UserTask(Long userTaskId, User user, String taskTitle,
			String taskLogo, String taskEndTime, String appRegisterName,
			String weChatOfficialAccountReply, String taskBriefing,
			Double taskBonus, Double taskBonus1, Integer taskStatus,
			String taskOrderCode, String taskOrderTime,
			String taskCompletionTime, Integer taskType, String taskName,
			String taskInfo, String taskQr, int uploadTime,
			Set taskScreenshots, Long taskId, String randomCode,
			String taskReson, String originalID, Double yongjing,
			Double tuiguang) {
		super();
		this.userTaskId = userTaskId;
		this.user = user;
		this.taskTitle = taskTitle;
		this.taskLogo = taskLogo;
		this.taskEndTime = taskEndTime;
		this.appRegisterName = appRegisterName;
		this.weChatOfficialAccountReply = weChatOfficialAccountReply;
		this.taskBriefing = taskBriefing;
		this.taskBonus = taskBonus;
		this.taskBonus1 = taskBonus1;
		this.taskStatus = taskStatus;
		this.taskOrderCode = taskOrderCode;
		this.taskOrderTime = taskOrderTime;
		this.taskCompletionTime = taskCompletionTime;
		this.taskType = taskType;
		this.taskName = taskName;
		this.taskInfo = taskInfo;
		this.taskQr = taskQr;
		this.uploadTime = uploadTime;
		this.taskScreenshots = taskScreenshots;
		this.taskId = taskId;
		this.randomCode = randomCode;
		this.taskReson = taskReson;
		this.originalID = originalID;
		this.yongjing = yongjing;
		this.tuiguang = tuiguang;
	}

	public Long getUserTaskId() {
		return this.userTaskId;
	}

	public void setUserTaskId(Long userTaskId) {
		this.userTaskId = userTaskId;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTaskTitle() {
		return this.taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public String getTaskLogo() {
		return this.taskLogo;
	}

	public void setTaskLogo(String taskLogo) {
		this.taskLogo = taskLogo;
	}

	public String getTaskBriefing() {
		return this.taskBriefing;
	}

	public void setTaskBriefing(String taskBriefing) {
		this.taskBriefing = taskBriefing;
	}

	public Double getTaskBonus() {
		return this.taskBonus;
	}

	public void setTaskBonus(Double taskBonus) {
		this.taskBonus = taskBonus;
	}

	public Integer getTaskStatus() {
		return this.taskStatus;
	}

	public void setTaskStatus(Integer taskStatus) {
		this.taskStatus = taskStatus;
	}

	public String getTaskOrderCode() {
		return this.taskOrderCode;
	}

	public void setTaskOrderCode(String taskOrderCode) {
		this.taskOrderCode = taskOrderCode;
	}

	public String getTaskOrderTime() {
		return this.taskOrderTime;
	}

	public void setTaskOrderTime(String taskOrderTime) {
		this.taskOrderTime = taskOrderTime;
	}

	public String getTaskCompletionTime() {
		return this.taskCompletionTime;
	}

	public void setTaskCompletionTime(String taskCompletionTime) {
		this.taskCompletionTime = taskCompletionTime;
	}

	public Integer getTaskType() {
		return this.taskType;
	}

	public void setTaskType(Integer taskType) {
		this.taskType = taskType;
	}

	public String getTaskName() {
		return this.taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskInfo() {
		return this.taskInfo;
	}

	public void setTaskInfo(String taskInfo) {
		this.taskInfo = taskInfo;
	}

	public String getTaskQr() {
		return this.taskQr;
	}

	public String getAppRegisterName() {
		return appRegisterName;
	}

	public void setAppRegisterName(String appRegisterName) {
		this.appRegisterName = appRegisterName;
	}

	public void setTaskQr(String taskQr) {
		this.taskQr = taskQr;
	}

	public Set getTaskScreenshots() {
		return this.taskScreenshots;
	}

	public void setTaskScreenshots(Set taskScreenshots) {
		this.taskScreenshots = taskScreenshots;
	}


	public String getWeChatOfficialAccountReply() {
		return weChatOfficialAccountReply;
	}

	public void setWeChatOfficialAccountReply(String weChatOfficialAccountReply) {
		this.weChatOfficialAccountReply = weChatOfficialAccountReply;
	}


	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public Double getTaskBonus1() {
		return taskBonus1;
	}

	public void setTaskBonus1(Double taskBonus1) {
		this.taskBonus1 = taskBonus1;
	}

	public String getTaskReson() {
		return taskReson;
	}

	public void setTaskReson(String taskReson) {
		this.taskReson = taskReson;
	}

	public int getUploadTime() {
		return uploadTime;
	}

	public void setUploadTime(int uploadTime) {
		this.uploadTime = uploadTime;
	}

	public String getRandomCode() {
		return randomCode;
	}

	public void setRandomCode(String randomCode) {
		this.randomCode = randomCode;
	}

	public String getTaskEndTime() {
		return taskEndTime;
	}

	public void setTaskEndTime(String taskEndTime) {
		this.taskEndTime = taskEndTime;
	}

	public String getOriginalID() {
		return originalID;
	}

	public void setOriginalID(String originalID) {
		this.originalID = originalID;
	}

}