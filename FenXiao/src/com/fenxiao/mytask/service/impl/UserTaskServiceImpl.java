package com.fenxiao.mytask.service.impl;

import java.text.ParseException;
import java.util.List;

import com.fenxiao.common.Pager;
import com.fenxiao.mytask.dao.UserTaskDao;
import com.fenxiao.mytask.entity.UserTask;
import com.fenxiao.mytask.service.UserTaskService;
import com.fenxiao.task.entity.TaskScreenshot;
import com.fenxiao.user.entity.User;

public class UserTaskServiceImpl implements UserTaskService {

	private UserTaskDao userTaskDao;
	@Override
	public int addUserTask(UserTask userTask) {
		// TODO Auto-generated method stub
		return userTaskDao.addUserTask(userTask);
	}
	
	@Override
	public UserTask queryUaserTask(long taskId,long userId) {
		// TODO Auto-generated method stub
		return userTaskDao.queryUserTask(taskId,userId);
	}
	
	@Override
	public List<UserTask> queryPeopleTask(long userId) {
		// TODO Auto-generated method stub
		return userTaskDao.queryPeopleTask(userId);
	}
	
	@Override
	public UserTask queryUserTaskById(long userTaskId) {
		// TODO Auto-generated method stub
		return userTaskDao.queryUserTaskById(userTaskId);
	}
	
	@Override
	public int updateUserTaskAppRegisterName(long userTaskId,String appRegisterName) {
		// TODO Auto-generated method stub
		return userTaskDao.updateAppRegisterName(userTaskId, appRegisterName);
	}
	
	@Override
	public List<UserTask> queryThoseTaskCompleted(long userId) {
		// TODO Auto-generated method stub
		return userTaskDao.queryThoseTaskCompleted(userId);
	}
	
	@Override
	public int updateWeChatReply(String content, String weChatOfficialAccountReply,String originalID) {
		// TODO Auto-generated method stub
		return userTaskDao.updateWeChatReply(content,weChatOfficialAccountReply,originalID);
	}
	//团队奖金
	@Override
	public List queryTeamTaskForTheMan(User user) {
		// TODO Auto-generated method stub
		return userTaskDao.queryTeamTaskForTheMan(user);
	}
	
	//分页查询推广佣金
	public Pager queryTaskPagetuiguan(int pageSize,int page,User user){
		return userTaskDao.queryTaskPagetuiguan(pageSize, page,user);
				
	}
	public UserTaskDao getUserTaskDao() {
		return userTaskDao;
	}
	public void setUserTaskDao(UserTaskDao userTaskDao) {
		this.userTaskDao = userTaskDao;
	}
	
	@Override
	public List<UserTask> shaixuan(long userId, Integer taskStatus,
			Integer taskType,Integer taskTime) throws ParseException {
		// TODO Auto-generated method stub
		return userTaskDao.shaixuan(userId, taskStatus, taskType,taskTime);
	}
	public Pager queryAppTaskPage(int pageSize,int page,String search){
		return userTaskDao.queryAppTaskPage(pageSize, page, search);
				
	}
	
	public List<TaskScreenshot> queryShotList(UserTask userTask){
		return userTaskDao.queryShotList(userTask);
	}
	
    public UserTask queryUserTaskInfo(long userTaskId){
    	return userTaskDao.queryUserTaskInfo(userTaskId);
    }
    /**
     * 获取订单编号
     */
  	public String getOrderCode(){
  		return userTaskDao.getOrderCode();
  	}
	/**
  	 * 修改
  	 */
  	public int updateUserTask(UserTask userTask){
  		return userTaskDao.updateUserTask(userTask);
  	}
  	/**
	 * 查询所有的已审核任务
	 */
	public Pager queryAllTaskPage(int pageSize,int page,String search){
		return userTaskDao.queryAllTaskPage(pageSize, page, search);
	}
	/**
	 * 查询成功的已审核任务
	 */
	public Pager queryAllTaskPageChengong(int pageSize,int page,String search,int zhuangtia,String modile,long hehuoId,long dailiId,long yuanId){
		return userTaskDao.queryAllTaskPageChengong(pageSize, page, search,zhuangtia,modile,hehuoId,dailiId,yuanId);
	}
	/**
	 * 查询成功的已审核任务收益
	 */
	public Double queryAllTaskshouyi(String search,int zhuangtia,String modile,long hehuoId,long dailiId,long yuanId){
		return userTaskDao.queryAllTaskshouyi( search,zhuangtia,modile,hehuoId,dailiId,yuanId);
	}
	/**
	 * 批量删除
	 */
	public int deleteUserTask(String numbers){
		return userTaskDao.deleteUserTask(numbers);
	}
	/**
	 * 删除
	 */
	public int deleteTask(long userTaskId){
		return userTaskDao.deleteTask(userTaskId);
	}
	/**
	 * 删除截图
	 */
	public int deleteScreenshot(long userTaskId){
		return userTaskDao.deleteTask(userTaskId);
	}
	
	@Override
	public int updateUserTaskUploadTime(UserTask userTask) {
		// TODO Auto-generated method stub
		return userTaskDao.updateUserTaskUploadTime(userTask);
	}
	
	@Override
	public int updateUserTaskByMessage(String content, String reply) {
		// TODO Auto-generated method stub
		return userTaskDao.updateUserTaskByMessage(content, reply);
	}
	
	@Override
	public UserTask queryUserTaskByReply(String content, String originalID) {
		// TODO Auto-generated method stub
		return userTaskDao.queryUserTaskByReply(content, originalID);
	}
	public List queryGuanZhuUser(long taskId){
		return userTaskDao.queryGuanZhuUser(taskId);
	}

	@Override
	public List<UserTask> queryUserTasks(long taskId, long userId) {
		
		return userTaskDao.queryUserTasks(taskId, userId);
	}

	@Override
	public Pager queryMyTask(int pageSize, int page,long userId) {
		// TODO Auto-generated method stub
		return userTaskDao.queryMyTask(pageSize, page, userId);
	}

	@Override
	public Pager queryMyTaskfy(int pageSize, int page, long userId,
			Integer taskStatus, Integer taskType, Integer taskPrice) {
		// TODO Auto-generated method stub
		return userTaskDao.queryMyTaskfy(pageSize, page, userId, taskStatus, taskType, taskPrice);
	}
		//分页查询团队奖金
	@Override
	public Pager queryTaskPagetuangdui(int pageSize, int page, User user) {
		// TODO Auto-generated method stub
		return userTaskDao.queryTaskPagetuangdui(pageSize, page, user);
	}
	//分页查询团队奖金
	@Override
	public Pager queryTaskMyyj(int pageSize, int page, long userId) {
		// TODO Auto-generated method stub
		return userTaskDao.queryTaskMyyj(pageSize, page, userId);
	}

	@Override
	public Pager queryMyTaskZt(int pageSize, int page, long userId,
			Integer taskStatus, Integer taskType, Integer taskPrice) {
		// TODO Auto-generated method stub
		return userTaskDao.queryMyTaskZt(pageSize, page, userId, taskStatus, taskType, taskPrice);
	}

	@Override
	public List<UserTask> queryAllTaskdaisheng() {
		
		return userTaskDao.queryAllTaskdaisheng();
	}
}
