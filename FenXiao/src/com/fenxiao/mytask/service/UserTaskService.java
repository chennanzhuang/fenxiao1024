package com.fenxiao.mytask.service;

import java.text.ParseException;
import java.util.List;

import com.fenxiao.common.Pager;
import com.fenxiao.mytask.entity.UserTask;
import com.fenxiao.task.entity.TaskScreenshot;
import com.fenxiao.user.entity.User;

public interface UserTaskService {
	/**
	 * 查询所有状态为待审
	 */
	public List<UserTask> queryAllTaskdaisheng();
	/**
	 * 查询成功的已审核任务
	 */
	public Pager queryAllTaskPageChengong(int pageSize,int page,String search,int zhuangtia,String modile,long hehuoId,long dailiId,long yuanId);
	/**
	 * 查询成功的已审核任务收益
	 */
	public Double queryAllTaskshouyi(String search,int zhuangtia,String modile,long hehuoId,long dailiId,long yuanId);
	 /* 分页查询状态数据
	 * */
	public Pager queryMyTaskZt(int pageSize,int page,long userId,Integer taskStatus,
			Integer taskType, Integer taskPrice);
	//分页查询任务佣金
			public Pager queryTaskMyyj(int pageSize,int page,long userId);
	//分页查询团队佣金
			public Pager queryTaskPagetuangdui(int pageSize,int page,User user);
	//分页查询推广佣金
		public Pager queryTaskPagetuiguan(int pageSize,int page,User user);
	/**
	 * 分页查询我的任务
	 */
	public Pager queryMyTask(int pageSize,int page,long userId);
	/**
	 * 分页查询的数据
	 * */
	public Pager queryMyTaskfy(int pageSize,int page,long userId,Integer taskStatus,
			Integer taskType,Integer taskPrice);
	public int addUserTask(UserTask userTask);
	
	public UserTask queryUaserTask(long taskId,long userId);
	//查询是否存在完成任务
		public List<UserTask> queryUserTasks(long taskId,long userId);
	
	public List<UserTask> queryPeopleTask(long userId);
	
	public UserTask queryUserTaskById(long userTaskId);
	
	public int updateUserTaskAppRegisterName(long userTaskId,String appRegisterName);
	
	public List<UserTask> queryThoseTaskCompleted(long userId);
	
	public int updateWeChatReply(String content,String weChatOfficialAccountReply,String originalID);
	
	public UserTask queryUserTaskByReply(String content,String originalID);
	
	public List queryTeamTaskForTheMan(User user);
	
	public List<UserTask> shaixuan(long userId,Integer taskStatus,Integer taskType,Integer taskTime) throws ParseException;
	
	public int updateUserTaskUploadTime(UserTask userTask);
	/**
	 * 查询所有的APP审核任务
	 */
	public Pager queryAppTaskPage(int pageSize,int page,String search);

	 /**
		 * 查询任务截图
		 */
		public List<TaskScreenshot> queryShotList(UserTask userTask);
	    /**
	     * 详情
	     */
	    public UserTask queryUserTaskInfo(long userTaskId);
    
    /**
     * 获取订单编号
     */
  	public String getOrderCode();
	/**
  	 * 修改
  	 */
  	public int updateUserTask(UserTask userTask);
  	/**
	 * 查询所有的已审核任务
	 */
	public Pager queryAllTaskPage(int pageSize,int page,String search);
	/**
	 * 批量删除
	 */
	public int deleteUserTask(String numbers);
	/**
	 * 删除
	 */
	public int deleteTask(long userTaskId);
	/**
	 * 删除截图
	 */
	public int deleteScreenshot(long userTaskId);
	
	public int updateUserTaskByMessage(String content,String reply);
	public List queryGuanZhuUser(long taskId);
}
