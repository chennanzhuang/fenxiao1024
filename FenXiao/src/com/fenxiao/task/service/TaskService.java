package com.fenxiao.task.service;

import java.util.List;

import com.fenxiao.common.Pager;
import com.fenxiao.task.entity.Task;
import com.fenxiao.task.entity.TaskScreenshot;
import com.fenxiao.user.entity.User;

public interface TaskService {
	/**
	  * 分页查询任务需求列表
	  */
	public Pager queryTaskxq(int pageSize,int page,long user);
	/**
	  * 分页查询正在进行的任务
	  */
	public Pager queryTaskStar(int pageSize,int page,String search,int taskType, int bounts,String area,User user);
	/**
	 * 分页点亮查询
	 * */
	public Pager queryTaskdl(User user,int pageSize,int page,String search);
	/**
	 * 分页完成
	 * */
	public Pager queryTaskwc(User user,int pageSize,int page,String search);
	/**
	  * 分页查询需求
	  */
	public Pager findTaskPage(int pageSize,int page,String search);
	
	/**
	 * 更新
	 */
	public int updateTask(Task task);
	/**
	 * 查询详情
	 */
	public Task queryTaskInfo(long taskId);
	/**
	 * 添加任务
	 */
	public int addTask(Task task);
	/**
	 * 批量删除
	 */
	public int deleteAllTask(String number);
	/**
	 * 删除
	 */
	public int deleteTask(long taskId);
	/**
	  * 分页查询任务
	  */
  public Pager queryTaskPage(int pageSize,int page,String search);
  /**
	  * 分页查询结束任务
	  */
	public Pager queryEndTaskPage(int pageSize,int page,String search);
	/**
	 * 分页查询审核的app的任务
	 */
	public Pager queryAppTaskPage(int pageSize,int page,String search);
	
	public List<Task> getTaskAll(User user,Integer taskStatus,Integer taskType);

	public List<Task> getThoseTaskscompleted(long userId);

	public Task queryTaskById(long taskId);

	public int updateTaskById(long taskId,String appRegisterName);
	//需求列表
	public List<Task> queryTaskList(long userid);
	//编码
    public String getTaskCode(); 

public Task queryTaskByContent(String content,String fromUserName);

public int updateTaskByMessage(String content,String originalID);

public int updateTaskByMessage(long taskId,double taskBonus);
/**
 * 查询正在进行中的任务
 */
public List<Task> onTaskList(int taskType, int bounts,String area,User user);
}
