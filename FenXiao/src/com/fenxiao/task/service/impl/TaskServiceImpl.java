package com.fenxiao.task.service.impl;

import java.util.List;

import com.fenxiao.common.Pager;
import com.fenxiao.task.dao.TaskDao;
import com.fenxiao.task.entity.Task;
import com.fenxiao.task.entity.TaskScreenshot;
import com.fenxiao.task.service.TaskService;
import com.fenxiao.user.entity.User;

public class TaskServiceImpl implements TaskService {

	private TaskDao taskDao;

	public TaskDao getTaskDao() {
		return taskDao;
	}

	public void setTaskDao(TaskDao taskDao) {
		this.taskDao = taskDao;
	}

	/**
	 * 分页查询需求
	 */
	public Pager findTaskPage(int pageSize, int page, String search) {

		return taskDao.findTaskPage(pageSize, page, search);
	}

	/**
	 * 更新
	 */
	public int updateTask(Task task) {
		return taskDao.updateTask(task);
	}

	/**
	 * 查询详情
	 */
	public Task queryTaskInfo(long taskId) {

		return taskDao.queryTaskInfo(taskId);
	}

	/**
	 * 添加任务
	 */
	public int addTask(Task task) {
		return taskDao.addTask(task);
	}

	/**
	 * 批量删除
	 */
	public int deleteAllTask(String number) {
		return taskDao.deleteAllTask(number);
	}

	/**
	 * 删除
	 */
	public int deleteTask(long taskId) {
		return taskDao.deleteTask(taskId);
	}

	/**
	 * 分页查询任务
	 */
	public Pager queryTaskPage(int pageSize, int page, String search) {
		return taskDao.queryTaskPage(pageSize, page, search);
	}

	/**
	 * 分页查询单个完成任务
	 */
	public Pager queryTaskwc(User user, int pageSize, int page, String search) {
		return taskDao.queryTaskwc(user, pageSize, page, search);
	}

	/**
	 * 分页查询结束任务
	 */
	public Pager queryEndTaskPage(int pageSize, int page, String search) {
		return taskDao.queryEndTaskPage(pageSize, page, search);
	}

	/**
	 * 分页查询审核的app的任务
	 */
	public Pager queryAppTaskPage(int pageSize, int page, String search) {
		return taskDao.queryAppTaskPage(pageSize, page, search);
	}

	@Override
	public List<Task> getTaskAll(User user, Integer taskStatus, Integer taskType) {
		// TODO Auto-generated method stub
		return taskDao.getTaskAll(user, taskStatus, taskType);
	}

	@Override
	public List<Task> getThoseTaskscompleted(long userId) {
		// TODO Auto-generated method stub
		return taskDao.getThoseTasksCompleted(userId);
	}

	@Override
	public Task queryTaskById(long taskId) {
		// TODO Auto-generated method stub
		return taskDao.queryTaskById(taskId);
	}

	@Override
	public int updateTaskByMessage(String content, String originalID) {
		// TODO Auto-generated method stub
		return taskDao.updateTaskByMessage(content, originalID);
	}

	@Override
	public int updateTaskByMessage(long taskId, double taskBonus) {
		// TODO Auto-generated method stub
		return taskDao.updateTaskByMessage(taskId, taskBonus);
	}

	@Override
	public Task queryTaskByContent(String content, String fromUserName) {
		// TODO Auto-generated method stub
		return taskDao.queryTaskByContent(content, fromUserName);
	}

	@Override
	public int updateTaskById(long taskId, String appRegisterName) {
		// TODO Auto-generated method stub
		return taskDao.updateTaskById(taskId, appRegisterName);
	}

	// 需求列表
	public List<Task> queryTaskList(long userid) {
		return taskDao.queryTaskList(userid);
	}
	

	// 编码
	public String getTaskCode() {
		return taskDao.getTaskCode();
	}

	/**
	 * 查询正在进行中的任务
	 */
	public List<Task> onTaskList(int taskType, int bounts, String area,
			User user) {
		return taskDao.onTaskList(taskType, bounts, area, user);
	}

	/**
	 * 点亮任务查询
	 * */
	@Override
	public Pager queryTaskdl(User user, int pageSize, int page, String search) {
		// TODO Auto-generated method stub
		return taskDao.queryTaskdl(user, pageSize, page, search);
	}

	/**
	  * 分页查询正在进行的任务
	  */
	public Pager queryTaskStar(int pageSize,int page,String search,int taskType, int bounts,String area,User user) {
		// TODO Auto-generated method stub
		return taskDao.queryTaskStar(pageSize, page, search, taskType, bounts, area, user);
	}
	/**
	  * 分页查询任务需求列表
	  */
	public Pager queryTaskxq(int pageSize,int page,long user){
		return taskDao.queryTaskxq(pageSize, page, user);
	};
}
