package com.fenxiao.task.service.impl;

import com.fenxiao.task.dao.TaskImgScreenshotDao;
import com.fenxiao.task.entity.TaskScreenshot;
import com.fenxiao.task.service.TaskImgScreenshotService;

public class TaskImgScreenshotServiceImpl implements TaskImgScreenshotService {
	private TaskImgScreenshotDao  taskImgScreenshotDao;

	@Override
	public int addTaskImgScreenshot(TaskScreenshot taskScreenshot) {
		// TODO Auto-generated method stub
		return taskImgScreenshotDao.addTakScreenShot(taskScreenshot);
	}

	public TaskImgScreenshotDao getTaskImgScreenshotDao() {
		return taskImgScreenshotDao;
	}

	public void setTaskImgScreenshotDao(TaskImgScreenshotDao taskImgScreenshotDao) {
		this.taskImgScreenshotDao = taskImgScreenshotDao;
	}

}
