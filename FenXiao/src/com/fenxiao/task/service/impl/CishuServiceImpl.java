package com.fenxiao.task.service.impl;



import java.util.List;

import com.fenxiao.task.dao.CishuDao;
import com.fenxiao.task.entity.cishu;
import com.fenxiao.task.service.CishuService;

public class CishuServiceImpl implements CishuService{
		private  CishuDao  cishuDao;
		
	public CishuDao getCishuDao() {
			return cishuDao;
		}

		public void setCishuDao(CishuDao cishuDao) {
			this.cishuDao = cishuDao;
		}

	@Override
	public cishu queryAll() {
		   
		// TODO Auto-generated method stub
		return cishuDao.queryAll();
	}

	@Override
	public int updatecishu(cishu cishu) {
		cishuDao.updatecishu(cishu);
		return 1;
	}

	@Override
	public int updateliou(cishu cishu) {
		 cishuDao.updateliou(cishu);
		return 1;
	}

}
