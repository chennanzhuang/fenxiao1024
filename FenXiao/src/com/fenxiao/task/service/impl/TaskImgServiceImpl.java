package com.fenxiao.task.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fenxiao.task.dao.TaskImgDao;
import com.fenxiao.task.entity.TaskImg;
import com.fenxiao.task.service.TaskImgService;

@Service
@Transactional
public class TaskImgServiceImpl implements TaskImgService {
	@Resource
    private TaskImgDao taskImgDao;

	@Override
	public int addTaskImg(TaskImg taskImg) {
		// TODO Auto-generated method stub
		return taskImgDao.addTaskImg(taskImg);
	}
	
	@Override
	public List<TaskImg> queryTaskImg(long taskId) {
		// TODO Auto-generated method stub
		return taskImgDao.queryTaskImg(taskId);
	}

	public TaskImgDao getTaskImgDao() {
		return taskImgDao;
	}

	public void setTaskImgDao(TaskImgDao taskImgDao) {
		this.taskImgDao = taskImgDao;
	}

}
