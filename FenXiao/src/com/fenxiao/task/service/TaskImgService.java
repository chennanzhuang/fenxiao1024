package com.fenxiao.task.service;

import java.util.List;

import com.fenxiao.task.entity.TaskImg;

public interface TaskImgService {
	public int addTaskImg(TaskImg taskImg);
	
	public List<TaskImg> queryTaskImg(long taskId);

}
