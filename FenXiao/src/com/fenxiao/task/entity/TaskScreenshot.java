package com.fenxiao.task.entity;

import com.fenxiao.mytask.entity.UserTask;
// default package

/**
 *截图
 * TaskScreenshot entity. @author MyEclipse Persistence Tools
 */

public class TaskScreenshot implements java.io.Serializable {

	// Fields

	private Long screenshotId;
	private UserTask userTask;//我的任务
	private String screenshotUrl;//URL
	private String screenshotTime;//时间

	// Constructors

	/** default constructor */
	public TaskScreenshot() {
	}

	/** full constructor */
	public TaskScreenshot(UserTask userTask, String screenshotUrl,
			String screenshotTime) {
		this.userTask = userTask;
		this.screenshotUrl = screenshotUrl;
		this.screenshotTime = screenshotTime;
	}

	// Property accessors

	public Long getScreenshotId() {
		return this.screenshotId;
	}

	public void setScreenshotId(Long screenshotId) {
		this.screenshotId = screenshotId;
	}

	public UserTask getUserTask() {
		return this.userTask;
	}

	public void setUserTask(UserTask userTask) {
		this.userTask = userTask;
	}

	public String getScreenshotUrl() {
		return this.screenshotUrl;
	}

	public void setScreenshotUrl(String screenshotUrl) {
		this.screenshotUrl = screenshotUrl;
	}

	public String getScreenshotTime() {
		return this.screenshotTime;
	}

	public void setScreenshotTime(String screenshotTime) {
		this.screenshotTime = screenshotTime;
	}

}