package com.fenxiao.task.entity;
// default package

/**
 *任务图片
 * TaskImg entity. @author MyEclipse Persistence Tools
 */

public class TaskImg implements java.io.Serializable {

	// Fields

	private Long id;
	private Task task;//需求
	private String imgUrl;//地址
	private String imgTime;//时间

	// Constructors

	/** default constructor */
	public TaskImg() {
	}

	

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Task getTask() {
		return this.task;
	}

	public void setTask(Task task) {
		this.task = task;
	}


	public String getImgTime() {
		return this.imgTime;
	}

	public void setImgTime(String imgTime) {
		this.imgTime = imgTime;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

}