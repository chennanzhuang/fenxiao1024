package com.fenxiao.task.entity;
// default package

import java.util.HashSet;
import java.util.Set;

import com.fenxiao.user.entity.User;

/**
 * 任务表
 * Task entity. @author MyEclipse Persistence Tools
 */

public class Task implements java.io.Serializable {
	// Fields

	private Long taskId;
	private User user;//会员
	private String taskTitle;//标题
	private String taskLogo;//logo
	private String taskBriefing;//简介
	private Double taskBonus;//任务奖金
	private Double taskTotalBonus;//任务总奖金
	private Integer taskNumber;//任务完成数
	private String taskBeginTime;//开始时间
	private String taskEndTime;//结束时间
	private String taskResidualTime;//剩余时间
	private Integer taskStatus;//任务状态（0、未支付 1、正在进行中，2，已退款 3，已结束,4,待审核）
	private Integer taskType;//任务类型（1、关注公众号2、广告宣传3、app下载）
	private String taskCode;//任务编码
	private Integer taskTotalNumber;//任务总数
	private Double taskSBonus;//剩余金额
	private Double taskBonus1;//普通金额
//	private Integer taskSurplus;//任务剩余数
	private String mobilePhone;//手机号
	private String taskContacts;//联系人
	private Double referrerPercentage;//引荐人百分比
	private Double headquartersPercentage;//总部百分比
	private Double agentPercentage;//代理商百分比
	private Double memberPercentage;//会员百分比
	private String dateNum;//总天数
	private int auditStatus;//审核状态
	private long taskSustainedDay;
	private long taskSustainedHour;
	private long taskSustainedMinute;
	private String randomCode;
	private Integer taskRemainderNumber;//任务剩余数
	private String  appAndroidUrl;//安卓地址
	private String  appIOSUrl;//IOS地址guan
	private String taskAddress;//地址（废弃）
	private String adUrl;//广告url
	private String appRegisterName;
	private Set taskImgs = new HashSet(0);//
	private String weChatOfficialAccountReply;
    private String originalID;//原始id
    private String pubTime;//发布时间
    private String taskInfo;//详情
    private String taskOr;//二维码
    private  String province;//省
    private String city;//市
    private String area;//区
    private String  weChat;//公众号微信号
    private Double heji;//合计
    private Double shouxu;//手续费
    private Integer shengc ;//需要传任务

	// Constructors

	

	/** default constructor */
	public Task() {
		
		
	}

	public Integer getShengc() {
		return shengc;
	}

	public void setShengc(Integer shengc) {
		this.shengc = shengc;
	}

	public Double getHeji() {
		return heji;
	}

	public void setHeji(Double heji) {
		this.heji = heji;
	}

	public Double getShouxu() {
		return shouxu;
	}

	public void setShouxu(Double shouxu) {
		this.shouxu = shouxu;
	}

	/** full constructor */
	public Task(User user, String taskTitle, String taskLogo,
			String taskBriefing, Double taskBonus, Integer taskNumber,
			String taskBeginTime, String taskEndTime, String taskResidualTime,
			Integer taskStatus, Integer taskType, String taskCode,
			Integer taskTotalNumber, String mobilePhone,
			String taskContacts, Double referrerPercentage,
			Double headquartersPercentage, Double agentPercentage,
			Double memberPercentage,Integer auditStatus, Set taskImgs,
			String pubTime,String dateNum,String taskInfo,String  appAndroidUrl,
			String  appIOSUrl,String taskOr,Double taskTotalBonus,Double taskSBonus,Double taskBonus1
			,String province,String city,String area,String adUrl,String  weChat,String originalID) {
		this.user = user;
		this.taskTitle = taskTitle;
		this.taskLogo = taskLogo;
		this.taskBriefing = taskBriefing;
		this.taskBonus = taskBonus;
		this.taskNumber = taskNumber;
		this.taskBeginTime = taskBeginTime;
		this.taskEndTime = taskEndTime;
		this.taskResidualTime = taskResidualTime;
		this.taskStatus = taskStatus;
		this.taskType = taskType;
		this.taskCode = taskCode; 
		this.taskTotalNumber = taskTotalNumber;
		//this.taskSurplus = taskSurplus;
		this.mobilePhone = mobilePhone;
		this.taskContacts = taskContacts;
		this.referrerPercentage = referrerPercentage;
		this.headquartersPercentage = headquartersPercentage;
		this.agentPercentage = agentPercentage;
		this.memberPercentage = memberPercentage;
		this.auditStatus=auditStatus;
		this.taskImgs = taskImgs;
		this.pubTime=pubTime;
		this.dateNum=dateNum;
		this.taskInfo=taskInfo;
		this.appAndroidUrl=appAndroidUrl;
		this.appIOSUrl=appIOSUrl;
		this.taskOr=taskOr;
		this.taskTotalBonus=taskTotalBonus;
		this.taskSBonus=taskSBonus;
		this.taskBonus1=taskBonus1;
		this.province=province;
		this.city=city;
		this.area=area;
		this.adUrl=adUrl;
		this.weChat=weChat;
		this.originalID=originalID;
	}

	// Property accessors

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTaskTitle() {
		return this.taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public String getTaskLogo() {
		return this.taskLogo;
	}

	public void setTaskLogo(String taskLogo) {
		this.taskLogo = taskLogo;
	}

	public String getTaskBriefing() {
		return this.taskBriefing;
	}

	public void setTaskBriefing(String taskBriefing) {
		this.taskBriefing = taskBriefing;
	}

	public Double getTaskBonus() {
		return this.taskBonus;
	}

	public void setTaskBonus(Double taskBonus) {
		this.taskBonus = taskBonus;
	}

	public Integer getTaskNumber() {
		return this.taskNumber;
	}

	public void setTaskNumber(Integer taskNumber) {
		this.taskNumber = taskNumber;
	}

	public String getTaskBeginTime() {
		return this.taskBeginTime;
	}

	public void setTaskBeginTime(String taskBeginTime) {
		this.taskBeginTime = taskBeginTime;
	}

	public String getTaskEndTime() {
		return this.taskEndTime;
	}

	public void setTaskEndTime(String taskEndTime) {
		this.taskEndTime = taskEndTime;
	}

	public String getTaskResidualTime() {
		return this.taskResidualTime;
	}

	public void setTaskResidualTime(String taskResidualTime) {
		this.taskResidualTime = taskResidualTime;
	}

	public Integer getTaskStatus() {
		return this.taskStatus;
	}

	public void setTaskStatus(Integer taskStatus) {
		this.taskStatus = taskStatus;
	}

	public Integer getTaskType() {
		return this.taskType;
	}

	public void setTaskType(Integer taskType) {
		this.taskType = taskType;
	}

	public String getTaskCode() {
		return this.taskCode;
	}

	public void setTaskCode(String taskCode) {
		this.taskCode = taskCode;
	}

	public Integer getTaskTotalNumber() {
		return this.taskTotalNumber;
	}

	public void setTaskTotalNumber(Integer taskTotalNumber) {
		this.taskTotalNumber = taskTotalNumber;
	}

	/*public Integer getTaskSurplus() {
		return this.taskSurplus;
	}

	public void setTaskSurplus(Integer taskSurplus) {
		this.taskSurplus = taskSurplus;
	}
*/
	public String getMobilePhone() {
		return this.mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getTaskContacts() {
		return this.taskContacts;
	}

	public void setTaskContacts(String taskContacts) {
		this.taskContacts = taskContacts;
	}

	public Double getReferrerPercentage() {
		return this.referrerPercentage;
	}

	public void setReferrerPercentage(Double referrerPercentage) {
		this.referrerPercentage = referrerPercentage;
	}

	public Double getHeadquartersPercentage() {
		return this.headquartersPercentage;
	}

	public void setHeadquartersPercentage(Double headquartersPercentage) {
		this.headquartersPercentage = headquartersPercentage;
	}

	public Double getAgentPercentage() {
		return this.agentPercentage;
	}

	public void setAgentPercentage(Double agentPercentage) {
		this.agentPercentage = agentPercentage;
	}

	public Double getMemberPercentage() {
		return this.memberPercentage;
	}

	public void setMemberPercentage(Double memberPercentage) {
		this.memberPercentage = memberPercentage;
	}

	public int getAuditStatus() {
		return auditStatus;
	}

	public void setAuditStatus(int auditStatus) {
		this.auditStatus = auditStatus;
	}

	public Set getTaskImgs() {
		return this.taskImgs;
	}

	public void setTaskImgs(Set taskImgs) {
		this.taskImgs = taskImgs;
	}

	public String getPubTime() {
		return pubTime;
	}

	public void setPubTime(String pubTime) {
		this.pubTime = pubTime;
	}

	

	public String getDateNum() {
		return dateNum;
	}

	public void setDateNum(String dateNum) {
		this.dateNum = dateNum;
	}

	public String getTaskInfo() {
		return taskInfo;
	}

	public void setTaskInfo(String taskInfo) {
		this.taskInfo = taskInfo;
	}

	public long getTaskSustainedDay() {
		return taskSustainedDay;
	}

	public void setTaskSustainedDay(long taskSustainedDay) {
		this.taskSustainedDay = taskSustainedDay;
	}

	public long getTaskSustainedHour() {
		return taskSustainedHour;
	}

	public void setTaskSustainedHour(long taskSustainedHour) {
		this.taskSustainedHour = taskSustainedHour;
	}

	public long getTaskSustainedMinute() {
		return taskSustainedMinute;
	}

	public void setTaskSustainedMinute(long taskSustainedMinute) {
		this.taskSustainedMinute = taskSustainedMinute;
	}

	public Integer getTaskRemainderNumber() {
		return taskRemainderNumber;
	}

	public void setTaskRemainderNumber(Integer taskRemainderNumber) {
		this.taskRemainderNumber = taskRemainderNumber;
	}

	public String getTaskAddress() {
		return taskAddress;
	}

	public void setTaskAddress(String taskAddress) {
		this.taskAddress = taskAddress;
	}

	public String getAppRegisterName() {
		return appRegisterName;
	}

	public void setAppRegisterName(String appRegisterName) {
		this.appRegisterName = appRegisterName;
	}


	public String getWeChatOfficialAccountReply() {
		return weChatOfficialAccountReply;
	}

	public void setWeChatOfficialAccountReply(String weChatOfficialAccountReply) {
		this.weChatOfficialAccountReply = weChatOfficialAccountReply;
	}

	public String getRandomCode() {
		return randomCode;
	}

	public void setRandomCode(String randomCode) {
		this.randomCode = randomCode;
	}

	

	public String getAppAndroidUrl() {
		return appAndroidUrl;
	}


	public void setAppAndroidUrl(String appAndroidUrl) {
		this.appAndroidUrl = appAndroidUrl;
	}

	public String getAppIOSUrl() {
		return appIOSUrl;
	}

	public void setAppIOSUrl(String appIOSUrl) {
		this.appIOSUrl = appIOSUrl;
	}

	public String getTaskOr() {
		return taskOr;
	}

	public void setTaskOr(String taskOr) {
		this.taskOr = taskOr;
	}

	public Double getTaskTotalBonus() {
		return taskTotalBonus;
	}

	public void setTaskTotalBonus(Double taskTotalBonus) {
		this.taskTotalBonus = taskTotalBonus;
	}

	public Double getTaskSBonus() {
		return taskSBonus;
	}

	public void setTaskSBonus(Double taskSBonus) {
		this.taskSBonus = taskSBonus;
	}

	public Double getTaskBonus1() {
		return taskBonus1;
	}

	public void setTaskBonus1(Double taskBonus1) {
		this.taskBonus1 = taskBonus1;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getAdUrl() {
		return adUrl;
	}

	public void setAdUrl(String adUrl) {
		this.adUrl = adUrl;
	}

	public String getWeChat() {
		return weChat;
	}

	public void setWeChat(String weChat) {
		this.weChat = weChat;
	}

	public String getOriginalID() {
		return originalID;
	}

	public void setOriginalID(String originalID) {
		this.originalID = originalID;
	}

}