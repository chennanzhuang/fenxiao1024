package com.fenxiao.task.dao;



import java.util.List;

import com.fenxiao.task.entity.cishu;

public interface CishuDao {
	//查询次数
	public cishu queryAll();
	//添加次数
	public int updatecishu(cishu cishu);
	//添加浏览次数
	public int updateliou(cishu cishu);
}
