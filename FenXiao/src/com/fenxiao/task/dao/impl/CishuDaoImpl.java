package com.fenxiao.task.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

import com.fenxiao.task.dao.CishuDao;
import com.fenxiao.task.entity.cishu;

public class CishuDaoImpl implements CishuDao{
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	@Override
	public cishu queryAll() {
	   String hql="select a from cishu a";
	   Query query=sessionFactory.getCurrentSession().createQuery(hql);
	        Object list = query.uniqueResult();
	       if(list!=null){
	    	   return  (cishu) list;
	       }else {
			return null;
		}
	    	
	}

	public int upcishu(Integer cishu) {
		String hql= "update cishu c set c.zhuche="+cishu;	
		return sessionFactory.getCurrentSession().createQuery(hql).executeUpdate();	
	}

	@Override
	public int updatecishu(cishu cishu) {
		String hql= "update cishu c set c.zhuche="+cishu.getZhuche();	
		
		return sessionFactory.getCurrentSession().createQuery(hql).executeUpdate();
	}

	@Override
	public int updateliou(cishu cishu) {
		String hql= "update cishu c set c.lioulang="+cishu.getLioulang();	
		
		return sessionFactory.getCurrentSession().createQuery(hql).executeUpdate();
	}

}
