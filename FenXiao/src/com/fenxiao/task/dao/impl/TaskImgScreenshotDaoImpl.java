package com.fenxiao.task.dao.impl;

import org.hibernate.SessionFactory;

import com.fenxiao.base.BaseDaoImpl;
import com.fenxiao.task.dao.TaskImgScreenshotDao;
import com.fenxiao.task.entity.TaskImg;
import com.fenxiao.task.entity.TaskScreenshot;

public class TaskImgScreenshotDaoImpl extends BaseDaoImpl<TaskScreenshot> implements TaskImgScreenshotDao {
	private  SessionFactory sessionFactory;
	@Override
	public int addTakScreenShot(TaskScreenshot taskScreenshot) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(taskScreenshot);
		return 1;
	}
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
