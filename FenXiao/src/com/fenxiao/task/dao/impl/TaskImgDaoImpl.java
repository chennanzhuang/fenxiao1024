package com.fenxiao.task.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

import com.fenxiao.base.BaseDaoImpl;
import com.fenxiao.task.dao.TaskImgDao;
import com.fenxiao.task.entity.TaskImg;
import com.fenxiao.user.entity.User;

public class TaskImgDaoImpl extends BaseDaoImpl<TaskImg> implements TaskImgDao {
	private  SessionFactory sessionFactory;
public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public int addTaskImg(TaskImg taskImg) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(taskImg);
		return 1;
	}
	
	@Override
	public List<TaskImg> queryTaskImg(long taskId) {
		// TODO Auto-generated method stub
		String hql="select t from TaskImg t where t.task.taskId="+taskId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<TaskImg> list=query.list();
		if(list!=null){
			return list;
		}else{
		return null;
	}
	}
	
	

}
