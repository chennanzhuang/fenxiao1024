package com.fenxiao.task.dao;

import java.util.List;

import com.fenxiao.task.entity.TaskImg;

public interface TaskImgDao {
	public int addTaskImg(TaskImg taskImg);

	public List<TaskImg> queryTaskImg(long taskId);
}
