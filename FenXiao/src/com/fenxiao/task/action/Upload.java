package com.fenxiao.task.action;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.fenxiao.mytask.entity.UserTask;
import com.fenxiao.mytask.service.UserTaskService;
import com.fenxiao.task.entity.TaskScreenshot;
import com.fenxiao.task.service.TaskImgScreenshotService;
import com.fenxiao.task.service.TaskService;

public class Upload extends HttpServlet {
	private TaskImgScreenshotService taskImgScreenshotService;
	private TaskService taskService;
	private UserTaskService userTaskService;
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String taskId=request.getParameter("taskId");
		String openId=request.getParameter("openId");
		/*String appRegisterName=request.getParameter("appRegisterName");
		String taskStatus=request.getParameter("taskStatus");*/
		
		
		String savePath = this.getServletConfig().getServletContext().getRealPath("/");
				
		savePath = savePath + "uploads/";
		String imageName = "";
		File f1 = new File(savePath);
		System.err.println(savePath+"----------------------");
		if (!f1.exists()) {
			f1.mkdirs();
		}
		DiskFileItemFactory fac = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(fac);
		upload.setHeaderEncoding("UTF-8");
		List fileList = null;
		try {
			fileList = upload.parseRequest(request);
		} catch (FileUploadException ex) {
			return;
		}
		
		Iterator<FileItem> it = fileList.iterator();
		String name = "";
		String extName = "";
		
		while (it.hasNext()) {
			FileItem item = it.next();
			if (!item.isFormField()) {
				name = item.getName();
				long size = item.getSize();
				String type = item.getContentType();
				System.out.println(size + " " + type);
				if (name == null || name.trim().equals("")) {
					continue;
				}
				// 扩展名格式：
				if (name.lastIndexOf(".") >= 0) {
					extName = name.substring(name.lastIndexOf("."));
				}
				File file = null;
				do {
					// 生成文件名：
					name = UUID.randomUUID().toString();
					file = new File(savePath + name + extName);
				} while (file.exists());
				File saveFile = new File(savePath + name + extName);
				
				
				try {
					item.write(saveFile);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		imageName ="uploads/" + name + extName;
		System.err.println(imageName+"----------------");
		
		 ServletContext servletContext = this.getServletContext();    
		 WebApplicationContext wac = null;     
		 wac = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);    
		 this.taskImgScreenshotService = (TaskImgScreenshotService) wac.getBean("taskImgScreenshotService");
		 this.taskService = (TaskService) wac.getBean("taskService");
		 this.userTaskService = (UserTaskService) wac.getBean("userTaskService");
		 TaskScreenshot taskScreenshot=new TaskScreenshot();
		 UserTask userTask=userTaskService.queryUserTaskById(Long.parseLong(taskId));
		// userTask.setUserTaskId(Long.parseLong(taskId));
		 userTask.setTaskStatus(5);
		 int uploadTime=userTask.getUploadTime();
		 userTask.setUploadTime(uploadTime+1);
		 userTaskService.updateUserTask(userTask);
		 taskScreenshot.setUserTask(userTask);
		 taskScreenshot.setScreenshotUrl(imageName);
		 Date date=new Date();
		 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 String time=sdf.format(date);
		 taskScreenshot.setScreenshotTime(time);
		 taskImgScreenshotService.addTaskImgScreenshot(taskScreenshot);
		 
		 request.getRequestDispatcher("/UserTaskAction_queryTaskForTheMan?openId="+openId).forward(request, response);
		 
	}
}
