package com.fenxiao.task.action;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.opensymphony.xwork2.ActionSupport;

public class YeMianAction extends ActionSupport{
	private String url;
	
	//获取页面跳转
	public String tiaozhuang(){
		Pattern p = Pattern.compile("http://"); 
		Matcher m = p.matcher(url); 
		if(!m.lookingAt()){
			url="http://"+url;
		}
		return SUCCESS;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}
