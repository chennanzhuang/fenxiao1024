package com.fenxiao.util;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.fenxiao.admin.dao.impl.AdminDaoImpl;
import com.fenxiao.admin.entity.Admin;
import com.fenxiao.admin.service.AdminService;
import com.fenxiao.admin.service.impl.AdminServiceImpl;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;
/**
 * 会员拦截器
 * @author Administrator
 *
 */
public class LoginInterceptor extends MethodFilterInterceptor {
 

	protected String doIntercept(ActionInvocation invocation) throws Exception {
		AdminService adminService = new AdminServiceImpl(); 
		HttpServletRequest request = ServletActionContext.getRequest();
		    HttpSession session = request.getSession();
	         Admin admin = (Admin) session.getAttribute("admin");  
	
	        // 如果没有登陆，或者登陆所有的用户名不是yuewei，都返回重新登陆  
	        String check = invocation.getAction().getClass().getSimpleName();
	        if(("AdminAction").equals(check)){	
	        	 if ( admin != null) {  
	                 System.out.println("test");  
	                 return invocation.invoke();  
	             }  
	        	 else{
	        		 
	        		 return "globalLogin"; 
	        	 }
	        
	        }
	    else if(admin != null ){
	        	return invocation.invoke();  
	        }
	        else
	        return "commenSession";  
		}  

}
