package com.fenxiao.util;

/**
 * 
 * 提示信息
 * 
 * @author ChenYong
 * 
 */
public class PromptMessage {
	/** 状态 */
	private String status;
	/** 错误代号 */
	private Integer error;
	/** 提示信息 */
	private String message;
	/** Url */
	private String url;

	// Constructor
	public PromptMessage() {
	}

	public PromptMessage(String status, String message, Integer error, String url) {
		this.status = status;
		this.message = message;
		this.error = error;
		this.url = url;
	}

	// Getters and Setters
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getError() {
		return error;
	}

	public void setError(Integer error) {
		this.error = error;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
