package com.fenxiao.util;

import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.fenxiao.admin.entity.Admin;
import com.fenxiao.user.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;
/**
 * 会员拦截器
 * @author Administrator
 *
 */
public class LogonInterceptor2 extends MethodFilterInterceptor {
 

	protected String doIntercept(ActionInvocation invocation) throws Exception {
		 HttpServletRequest request = ServletActionContext.getRequest();
		    HttpSession session = request.getSession();
//		ActionContext ctx = invocation.getInvocationContext(); 
//	       Map session = ctx.getSession();  
	               Object user = session.getAttribute("user");  
	             Cookie[] c = request.getCookies();
	             String user1=null;
	              if(user==null&&c!=null&&!"".equals(c)){
	            	  for (int i = 0; i < c.length; i++) {
	            		  if("userid".equals(c[i].getName())){ 
	            			  if(c[i].getValue()!=null&&!"".equals(c[i].getValue())){ 
	            				  user1 = c[i].getValue();
	            				  user=c[i].getValue();
	            				  session.setAttribute("user",Long.parseLong(user1));
	  
	            			  }
	            		  }
					}
	              }
	            	  // 如果没有登陆，或者登陆所有的用户名不是yuewei，都返回重新登陆  
	      	        String check = invocation.getAction().getClass().getSimpleName();
	      	        if(("UserAction").equals(check)){
	      	        	if ( user != null&&!"".equals(user)) {  
	      	        		System.out.println("test");  
	      	        		return invocation.invoke();  
	      	        	}  
	      	        	else{
	      	        		
	      	        		return "globalLogin"; 
	      	        	}
	      	        	
	      	        }
	      	        else if(user != null&&!"".equals(user) ){
	      	        	return invocation.invoke();  
	      	        }
	      	        else
	      	        	return "commenSession";  
	      	}
	        //User user = (User)session.get("user");
}

