package com.fenxiao.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;



import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class Tools {
	public static String recomCodePrefix = "rec"; //邀请码前缀
	public static String recomCodeSuffix = "00000001"; //邀请码后缀
	
	public static String getRandomFileName() {  
        SimpleDateFormat simpleDateFormat;  
        simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");  
        Date date = new Date();  
        String str = simpleDateFormat.format(date);  
        Random random = new Random();  
        int rannum = (int) (random.nextDouble() * (99999 - 10000 + 1)) + 1000;// 获取4位随机数  
        return  str+rannum;// 当前时间  
	}  
	
	
	public static String gainKm(String m){
		if(m.length()>3){
			String temp=m.substring(0, m.length()-2);
		
		Double distance = 	Double.valueOf(temp);
		distance=distance/10;
		String km = String.valueOf(distance);
			return km+"km";
		}
		return m+"m";
	}
	
	

	public static String getBeforeDate(){
		String formatDate = null;
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1); //得到前?
		Date date = calendar.getTime();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		formatDate = df.format(date);
		
		return formatDate;
	}
	
	
	public static String getFormatDate() {
		String formatDate = null;
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		formatDate = df.format(date);
		return formatDate;
	}
	
	public static String getFormatDatebyDay() {
		String formatDate = null;
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		formatDate = df.format(date);
		return formatDate;
	}
	
	
	 private static final double EARTH_RADIUS = 6378.137;
	private static double rad(double d)
	{
	   return d * Math.PI / 180.0;
	}

	 /**
     * 根据两个位置的经纬度，来计算两地的距离（单位为KM�?
     * 参数为String类型
     * @param lat1 用户经度
     * @param lng1 用户纬度
     * @param lat2 商家经度
     * @param lng2 商家纬度
     * @return
     */
	   public static String getDistance2(String lat1Str, String lng1Str, String lat2Str, String lng2Str){
	       
		   Double lat1 = Double.parseDouble(lat1Str);
	        Double lng1 = Double.parseDouble(lng1Str);
	        Double lat2 = Double.parseDouble(lat2Str);
	        Double lng2 = Double.parseDouble(lng2Str);
		   
		   double radLat1 = Math.toRadians(lat1);
	        double radLat2 = Math.toRadians(lat2);
	        double a = radLat1 - radLat2;
	        double b = Math.toRadians(lng1) - Math.toRadians(lng2);
	        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(radLat1)
	                * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
	        s = s * 6378137.0;// 取WGS84标准参�?椭球中的地球长半�?单位:m)
	        s = Math.round(s * 10000) / 10000;
	        String distance =String.valueOf(s);
	        distance = distance.substring(0, distance.indexOf("."));
	        return distance;
	    }
	
	
	
	
	 public static String getDistance(String lat1Str, String lng1Str, String lat2Str, String lng2Str) {
	        Double lat1 = Double.parseDouble(lat1Str);
	        Double lng1 = Double.parseDouble(lng1Str);
	        Double lat2 = Double.parseDouble(lat2Str);
	        Double lng2 = Double.parseDouble(lng2Str);
		
	   double radLat1 = rad(lat1);
	   double radLat2 = rad(lat2);
	   double a = radLat1 - radLat2;
	   double b = rad(lng1) - rad(lng2);
	   double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) + 
	    Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)));
	   s = s * EARTH_RADIUS;
	   s = Math.round(s * 10000) / 10000;
	   String distanceStr = s+"";
//	   distanceStr = distanceStr.
//           substring(0, distanceStr.indexOf("."));
//      
	   return distanceStr;
	}
	 
	 public static String getRandomString() { //length表示生成字符串的长度  
		    String base = "abcdefghijklmnopqrstuvwxyz0123456789";     
		    Random random = new Random();     
		    StringBuffer sb = new StringBuffer();     
		    for (int i = 0; i < 20; i++) {     
		        int number = random.nextInt(base.length());     
		        sb.append(base.charAt(number));     
		    }     
		    return sb.toString();     
		 }
}
