package com.fenxiao.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderNumber {

	public static String recomCodePrefix = "rec"; //邀请码前缀
	public static String recomCodeSuffix = "00000001"; //邀请码后缀
	public static String taskCodePrefix = "tc"; //任务编码前缀
	public static String taskCodeSuffix = "00000001"; //任务编码后缀
	public static String orderCodePrefix = "oc"; //任务编码前缀
	public static String orderCodeSuffix = "00000001"; //任务编码后缀
	
	
	
	public static String getFormatDate() {
		String formatDate = null;
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		formatDate = df.format(date);
		return formatDate;
	}
    
}
