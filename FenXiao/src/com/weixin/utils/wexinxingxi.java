package com.weixin.utils;

import net.sf.json.JSONObject;

import com.fenxiao.user.entity.User;

public class wexinxingxi {
	/**
	   * 获取用户信息
	   * 
	   * @param accessToken 接口访问凭证
	   * @param openId 用户标识
	   * @return WeixinUserInfo
	   */
	  public static User getUserInfo(String accessToken, String openId) {
		  User weixin = null;
	    // 拼接请求地址
	    String requestUrl = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID";
	    requestUrl = requestUrl.replace("ACCESS_TOKEN", accessToken).replace("OPENID", openId);
	    // 获取用户信息
	    JSONObject jsonObject = Utils.httpsRequest(requestUrl, "GET", null);
	    if (null != jsonObject) {
	      try {
	    	  weixin = new User();
//	        // 关注状态（1是关注，0是未关注），未关注时获取不到其余信息
//	    	  User.setSubscribe(jsonObject.getInt("subscribe"));
	        // 昵称
    	  weixin.setNickName(jsonObject.getString("nickname"));
	        // 用户的性别（1是男性，2是女性，0是未知）
	    	  weixin.setSex(jsonObject.getInt("sex"));
	        // 用户头像
	    	  weixin.setUserPhoto(jsonObject.getString("headimgurl"));
	      } catch (Exception e) {
	       e.printStackTrace();
	      }
	    }
	    return weixin;
	  }
}
