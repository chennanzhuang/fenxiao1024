<%@ page language="java" contentType="text/html; charset=GBK" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  	<head>
		<title>Uploadify</title>
		<link href="css/default.css" rel="stylesheet" type="text/css" />
		<link href="css/uploadify.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="js/swfobject.js"></script>
		<script type="text/javascript" src="js/jquery.uploadify.v2.0.1.js"></script>
	</head>
	<body>
		<div id="fileQueue"></div>
		<input type="file" name="uploadify" id="uploadify" />
		<p>
		<!-- 
		<a href="javascript:jQuery('#uploadify').uploadifyUpload()">开始上传</a>&nbsp;
		 -->
		<a href="javascript:jQuery('#uploadify').uploadifyClearQueue()">取消所有上传</a>
		</p>
		<script type="text/javascript">
		$(document).ready(function() {
			$("#uploadify").uploadify({
				'uploader'       : 'uploadify.swf',
				/* 'script'         : 'Upload_doUpload', */
				'script'         : 'servlet/Upload?name=yangxiang',
				'cancelImg'      : 'images/cancel.png',
				'folder'         : 'uploads',
				'queueID'        : 'fileQueue',
				'auto'           : true,
				'multi'          : true,
				'simUploadLimit' : 2,
				'buttonText'	 : 'BROWSE'//,
			});
		});
		</script>
	</body>
</html>