<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>提现审核</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  <link rel="stylesheet" type="text/css" href="jsp/css/css.css">
  <script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="jsp/layer/layer.js"></script>
</head>
<script type="text/javascript">
$(function (){
	  $("#sub").click(function(){
		  $(".form").attr("action","PresentAction_AuditPresentSuccess").submit();
	  });  
	  $("#ref").click(function(){
		  $(".form").attr("action","PresentAction_AuditPresentError").submit();
	  });
})

</script>
<body>


<div class="nav">
	<div class="nav_title">
    	<h2><img class="nav_img" src="jsp/img/tab.gif" /><a class="nav_a" href="#">提现审核</a></h2>
    </div>
</div>

<form action="" method="post" class="form" name="form1" enctype="multipart/form-data">
<input type="hidden" name="admin.adminId" value="${session.admin.adminId}">
<input type="hidden" name="user.id" id="userid" size="40" value="${param.user.id}" />
<input type="hidden" name="presentRecord.presentId" id="presentId" size="40" value="${presentRecord.presentId}" />

<div class="list">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="details">
        <tbody>     
	      <tr>
	      	<td width="14%"><label class="label">用户名：</label></td>
	      	<td width="86%"><input type="text" name="username" id="username" size="40" value="${presentRecord.user.username}" readonly="readonly"/></td>

	      </tr>
          <tr>
	      	<td width="14%"><label class="label">开户银行：</label></td>
	      	<td width="86%"><input type="text" name="bankName" id="bankName" size="40" value="${bankcard.bankName}" readonly="readonly"/></td>
	      </tr>
          <tr>
	      	<td width="14%"><label class="label">卡号：</label></td>
	      	<td width="86%"><input type="text" name="cardCode" id="cardCode" size="40" value="${bankcard.cardCode}" readonly="readonly"/></td>
	      </tr>
	       <tr>
	      	<td width="14%"><label class="label">开户名称：</label></td>
	      	<td width="86%"><input type="text" name="cardName" id="cardName" size="40" value="${bankcard.cardName}" readonly="readonly"/></td>
	      </tr>
          <tr>
	      	<td><label class="label">开户手机号：</label></td>
	      	<td><input type="text" name="cardMobile" id="cardMobile" size="40" value="${bankcard.cardMobile}" readonly="readonly"/></td>
	      </tr>
	      <tr>
	      	<td><label class="label">用户余额：</label></td>
	      	<td><input type="text" name="balance" id="balance" size="40" value="${presentRecord.user.balance}" readonly="readonly"/></td>
	      </tr>
	      <tr>
	      	<td><label class="label">提现金额：</label></td>
	      	<td><input type="text" name="presentMoney" id="presentMoney" size="40" value="${presentRecord.presentMoney}" readonly="readonly"/></td>
	      </tr>
	      <tr>
	      	<td><label class="label">手续费：</label></td>
	      	<td><input type="text" name="presentRecord.presentFee" id="presentFee" size="40" value="${presentRecord.presentFee }" /></td>
	      </tr> 
	      <tr>
	      	<td><label class="label">结果：</label></td>
	      	<td><input type="text" name="presentRecord.presentReson" id="presentReson" size="40" value="" /></td>
	      </tr> 
        </tbody>
  </table>
</div>

<div class="footer">
	<div class="save_button">
        <span class="span_button" id="sub">审核通过</span>
        <span class="span_button"   id="ref">审核不通过</span>
    </div>
</div>

</form>

</body>
<script type="text/javascript">
  var msg="${msg}";
  if(msg!=""){
	  layer.msg(msg, {
		    time: 10000,       
		  });
  }
</script>
</html>