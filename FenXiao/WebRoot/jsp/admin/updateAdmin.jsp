<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>修改管理员</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  <link rel="stylesheet" type="text/css" href="jsp/css/css.css">
  <script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="jsp/layer/layer.js"></script>
  <%-- <style type="text/css">
  .footer1{ bottom:0; height:45px; line-height:45px; 
            width:100%; text-align:center;}
.footer1 .save_button{ min-width:200px; margin-left:150px;}
.footer1 .span_button{ min-width:60px; display:block; float:left; height:35px;
                      line-height:35px; background:#146AC1; padding:0 30px;
                      margin-left:50px;margin-top:5px; color:#FFF;  border-radius:3px;
                      cursor:pointer; font-size:16px;}
  </style> --%>
</head>
<script type="text/javascript">
$(function (){
	
	  $("#sub").click(function(){
		  $(".form").submit();
	  });  
	 
})

</script>
<body>


<div class="nav">
	<div class="nav_title">
    	<h2><img class="nav_img" src="jsp/img/tab.gif" /><a class="nav_a" href="#">修改管理员</a></h2>
    </div>
</div>

<form action="AdminAction_updateAdmin" method="post" class="form" name="form1" enctype="multipart/form-data">
<input type="hidden" name="adminId" value="${session.admin.adminId}">
<input type="hidden" name="admin.adminId" id="adminId" value="${admin.adminId}">
<input type="hidden" name="admin.adminPass" id="adminPass" size="40" value="${admin.adminPass}" />

<div class="list">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="details">
        <tbody>
         <tr>
	      	<td width="14%"><label class="label">头像：</label></td>
	      	<td width="86%">
	      	<div id="preview1">
	      	<c:if test="${admin.adminPhoto==null}">
	      	<img alt="" src="jsp/img/111.png" name="" with="100" height="100">
	      	</c:if>
	      	<c:if test="${admin.adminPhoto!=null}">
	      	<img alt="" src="${admin.adminPhoto}" name="admin.adminPhoto" with="100" height="100">
	      	</c:if>
	      	
	      	</div>
	      	<input type="file" name="fileTest" onchange="preview1(this)" /></td>
             <script>  
       function preview1(file)  {
               var prevDiv = document.getElementById('preview1');
               if (file.files && file.files[0]){
                       var reader = new FileReader();
                         reader.onload = function(evt){
                        prevDiv.innerHTML = '<img src="' + evt.target.result + '" with="100" height="100"/>';
                             }  
                       reader.readAsDataURL(file.files[0]);
                         }else{
                  prevDiv.innerHTML = '<div class="img1" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src=\'' + file.value + '\')" ></div>';
    }
    }
 </script>	      
	      
	      </tr>
	      <tr>
	      	<td width="14%"><label class="label">账号：</label></td>
	      	<td width="86%"><input type="text" name="admin.adminAccount" id="adminAccount" size="40" value="${admin.adminAccount}" /></td>

	      </tr>
          <tr>
	      	<td width="14%"><label class="label">用户名：</label></td>
	      	<td width="86%"><input type="text" name="admin.adminName" id="adminName" size="40" value="${admin.adminName}" /></td>
	      </tr>
          <tr>
	      	<td><label class="label">性别：</label></td>
	      	<td>
	      	<c:if test="${admin.sex==0}">
	      		<input name="admin.sex" type="radio" id="sex" value="0" checked="checked" />
      	   男
      	      <input type="radio" name="admin.sex" id="sex2" value="1" />
   	        女
	      	</c:if>
	      		<c:if test="${admin.sex==1}">
	      		<input name="admin.sex" type="radio" id="sex" value="0" />
      	   男
      	      <input type="radio" name="admin.sex" id="sex2" value="1" checked="checked" />
   	        女
	      	</c:if>
	      </td>
	      </tr>     
          <tr>
	      	<td><label class="label">联系方式：</label></td>
	      	<td><input type="text" name="admin.adminPhone" id="adminPhone" size="40" value="${admin.adminPhone}" /></td>
	      </tr>
	      <tr>
	      	<td><label class="label">微信号：</label></td>
	      	<td><input type="text" name="admin.adminWeChatId" id="adminWeChatId" size="40" value="${admin.adminWeChatId }" /></td>
	      </tr>
	      <tr>
	      	<td><label class="label">二维码：</label></td>
	      	<td>	

	    
 <div id="preview">
	      	<c:if test="${admin.adminOr!=null}">
	      	<img alt="" src="${admin.adminOr}" name="admin.adminOr" with="100" height="100">
	      	</c:if>
 </div>
<input type="file" onchange="preview(this)" name="fileTest1" />
</td>
	      </tr>
	      
 <script>  
 function preview(file)
 {
 var prevDiv = document.getElementById('preview');
 if (file.files && file.files[0])
 {
 var reader = new FileReader();
 reader.onload = function(evt){
 prevDiv.innerHTML = '<img src="' + evt.target.result + '" with="100" height="100"/>';
}  
 reader.readAsDataURL(file.files[0]);
}
 else  
 {
 prevDiv.innerHTML = '<div class="img1" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src=\'' + file.value + '\') ; with:100px;height:100px" with="100" height="100"></div>';
 }
 }
 </script>	      
	      
	      <tr>
	      	<td><label class="label">推荐码：</label></td>
	      	<td><input type="text" name="admin.recommendationCode" id="recommendationCode" size="40" value="${admin.recommendationCode}" /></td>
	      </tr> 
	      
	      <tr>
	      	<td><label class="label">佣金：</label></td>
	      	<td><input type="text" name="admin.adminMoney" id="adminMoney" size="40" value="${admin.adminMoney}" /></td>
	      </tr> 
	        <tr>
	      	<td><label class="label">余额：</label></td>
	      	<td><input type="text" name="admin.adminBalance" id="adminBalance" size="40" value="${admin.adminBalance}" /></td>
	      </tr> 
	      <tr>
	      	<td><label class="label">角色：</label></td>
	      	<c:if test="${session.admin.adminRole.id==1}">
	      	<td>
	      	<c:if test="${admin.adminRole.id==1}">
	      	<input type="radio" name="roleId" id="role" size="40" value="1" checked="checked"/>超级管理员
	      	<input type="radio" name="roleId" id="role" size="40" value="2" />普通管理员
	      	</c:if>
	      	<c:if test="${admin.adminRole.id==2}">
	      	<input type="radio" name="roleId" id="role" size="40" value="1" />超级管理员
	      	<input type="radio" name="roleId" id="role" size="40" value="2" checked="checked"/>普通管理员
	      	</c:if>
	      	</td>
	      	</c:if>
	      </tr>
      <tr>
	      	<td><label class="label">注册时间：</label></td>
	      	<td><input type="text" name="admin.regTime" id="regTime" size="40" value="${admin.regTime}" disabled="disabled"/></td>
	      </tr> 
        </tbody>
  </table>
</div>

<div class="footer">
	<div class="save_button">
        <span class="span_button" id="sub">确 认</span>
       <a href="AdminAction_queryAdmin">
        <span class="span_button"   id="ref">取 消</span></a>
    </div>
</div>

</form>

</body>
<script type="text/javascript">
  var msg="${msg}";
  if(msg!=""){
	  layer.msg(msg, {
		    time: 10000,       
		  });
  }
</script>
</html>