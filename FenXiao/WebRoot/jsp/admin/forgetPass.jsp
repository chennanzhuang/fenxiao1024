<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en" class="no-js">

<head>
    <base href="<%=basePath%>">
    
   <title>登陆</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

<link rel="stylesheet" type="text/css" href="jsp/css/login.css">
<script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="jsp/layer/layer.js"></script>
</head>
<body>
<body>
<div class="Content1">
	<div class="Content">
	<form name="loginform" id="loginform" method="post" action="AdminAction_updatePass" onSubmit="return loginformsave()">
	  <table width="94%" border="0" align="center" cellpadding="0" cellspacing="0" >
	    <tr>
	      <td width="14%">用&nbsp;&nbsp;户&nbsp;&nbsp;名：</td>
	      <td width="42%"><input type="text" name="adminAccount"   class="infoInput" maxlength="20" id="adminAccount" style="width:160px; height:24px; margin-top:5px; font-size:16px; padding-left:2px;" /></td>
	      <td width="46%" rowspan="2"><img src="jsp/img/logo.jpg" width="237" height="61" /></td>
        </tr>
	    <tr>
	      <td height="36">新&nbsp;&nbsp;密&nbsp;&nbsp;码：</td>
	      <td><input type="password" name="adminPass" id="adminPass" style="width:160px; height:24px; margin-top:12px; font-size:16px; padding-left:2px;" /></td>
        </tr>
	     <tr>
	      <td height="36">确认密码：</td>
	      <td><input type="password" name="adminPass2" id="adminPass2" style="width:160px; height:24px; margin-top:12px; font-size:16px; padding-left:2px;" /></td>
           <td width="46%" rowspan="2"><img id="checkImg" name="imgcode" onclick="change()" class="captchaImage" style="width:237px; height:61px; text-transform: uppercase; margin-top:12px; font-size:16px; padding-left:2px;" src="checkImg.action"></td>
        </tr>
        <tr>
	      <td height="36">手机号码：</td>
	      <td><input type="text" name="adminPhone" id="adminPhone" style="width:160px; height:24px; margin-top:12px; font-size:16px; padding-left:2px;" /></td>
        </tr>
        <tr>
	      <td height="36">手机验证：</td>
	      <td><input type="text" id="smsCheckCode" style="width:160px; height:24px; margin-top:12px; font-size:16px; padding-left:2px;"> <input
						type="hidden" id="backstage" value=""> <input
						type="button" value="获取验证码" class="huoqucode" id="btnSendCode"
						style="height:30px;width:28%;font-size:10px;"
						onClick="getSMSCheckCode(this),sendMessage()" />
	      </td>
        </tr>
         <tr>
	      <td>验证码：</td>
	      <td><input type="text" name="seccode" id="seccode" style="width:160px; height:24px; margin-top:12px; font-size:16px; padding-left:2px;" /></td>
           
        </tr>
	    <tr>
        
	      <td>&nbsp;</td>
	      <td align="left" valign="middle"><p>
	        <input type="submit" name="submit" id="submit" value="修   改" style="width: 160px;height:34px; cursor: pointer; background: #06F; border: 0; color: #FFF; font-size: 14px; font-weight: 600; border-radius: 4px; position: relative; top: 10px;" />
	      </p></td>
	    <td align="left" valign="middle">
	  &nbsp;
	    </td>
        </tr>
	    
      </table>
	  </form>
	</div>
      <div class="End">我们愿意成为您可靠的长期合作伙伴</div>
</div>

</body>
<script type="text/javascript">
function change(){
		var img1 = document.getElementById("checkImg");
		img1.src="${pageContext.request.contextPath}/checkImg.action?"+new Date().getTime();
	}

</script>
 <script type="text/javascript">
var msg="${msg}";
 if(msg!=""){
	 layer.msg(msg, {
		    time: 10000, //20s后自动关闭
         
		  });
 }


</script>
 <script type="text/javascript">
$("#submit").click(
						function() {
					var adminPhone = $("#adminPhone").val();
							if ($.trim(adminPhone) == "") {
								layer.msg("手机号不能为空！", {
									time : 3000,
								});
							
								return false;
							}	
							var myreg = /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
							if (!myreg.test($("#adminPhone").val())) {
								layer.msg("请输入有效手机号！", {
									time : 3000,
									offset : 0,
									shift : 6
								});
								return false;
							}
							$.ajax({
								type : "POST",
								url : "AdminAction_checkMobile",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
								async : false,
								data : {
									"adminPhone" : adminPhone
								},
								//dataType:"json",
								success : function(data) {
									if (data.code == 1) {
										layer.msg("手机号码输入错误！", {
											time : 3000,
										});
										$("#adminPhone").val("");
					
										return false;
									} 
								}
							});
								var smsCheckCode = $("#smsCheckCode").val();
														var backstage = $("#backstage").val();
														if ($.trim(smsCheckCode) == "") {
															layer.msg("手机验证码不能为空！", {
																time : 3000,
															});
															return false;
														}
														if ($.trim(backstage) == "") {
															layer.msg("请先获取验证码！", {
																time : 3000,
																offset : 0,
																shift : 6
															});
							
															return false;
														}
														if (smsCheckCode != backstage && backstage != "") {
															layer.msg("手机验证码不正确！", {
																time : 3000,
															});
										
															return false;
														}			

});

</script>
<script type="text/javascript">
	function getSMSCheckCode(object) {
		/* alert(132); */
		var adminPhone = $("#adminPhone").val();
// 		var mobileCheck = $("#mobileCheck111").val();

		if ($.trim(adminPhone) == "") {
			layer.msg("手机号不能为空！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}
		;

		var myreg = /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
		if (!myreg.test($("#adminPhone").val())) {
			layer.msg("请输入有效手机号！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}
		$
				.ajax({
					type : "POST",
					url : "AdminAction_checkMobile",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
					data : {
						"adminPhone" : adminPhone
					},
					
					//dataType:"json",
					success : function(data) {
						if (data.code == 1) {
							layer.msg("手机输入错误！", {
								time : 3000,
								offset : 0,
								shift : 6
							});
							$("#adminPhone").val("");
						} else if (data.code == 0) {
						
							$
									.ajax({
										type : "GET",
										url : "AdminAction_getSMSCheckCode",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
										data : {
											"adminPhone" : adminPhone
										},
										//dataType:"json",
										success : function(data) {
											if (data.codeT == 0) {
												alert(data.msg);
											} else {
												$("#backstage").val(data.code);
												$(object)
														.html(
																"<font color='green' size='2'>验证码已发</font>");
											}
										}
									});

						}
					}
				});

	}
</script>
<script type="text/javascript">
	var InterValObj; //timer变量，控制时间
	var count = 60; //间隔函数，1秒执行
	var curCount;//当前剩余秒数

	function sendMessage() {
		curCount = count;
		var adminPhone = $("#adminPhone").val();
		if ($.trim(adminPhone) == "") {
			layer.msg("手机号不能为空！", {
				time : 3000,
			});
			return false;
		}

		var myreg = /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
		if (!myreg.test($("#adminPhone").val())) {
			layer.msg("请输入有效手机号！", {
				time : 3000,
			});
			return false;
		}
		$("#btnSendCode").attr("disabled", "true");
		$("#btnSendCode").val("请在" + curCount + "秒内输入验证码");
		InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次

	}

	//timer处理函数
	function SetRemainTime() {
		if (curCount == 0) {
			window.clearInterval(InterValObj);//停止计时器
			$("#btnSendCode").removeAttr("disabled");//启用按钮
			$("#btnSendCode").val("重新发送验证码");
		} else {
			curCount--;
			$("#btnSendCode").val("请在" + curCount + "秒内输入验证码");
		}
	}
</script>
 <script type="text/javascript">
   $(function(){
	   
	$("#adminAccount").blur(function(){
		var adminAccount=$("#adminAccount").val();
		$.ajax({ 
			 type: "POST",
             url: "${pageContext.request.contextPath}/AdminAction_queryAccount",
             data: {
            	 "adminAccount":adminAccount 
             },
             dataType: "json",
			success: function(data){
				layer.msg(data.mesg, {
				    time: 10000, //20s后自动关闭
		         
				  });
	        
	      }});
	}); 
	$("#adminPass").blur(function(){
		var adminPass=$("#adminPass").val();
		if(adminPass.length==0){
			layer.msg("密码不能为空！", {
			    time: 10000, //20s后自动关闭
	         
			  });
		}
		
	});
	$("#adminPass2").blur(function(){
		var adminPass=$("#adminPass").val();
		var adminPass2=$("#adminPass2").val();
		if(adminPass2.length==0||adminPass2!=adminPass){
			layer.msg("两次输入密码不一致，请重新输入！", {
			    time: 10000, //20s后自动关闭
	         
			  });
		}
		
	});
	$("#adminPhone")
				.blur(
						function() {
							var adminPhone = $("#adminPhone").val();
							if ($.trim(adminPhone) == "") {
								layer.msg("手机号不能为空！", {
									time : 3000,
								});
							}
							var myreg = /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
							if (!myreg.test($("#adminPhone").val())) {
								layer.msg("请输入有效手机号！", {
									time : 3000,
								});
								return;
							}

							$.ajax({
								type : "GET",
								url : "AdminAction_checkMobile",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
								data : {
									"adminPhone" : adminPhone
								},
								//dataType:"json",
								success : function(data) {
									if (data.code == 1) {
										layer.msg("手机号码错误！", {
											time : 3000,
										});
										return;
									}
								}
							});
						});
						$("#smsCheckCode").blur(function() {
					var smsCheckCode = $("#smsCheckCode").val();
					var backstage = $("#backstage").val();
					if ($.trim(smsCheckCode) == "") {
						layer.msg("手机验证码不能为空！", {
							time : 3000,
						});
					}
					if (smsCheckCode != backstage && backstage != "") {
						layer.msg("手机验证码不正确！", {
							time : 3000,
						});
					}
				});
	$("#seccode").blur(function(){
		var seccode=$("#seccode").val();
		
		if(seccode.length==0){
			layer.msg("验证码不能为空！", {
			    time: 10000, //20s后自动关闭
	         
			  });
		}
		
	});
	
    })


</script>
</html>
