<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>添加管理员</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  <link rel="stylesheet" type="text/css" href="jsp/css/css.css">
  <script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="jsp/layer/layer.js"></script>
</head>
<script type="text/javascript">
$(function (){
	  $("#sub").click(function(){
		  $(".form").submit();
	  });  
	  $("#ref").click(function(){
		  $(".form")[0].reset();
	  });
})

</script>
<body>


<div class="nav">
	<div class="nav_title">
    	<h2><img class="nav_img" src="jsp/img/tab.gif" /><a class="nav_a" href="#">添加管理员</a></h2>
    </div>
</div>

<form action="AdminAction_addAdmin" method="post" class="form" name="form1" enctype="multipart/form-data">
<input type="hidden" name="adminId" value="${session.admin.adminId}">
<input type="hidden" name="admin.adminPass" id="adminPass" size="40" value="000000" />

<div class="list">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="details">
        <tbody>
         <tr>
	      	<td width="14%"><label class="label">头像：</label></td>
	      	<td width="86%">
	      	<div id="preview1"></div>
	      	<input type="file" name="fileTest" onchange="preview1(this)" /></td>
             <script>  
       function preview1(file)  {
               var prevDiv = document.getElementById('preview1');
               if (file.files && file.files[0]){
                       var reader = new FileReader();
                         reader.onload = function(evt){
                        prevDiv.innerHTML = '<img src="' + evt.target.result + '" with="100" height="100"/>';
                             }  
                       reader.readAsDataURL(file.files[0]);
                         }else{
                  prevDiv.innerHTML = '<div class="img1" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src=\'' + file.value + '\')" ></div>';
    }
    }
 </script>	      
	      
	      </tr>
	      <tr>
	      	<td width="14%"><label class="label">账号：</label></td>
	      	<td width="86%"><input type="text" name="admin.adminAccount" id="adminAccount" size="40" value="" /></td>

	      </tr>
          <tr>
	      	<td width="14%"><label class="label">用户名：</label></td>
	      	<td width="86%"><input type="text" name="admin.adminName" id="adminName" size="40" value="" /></td>
	      </tr>
          <tr>
	      	<td><label class="label">性别：</label></td>
	      	<td><input name="admin.sex" type="radio" id="sex" value="0" checked="checked" />
      	   男
      	      <input type="radio" name="admin.sex" id="sex2" value="1" />
   	        女</td>
	      </tr>     
          <tr>
	      	<td><label class="label">联系方式：</label></td>
	      	<td><input type="text" name="admin.adminPhone" id="adminPhone" size="40" value="" /></td>
	      </tr>
	      <tr>
	      	<td><label class="label">微信号：</label></td>
	      	<td><input type="text" name="admin.adminWeChatId" id="adminWeChatId" size="40" value="" /></td>
	      </tr>
	      <tr>
	      	<td><label class="label">二维码：</label></td>
	      	<td>	

	    
 <div id="preview"></div>
<input type="file" onchange="preview(this)" name="fileTest1" />
</td>
	      </tr>
	      
 <script>  
 function preview(file)
 {
 var prevDiv = document.getElementById('preview');
 if (file.files && file.files[0])
 {
 var reader = new FileReader();
 reader.onload = function(evt){
 prevDiv.innerHTML = '<img src="' + evt.target.result + '" with="100" height="100"/>';
}  
 reader.readAsDataURL(file.files[0]);
}
 else  
 {
 prevDiv.innerHTML = '<div class="img1" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src=\'' + file.value + '\') ; with:100px;height:100px" with="100" height="100"></div>';
 }
 }
 </script>	      
	      
	      <tr>
	      	<td><label class="label">推荐码：</label></td>
	      	<td><input type="text" name="admin.recommendationCode" id="recommendationCode" size="40" value="" /></td>
	      </tr> 
	      
	      <tr>
	      	<td><label class="label">角色：</label></td>
	      	<td><input type="radio" name="roleId" id="role" size="40" value="1" />超级管理员
	      	<input type="radio" name="roleId" id="role" size="40" value="2" checked="checked"/>普通管理员
	      	</td>
	      </tr>
      
        </tbody>
  </table>
</div>

<div class="footer">
	<div class="save_button">
        <span class="span_button" id="sub">确 认</span>
        <span class="span_button"   id="ref">重 置</span>
        <span class="span_button" onclick="windows.location.href='AdminAction_queryAdmin'">取 消</span>
    </div>
</div>

</form>

</body>
<script type="text/javascript">
  var msg="${msg}";
  if(msg!=""){
	  layer.msg(msg, {
		    time: 10000,       
		  });
  }
</script>
</html>