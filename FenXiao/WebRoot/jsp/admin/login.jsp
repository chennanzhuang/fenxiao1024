<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en" class="no-js">

<head>
    <base href="<%=basePath%>">
    
   <title>登陆</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

<link rel="stylesheet" type="text/css" href="jsp/css/login.css">
<script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="jsp/layer/layer.js"></script>
</head>
<body>
<body>
<div class="Content1">
	<div class="Content">
	<form name="loginform" id="loginform" method="post" action="AdminAction_queryLogin" onSubmit="return loginformsave()">
	  <table width="94%" border="0" align="center" cellpadding="0" cellspacing="0" >
	    <tr>
	      <td width="12%">用户名：</td>
	      <td width="42%"><input type="text" name="adminAccount" class="infoInput" maxlength="20" id="adminAccount" style="width:160px; height:24px; margin-top:5px; font-size:16px; padding-left:2px;" /></td>
	      <td width="46%" rowspan="2"><img src="jsp/img/logo.jpg" width="237" height="61" /></td>
        </tr>
	    <tr>
	      <td height="36">密&nbsp;&nbsp;&nbsp;码：</td>
	      <td><input type="password" name="adminPass" id="adminPass" style="width:160px; height:24px; margin-top:12px; font-size:16px; padding-left:2px;" /></td>
        </tr>
	    <tr>
	      <td>验证码：</td>
	      <td><input type="text" name="seccode" id="seccode" style="width:160px; height:24px; margin-top:12px; font-size:16px; padding-left:2px;" /></td>
           <td width="46%" rowspan="2"><img id="checkImg" name="imgcode" onclick="change()" class="captchaImage" style="width:237px; height:61px; text-transform: uppercase; margin-top:12px; font-size:16px; padding-left:2px;" src="checkImg.action"></td>
        </tr>
         
	    <tr>
        
	      <td>&nbsp;</td>
	      <td align="left" valign="middle"><p>
	        <input type="submit" name="submit" value="登   录" style="width: 160px;height:34px; cursor: pointer; background: #06F; border: 0; color: #FFF; font-size: 14px; font-weight: 600; border-radius: 4px; position: relative; top: 10px;" />
	      </p></td>
	    
        </tr>
	    <tr>
      
	      <td align="right">&nbsp;</td>
	      <td  valign="middle"><p style="color: red; font-size: 12px;margin-top: 10px">${msg}</p></td>
	      <td><span style=" margin-right:25px"><a href="${pageContext.request.contextPath}/jsp/admin/forgetPass.jsp" style="margin-top: 10px">忘记密码?点击找回密码</a></span></td>
        </tr>
      </table>
	  </form>
	</div>
      <div class="End">我们愿意成为您可靠的长期合作伙伴</div>
</div>

</body>
<script type="text/javascript">
	$(function (){
	    		if (top != window) {
	    			//保持当前页面为顶级页面
	    			window.top.location.href = window.location.href;
	    		}
	    		
				$(document.documentElement).on('keyup',function(event){
					if (event.keyCode==13){
						login();
					}
				});
	    	});
</script>
<script type="text/javascript">

function change(){
		var img1 = document.getElementById("checkImg");
		img1.src="${pageContext.request.contextPath}/checkImg.action?"+new Date().getTime();
	}

</script>
<%-- <script type="text/javascript">
var msg="${msg}";
 if(msg!=""){
	 layer.msg(msg, {
		    time: 10000, //20s后自动关闭
         
		  });
 }


</script> --%>
</html>
