<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>修改口令</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  <link rel="stylesheet" type="text/css" href="jsp/css/css.css">
  <script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="jsp/layer/layer.js"></script>
  <%-- <style type="text/css">
  .footer1{ bottom:0; height:45px; line-height:45px; 
            width:100%; text-align:center;}
.footer1 .save_button{ min-width:200px; margin-left:150px;}
.footer1 .span_button{ min-width:60px; display:block; float:left; height:35px;
                      line-height:35px; background:#146AC1; padding:0 30px;
                      margin-left:50px;margin-top:5px; color:#FFF;  border-radius:3px;
                      cursor:pointer; font-size:16px;}
  </style> --%>
</head>
<script type="text/javascript">
$(function (){
	  $("#sub").click(function(){
	 var adminyuan=$("#adminyuan").val();
		var adminId=$("#adminId").val();
			$.ajax({ 
				 type: "POST",
	             url: "AdminAction_querymima",
	             data: {
	            	 "adminyuan":adminyuan,
	            	 "adminId":adminId
	             },
	             dataType: "json",
				success: function(data){
				if(data.code==0){
					layer.msg("原密码输入错误", {
					    time: 10000, //20s后自动关闭
			         
					  });
					   return;
				}
		      }
		      });
		      var adminAccount=$("#adminAccount").val();
			if($.trim(adminAccount)==""){
				layer.msg("请输入密码", {
					    time: 10000, //20s后自动关闭
			         
					  });
					  return;
			}
			
			var chongfuPass=$("#chongfuPass").val();
			if($.trim(chongfuPass)==""||$.trim(chongfuPass)!=$.trim(adminAccount)){
				layer.msg("两次密码不一致", {
					    time: 3000, //20s后自动关闭
					  });
					  return;
			}else {
				
		  $(".form").submit();
			}
					
	  });  
	 
})

</script>
<body>


<div class="nav">
	<div class="nav_title">
    	<h2><img class="nav_img" src="jsp/img/tab.gif" /><a class="nav_a" href="#">修改管理员</a></h2>
    </div>
</div>

<form action="AdminAction_querykouling" method="post" class="form" name="form1" enctype="multipart/form-data">
<input type="hidden" name="adminId" value="${session.admin.adminId}" id="adminId">
<!-- <input type="hidden" name="adminId" id="adminId" value="${adminId}"> -->
<!-- <input type="hidden" name="admin.adminPass" id="adminPass" size="40" value="${admin.adminPass}" /> -->

<div class="list">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="details">
        <tbody>
	      <tr>
	      	<td width="14%"><label class="label">原密码：</label></td>
	      	<td width="86%"><input type="password" name="adminyuan" id="adminyuan" size="40" /></td>
	      </tr>
	      <tr>
	      	<td width="14%"><label class="label">新密码：</label></td>
	      	<td width="86%"><input type="password" name="adminAccount" id="adminAccount" size="40" /></td>
	      </tr>
	      <tr>
	      	<td width="14%"><label class="label">确认密码：</label></td>
	      	<td width="86%"><input type="password" name="chongfuPass" id="chongfuPass" size="40" /></td>
	      </tr>
        
        </tbody>
  </table>
</div>

<div class="footer">
	<div class="save_button">
        <span class="span_button" id="sub">确 认</span>
       <a href="AdminAction_queryAdmin">
        <span class="span_button"   id="ref">取 消</span></a>
    </div>
</div>

</form>

</body>

<script type="text/javascript">
$(function(){
		$("#adminyuan").blur(function(){
			var adminyuan=$("#adminyuan").val();
			var adminId=$("#adminId").val();
			$.ajax({ 
				 type: "POST",
	             url: "AdminAction_querymima",
	             data: {
	            	 "adminyuan":adminyuan,
	            	 "adminId":adminId
	             },
	             dataType: "json",
				success: function(data){
				if(data.code==0){
					layer.msg("原密码输入错误", {
					    time: 10000, //20s后自动关闭
			         
					  });
					   return;
				}
		      }
		      });
		});
		
		$("#adminAccount").blur(function(){
			var adminAccount=$("#adminAccount").val();
			if($.trim(adminAccount)==""){
				layer.msg("请输入密码", {
					    time: 10000, //20s后自动关闭
			         
					  });
					  return;
			}
		});
		$("#chongfuPass").blur(function(){
			var chongfuPass=$("#chongfuPass").val();
			var adminAccount=$("#adminAccount").val();
			if($.trim(chongfuPass)==""||$.trim(chongfuPass)!=$.trim(adminAccount)){
				layer.msg("两次密码不一致", {
					    time: 3000, //20s后自动关闭
					  });
					  return;
			}
		});
});
</script>
</html>