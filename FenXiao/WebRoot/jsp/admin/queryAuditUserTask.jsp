<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>收益统计</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" type="text/css" href="jsp/css/css.css">
<script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>
<style type="text/css">
.page font{border: 1px solid #7F7F7F; color: #7F7F7F; display: inline-block; font-family: 'Tahoma'; margin: 0 3px; text-decoration: none;height: 24px; line-height: 24px; padding: 0 10px; text-align: center;cursor: pointer;}
.a1{
display: block;
    min-width: 30px;
    margin-left: 15px;
    line-height: 28px;
   max-width:80px;
    border: 1px solid #CCC;
    padding: 0 15px;
    font-size: 14px;
}
.a1:hover{ background:#09F; color:white; border:1px solid #09F;}
#a2:hover{color: #FF8040;cursor: pointer;}
</style>
<script type="text/javascript">
$(function(){
	$("#sub").click(function(){

		$("#form").submit();
	});
	
	
});

</script>
</head>
<body>


<div class="nav" style="height: 60px">

	<div class="nav_title" style="margin-top: 15px ">
    	<h2><img class="nav_img" src="jsp/img/tab.gif" /><a class="nav_a" href="#">收益统计列表</a></h2>
    </div>
    <form action="AdminAction_queryAuditUserTask" id="form"method="post">
    			<select id="taskType" name="taskType"
						onchange="a(value)">
							<option value="">任 务 类 型</option>
							<option value="1">关注公众号</option>
							<option value="2">广 告 宣 传</option>
							<option value="3">APP 下 载</option>
					</select>
	 		<select data-placeholder="合伙人查询" id="agentHehuo" name="agentHehuo" class="form-control"
                                data-rel="chosen" onchange="hehuo(value)">
                                    <option value="-1">---合伙人---</option>
                            <c:forEach items="${agentlist }" var="agent">
                                    <option value="${agent.agentId}">${agent.agentName}</option>
                            </c:forEach>
                        </select>
	 		<select data-placeholder="代理商" id="agentdaili" name="agentdaili" class="form-control"
                                data-rel="chosen" onchange="dailishang(value)">
                                    <option value="-1">---代理商---</option>
                        </select>
	 		<select data-placeholder="员工" id="Useryuan" name="Useryuan" class="form-control"
                                data-rel="chosen">
                                    <option value="-1">---员工---</option>
                        </select>
    <div class="nav_button" style="margin-right: 105px">
    <span style="float:left;line-height:40px;font-size:16px">任务名称:</span><input name="search" id="search" class="a_button rand">
    <span style="float:left;line-height:40px;font-size:16px">会员手机号码:</span> <input name="modile" id="modile" class="a_button rand">
     <a class="a_button rand" id="sub" style="cursor: pointer;">搜索</a>
</div>
</form>
</div>
<input type="hidden" name="taskType" value="${taskType }" id="taskType1">
<input type="hidden" name="search" value="${search }" id="search1">
<input type="hidden" name="modile" value="${modile }" id="modile1">
<input type="hidden" name="agentHehuo" value="${agentHehuo }" id="agentHehuo1">
<input type="hidden" name="daili" value="${daili }" id="daili1">
<input type="hidden" name="Useryuan" value="${Useryuan }" id="Useryuan1">
<input type="hidden" name="Tola" value="${Tola }" id="Tola">
<input type="hidden" name="currentPage" value="${pageBean.currentPage}" id="currentPage">
<div class="list">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="list_table">
      <thead>
	    <tr>
           <td width="12%">订单号</td>
	      <td width="12%">标题</td>
	      <td width="8%">总收益</td>
	      <td width="8%">任务奖金</td>
	      <td width="8%">类型</td>
	       <td width="10%">提交人</td>
	       <td width="10%">下单时间</td>
	       <td width="10%">审核状态</td>
	       <td width="10%">手机号码</td>
        </tr>
        </thead>
        <tbody>
        <input type="hidden" name="numbers" value="" id="numbers">
        <c:forEach items="${usertaskList}" var="task" varStatus="s" >
         <tr>
           <td><div align="center">${task[0].taskOrderCode}</div></td>
	      <td><div align="center"><a data-nid="45832585935"  id="a2" trace="auction" traceidx="0" trace-pid="">${task[0].taskTitle}</a></div></td>
	      <td><div align="center">${task[0].taskBonus}</div></td>
	      <td><div align="center">${task[0].taskBonus1}</div></td>

	      <td><div align="center">
	      <c:if test="${task[0].taskType==1}">关注公众号</c:if>
	       <c:if test="${task[0].taskType==2}">广告宣传</c:if>
	        <c:if test="${task[0].taskType==3}">APP下载</c:if>
	      </div></td>
	       <td><div align="center">${task[1].username}</div></td>
	        <td><div align="center">${task[0].taskOrderTime}</div></td>
	        <td><div align="center">
	     	  任务完成
	       </div></td>
	        <td><div align="center">${task[0].user.mobile}</div></td>
        </tr>
        </c:forEach>
	   
        </tbody>
  </table>
</div>
<div id="shouxufie">
任务总收益:${hehuoshu }
<c:if test="${agentHehuo!=-1&&daili==-1&&Useryuan==-1}">
	合伙人提成:${hehuoshouyi}
</c:if>
<c:if test="${agentHehuo!=-1&&daili!=-1&&Useryuan==-1}">
	代理商提成:${dailisahng}
</c:if>
<c:if test="${agentHehuo!=-1&&daili!=-1&&Useryuan!=-1&&agentHehuo!=0&&daili!=0&&Useryuan!=0}">
	员工提成:${yuangong}
</c:if>
</div>
<div class="page">
               <div align="center" style="float: left;">
					<font style="">共<s:property value="pageBean.allRow"/>条记录</font>
						<font>第<s:property value="pageBean.currentPage"/>/共<s:property value="pageBean.totalPage"/>页</font>
						</div>
							<div align="center" id="page">
							<s:if test="%{pageBean.currentPage == 1}">
           <font class="current disabled" style="background:#FFF; color:#666;">首页</font> 
           <font class="current disabled" style="background:#FFF; color:#666;">上一页</font>
         </s:if>
         <s:else>
   <a href="${ pageContext.request.contextPath }/AdminAction_queryAuditUserTask?page=1&search=${search}&taskType=${taskType}&modile=${modile}&agentHehuo=${agentHehuo}&daili=${daili}&Useryuan=${Useryuan}">首页</a>
    <a href='${ pageContext.request.contextPath }/AdminAction_queryAuditUserTask?page=<s:property value="%{pageBean.currentPage-1}"/>&search=${search}&taskType=${taskType}&modile=${modile}&agentHehuo=${agentHehuo}&daili=${daili}&Useryuan=${Useryuan}'>上一页</a>
    </s:else>
					
				<s:iterator var="i" begin="1" end="pageBean.totalPage">
				
					<a href="${ pageContext.request.contextPath }/AdminAction_queryAuditUserTask?page=<s:property value="#i"/>&search=${search}&taskType=${taskType}&modile=${modile}&agentHehuo=${agentHehuo}&daili=${daili}&Useryuan=${Useryuan}"><s:property value="#i"/></a>
				
				<s:else>
					<span class="currentPage"><s:property value="#i"/></span>
				</s:else>
			</s:iterator>
			<s:if test="%{pageBean.allRow !=0}">
						<s:if test="%{pageBean.currentPage!= pageBean.totalPage}">
						<a href='${ pageContext.request.contextPath }/AdminAction_queryAuditUserTask?page=<s:property value="%{pageBean.currentPage+1}"/>&search=${search}&taskType=${taskType}&modile=${modile}&agentHehuo=${agentHehuo}&daili=${daili}&Useryuan=${Useryuan}'>下一页</a>
                        <a href='${ pageContext.request.contextPath }/AdminAction_queryAuditUserTask?page=<s:property value="pageBean.totalPage"/>&search=${search}&taskType=${taskType}&modile=${modile}&agentHehuo=${agentHehuo}&daili=${daili}&Useryuan=${Useryuan}'>末页</a>
					</s:if>
                <s:else>
         <font  style=" background:#FFF; color:#666;">下一页</font> 
           <font  style=" background:#FFF; color:#666;">末页</font>
         </s:else>
				 </s:if>
          <s:else>
         <font  style=" background:#FFF; color:#666;">下一页</font> 
           <font  style=" background:#FFF; color:#666;">末页</font>
           </s:else>		
					</div>


   
</div>

<script type="text/javascript">
// ;(function(){
//     var page = $("#currentPage").val();
// 	var a = parseInt(pae);
// 	var b = a + 4;
// 	var c = a + 10;
// 	var d = a + 11;
// 	if(c >= ($('#page a').length)-4){
// 		b = a - 4;
// 		c = a - 10;
// 		d = a - 11;
// 	}
// 	$('#page a').slice(b,c).css({
// 		"display":"none"
// 	});
// 	$('#page a').slice(b,c).on('click',function(){
// 		return false;
// 	});
// 	$('#page a').slice(b,d).html('...').css('border','none');
// })();
 var value = $("#agentHehuo1").val();
 var commdon = "<option value='-1'>---代理商---</option>";
 	if(value==-1){
 		$("#agentdaili").html('');
 		$("#agentdaili").append(commdon);
 	}else{
 		$.ajax({
			type : "GET",
			url : "jsonAction_queryAlldaili",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
			data : {
			"agentId" : value
				},
				//dataType:"json",
				success : function(result) {
					var jsonArray = eval("(" + result + ")");
				for (var i = 0; i < jsonArray.length; i++) {
						commdon+="<option value='"+jsonArray[i].agentId+"'>"+jsonArray[i].agentName+"</option>";
					}
					$("#agentdaili").html("");
 					$("#agentdaili").append(commdon);
				}
		});
 	}
 
 var daili2 = $("#daili1").val();
 var commdon1 = "<option value='-1'>---员工---</option>";
 	if(daili2==-1){
 		$("#Useryuan").html('');
 		$("#Useryuan").append(commdon1);
 	}else{
 		$.ajax({
			type : "GET",
			url : "jsonAction_queryAllyuan",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
			data : {
			"dailiId" : daili2
				},
				//dataType:"json",
				success : function(result) {
					var jsonArray = eval("(" + result + ")");
				for (var i = 0; i < jsonArray.length; i++) {
						commdon1+="<option value='"+jsonArray[i].UserId+"'>"+jsonArray[i].UserName+"</option>";
					}
					$("#Useryuan").html('');
 					$("#Useryuan").append(commdon1);
				}
		});
 	}

 	
		
</script>
<script type="text/javascript">
function hehuo(value){
 var commdon = "<option value='-1'>---代理商---</option>";
 	if(value==-1){
 		$("#agentdaili").html('');
 		$("#agentdaili").append(commdon);
 	}else{
 		$.ajax({
			type : "GET",
			url : "jsonAction_queryAlldaili",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
			data : {
			"agentId" : value
				},
				//dataType:"json",
				success : function(result) {
					var jsonArray = eval("(" + result + ")");
				for (var i = 0; i < jsonArray.length; i++) {
						commdon+="<option value='"+jsonArray[i].agentId+"'>"+jsonArray[i].agentName+"</option>"
					}
					$("#agentdaili").html('');
 					$("#agentdaili").append(commdon);
				}
		});
 	}
 }
 function dailishang(value){
 var commdon = "<option value='-1'>---员工---</option>";
 	if(value==-1){
 		$("#Useryuan").html('');
 		$("#Useryuan").append(commdon);
 	}else{
 		$.ajax({
			type : "GET",
			url : "jsonAction_queryAllyuan",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
			data : {
			"dailiId" : value
				},
				//dataType:"json",
				success : function(result) {
					var jsonArray = eval("(" + result + ")");
					if(jsonArray!=null){		
				for (var i = 0; i < jsonArray.length; i++) {
						commdon+="<option value='"+jsonArray[i].UserId+"'>"+jsonArray[i].UserName+"</option>"
					}
					$("#Useryuan").html('');
 					$("#Useryuan").append(commdon);
					}else{
					$("#Useryuan").html('');
					$("#Useryuan").append(commdon);
					}
				}
		});
 	}
 }
</script>

</body>
<script type="text/javascript">
	function a(value) {
	var modile = $("#modile").val();
	var agentHehuo = $("#agentHehuo").val();
	var Useryuan = $("#Useryuan").val();
	var daili = $("#agentdaili").val();
	var search = $("#search").val();
		window.location.href = "${pageContext.request.contextPath}/AdminAction_queryAuditUserTask?taskType="+value+"&modile="+modile+"&agentHehuo="+agentHehuo+"&agentdaili="+daili+"&Useryuan="+Useryuan+"&search="+search;
	}

</script>
<script type="text/javascript">
var search = $("#search1").val();
	var modile = $("#modile1").val();
	var agentHehuo = $("#agentHehuo1").val();
	var daili = $("#daili1").val();
	var Useryuan = $("#Useryuan1").val();
// 	alert(Useryuan)
// 	alert(daili)
// 	alert(agentHehuo)
		var taskType = $("#taskType1").val();
		var timer = null;
		var block = function(){
			if (taskType != "") {
				$("#taskType").val(taskType);
			}
			if(search != null||$.trim(search)!=""){
			$("#search").val(search);
			}
			if(modile != null||$.trim(modile)!=""){
			$("#modile").val(modile);
			}
			if(agentHehuo!=null||$.trim(agentHehuo)!=""){
			$("#agentHehuo").val(agentHehuo);
			}
			if(daili!=null||$.trim(daili)!=""){
	// 		alert("代理三")
			$("#agentdaili").val(daili);
			}
			if(Useryuan!=null||$.trim(Useryuan)!=""){
			$("#Useryuan").val(Useryuan);
			}
		}
		timer = setInterval(function(){
			block();
			clearInterval(timer)
		},100)
</script>
</html>