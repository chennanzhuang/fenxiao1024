<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>详情页面</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" type="text/css" href="jsp/css/style.css">
        <link rel="stylesheet" type="text/css" href="jsp/css/base.css">
       <link rel="stylesheet" type="text/css" href="jsp/css/css1.css">
<script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>
  </head>
  
 <body>
	
<div class="nav">
	<div class="nav_title">
    	<h2><img class="nav_img" src="jsp/img/tab.gif" /><a class="nav_a" href="#">管理员详情</a></h2>
    </div>
</div>

<div class="resume-l fl">
    <div class="resume-l1">
    	<div class="resume-Introduction">
        	<div class="resume-Introduction-l" style="margin-top: 10px;">
        	<c:if test="${admin.adminPhoto==null}">
	         <img src="jsp/img/111.png" width="120" height="130" style="margin-bottom: 10px;">
	      </c:if>
	       <c:if test="${admin.adminPhoto!=null}">
	      <img src="${admin.adminPhoto}"  width="120" height="130" style="margin-bottom: 10px;"/>
	      </c:if>
              
               
                <p > 性别：
                 <c:if test="${admin.sex==0}">
                <input name=""   type="radio" value="" checked="checked"/> 男 
                <input name="" type="radio" value="" /> 女</p>
            </c:if>
             <c:if test="${admin.sex==1}">
                <input name=""   type="radio" value="" /> 男 
                <input name="" type="radio" value="" checked="checked"/> 女</p>
            </c:if>
            </div>
           
            <div class="resume-Introduction-name">
            	<ul>
                	<li> <span>账&nbsp;&nbsp;号：</span><input type="text" value="${admin.adminAccount}"></li>
                    <li> <span>名&nbsp;&nbsp;称：</span><input type="text" value="${admin.adminName}"></li>
                    <li> <span>联系方式：</span><input type="text" value="${admin.adminPhone}"></li>
                    <li> <span>推      荐     码：</span><input type="text" value="${admin.recommendationCode}"></li>
	                <li> <span>微      信     号：</span><input type="text" value="${admin.adminWeChatId}"></li>
                    <li> <span>注册时间：</span><input type="text" value="${admin.regTime}"></li>
                    <c:if test="${admin.adminOr !=null}">
                     <li> <span>二      维     码：</span></li>
                     
                      <li style="width:200px; center;margin: auto;"><img src="${admin.adminOr}"  width="130px" height="130px"/></li> 
                     </c:if>
                      <li> <span>佣&nbsp;&nbsp;金：</span><input type="text" value="${admin.adminMoney}"></li>
                      <li> <span>余&nbsp;&nbsp;额：</span><input type="text" value="${admin.adminBalance}"></li>
                       <li> <span>角&nbsp;&nbsp;色：</span>
                       <c:if test="${admin.adminRole.id==1}">
                       <input type="text" value="超级管理员">
                       </c:if>
                       <c:if test="${admin.adminRole.id==2}">
                       <input type="text" value="普通管理员">
                       </c:if>
                       </li>
                     
                   
                      </ul>
                     <%-- <div class="footer">
	                   <div class="save_button">
                        <span class="span_button"  id="sub"><a href="history.go(-1)">确 认</a></span>
                        <span class="span_button"   id="ref"><a href="history.go(-1)">取 消</a></span>
                          </div> --%>
                         </div>
                </div>
              </div>
           </div>
        </div>
     </body>
</html>
                     