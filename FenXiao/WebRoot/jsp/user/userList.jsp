<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>员工列表</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" type="text/css" href="jsp/css/css.css">
<script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>
<style type="text/css">
.page font{border: 1px solid #7F7F7F; color: #7F7F7F; display: inline-block; font-family: 'Tahoma'; margin: 0 3px; text-decoration: none;height: 24px; line-height: 24px; padding: 0 10px; text-align: center;cursor: pointer;}
.a1{
display: block;
    min-width: 30px;
    margin-left: 15px;
    line-height: 28px;
   max-width:80px;
    border: 1px solid #CCC;
    padding: 0 15px;
    font-size: 14px;
}
.a1:hover{ background:#09F; color:white; border:1px solid #09F;}
#a2:hover{color: #FF8040;cursor: pointer;}
</style>
<script type="text/javascript">
$(function(){
	$("#sub").click(function(){
		$("#form").submit();
		
	});
	
	
})

</script>
<script>
	$(function(){
	
		var delBtn = $("#delete");
		var selectTool = $("#selectTool");
		var value= $("#agentId").val();
		selectTool.click(function() {
		if (selectTool.val() == "1"){
		  $(":checkbox").prop('checked', true);  
		selectTool.attr("value","2");
		}
		else if (selectTool.val() == "2"){
		 $(":checkbox").prop("checked", false);  
		selectTool.attr("value","1");
		}
		});
		//console.info(delBtn);
		delBtn.click(function() {
		var str=""; 
		$("input[type='checkbox']:checked").each(function(){ 
		str+=$(this).val()+","; 
		console.info(str);
		});
		var numbers = $("#numbers");
		str =str.substring(0,str.length-1);
				numbers.attr("value",str);
		if(str != ""){
			window.location.href = "UserAction_deleteAllStaff?num="+str+"&agentId="+value; 
		}
	});
});
</script>

</head>
<body>


<div class="nav">

	<div class="nav_title">
    	<h2><img class="nav_img" src="jsp/img/tab.gif" /><a class="nav_a" href="#">员工列表</a></h2>
    </div>
    <form action="UserAction_queryUserList" id="form"method="post">
    <div class="nav_button">
    <input type="hidden" name="agentId" id="agentId" value="${agent.agentId}">
    <input name="search" id="search" class="a_button rand">
     <a class="a_button rand" id="sub" style="cursor: pointer;">搜索</a>
</form>
<a class="a_button rand" id="add" href="UserAction_addStaffPage?agentId=${agent.agentId}" style="cursor: pointer;">添加</a>
<a class="a_button rand" id="delete" style="cursor: pointer;">删除</a>
    </div>
</div>


<div class="list">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="list_table">
      <thead>
	    <tr>
	      <td width="6%">
          <input style="position:relative;right:5px;top:2px;" id="selectTool" value="1" type="checkbox" name="checkbox" id="checkbox" /> 
              <label for="checkbox">全选</label>
          </td>
	      <td width="10%">头像</td>
	      <td width="10%">账号</td>
	      <td width="15%">名称</td>
	       <td width="8%">性别</td>
	      <td width="10%">推荐码</td>
	       <td width="8%">状态</td>
	       <td width="13%">注册时间</td>
	       <td width="23%">操作</td>
        </tr>
        </thead>
        <tbody>
        <input type="hidden" name="numbers" value="" id="numbers">
        <c:forEach items="${userList}" var="user" varStatus="s" >
         <tr>
	      <td><div align="center">
	        <input type="checkbox"class="toolBtn" name="checkbox2" id="checkbox2" value="${user.id}" />
          </div></td>
	      <td><div align="center">
	      <c:if test="${user.userPhoto==null}">
	       <img class="thumb_img" src="jsp/img/111.png" style="width:70px;height: 60px"/>
	      </c:if>
	       <c:if test="${user.userPhoto!=null}">
	      <img class="thumb_img" src="${user.userPhoto}" style="width:70px;height: 60px"/>
	      </c:if>
	      </div></td>
	      <td><div align="center"><a data-nid="45832585935" href="UserAction_queryStaffInfo?user.id=${user.id}" id="a2" trace="auction" traceidx="0" trace-pid="">${user.username}</a></div></td>
	      <td><div align="center">${user.nickName}</div></td>
	    
	      <td><div align="center">
	       <c:if test="${user.sex==1}">男</c:if>
	      <c:if test="${user.sex==2}">女</c:if>
	      </div></td>
	      <td><div align="center">${user.recommendationCode}</div></td>
	     <td><div align="center">
	      <c:if test="${user.userStatus==0}">可用</c:if>
	      <c:if test="${user.userStatus==1}"><font style="color: red;">禁用</font></c:if>
	   
	      </div></td>
	       <td><div align="center">${user.regTime}</div></td>
	       <td><div align="center" >
	       <a class="a_edit" href="UserAction_updateStaffPage?user.id=${user.id}&agentId=${agent.agentId}">修改</a>
	      <c:if test="${user.userStatus==0 }">
	       <a class="a_edit" href="UserAction_guangbi?user.userStatus=1&user.id=${user.id }">关闭</a>	    
	     </c:if>
	      <c:if test="${user.userStatus==1 }">
	       <a class="a_edit" href="UserAction_guangbi?user.userStatus=0&user.id=${user.id }">开启</a>
	       </c:if>
           <a class="a_del" href="UserAction_deleteStaff?user.id=${user.id}&agentId=${agent.agentId}">删除</a>
	      </div></td>
        </tr>
        </c:forEach>
	   
        </tbody>
  </table>
</div>

<div class="page">
               <div align="center" style="float: left;" >
					<font style="">共<s:property value="pageBean.allRow"/>条记录</font>
						<font>第<s:property value="pageBean.currentPage"/>/共<s:property value="pageBean.totalPage"/>页</font>
						</div>
							<div align="center" >
							<s:if test="%{pageBean.currentPage == 1}">
           <font class="current disabled" style="background:#FFF; color:#666;">首页</font> 
           <font class="current disabled" style="background:#FFF; color:#666;">上一页</font>
         </s:if>
         <s:else>
   <a href="${ pageContext.request.contextPath }/UserAction_queryUserList?page=1&agentId=${agent.agentId}&search=${search}">首页</a>
    <a href='${ pageContext.request.contextPath }/UserAction_queryUserList?page=<s:property value="%{pageBean.currentPage-1}"/>&agentId=${agent.agentId}&search=${search}'>上一页</a>
    </s:else>
					
				<s:iterator var="i" begin="1" end="pageBean.totalPage">
				
					<a href="${ pageContext.request.contextPath }/UserAction_queryUserList?page=<s:property value="#i"/>&agentId=${agent.agentId}&search=${search}"><s:property value="#i"/></a>
				
				<s:else>
					<span class="currentPage"><s:property value="#i"/></span>
				</s:else>
			</s:iterator>
			<s:if test="%{pageBean.allRow !=0}">
						<s:if test="%{pageBean.currentPage!= pageBean.totalPage}">
						<a href='${ pageContext.request.contextPath }/UserAction_queryUserList?page=<s:property value="%{pageBean.currentPage+1}"/>&agentId=${agent.agentId}&search=${search}'>下一页</a>
                        <a href='${ pageContext.request.contextPath }/UserAction_queryUserList?page=<s:property value="pageBean.totalPage"/>&agentId=${agent.agentId}&search=${search}'>末页</a>
					</s:if>
                <s:else>
         <font  style=" background:#FFF; color:#666;">下一页</font> 
           <font  style=" background:#FFF; color:#666;">末页</font>
         </s:else>
				 </s:if>
          <s:else>
         <font  style=" background:#FFF; color:#666;">下一页</font> 
           <font  style=" background:#FFF; color:#666;">末页</font>
           </s:else>		
					</div>


   
</div>


</body>
</html>
