<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>员工详情</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
		<link rel="stylesheet" type="text/css" href="jsp/css/style.css">
        <link rel="stylesheet" type="text/css" href="jsp/css/base.css">
       <link rel="stylesheet" type="text/css" href="jsp/css/css1.css">
<script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>
  </head>
  
 <body>
	
<div class="nav">
	<div class="nav_title">
    	<h2><img class="nav_img" src="jsp/img/tab.gif" /><a class="nav_a" href="#">员工详情</a></h2>
    </div>
</div>

<div class="resume-l fl">
    <div class="resume-l1">
    	<div class="resume-Introduction">
        	<div class="resume-Introduction-l" style="margin-top: 10px;">
        	<c:if test="${user.userPhoto==null}">
	         <img src="jsp/img/111.png" width="120" height="130" style="margin-bottom: 10px;">
	      </c:if>
	       <c:if test="${user.userPhoto!=null}">
	      <img src="${user.userPhoto}"  width="120" height="130" style="margin-bottom: 10px;"/>
	      </c:if>
              
               
                <p > 性别：
                 <c:if test="${user.sex==0}">
                <input name=""   type="radio" value="" checked="checked"/> 男 
                <input name="" type="radio" value="" /> 女</p>
            </c:if>
             <c:if test="${user.sex==1}">
                <input name=""   type="radio" value="" /> 男 
                <input name="" type="radio" value="" checked="checked"/> 女</p>
            </c:if>
            </div>
           
            <div class="resume-Introduction-name">
            	<ul>
                	<li> <span>账&nbsp;&nbsp;号：</span><input type="text" value="${user.username}"></li>
                    <li> <span>昵&nbsp;&nbsp;称：</span><input type="text" value="${user.nickName}"></li>
                    <li> <span>联系方式：</span><input type="text" value="${user.mobile}"></li>
                    <li> <span>推      荐     码：</span><input type="text" value="${user.recommendationCode}"></li>
                    <li> <span>地&nbsp;&nbsp;址：</span><input type="text" value="${user.dress}"></li>
                    <li> <span>状&nbsp;&nbsp;态:</span><c:if test="${user.userStatus==0}"><input type="text" value="可用"></c:if>
	                 <c:if test="${user.userStatus==1}"><input type="text" value="关闭"></c:if>
	                 </li>
	                <li> <span>微      信     号：</span><input type="text" value="${user.weChatId}"></li>
                    <li> <span>注册时间：</span><input type="text" value="${user.regTime}"></li>
                     
                     <c:if test="${user.userQr!=null}">
                     <li> <span>二      维     码：</span></li>   
                      <li style="width:200px; center;margin: auto;"><img src="${user.userQr}"  width="130px" height="130px"/></li>
                      </c:if> 
                      <li> <span>佣&nbsp;&nbsp;金：</span><input type="text" value="${user.totalMoney}"></li>
                      <li> <span>余&nbsp;&nbsp;额：</span><input type="text" value="${user.balance}"></li>
                       <li> <span>推广佣金：</span><input type="text" value="${user.promotionCommission}"></li>
                      <li> <span>任务佣金：</span><input type="text" value="${user.taskCommission}"></li>
                      <li> <span>团队佣金：</span><input type="text" value="${user.teamCommissions}"></li>
                  
                      </ul>
                     <%-- <div class="footer">
	                   <div class="save_button">
                        <span class="span_button"  id="sub"><a href="history.go(-1)">确 认</a></span>
                        <span class="span_button"   id="ref"><a href="history.go(-1)">取 消</a></span>
                          </div> --%>
                         </div>
                </div>
              </div>
           </div>
        </div>
     </body>
</html>
                  