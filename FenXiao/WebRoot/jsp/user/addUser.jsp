<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>添加员工</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  <link rel="stylesheet" type="text/css" href="jsp/css/css.css">
<script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>

</head>
<script type="text/javascript">
$(function (){
	  $("#sub").click(function(){
		  $(".form").attr("action","UserAction_addAllUser").submit();
	  });  
	  $("#ref").click(function(){
		  $(".form").attr("action","UserAction_queryAllUserList");
		  $(".form").submit();
	  });
})


</script>
<body>


<div class="nav">
	<div class="nav_title">
    	<h2><img class="nav_img" src="jsp/img/tab.gif" /><a class="nav_a" href="#">添加员工</a></h2>
    </div>
</div>

<form action="" method="post" class="form" name="form1" enctype="multipart/form-data">
<input type="hidden" id="agentId" name="agentId" value="${agent.agentId}">
<input type="hidden" name="user.recomCode" id="recomCode" size="40" value="${agent.recommendationCode}" />
<input type="hidden" name="user.userStatus" id="userStatus" size="40" value="0" />
<input type="hidden" name="user.userLevel" id="userLevel" size="40" value="0" />
<div class="list">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="details">
        <tbody>
         <tr>
	      	<td width="14%"><label class="label">头像：</label></td>
	      	<td width="86%">
	      	<div id="preview1"></div>
	      	<input type="file" name="fileTest" onchange="preview1(this)" /></td>
             <script>  
       function preview1(file)  {
               var prevDiv = document.getElementById('preview1');
               if (file.files && file.files[0]){
                       var reader = new FileReader();
                         reader.onload = function(evt){
                        prevDiv.innerHTML = '<img src="' + evt.target.result + '" with="100" height="100"/>';
                             }  
                       reader.readAsDataURL(file.files[0]);
                         }else{
                  prevDiv.innerHTML = '<div class="img1" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src=\'' + file.value + '\')" ></div>';
    }
    }
 </script>	      
	      
	      </tr>
	      <tr>
	      	<td width="14%"><label class="label">账号：</label></td>
	      	<td width="86%"><input type="text" name="user.username" id="username" size="40" value="" /></td>

	      </tr>
          <tr>
	      	<td width="14%"><label class="label">昵称：</label></td>
	      	<td width="86%"><input type="text" name="user.nickName" id="nickName" size="40" value="" /></td>

	      </tr>
	       <tr>
	      	<td width="14%"><label class="label">密码：</label></td>
	      	<td width="86%"><input type="text" name="user.userPassword" id="userPassword" size="40" value="" /></td>

	      </tr>
	       <tr>
	      	<td width="14%"><label class="label">确认密码：</label></td>
	      	<td width="86%"><input type="text" name="userPassword" id="userPassword1" size="40" value="" /></td>

	      </tr>
          <tr>
	      	<td><label class="label">性别：</label></td>
	      	<td><input name="user.sex" type="radio" id="sex" value="0" checked="checked" />
      	   男
      	      <input type="radio" name="user.sex" id="sex2" value="1" />
   	        女</td>
	      </tr>
      
      
      
          <tr>
	      	<td><label class="label">联系方式：</label></td>
	      	<td><input type="text" name="user.mobile" id="mobile" size="40" value="" /></td>
	      </tr>
	      <tr>
	      	<td><label class="label">微信号：</label></td>
	      	<td><input type="text" name="user.weChatId" id="weChatId" size="40" value="" /></td>
	      </tr>
	       <tr>
	      	<td><label class="label">推荐码：</label></td>
	      	<td><input type="text" name="user.recomCode" id="recomCode" size="40" value="" /></td>
	      </tr>
	      <tr>
	      	<td><label class="label">二维码：</label></td>
	      	<td>	

	    
 <div id="preview"></div>
<input type="file" onchange="preview(this)" name="fileTest1" />
</td>
	      </tr>
	      
 <script>  
 function preview(file)
 {
 var prevDiv = document.getElementById('preview');
 if (file.files && file.files[0])
 {
 var reader = new FileReader();
 reader.onload = function(evt){
 prevDiv.innerHTML = '<img src="' + evt.target.result + '" with="100" height="100"/>';
}  
 reader.readAsDataURL(file.files[0]);
}
 else  
 {
 prevDiv.innerHTML = '<div class="img1" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src=\'' + file.value + '\') ; with:100px;height:100px" with="100" height="100"></div>';
 }
 }
 </script>	      

	      <tr>
	      	<td><label class="label">地址：</label></td>
	      	<td><input type="text" name="user.dress" id="dress" size="40" value="" /></td>
	      </tr>
      
        </tbody>
  </table>
</div>

<div class="footer">
	<div class="save_button">
        <span class="span_button" id="sub">确 认</span>
        <span class="span_button"   id="ref">取 消</span>
    </div>
</div>

</form>

</body>
<script type="text/javascript">
  var mesg="${msg}";
  if(mesg!=""){
	  alert(mesg);
  } 
</script>
</html>