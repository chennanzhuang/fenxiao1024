<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'main.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<LINK href="jsp/css/admin.css" type="text/css" rel="stylesheet">
<script type="text/javascript">
function disptime( ) 
 { 
 
 var time = new Date( ); //获得当前时间 
 var year=time.getFullYear();
 var date=time.getDate();
 var month=time.getMonth()+1;
 var hour = time.getHours( ); //获得小时、分钟、秒 
 var minute = time.getMinutes( ); 
 var second = time.getSeconds( ); 
 if (month<10) //按12小时制显示 
 { 
	 month="0"+month; 
  
 } 
 if (date<10) //按12小时制显示 
 { 
	 date="0"+date; 
  
 } 
 if (hour<10) //按12小时制显示 
 { 
 hour="0"+hour; 
  
 } 
 if (minute < 10) //如果分钟只有1位，补0显示 
 minute="0"+minute; 
 if (second < 10) //如果秒数只有1位，补0显示 
 second="0"+second; 
 /*设置文本框的内容为当前时间*/ 
 document.getElementById("myclock").innerHTML ="当前时间："+year+"年"+month+"月"+date+"日"+" "+hour+":"+minute+":"+second; 
 /*设置定时器每隔1秒(1000毫秒)，调用函数disptime()执行，刷新时钟显示*/ 
 var myTime = setTimeout("disptime()",1000); 
 
 } 
 
</script>
</HEAD>
<BODY>
<TABLE cellSpacing=0 cellPadding=0 width="100%" align=center border=0>
  <TR height=28>
    <TD background=jsp/images/title_bg1.jpg>当前位置: </TD></TR>
  <TR>
    <TD bgColor=#b1ceef height=1></TD></TR>
  <TR height=20>
    <TD background=jsp/images/shadow_bg.jpg></TD></TR></TABLE>
<TABLE cellSpacing=0 cellPadding=0 width="90%" align=center border=0>
  <TR height=100>
    <TD align=middle width=100><IMG height=100 src="jsp/images/admin_p.gif" 
      width=90></TD>
    <TD width=60>&nbsp;</TD>
    <TD>
      <TABLE height=100 cellSpacing=0 cellPadding=0 width="100%" border=0>
        
        <TR>
          <TD><span id="myclock"></span></TD></TR>
        <TR>
          <TD style="FONT-WEIGHT: bold; FONT-SIZE: 16px">${session.agent.agentAccount }</TD></TR>
        <TR>
          <TD>欢迎进入网站管理中心！</TD></TR></TABLE></TD></TR>
  <TR>
    <TD colSpan=3 height=10></TD></TR></TABLE>
<TABLE cellSpacing=0 cellPadding=0 width="95%" align=center border=0>
  <TR height=20>
    <TD></TD></TR>
  <TR height=22>
    <TD style="PADDING-LEFT: 20px; FONT-WEIGHT: bold; COLOR: #ffffff" 
    align=middle background=jsp/images/title_bg2.jpg>您的相关信息</TD></TR>
  <TR bgColor=#ecf4fc height=12>
    <TD></TD></TR>
  <TR height=20>
    <TD></TD></TR></TABLE>
<TABLE cellSpacing=0 cellPadding=2 width="95%" align=center border=0>
  <TR>
    <TD align=right width=100>登陆帐号：</TD>
    <TD style="COLOR: #880000">${session.agent.agentAccount }</TD></TR>
  <TR>
    <TD align=right>真实姓名：</TD>
    <TD style="COLOR: #880000">${session.agent.agentName }</TD></TR>
  <TR>
    <TD align=right>注册时间：</TD>
    <TD style="COLOR: #880000">${session.agent.agentRegTime }</TD></TR>
<!--   <TR> -->
<!--     <TD align=right>登陆次数：</TD> -->
<!--     <TD style="COLOR: #880000">${session.agent.agentRegTime }</TD></TR> -->
  <TR>
    <TD align=right>上线时间：</TD>
    <TD style="COLOR: #880000">2008-12-27 17:02:54</TD></TR>
<!--   <TR> -->
<!--     <TD align=right>IP地址：</TD> -->
<!--     <TD style="COLOR: #880000">222.240.172.117</TD></TR> -->
<!--   <TR> -->
<!--     <TD align=right>身份过期：</TD> -->
<!--     <TD style="COLOR: #880000">30 分钟</TD></TR> -->
  <TR>
    <TD align=right>网站开发QQ：</TD>
    <TD style="COLOR: #880000">215288671</TD></TR>
  </TABLE>
    </BODY></HTML>