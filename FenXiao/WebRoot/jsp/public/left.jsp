<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>菜单</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<META http-equiv=Content-Type content="text/html; charset=utf-8">
<LINK href="jsp/css/admin.css" type="text/css" rel="stylesheet">
<LINK href="dtree_/dtree.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="dtree_/dtree.js"></script>
<script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>


<!-- 
 <script type="text/javascript">
 	var d = new dTree('d');
 	$(function(){
 		//发送ajax请求
 		var url = "${pageContext.request.contextPath}/RoleAction_queryMenu";
 		$.post(
 			url,
 			{},
 			function(data){
 				if(data.retcode == "0"){
 					d.add('ROOT_MENU',-1,'');
 					$.each(data.menuList,function(i,value){
 						
 						
 						d.add(value.menuCode,value.menuParentCode,value.menuTitle ,value.menuUrl,'','rightFrame','','','',0);
 					});
 					$("#menuDiv").html(d.toString());
 				}else{
 					alert(data.retmsg);
 				}
 			}
 		);
 	});
 </script>
 -->

</HEAD>
<BODY >
<div id="menuDiv"></div>

<script type="text/javascript">
 	
 	$(function(){
 		var htmlobj="";
 		//发送ajax请求
 		var url = "${pageContext.request.contextPath}/RoleAction_queryMenu";
 		$.post(
 			url,
 			{},
 			function(data){
 				if(data.retcode == "0"){
 					for(var i=0;i < data.menuList.length; i++ ){
 						htmlobj+='<TABLE height="100%" cellSpacing=0 cellPadding=0 width=170 '
 							+'background=jsp/images/menu_bg.jpg border=0>'
 						 +'<TR>' 
 						  +'<TD vAlign=top align=middle>'  
 						      +'<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>'  
 						      +  '<TR>'
 						         +'<TD height=10></TD></TR></TABLE>' 
 						      +'<TABLE cellSpacing=0 cellPadding=0 width=150 border=0>'
 						       +' <TR height=22>'
 						         +' <TD style="PADDING-LEFT: 30px" background=jsp/images/menu_bt.jpg>'
 						      +' <A  class=menuParent onclick=expand('+data.menuList[i].menuCode+') href="javascript:void(0);">'+data.menuList[i].menuTitle+'</A></TD></TR>'
 						       +' <TR height=4>'
 						        +' <TD></TD></TR></TABLE>' 
 						     +' <TABLE id=child'+data.menuList[i].menuCode+' style="DISPLAY: none" cellSpacing=0 cellPadding=0  width=150 border=0>'
 						      +'</TABLE>';	
 					}
 				
 					$("#menuDiv").html(htmlobj);
 				}else{
 					alert(data.msg);
 				}
 			}
 		);
 	});
 	
 	function expand(el){
        var childObj = $("#child" + el);
		

		if (childObj.css("display")=='none')
		{
			childObj.css("display","block");
		}
		else
		{
			childObj.css("display","none");
			return;
		}
		var menuCode=el;
 		var htmlobj="";
 		
 		var url="${pageContext.request.contextPath}/RoleAction_queryMenuChild";
		$.ajax({
			 type : "POST",
				url : url,//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
				data : {
					"menuCode":menuCode
				},//{"orderId":orderId,"commant":commant}
				/* dataType : "json", */
				success : function(data){
 	 				/* console.log(data); */
 	 				if(data.retcode == "0"){
 	 					for(var i=0;i < data.menuList1.length; i++ ){

 	 						
 	 						htmlobj+='<TR height=20>'
 	 						       +' <TD align=middle width=30><IMG height=9 src="jsp/images/menu_icon.gif" width=9></TD>'
 	 						            +' <TD><A class=menuChild  href="${pageContext.request.contextPath}'+data.menuList1[i].menuUrl+'?agentId='+${session.agent.agentId}+'"'+'  target=main>'+data.menuList1[i].menuTitle+'</A></TD></TR>' 
 	 						       +'<TR height=4>'
 	 						       +' <TD colSpan=2></TD></TR>';	
 	 					}
 	 				
 	 			    	 childObj.html(htmlobj);
 
 				}else{
 					alert(data.msg);
 				}
				}
				});
 	
 	}			
 		/* 
 		var menuCode=el;
 		var htmlobj="";
 		var url="${pageContext.request.contextPath}/RoleAction_queryMenuChild";
 		$.post(
 	 			url,
 	 			{"menuCode":menuCode},
 	 			function(data){
 	 				console.log(data);
 	 				if(data.retcode == "0"){
 	 					for(var i=0;i < data.menuList1.length; i++ ){
 	 						htmlobj+='<TR height=20>'
 	 						       +' <TD align=middle width=30><IMG height=9 src="jsp/images/menu_icon.gif" width=9></TD>'
 	 						            +' <TD><A class=menuChild  href="#"  target=main></A>data.menuList1[i].menuTitle</TD></TR>' 
 	 						       +'<TR height=4>'
 	 						       +' <TD colSpan=2></TD></TR></TABLE>';	
 	 					}
 	 				
 	 			    	 $("#child"+menuCode).html(htmlobj);
 
 				}else{
 					alert(data.msg);
 					}
 			   
 				}
 				);	
 		
 		
 	}
 	 */
 	
 	
 
 </script>


<!-- 
<TABLE height="100%" cellSpacing=0 cellPadding=0 width=170 
background=jsp/images/menu_bg.jpg border=0>
  <TR>
    <TD vAlign=top align=middle>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        
        <TR>
          <TD height=10></TD></TR></TABLE>
      <TABLE cellSpacing=0 cellPadding=0 width=150 border=0>
        
        <TR height=22>
          <TD style="PADDING-LEFT: 30px" background=jsp/images/menu_bt.jpg><A 
            class=menuParent onclick=expand(1) 
            href="javascript:void(0);">关于我们</A></TD></TR>
        <TR height=4>
          <TD></TD></TR></TABLE>
      <TABLE id=child1 style="DISPLAY: none" cellSpacing=0 cellPadding=0 
      width=150 border=0>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>公司简介</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>荣誉资质</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>分类管理</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>子类管理</A></TD></TR>
        <TR height=4>
          <TD colSpan=2></TD></TR></TABLE>
      <TABLE cellSpacing=0 cellPadding=0 width=150 border=0>
        <TR height=22>
          <TD style="PADDING-LEFT: 30px" background=jsp/images/menu_bt.jpg><A 
            class=menuParent onclick=expand(2) 
            href="javascript:void(0);">新闻中心</A></TD></TR>
        <TR height=4>
          <TD></TD></TR></TABLE>
      <TABLE id=child2 style="DISPLAY: none" cellSpacing=0 cellPadding=0 
      width=150 border=0>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>公司新闻</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>分类管理</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>子类管理</A></TD></TR>
        <TR height=4>
          <TD colSpan=2></TD></TR></TABLE>
      <TABLE cellSpacing=0 cellPadding=0 width=150 border=0>
        <TR height=22>
          <TD style="PADDING-LEFT: 30px" background=jsp/images/menu_bt.jpg><A 
            class=menuParent onclick=expand(3) 
            href="javascript:void(0);">产品中心</A></TD></TR>
        <TR height=4>
          <TD></TD></TR></TABLE>
      <TABLE id=child3 style="DISPLAY: none" cellSpacing=0 cellPadding=0 
      width=150 border=0>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>产品展示</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>最新产品</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>分类管理</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>子类管理</A></TD></TR>
        <TR height=4>
          <TD colSpan=2></TD></TR></TABLE>
      <TABLE cellSpacing=0 cellPadding=0 width=150 border=0>
        <TR height=22>
          <TD style="PADDING-LEFT: 30px" background=jsp/images/menu_bt.jpg><A 
            class=menuParent onclick=expand(4) 
            href="javascript:void(0);">客户服务</A></TD></TR>
        <TR height=4>
          <TD></TD></TR></TABLE>
      <TABLE id=child4 style="DISPLAY: none" cellSpacing=0 cellPadding=0 
      width=150 border=0>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>客户服务</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>分类管理</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>子类管理</A></TD></TR>
        <TR height=4>
          <TD colSpan=2></TD></TR></TABLE>
      <TABLE cellSpacing=0 cellPadding=0 width=150 border=0>
        <TR height=22>
          <TD style="PADDING-LEFT: 30px" background=jsp/images/menu_bt.jpg><A 
            class=menuParent onclick=expand(5) 
            href="javascript:void(0);">经典案例</A></TD></TR>
        <TR height=4>
          <TD></TD></TR></TABLE>
      <TABLE id=child5 style="DISPLAY: none" cellSpacing=0 cellPadding=0 
      width=150 border=0>
        
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>分类管理</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>子类管理</A></TD></TR>
        <TR height=4>
          <TD colSpan=2></TD></TR></TABLE>
      <TABLE cellSpacing=0 cellPadding=0 width=150 border=0>
        
        <TR height=22>
          <TD style="PADDING-LEFT: 30px" background=jsp/images/menu_bt.jpg><A 
            class=menuParent onclick=expand(6) 
            href="javascript:void(0);">高级管理</A></TD></TR>
        <TR height=4>
          <TD></TD></TR></TABLE>
      <TABLE id=child6 style="DISPLAY: none" cellSpacing=0 cellPadding=0 
      width=150 border=0>
        
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>广告管理</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>访问统计</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>邮件发送设置</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>联系部门</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>用户留言</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>招聘职位</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>应聘人员</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>留言簿</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>产品订购</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>链接管理</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>文件管理</A></TD></TR>
        <TR height=20>
          <TD align=middle width=30><IMG height=9 
            src="jsp/images/menu_icon.gif" width=9></TD>
          <TD><A class=menuChild 
            href="#" 
            target=main>信息转移</A></TD></TR>
        <TR height=4>
          <TD colSpan=2></TD></TR></TABLE> -->
     </BODY></HTML>
