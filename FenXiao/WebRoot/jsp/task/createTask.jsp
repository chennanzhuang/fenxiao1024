<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>任务生成</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  <link rel="stylesheet" type="text/css" href="jsp/css/css.css">
<script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>
 <link rel="stylesheet" type="text/css" href="<%=basePath%>jsp/js/kindeditor-4.1.9/themes/default/default.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>jsp/js/kindeditor-4.1.9/plugins/code/prettify.css" />
		<script charset="utf-8" src="<%=basePath%>jsp/js/kindeditor-4.1.9/kindeditor-all.js"></script>
		<script charset="utf-8" src="<%=basePath%>jsp/js/kindeditor-4.1.9/lang/zh_CN.js"></script>
		<script charset="utf-8" src="<%=basePath%>jsp/js/kindeditor-4.1.9/plugins/code/prettify.js"></script>
	<script charset="utf-8" src="<%=basePath%>js/text.js"></script>	
	<script src="jsp/layer/layer.js" type="text/javascript"></script>
	   <script language="javascript" type="text/javascript" src="<%=basePath%>/js/My97DatePicker/WdatePicker.js"></script>
			<script>
			var editor;
			KindEditor.ready(function(K) {
				editor = K.create('textarea[name="content"]', {
					width : "500px",
					// 高
					height :"300px",
					// 允许浏览远程服务器的文件
					allowFileManager : true,
					resizeType : 1,
					allowPreviewEmoticons : false,
					uploadJson : '<%=basePath%>KindEditorAction_upload',
					
					allowFileManager : true,
					afterBlur: function(){this.sync();}
				});
			});
  

		</script>
		<script>
			KindEditor.ready(function(K) {
				var editor = K.editor({
				imageUploadLimit : 5,
				uploadJson : '<%=basePath%>KindEditorAction_upload',
					allowFileManager : true,
					 afterUpload: function (url) {  
					
                  var a = $("#uppics").val();  
                if (a.length > 0) {  
                       $("#uppics").val(a + "," + url);  
                   } else {  
                    $("#uppics").val(url);  
                  }  
                  alert($("#uppics").val())  
                    }
				});
				
				K('#J_selectImage').click(function() {
				var url ="";
					editor.loadPlugin('multiimage', function() {
					
						editor.plugin.multiImageDialog({
		
				
							clickFn : function(urlList) {
								var div = K('#J_imageView');
								div.html('');
								K.each(urlList, function(i, data) {
								
									div.append('<img style="width:50px;height:50px"  src="' + data.url + '">');
									url += data.url+"*";
									
								});
								
								div.append('<input type="hidden" name="imgList" value="' + url + '" />');
								editor.hideDialog();
							}
						});
					});
				});
			});
		</script>
</head>
	
<script type="text/javascript">
$(function (){
	  $("#sub").click(function(){
		  var strBeginDate = document.getElementById("d5221").value;
		     if(strBeginDate==""||strBeginDate==null){
		    	 //alert("开始时间不能为空！");
		    	 layer.msg("开始时间不能为空！", {
		 		    time: 5000,  
		 		    offset: 0,
		 		    shift: 6
		 		  });
		 	   return false; 
		     }
		    var strEndDate = document.getElementById("d5222").value;
		  if(strEndDate==""||strEndDate==null){
			 // alert("结束时间不能为空！");
			  layer.msg("结束时间不能为空！", {
				    time: 5000,  
				    offset: 0,
				    shift: 6
				  });
			  return false; 
		  }
		  var weChat= $("#weChat").val();
			if(weChat==""){
				layer.msg("微信公众号不能为空！", {
				    time: 5000,  
				    offset: 0,
				    shift: 6
				  });
				return false;	
			} 
			var originalID=$("#originalID").val();
			if(originalID==""){
				layer.msg("微信公众号原始Id不能为空！", {
				    time: 5000,  
				    offset: 0,
				    shift: 6
				  });
				return false;	
			} 
			var mobilePhone=$("#mobilePhone").val();
			var reg = /^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
			if(mobilePhone.length==0||!reg.test(mobilePhone)){
				layer.msg("手机号为空或手机号格式错误！", {
				    time: 5000,  
				    offset: 0,
				    shift: 6
				  });
				return false;	
			
		 }
		  $(".form").submit();
	  });  
	  $("#ref").click(function(){
	
		  $(".form").attr("action","TaskAction_queryTask");
		  $(".form").submit();
	  });
})


</script>

<body>


<div class="nav">
	<div class="nav_title">
    	<h2><img class="nav_img" src="jsp/img/tab.gif" /><a class="nav_a" href="#">生成任务</a></h2>
    </div>
</div>

<form action="TaskAction_createTask" method="post" class="form" name="form1" enctype="multipart/form-data">
<input type="hidden" name="task.taskId" value="${task.taskId}">
<input type="hidden" name="" id="uppics" />
<div class="list">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="details">
        <tbody>
         <tr>
	      	<td width="14%"><label class="label">logo：</label></td>
	      	<td width="86%">
	      	<div id="preview1">
	      	<c:if test="${task.taskLogo!=null}">
	      	<img  src="${task.taskLogo}" name="task.taskLogo"  with="100" height="100"  >
	      	</c:if>
	      	</div>
	      	<input type="file" name="fileTest" onchange="preview1(this)" /></td>
             <script>  
       function preview1(file)  {
               var prevDiv = document.getElementById('preview1');
               if (file.files && file.files[0]){
                       var reader = new FileReader();
                         reader.onload = function(evt){
                        prevDiv.innerHTML = '<img src="' + evt.target.result + '" with="100" height="100"/>';
                             }  
                       reader.readAsDataURL(file.files[0]);
                         }else{
                  prevDiv.innerHTML = '<div class="img1" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src=\'' + file.value + '\')" ></div>';
    }
    }
 </script>	      
	      
	      </tr>
	      <tr>
	      	<td width="14%"><label class="label">标题：</label></td>
	      	<td width="86%"><input type="text" name="task.taskTitle" id="taskTitle" size="40" value="${task.taskTitle}" readonly="readonly"/></td>

	      </tr>
          <tr>
	      	<td width="14%"><label class="label">简介：</label></td>
	      	<td width="86%"><input type="text" name="task.taskBriefing" id="taskBriefing" size="40" value="${task.taskBriefing}" readonly="readonly"/></td>

	      </tr>

          <tr>
	      	<td><label class="label">任务总数：</label></td>
	      	<td>
	      	<input name="task.taskTotalNumber" type="text" size="40" id="taskTotalNumber" value="${task.taskTotalNumber}" readonly="readonly" />
   	        </td>
	      </tr>
	      <tr>
	      	<td><label class="label">任务总金额：</label></td>
	      	<td>
	      	<input name="task.taskTotalBonus" type="text" size="40" id="taskTotalBonus" value="${task.taskTotalBonus}" readonly="readonly" />
   	        </td>
	      </tr>
	      
	       <tr>
	      	<td><label class="label">任务类型：</label></td>
	      	<td>
	      	<c:if test="${task.taskType==1}">
	      	<input name="taskType" type="text" id="taskType" size="40" value="关注公众号" readonly="readonly" />
   	       </c:if>
   	       <c:if test="${task.taskType==2}">
	      	<input name="taskType" type="text" id="taskType"size="40" value="广告宣传" readonly="readonly" />
   	       </c:if>
   	       <c:if test="${task.taskType==3}">
	      	<input name="taskType" type="text" id="taskType"size="40" value="APP下载" readonly="readonly" />
   	       </c:if>
   	        </td>
	      </tr>
	        <c:if test="${task.taskType==2}">
        <tr>
	      	<td><label class="label">广告url：</label></td>
	      	<td><input type="text" name="task.adUrl" id="adUrl" size="40" value="${task.adUrl}" /></td>
	      </tr>
	       <tr>
	      	<td><label class="label">轮 播 图:</label></td>
	      	<td><div id="J_imageView"  ></div>
<input type="button" id="J_selectImage" value="批量上传"/>
</td>
	      </tr>
      
       
       
       </c:if>
        <c:if test="${task.taskType==3}">
        <tr>
	      	<td><label class="label">安卓下载url：</label></td>
	      	<td><input type="text" name="task.appAndroidUrl" id="appAndroidUrl" size="40" value="${task.appAndroidUrl}" /></td>
	      </tr>
       <tr>
	      	<td><label class="label">IOS下载url：</label></td>
	      	<td><input type="text" name="task.appIOSUrl" id="appIOSUrl" size="40" value="${task.appIOSUrl}" /></td>
	      </tr>
       </c:if>
      <tr>
	      	<td><label class="label">开始时间：</label></td>
	      	<td>
	      	<!--  <input type="text" class="Wdate" name="task.taskBeginTime" id="taskBeginTime"  style="width: 30%" size="40" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/> -->
	     <input id="d5221" class="Wdate" type="text"  name="task.taskBeginTime" style="width: 30%" onFocus="var d5222=$dp.$('d5222');WdatePicker({onpicked:function(){d5222.focus();},maxDate:'#F{$dp.$D(\'d5222\')}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
	     
	     </td>
	     
	     
	      </tr>
      <tr>
	      	<td><label class="label">结束时间：</label></td>
	      	<td>
	      	<!-- <input type="text"  class="Wdate" name="task.taskEndTime" id="taskEndTime" size="40" value="" style="width: 30%" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/> -->
	      	<input id="d5222" class="Wdate" name="task.taskEndTime"  style="width: 30%" type="text" onFocus="WdatePicker({minDate:'#F{$dp.$D(\'d5221\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:CalDays})"/>
	      
	      
	      	</td>
	     
	      </tr>
     
       <c:if test="${task.taskType==1}">
        <tr>
	      	<td><label class="label">公众号微信号：</label></td>
	      	<td><input type="text" name="task.weChat" id="weChat" size="40" value="" onblur="onweChat()" /></td>
	      </tr>
       <tr>
	      	<td><label class="label">公众号原始ID：</label></td>
	      	<td><input type="text" name="task.originalID" id="originalID" size="40" value="" onblur="checkOriginalID()" /></td>
	      </tr>
       </c:if>
        
         <tr>
	      	<td><label class="label">总天数：</label></td>
	      	<td><input type="text" name="dateNum" id="txtDays" size="40" value="" readonly="readonly"/>天</td>
	      </tr>

         <tr>
	      	<td><label class="label">联系方式：</label></td>
	      	<td><input type="text" name="task.mobilePhone" id="mobilePhone" size="40" value="" onblur="checkMobilePhone()" maxlength="11"/></td>
	      </tr>
         <tr>
	      	<td><label class="label">是否上传图片：</label></td>
	      	<td><input type="radio" name="task.shengc" id="shengc" size="40" value="1"  maxlength="11" checked="checked"/>需要
	      		<input type="radio" name="task.shengc" id="shengc" size="40" value="2"  maxlength="11"/>不需要
	      	</td>
	      </tr>
	        <tr>
	      	<td><label class="label">地址：</label></td>
	      	<td>
	      	<select id="select_province" name="task.province" style="width:80px;"></select>
			<select id="select_city" name="task.city"  style="width:80px"></select>
			<select id="select_area" name="task.area"  style="width:80px"></select>
   	        </td>
	      </tr>
	      <tr>
	      	<td><label class="label">详情：</label></td>
	      	<td>
	     <textarea id="content" name="content"></textarea>
	      </td>
	      </tr>
	      
	      
        </tbody>
  </table>
</div>

<div class="footer">
	<div class="save_button">
        <span class="span_button" id="sub">生成任务</span>
        <span class="span_button"   id="ref">取 消</span>
    </div>
</div>

</form>

</body>
<script type="text/javascript">


function CalDays() {
	 
    var strBeginDate = document.getElementById("d5221").value;
     if(strBeginDate==""||strBeginDate==null){
    	 //alert("开始时间不能为空！");
    	 layer.msg("开始时间不能为空！", {
 		    time: 5000,  
 		    offset: 0,
 		    shift: 6
 		  });
 	   return false; 
     }
    var strEndDate = document.getElementById("d5222").value;
  if(strEndDate==""||strEndDate==null){
	 // alert("结束时间不能为空！");
	  layer.msg("结束时间不能为空！", {
		    time: 5000,  
		    offset: 0,
		    shift: 6
		  });
	  return false; 
  }
    var timeSpan = countTimeLength("D", strBeginDate, strEndDate);
    

    txtDays.value = timeSpan;
  return true;
}
/**  
* 计算两日期时间差  
* @param {} interval 计算类型：D是按照天、H是按照小时、M是按照分钟、S是按照秒、T是按照毫秒  
* @param {} date1 起始日期  
* @param {} date2 结束日期  
* @return {}  
*/  
function countTimeLength(interval, date1, date2) {
    var objInterval = { 'D': 1000 * 60 * 60 * 24, 'H': 1000 * 60 * 60, 'M': 1000 * 60, 'S': 1000, 'T': 1 };
    interval = interval.toUpperCase();
    var dt1 = Date.parse(StringToDate(date1));
    var dt2 = Date.parse(StringToDate(date2));
    try {
        return ((((dt2 - dt1) / objInterval[interval]).toFixed(2))); //保留两位小数点   
    } catch (e) {
        return e.message;
    }
}
function StringToDate(string) {
    return new Date(Date.parse(string.replace(/-/g, "/")));
}  

function onweChat(){
	var weChat= $("#weChat").val();
	if(weChat==""){
		layer.msg("微信公众号不能为空！", {
		    time: 5000,  
		    offset: 0,
		    shift: 6
		  });
		return false;	
	}

}
function checkOriginalID(){
	var originalID=$("#originalID").val();
	if(originalID==""){
		layer.msg("微信公众号原始Id不能为空！", {
		    time: 5000,  
		    offset: 0,
		    shift: 6
		  });
		return false;	
	}
}
 function checkMobilePhone(){
	var mobilePhone=$("#mobilePhone").val();
	 var reg = /^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
	if(mobilePhone==""||!reg.test(mobilePhone)){
		layer.msg("手机号为空或手机号格式错误！", {
		    time: 5000,  
		    offset: 0,
		    shift: 6
		  });
		return false;	
	
 }
	
}

</script>
<script type="text/javascript">
  var mesg="${msg}";
  if(mesg!=""){
	  alert(mesg);
  }


region_init("select_province","select_city","select_area"); 
</script>

</html>