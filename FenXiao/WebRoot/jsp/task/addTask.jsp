<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>任务生成</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  <link rel="stylesheet" type="text/css" href="jsp/css/css.css">
<script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>
 <link rel="stylesheet" type="text/css" href="<%=basePath%>jsp/js/kindeditor-4.1.9/themes/default/default.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>jsp/js/kindeditor-4.1.9/plugins/code/prettify.css" />
		<script charset="utf-8" src="<%=basePath%>jsp/js/kindeditor-4.1.9/kindeditor-all.js"></script>
		<script charset="utf-8" src="<%=basePath%>jsp/js/kindeditor-4.1.9/lang/zh_CN.js"></script>
		<script charset="utf-8" src="<%=basePath%>jsp/js/kindeditor-4.1.9/plugins/code/prettify.js"></script>
		
		<script type="text/javascript">
		var editor;
		KindEditor.ready(function(K) {
			editor = K.create('textarea[name="content"]', {
				width : "500px",
				// 高
				height :"300px",
				// 允许浏览远程服务器的文件
				allowFileManager : true,
				resizeType : 1,
				allowPreviewEmoticons : false,
				uploadJson : '<%=basePath%>KindEditorAction_upload',
				
				allowFileManager : true,
				afterBlur: function(){this.sync();}
			});
		});
		
		<%-- var editor;
			KindEditor.ready(function(K) {
				editor = K.create("textarea[name='content']", {
					// 宽
					width : "400px",
					// 高
					height :"200px",
					// 允许浏览远程服务器的文件
					allowFileManager : true,
					resizeType : 1,
					allowPreviewEmoticons : false,
					uploadJson : '<%=basePath%>KindEditorAction_upload',
					items : [
                    "fontname", "fontsize", "|",
                    "forecolor", "hilitecolor", "bold", "italic", "underline", "removeformat", "|", 
                     "justifyleft", "justifycenter", "justifyright", "justifyfull", "insertorderedlist", "insertunorderedlist", "indent", "outdent", "subscript", "superscript", "|", 
                     "emoticons", "image", "multiimage", "link", "unlink", "fullscreen"],
						afterBlur: function(){this.sync();}
				});
			}); --%>
	</script>
</head>
	
<script type="text/javascript">
$(function (){
	  $("#sub").click(function(){
		  $(".form").submit();
	  });  
	  $("#ref").click(function(){
	
		  $(".form").attr("action","TaskAction_queryTask");
		  $(".form").submit();
	  });
})


</script>
 <%-- <script type="text/javascript">

     function a(value){
    
    	 if(value==1){
    		 $(".a1").css("display","block");
    	   	  $(".a2").css("display","none");
    	   	  $(".a3").css("display","none");  
    	 }
    	 if(value==2){
    		 $(".a1").css("display","none");
    	   	  $(".a2").css("display","block");
    	   	  $(".a3").css("display","none"); 
    	 }
    	 if(value==3){
    		 $(".a1").css("display","none");
    	   	  $(".a2").css("display","none");
    	   	  $(".a3").css("display","block");
    	 }
     }

</script> --%>
<body>


<div class="nav">
	<div class="nav_title">
    	<h2><img class="nav_img" src="jsp/img/tab.gif" /><a class="nav_a" href="#">生成任务</a></h2>
    </div>
</div>

<form action="TaskAction_addTask" method="post" class="form" name="form1" enctype="multipart/form-data">
<div class="list">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="details">
        <tbody>
         <tr>
	      	<td width="19%"><label class="label">logo：</label></td>
	      	<td width="81%">
	      	<div id="preview1"></div>
	      	
	      	<input type="file" name="fileTest" onchange="preview1(this)" /></td>
             <script>  
       function preview1(file)  {
               var prevDiv = document.getElementById('preview1');
               if (file.files && file.files[0]){
                       var reader = new FileReader();
                         reader.onload = function(evt){
                        prevDiv.innerHTML = '<img src="' + evt.target.result + '" with="100" height="100"/>';
                             }  
                       reader.readAsDataURL(file.files[0]);
                         }else{
                  prevDiv.innerHTML = '<div class="img1" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src=\'' + file.value + '\')" ></div>';
    }
    }
 </script>	      
	      
	      </tr>
	      <tr>
	      	<td width="19%"><label class="label">标题：</label></td>
	      	<td width="81%"><input type="text" name="task.taskTitle" id="taskTitle" size="40" value=""/></td>

	      </tr>
          <tr>
	      	<td width="19%"><label class="label">简介：</label></td>
	      	<td width="81%"><input type="text" name="task.taskBriefing" id="taskBriefing" size="40" value="" /></td>

	      </tr>

          <tr>
	      	<td><label class="label">任务总数：</label></td>
	      	<td>
	      	<input name="task.taskTotalNumber" type="text" id="taskTotalNumber" size="40"value="" />
   	        </td>
	      </tr>
	       <tr>
	      	<td><label class="label">任务总金额：</label></td>
	      	<td>
	      	<input name="task.taskTotalBonus" type="text" id="taskTotalBonus"size="40" value="" />
   	        </td>
	      </tr>
	     
	      <tr>
	      	<td><label class="label">任务类型：</label></td>
	      	<td>
	      	<select name="task.taskType"  style="width:299px" id="taskType" onchange="a(value)">
	      	<option value="1" id="a1">关注公众号</option>
	      	<option value="2" id="a2" >广告宣传</option>
	      	<option value="3" id="a3" >APP下载</option>
	      	</select>
	      	
   	        </td>
	      </tr>
      <tr>
	      	<td><label class="label">开始时间：</label></td>
	      	<td><input type="text" name="task.taskBeginTime" id="taskBeginTime" size="40" value="${task.taskBeginTime }" /></td>
	      </tr>
      <tr>
	      	<td><label class="label">结束时间：</label></td>
	      	<td><input type="text" name="task.taskEndTime" id="taskEndTime" size="40" value="" /></td>
	      </tr>
       <tr>
	      	<td><label class="label">总天数：</label></td>
	      	<td><input type="text" name="task.dateNum" id="dateNum" size="40" value="" /></td>
	      </tr>
	      
	        <tr class="a1">
	      	<td width="19%" style="width: 14%;"><label class="label">二维码：</label></td>
	      	<td width="81%" style="width: 86%;">
	      	<div id="preview"></div>
	      	
	      	<input type="file" name="fileTest1" onchange="preview(this)" /></td>
             <script>  
       function preview(file)  {
               var prevDiv = document.getElementById('preview');
               if (file.files && file.files[0]){
                       var reader = new FileReader();
                         reader.onload = function(evt){
                        prevDiv.innerHTML = '<img src="' + evt.target.result + '" with="100" height="100"/>';
                             }  
                       reader.readAsDataURL(file.files[0]);
                         }else{
                  prevDiv.innerHTML = '<div class="img1" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src=\'' + file.value + '\')" ></div>';
    }
    }
 </script>	      
	      
	      </tr>

	      
	      <tr class="a3">
	      	<td><label class="label">广告路径：</label></td>
	      	<td><input type="text" name="task.adUrl" id="adUrl" size="40" value="" /></td>
	      </tr>
	      
	      <tr class="a3">
	      	<td><label class="label">安卓路径：</label></td>
	      	<td><input type="text" name="task.appAndroidUrl" id="appAndroidUrl" size="40" value="" /></td>
	      </tr>
	      
	      
	      <tr class="a3">
	      	<td><label class="label">IOS路径：</label></td>
	      	<td><input type="text" name="task.appIOSUrl" id="appIOSUrl" size="40" value="" /></td>
	      </tr>
	     
	      
          <tr>
	      	<td><label class="label">联系方式：</label></td>
	      	<td><input type="text" name="task.taskContacts" id="taskContacts" size="40" value="" /></td>
	      </tr>
	      <tr>
	      	<td><label class="label">发布人：</label></td>
	      	<td>
	      	<select name="user.id"  style="width:299px">
	      	<c:forEach items="${userList}" var="user">
	      	<option value="${user.id}">${user.username} </option>
	      	</c:forEach>
	      	</select>
           </td>
	      </tr>
	      <tr>
	      	<td><label class="label">详情：</label></td>
	      	<td>
	     <textarea id="content" name="content" style="width:800px; height:600px;"></textarea>
	      </td>
	      </tr>
	      
	      
        </tbody>
  </table>
</div>

<div class="footer">
	<div class="save_button">
        <span class="span_button" id="sub">确认</span>
        <span class="span_button"   id="ref">取 消</span>
    </div>
</div>

</form>

</body>
<script type="text/javascript">
  var mesg="${msg}";
  if(mesg!=""){
	  alert(mesg);
  }
  
</script>
</html>