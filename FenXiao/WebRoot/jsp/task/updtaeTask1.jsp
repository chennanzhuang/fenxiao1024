<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>修改任务</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

    <link rel="stylesheet" type="text/css" href="jsp/css/css.css">
    <script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<%=basePath%>jsp/js/kindeditor-4.1.9/themes/default/default.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>jsp/js/kindeditor-4.1.9/plugins/code/prettify.css" />
    <script charset="utf-8" src="<%=basePath%>jsp/js/kindeditor-4.1.9/kindeditor-all.js"></script>
	<script charset="utf-8" src="<%=basePath%>jsp/js/kindeditor-4.1.9/lang/zh_CN.js"></script>
	<script charset="utf-8" src="<%=basePath%>jsp/js/kindeditor-4.1.9/plugins/code/prettify.js"></script>
		
	
			<script>
			var editor;
			KindEditor.ready(function(K) {
				editor = K.create('textarea[name="content"]', {
					width : "1000px",
					// 高
					height :"500px",
					// 允许浏览远程服务器的文件
					allowFileManager : true,
					resizeType : 1,
					allowPreviewEmoticons : false,
					uploadJson : '<%=basePath%>KindEditorAction_upload',
					
					allowFileManager : true,
					afterBlur: function(){this.sync();}
				});
			});
  
  

		</script>
		
</head>
	
<script type="text/javascript">
$(function (){
	  $("#sub").click(function(){
		  $(".form").submit();
	  });  
	  $("#ref").click(function(){
	
		  $(".form").attr("action","TaskAction_findTaskPage");
		  $(".form").submit();
	  });
})


</script>
<body>


<div class="nav">
	<div class="nav_title">
    	<h2><img class="nav_img" src="jsp/img/tab.gif" /><a class="nav_a" href="#">修改任务</a></h2>
    </div>
</div>

<form action="TaskAction_updtaeTask1" method="post" class="form" name="form1" enctype="multipart/form-data">
 <input type="hidden" name="task.taskId" value="${task.taskId}">
  <input type="hidden" name="task.taskStatus" value="${task.taskStatus}">
<div class="list">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="details">
        <tbody>
         <tr>
	      	<td width="14%"><label class="label">logo：</label></td>
	      	<td width="86%">
	      	<div id="preview1">
	      	<c:if test="${task.taskLogo!=null}">
	      	<img  src="${task.taskLogo}" name="task.taskLogo"  with="100" height="100"  >
	      	</c:if>
	      	</div>
	      	<input type="file" name="fileTest" onchange="preview1(this)" /></td>
             <script>  
       function preview1(file)  {
               var prevDiv = document.getElementById('preview1');
               if (file.files && file.files[0]){
                       var reader = new FileReader();
                         reader.onload = function(evt){
                        prevDiv.innerHTML = '<img src="' + evt.target.result + '" with="100" height="100"/>';
                             }  
                       reader.readAsDataURL(file.files[0]);
                         }else{
                  prevDiv.innerHTML = '<div class="img1" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src=\'' + file.value + '\')" ></div>';
    }
    }
 </script>	      
	      
	      </tr>
	      <tr>
	      	<td width="14%"><label class="label">标题：</label></td>
	      	<td width="86%"><input type="text" name="task.taskTitle" id="taskTitle" size="40" value="${task.taskTitle}" /></td>

	      </tr>
          <tr>
	      	<td width="14%"><label class="label">简介：</label></td>
	      	<td width="86%"><input type="text" name="task.taskBriefing" id="taskBriefing" size="40" value="${task.taskBriefing}"/></td>

	      </tr>
         <tr>
	      	<td><label class="label">任务总佣金：</label></td>
	      	<td>
	      	<input name="task.taskTotalBonus" type="text" id="taskTotalBonus" value="${task.taskBonus}"/>
   	        </td>
	      </tr>
          <tr>
	      	<td><label class="label">任务总数：</label></td>
	      	<td>
	      	<input name="task.taskTotalNumber" type="text" id="taskTotalNumber" value="${task.taskTotalNumber}"/>
   	        </td>
	      </tr>
	      
	      
	       <tr>
	      	<td><label class="label">任务类型：</label></td>
	      	<td>
	      	<c:if test="${task.taskType==1}">
	      	<input name="taskType" type="text" id="taskType" value="关注公众号" readonly="readonly"/>
   	       </c:if>
   	       <c:if test="${task.taskType==2}">
	      	<input name="taskType" type="text" id="taskType" value="广告宣传" readonly="readonly"/>
   	       </c:if>
   	       <c:if test="${task.taskType==3}">
	      	<input name="taskType" type="text" id="taskType" value="APP下载" readonly="readonly"/>
   	       </c:if>
   	        </td>
	      </tr>
      <tr>
	      	<td><label class="label">开始时间：</label></td>
	      	<td><input type="text" name="task.taskBeginTime" id="taskBeginTime" size="40" value="${task.taskBeginTime}" /></td>
	      </tr>
      <tr>
	      	<td><label class="label">结束时间：</label></td>
	      	<td><input type="text" name="task.taskEndTime" id="taskEndTime" size="40" value="${task.taskEndTime}" /></td>
	      </tr>
       <tr>
	      	<td><label class="label">总天数：</label></td>
	      	<td><input type="text" name="task.dateNum" id="dateNum" size="40" value="${task.dateNum}" /></td>
	      </tr>
         <tr>
	      	<td><label class="label">联系方式：</label></td>
	      	<td><input type="text" name="task.mobilePhone" id="mobilePhone" size="40" value="${task.mobilePhone}" /></td>
	      </tr>
	      <tr>
	      	<td><label class="label">详情：</label></td>
	      	<td>
	     <textarea id="content" name="content">${task.taskInfo}</textarea>
	      </td>
	      </tr>
	            
        </tbody>
  </table>
</div>

<div class="footer">
	<div class="save_button">
        <span class="span_button" id="sub">确认</span>
        <span class="span_button"   id="ref">取 消</span>
    </div>
</div>

</form>

</body>
<script type="text/javascript">
  var mesg="${msg}";
  if(mesg!=""){
	  alert(mesg);
  }  
</script>
</html>