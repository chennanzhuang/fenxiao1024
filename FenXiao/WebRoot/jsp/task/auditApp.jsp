<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>审核APP任务</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

    <link rel="stylesheet" type="text/css" href="jsp/css/css.css">
    <script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<%=basePath%>jsp/js/kindeditor-4.1.9/themes/default/default.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>jsp/js/kindeditor-4.1.9/plugins/code/prettify.css" />
    <script charset="utf-8" src="<%=basePath%>jsp/js/kindeditor-4.1.9/kindeditor-all.js"></script>
	<script charset="utf-8" src="<%=basePath%>jsp/js/kindeditor-4.1.9/lang/zh_CN.js"></script>
	<script charset="utf-8" src="<%=basePath%>jsp/js/kindeditor-4.1.9/plugins/code/prettify.js"></script>
		
	<%-- 	 <script type="text/javascript">
			KindEditor.ready(function(K) {
				var editor = K.create("textarea[name='content']", {
					// 宽
					width : "400px",
					// 高
					height :"200px",
					// 允许浏览远程服务器的文件
					allowFileManager : true,
					// 上传文件
					uploadJson : '<%=basePath%>KindEditorAction_upload',
					// 获取远程服务器的文件
					/*fileManagerJson : '<%=basePath%>KindEditorAction_download',*/
					items : [
						"fontname", "fontsize", "|",
						"forecolor", "hilitecolor", "bold", "italic", "underline", "removeformat", "|", 
						"justifyleft", "justifycenter", "justifyright", "justifyfull", "insertorderedlist", "insertunorderedlist", "indent", "outdent", "subscript", "superscript", "|", 
						"emoticons", "image", "multiimage", "link", "unlink", "fullscreen"],
					
					
				}); 
				
				// 将远程服务器的图片在文本域中显示出来
				prettyPrint();
			});
		</script> --%>
			<script>
		
			var editor;
			KindEditor.ready(function(K) {
				editor = K.create('textarea[name="content"]', {
					width : "1000px",
					// 高
					height :"500px",
					// 允许浏览远程服务器的文件
					allowFileManager : true,
					resizeType : 1,
					allowPreviewEmoticons : false,
					uploadJson : '<%=basePath%>KindEditorAction_upload',
					
					allowFileManager : true,
					afterBlur: function(){this.sync();}
				});
			});

			<%-- var editor;
				KindEditor.ready(function(K) {
					editor = K.create("textarea[name='content']", {
						// 宽
						width : "400px",
						// 高
						height :"200px",
						// 允许浏览远程服务器的文件
						allowFileManager : true,
						resizeType : 1,
						allowPreviewEmoticons : false,
						uploadJson : '<%=basePath%>KindEditorAction_upload',
						items : [
	                    "fontname", "fontsize", "|",
	                    "forecolor", "hilitecolor", "bold", "italic", "underline", "removeformat", "|", 
	                     "justifyleft", "justifycenter", "justifyright", "justifyfull", "insertorderedlist", "insertunorderedlist", "indent", "outdent", "subscript", "superscript", "|", 
	                     "emoticons", "image", "multiimage", "link", "unlink", "fullscreen"],
							afterBlur: function(){this.sync();}
					});
				}); --%>
		</script>
		
</head>
	
<script type="text/javascript">
$(function (){
	  $("#sub").click(function(){
		  $(".form").submit();
		  $("#sub").attr({"disabled":"disabled"});
	  });  
	  $("#ref").click(function(){
	
		  $(".form").attr("action","UserTaskAction_queryAuditNo");
		  $(".form").submit();
	  });
})


</script>
<body>


<div class="nav">
	<div class="nav_title">
    	<h2><img class="nav_img" src="jsp/img/tab.gif" /><a class="nav_a" href="#">审核App任务</a></h2>
    </div>
</div>

<form action="UserTaskAction_queryAudit" method="post" class="form" name="form1" enctype="multipart/form-data">
 <input type="hidden" name="usertask.userTaskId" value="${usertask.userTaskId}">
 <input type="hidden" name="usertask.taskId" value="${usertask.taskId}">
 <input type="hidden" name="user.id" value="${user.id}">
<div class="list">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="details">
        <tbody>
         <tr>
	      	<td width="14%"><label class="label">订单号：</label></td>
	      	<td width="86%"><input type="text" name="usertask.taskOrderCode" id="taskOrderCode" size="40" value="${usertask.taskOrderCode}" readonly="readonly"/></td>

	      </tr>
        
        	<c:if test="${usertask.taskLogo!=null}">
         <tr>
	      	<td width="14%"><label class="label">logo：</label></td>
	      	<td width="86%">
	      	<div id="preview1">
	      
	      	<img  src="${usertask.taskLogo}" name="usertask.taskLogo"  with="100" height="100"  >
	      	
	      	</div>
	      </tr>
	      </c:if>
	      <tr>
	      	<td width="14%"><label class="label">标题：</label></td>
	      	<td width="86%"><input type="text" name="usertask.taskTitle" id="taskTitle" size="40" value="${usertask.taskTitle}" readonly="readonly"/></td>

	      </tr>
          <tr>
	      	<td width="14%"><label class="label">简介：</label></td>
	      	<td width="86%"><input type="text" name="usertask.taskBriefing" id="taskBriefing" size="40" value="${usertask.taskBriefing}" readonly="readonly"/></td>

	      </tr>
         <tr>
	      	<td><label class="label">任务佣金：</label></td>
	      	<td>
	      	<input name="usertask.taskBonus" type="text" id="taskBonus" value="${usertask.taskBonus}" readonly="readonly"/>
   	        </td>
	      </tr>
 
	       <tr>
	      	<td><label class="label">任务类型：</label></td>
	      	<td>
	      	<c:if test="${usertask.taskType==1}">
	      	<input name="taskType" type="text" id="taskType" value="关注公众号" readonly="readonly"/>
   	       </c:if>
   	       <c:if test="${usertask.taskType==2}">
	      	<input name="taskType" type="text" id="taskType" value="广告宣传"readonly="readonly"/>
   	       </c:if>
   	       <c:if test="${usertask.taskType==3}">
	      	<input name="taskType" type="text" id="taskType" value="APP下载"readonly="readonly"/>
   	       </c:if>
   	        </td>
	      </tr>
      <tr>
	      	<td><label class="label">下单时间：</label></td>
	      	<td><input type="text" name="usertask.taskOrderTime" id="taskOrderTime" size="40" value="${usertask.taskOrderTime}" readonly="readonly"/></td>
	      </tr>
    
	        <tr>
	      	<td><label class="label">申请人：</label></td>
	      	<td><input type="text" name="user.username" id="user" size="40" value="${user.username}" readonly="readonly"/></td>
	      </tr> 
	        <tr>
	      	<td><label class="label">用户名：</label></td>
	      	<td><input type="text" name="usertask.appRegisterName" id="user" size="40" value="${usertask.appRegisterName}" readonly="readonly"/></td>
	      </tr> 
	      <tr>
	      	<td><label class="label">审核图片：</label></td>
	      	<td>
	      	   <textarea id="content" name="content" readonly="readonly">
         <c:forEach items="${shotList}" var="shot">
	      	<img  src="<%=basePath%>${shot.screenshotUrl}" >
	      	</c:forEach> 
        </textarea>	      	     	
	      	</td>
	      </tr>     
	      <tr>
	      	<td><label class="label">审核结果：</label></td>
	      	<td>
	      	   <input type="text" id="taskReson" name="usertask.taskReson">
            	     	
	      	</td>
	      </tr>         
        </tbody>
  </table>
</div>

<div class="footer">
	<div class="save_button">
        <span class="span_button" id="sub">审核通过</span>
        <span class="span_button"   id="ref">审核不通过</span>
    </div>
</div>

</form>

</body>
<script type="text/javascript">
  var mesg="${msg}";
  if(mesg!=""){
	  alert(mesg);
  }
  
</script>
</html>