<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en" class="no-js">

<head>
    <base href="<%=basePath%>">
    
   <title>登陆</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

<link rel="stylesheet" type="text/css" href="jsp/css/login.css">
<script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>
</head>
<body>
<div class="Content1">
	<div class="Content">
	<form name="loginform" id="loginform" method="post" action="AgentAction_agentlogin" onSubmit="return loginformsave()">
	  <input type="hidden" name="archive" value="adminuser">
	  <input type="hidden" name="action" value="login_into">
	  <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" >
	    <tr>
	      <td width="12%">用户名：</td>
	      <td width="42%"><input type="text" name="agent.agentAccount" class="infoInput" maxlength="20" id="username" style="width:160px; height:24px; margin-top:5px; font-size:16px; padding-left:2px;" /></td>
	      <td width="46%" rowspan="3"><img src="jsp/img/logo.jpg" width="237" height="86" /></td>
        </tr>
	    <tr>
	      <td height="36">密&nbsp;&nbsp;&nbsp;码：</td>
	      <td><input type="password" name="agent.agentPass" id="password" style="width:160px; height:24px; margin-top:12px; font-size:16px; padding-left:2px;" /></td>
        </tr>
	    <tr>
	      <td>验证码：</td>
	      <td><input type="text" name="seccode" class="infoInput" id="seccode" maxlength="4" style="width:80px; height:24px; text-transform: uppercase; margin-top:12px; font-size:16px; padding-left:2px;"/>
	        <img id="checkImg" name="imgcode" onclick="change()" class="captchaImage" style="width:73px; height:24px; text-transform: uppercase; margin-top:12px; font-size:16px; padding-left:2px;" src="checkImg.action">
				
	        </td>
        </tr>
        <tr>
        <td>&nbsp;</td>
	      <td><p style="font: 12px;color: red;">${msg}</p></td>
	      <td>&nbsp;</td>
        </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="submit" name="submit" value="登&nbsp;&nbsp;&nbsp;录" style="width:165px; height:34px; cursor:pointer; background:#06F; border:0; color:#FFF; font-size:14px; font-weight:600; border-radius:4px; position:relative; top:15px;" /></td>
	      <td>&nbsp;</td>
        </tr>
      </table>
	  </form>
	</div>
      <div class="End">我们愿意成为您可靠的长期合作伙伴</div>
</div>

</body>
<script type="text/javascript">

function change(){
		var img1 = document.getElementById("checkImg");
		img1.src="${pageContext.request.contextPath}/checkImg.action?"+new Date().getTime();
	}

</script>
</html>
