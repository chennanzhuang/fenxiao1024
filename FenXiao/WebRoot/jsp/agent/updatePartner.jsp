<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>提交代理商资料</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  <link rel="stylesheet" type="text/css" href="jsp/css/css.css">
<script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>

</head>
<script type="text/javascript">
$(function (){
	  $("#sub").click(function(){
		  $(".form").submit();
	  });  
	  $("#ref").click(function(){
		  $(".form").reset();
	  });
})

</script>
<body>


<div class="nav">
	<div class="nav_title">
    	<h2><img class="nav_img" src="jsp/img/tab.gif" /><a class="nav_a" href="#">修改代理商</a></h2>
    </div>
</div>

<form action="AgentAction_updatePartner" method="post" class="form" name="form1" enctype="multipart/form-data">
<input type="hidden" name="agentId" value="${session.agent.agentId}">
<input type="hidden" name="agent.agentId" value="${agent.agentId}">
<input type="hidden" name="agent.agentPass" id="agentPass" size="40" value="${agent.agentPass}" />
<input type="hidden" name="agent.agentStatus" id="agentStatus" size="40" value="${agent.agentStatus}" />
<input type="hidden" name="roleId" id="role" size="40" value="2" />
<div class="list">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="details">
        <tbody>
         <tr>
	      	<td width="14%"><label class="label">头像：</label></td>
	      	<td width="86%">	      
	      	<div id="preview1">
	      	<c:if test="${agent.agentPhoto!=null}">
	      	<img src="${agent.agentPhoto}"name="agent.agentPhoto"  with="100" height="100"/>
	      	</c:if>
	      	</div>
	      	<input type="file" name="fileTest" onchange="preview1(this)" /></td>
             <script>  
       function preview1(file)  {
               var prevDiv = document.getElementById('preview1');
               if (file.files && file.files[0]){
                       var reader = new FileReader();
                         reader.onload = function(evt){
                        prevDiv.innerHTML = '<img src="' + evt.target.result + '" with="100" height="100"/>';
                             }  
                       reader.readAsDataURL(file.files[0]);
                         }else{
                  prevDiv.innerHTML = '<div class="img1" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src=\'' + file.value + '\')" ></div>';
    }
    }
 </script>	      
	      
	      </tr>
	      <tr>
	      	<td width="14%"><label class="label">账号：</label></td>
	      	<td width="86%"><input type="text" name="agent.agentAccount" id="agentAccount" size="40" value="${agent.agentAccount}" /></td>

	      </tr>
          <tr>
	      	<td width="14%"><label class="label">名称：</label></td>
	      	<td width="86%"><input type="text" name="agent.agentName" id="agentName" size="40" value="${agent.agentName}" /></td>

	      </tr>
          <tr>
	      	<td><label class="label">性别：</label></td>
	      	
	      	<td>
	      	<c:if test="${agent.sex==1}">
	      	<input name="agent.sex" type="radio" id="sex" value="1" checked="checked" />
      	   男
      	      <input type="radio" name="agent.sex" id="sex2" value="2" />
   	        女
   	        </c:if>
   	         	<c:if test="${agent.sex==2}">
	      	<input name="agent.sex" type="radio" id="sex" value="1"  />
      	   男
      	      <input type="radio" name="agent.sex" id="sex2" value="2"checked="checked" />
   	        女
   	        </c:if>
   	        </td>
	      </tr>
      
          <tr>
	      	<td><label class="label">联系方式：</label></td>
	      	<td><input type="text" name="agent.agentPhone" id="agentPhone" size="40" value="${agent.agentPhone}" /></td>
	      </tr>
	      <tr>
	      	<td><label class="label">微信号：</label></td>
	      	<td><input type="text" name="agent.agentWeChatId" id="agentWeChatId" size="40" value="${agent.agentWeChatId}" /></td>
	      </tr>
	      <tr>
	      	<td><label class="label">二维码：</label></td>
	      	<td>	

	    
 <div id="preview">
 <c:if test="${agent.agentOr!=null}">
 <img src="${agent.agentOr}" with="100" name="agent.agentOr" height="100"/>
 </c:if>
 </div>
<input type="file" onchange="preview(this)" name="fileTest1" />
</td>
	      </tr>
	      
 <script>  
 function preview(file)
 {
 var prevDiv = document.getElementById('preview');
 if (file.files && file.files[0])
 {
 var reader = new FileReader();
 reader.onload = function(evt){
 prevDiv.innerHTML = '<img src="' + evt.target.result + '" with="100" height="100"/>';
}  
 reader.readAsDataURL(file.files[0]);
}
 else  
 {
 prevDiv.innerHTML = '<div class="img1" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src=\'' + file.value + '\') ; with:100px;height:100px" with="100" height="100"></div>';
 }
 }
 </script>	      
	      
	      <tr>
	      	<td><label class="label">推荐码：</label></td>
	      	<td><input type="text" name="agent.recommendationCode" id="recommendationCode" size="40" value="${agent.recommendationCode}" readonly="readonly"/></td>
	      </tr> 
	      
	      <tr>
	      	<td><label class="label">地址：</label></td>
	      	<td><input type="text" name="agent.agentAdress" id="agentAdress" size="40" value="${agent.agentAdress}" /></td>
	      </tr>
       <tr>
	      	<td><label class="label">注册时间：</label></td>
	      	<td><input type="text" name="agent.agentRegTime" id="agentRegTime" size="40" value="${agent.agentRegTime}" /></td>
	      </tr>
        </tbody>
  </table>
</div>

<div class="footer">
	<div class="save_button">
        <span class="span_button" id="sub">确 认</span>
        <span class="span_button"   id="ref"><a href="AgentAction_queryPartnerByPage">取 消</a></span>
    </div>
</div>

</form>

</body>
<script type="text/javascript">
  var mesg="${msg}";
  if(mesg!=""){
	  alert(mesg);
  }
</script>
</html>