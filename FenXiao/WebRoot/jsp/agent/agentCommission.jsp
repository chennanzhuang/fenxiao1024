<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>代理商佣金记录</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" type="text/css" href="jsp/css/css.css">
<script type="text/javascript" src="jsp/js/jquery-1.8.3.min.js"></script>
<style type="text/css">
.page font{border: 1px solid #7F7F7F; color: #7F7F7F; display: inline-block; font-family: 'Tahoma'; margin: 0 3px; text-decoration: none;height: 24px; line-height: 24px; padding: 0 10px; text-align: center;cursor: pointer;}

</style>
<script type="text/javascript">
$(function(){
	$("#sub").click(function(){
		$("#form").submit();
		
	});
	
	
})

</script>


</head>
<body>


<div class="nav">

	<div class="nav_title">
    	<h2><img class="nav_img" src="jsp/img/tab.gif" /><a class="nav_a" href="#">代理商佣金</a></h2>
    </div>
    <form action="AgentAction_queryPageForAgent" id="form"method="post">
    <div class="nav_button">
    <input type="hidden" name="agentId" value="${session.agent.agentId}">
    <input name="search" id="search" class="a_button rand">
     <a class="a_button rand" id="sub" style="cursor: pointer;">搜索</a>
</form>
    </div>
</div>


<div class="list">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="list_table">
      <thead>
	    <tr>
	      <td width="6%">
                            序号
          </td>
	      <td width="15%">头像</td>
	      <td width="15%">账号</td>
	      <td width="15%">名称</td>
	      <td width="10%">推广佣金</td>
	      <td width="10%">任务佣金</td>
	      <td width="10%">团队佣金</td>
	      <td width="10%">可用余额</td>
	       <td width="15%">状态</td>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${agentList}" var="agent" varStatus="s" >
         <tr>
	      <td><div align="center">
	        ${s.index+1+pageBean.pageSize*(pageBean.currentPage-1)}
          </div></td>
	      <td><div align="center">
	      <c:if test="${agent.agentPhoto==null}">
	       <img class="thumb_img" src="jsp/img/111.png" style="width:70px;height: 60px"/>
	      </c:if>
	       <c:if test="${agent.agentPhoto!=null}">
	      <img class="thumb_img" src="${agent.agentPhoto}" style="width:70px;height: 60px"/>
	      </c:if>
	      </div></td>
	      <td><div align="center"><a data-nid="45832585935" href="#" target="_blank" trace="auction" traceidx="0" trace-pid="">${agent.agentAccount}</a></div></td>
	      <td><div align="center">${agent.agentName}</div></td>
	      <td><div align="center">${agent.promotionCommission}</div></td>
	      <td><div align="center">${agent.taskCommissions}</div></td>
	       <td><div align="center">${agent.teamCommissions}</div></td>
	        <td><div align="center">${agent.agentBalance}</div></td>
	      <td><div align="center">
	      <c:if test="${agent.agentStatus==1}">可用</c:if>
	        <c:if test="${agent.agentStatus==4}"><font style="color: red;">禁用</font></c:if>
	      </div></td>
        </tr>
        </c:forEach>
	   
        </tbody>
  </table>
</div>

<div class="page">
               <div align="center" style="float: left;" >
					<font style="">共<s:property value="pageBean.allRow"/>条记录</font>
						<font>第<s:property value="pageBean.currentPage"/>/共<s:property value="pageBean.totalPage"/>页</font>
						</div>
							<div align="center" >
							<s:if test="%{pageBean.currentPage == 1}">
           <font class="current disabled" style="background:#FFF; color:#666;">首页</font> 
           <font class="current disabled" style="background:#FFF; color:#666;">上一页</font>
         </s:if>
         <s:else>
   <a href="${ pageContext.request.contextPath }/AgentAction_queryPageForAgent?page=1&agentId=${session.agent.agentId}&search=${search}">首页</a>
    <a href='${ pageContext.request.contextPath }/AgentAction_queryPageForAgent?page=<s:property value="%{pageBean.currentPage-1}"/>&agentId=${session.agent.agentId}&search=${search}'>上一页</a>
    </s:else>
					
				<s:iterator var="i" begin="1" end="pageBean.totalPage">
				
					<a href="${ pageContext.request.contextPath }/AgentAction_queryPageForAgent?page=<s:property value="#i"/>&agentId=${session.agent.agentId}&search=${search}"><s:property value="#i"/></a>
				
				<s:else>
					<span class="currentPage"><s:property value="#i"/></span>
				</s:else>
			</s:iterator>
			<s:if test="%{pageBean.allRow !=0}">
						<s:if test="%{pageBean.currentPage!= pageBean.totalPage}">
						<a href='${ pageContext.request.contextPath }/AgentAction_queryPageForAgent?page=<s:property value="%{pageBean.currentPage+1}"/>&agentId=${session.agent.agentId}&search=${search}'>下一页</a>
                        <a href='${ pageContext.request.contextPath }/AgentAction_queryPageForAgent?page=<s:property value="pageBean.totalPage"/>&agentId=${session.agent.agentId}&search=${search}'>末页</a>
					</s:if>
                <s:else>
         <font  style=" background:#FFF; color:#666;">下一页</font> 
           <font  style=" background:#FFF; color:#666;">末页</font>
         </s:else>
				 </s:if>
          <s:else>
         <font  style=" background:#FFF; color:#666;">下一页</font> 
           <font  style=" background:#FFF; color:#666;">末页</font>
           </s:else>		
					</div>


   
</div>


</body>
</html>
