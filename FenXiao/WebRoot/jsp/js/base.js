//设置左边菜单高度
$(function(){
	var top_height = 83;	//顶部高度
	var left_menu = 154;    //左边菜单需要减去的高度
	var left_width = 220;  //左侧菜单宽度
	var height = $(window).height() - top_height; //浏览器当前窗口可视区域高度
	var sub_menu_height = $(window).height() - left_menu;
	var iframe_width = $(window).width() - left_width;
	$('#left_hidden').css('height',height);		//设置左边菜单高速
	$('#left_sub_menu').css('height',height);	
	$('.sub_menu').css('height',sub_menu_height);
	$('#right_iframe').css('height',height);
	$('#right_iframe').css('width',iframe_width);
	//监听窗口大小改变事件
	$(window).resize(function(){
		var height = $(window).height() - top_height; //浏览器当前窗口可视区域高度
		var sub_menu_height = $(window).height() - left_menu; 
		var iframe_width = $(window).width() - left_width;
		$('#left_hidden').css('height',height);
		$('#left_sub_menu').css('height',height);
		$('.sub_menu').css('height',sub_menu_height);
		$('#right_iframe').css('height',height);
		$('#right_iframe').css('width',iframe_width);
	});
})

//控制左边菜单缩放
$(function(){
	$('#left_hidden').click(function(){		
		var right_show = $('#right_show').val();
		if(right_show == 1){
			$('#left_sub_menu').hide();
			$('#right_show').val(0);
		}else{
			$('#left_sub_menu').show();
			$('#right_show').val(1);
		}
	})
})

//控制左边菜单显示或隐藏
function change_menu(id){
	$('.sub_menu').css('display','none');
	$('#sub_menu'+id).css('display','block');
	$('.menu li a').removeClass();
	$('#menu_hover'+id).attr('class','menu_hover');
}

//控制左边菜单显示或隐藏
function change_sub_menu(id){
	$('.sub_menu li a').removeClass();
	$('#sub_menu_a'+id).attr('class','sub_menu_hover');
}


