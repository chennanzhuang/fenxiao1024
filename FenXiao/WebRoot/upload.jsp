<%@ page language="java" contentType="text/html; charset=GBK" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>  
<head>  
<meta  charset="utf-8"/>  
    <title>files upload</title>  
    <script src="js/jquery-1.7.2.min.js"></script>  
    <script src="js/uplo.js"></script>  
    <script>  
        $(function () {  
            //var btn = $("#Button1");  
  
            var btn = $("#Button1").uploadFile({  
                url: "servlet/Upload",  
                fileSuffixs: ["jpg", "png", "gif", "txt"],  
                maximumFilesUpload: 4,//最大文件上传数  
                onComplete: function (msg) {  
                    $("#testdiv").append(msg + "<br/>");  
                },  
                onAllComplete: function () {  
                    alert("全部上传完成");  
                },  
                isGetFileSize: true,//是否获取上传文件大小，设置此项为true时，将在onChosen回调中返回文件fileSize和获取大小时的错误提示文本errorText  
  
                perviewElementId: "fileList", //设置预览图片的元素id  
                perviewImgStyle: { width: '80px', height: '58px', border: '1px solid #ebebeb' }//设置预览图片的样式  
            });  
  
            var upload = btn.data("uploadFileData");  
  
            $("#files").click(function () {  
                upload.submitUpload();  
            });  
        });  
    </script>  
  
</head>  
    <body>  
       
        <div style="width: 500px; height: auto;">  
          
            <input id="Button1" type="button" value="选择文件" />  
            <input id="files" type="button" value="上传" />  
            <!-- <div style="width: 420px; height: 180px; overflow:auto;border:1px solid #C0C0C0;">-->  
            <div id="fileList" style="margin-top: 10px; padding-top:10px; font-size: 13px; width:400px">  
                  
            </div>  
            <!-- </div>-->  
        </div>  
        <br/>
		
	    <div id="file_size" style="width: 400px; border-bottom: 1px solid #C0C0C0;"></div>  
		<br/>
		

    </body>  
</html> 