<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>发布需求</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=10" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport" content="width=device-width">
<script type="text/javascript" src="js/jquery-2.0.3.min.js"></script>
<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"
	type="text/javascript"></script>
<script src="jsp/layer/layer.js" type="text/javascript"></script>
<link href="bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">

<link href="css/foot.css" rel="stylesheet">
<link href="css/APPdoalown.css" rel="stylesheet">
<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"
	type="text/javascript"></script>

<style type="text/css">
/*.all_foot{height:70px;line-height: 0px;}*/
/*.all_foot_nav_navs{padding:12px 15px;}*/
.fgx {
	/*width: 100px;*/
	/*border:0.2px solid red;*/
	border-style: outset;
	margin-left: 0px;
	margin-top: 2px;
}
</style>

<script type="text/javascript">
	function a(value) {

		if (value == 1) {
			$(".div3").css("display", "block");
			$(".div1").css("display", "none");
			$(".div2").css("display", "none");
		}
		if (value == 2) {
			$(".div3").css("display", "none");
			$(".div1").css("display", "block");
			$(".div2").css("display", "none");
		}
		if (value == 3) {
			$(".div3").css("display", "none");
			$(".div1").css("display", "none");
			$(".div2").css("display", "block");
		}
	}
</script>
<script type="text/javascript">
	$(function() {

		$("#taskTitle").blur(function() {
			var taskTitle = $("#taskTitle").val();
			if (taskTitle == null || taskTitle == "") {
				layer.msg("标题不能为空！", {
					time : 5000,
					offset : 0,
					shift : 6
				});
				return false;
			}
		});

		$("#taskTotalNumber").blur(
				function() {
					var taskTotalNumber = $("#taskTotalNumber").val();
					if (taskTotalNumber == null || taskTotalNumber == ""
							|| isNaN(taskTotalNumber)) {
						layer.msg("总数为空或非数字！", {
							time : 5000,
							offset : 0,
							shift : 6
						});
						return false;
					}
				});
		$("#taskTitle1").blur(function() {
			var taskTitle1 = $("#taskTitle1").val();

			if (taskTitle1 == null || taskTitle1 == "") {
				layer.msg("标题不能为空！", {
					time : 5000,
					offset : 0,
					shift : 6
				});
				return false;
			}
		});

		$("#taskTotalBonus").blur(function() {
			var taskBonus = $("#taskTotalBonus").val();
			if (taskBonus == null || taskBonus == "" || isNaN(taskBonus)) {
				layer.msg("金额为空或非数字！", {
					time : 5000,
					offset : 0,
					shift : 6
				});
				return false;
			}
		});

		$("#taskTitle2").blur(function() {
			var taskTitle2 = $("#taskTitle2").val();
			if (taskTitle2 == null || taskTitle2 == "") {
				layer.msg("标题不能为空！", {
					time : 5000,
					offset : 0,
					shift : 6
				});
				return false;
			}
		});

		$("#taskBriefing").blur(function() {
			var taskBriefing = $("#taskBriefing").val();
			if (taskBriefing == null || taskBriefing == "") {
				layer.msg("简介不能为空！", {
					time : 5000,
					offset : 0,
					shift : 6
				});
				return false;
			}

		});
		$("#appAndroidUrl").blur(function() {
			var appAndroidUrl = $("#appAndroidUrl").val();
			if (appAndroidUrl == null || appAndroidUrl == "") {
				layer.msg("路径不能为空！", {
					time : 5000,
					offset : 0,
					shift : 6
				});
				return false;
			}
		});
		$("#appIOSUrl").blur(function() {
			var appIOSUrl = $("#appIOSUrl").val();
			if (appIOSUrl == null || appIOSUrl == "") {
				layer.msg("路径不能为空！", {
					time : 5000,
					offset : 0,
					shift : 6
				});
				return false;
			}
		});
		$("#taskContacts").blur(function() {
			var taskContacts = $("#taskContacts").val();
			if (taskContacts == null || taskContacts == "") {
				layer.msg("联系人不能为空！", {
					time : 5000,
					offset : 0,
					shift : 6
				});
				return false;
			}
		});

		$("#Phone")
				.blur(
						function() {
							var Phone = $("#Phone").val();
							var reg = /^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
							if (Phone.trim().length == 0 || !reg.test(Phone)) {
								layer.msg("手机号为空或格式不对！", {
									time : 10000,
									offset : 0,
									shift : 6
								});
								return false;
							}
						});
		$("#code1").blur(function() {
			var code = $("#code1").val();
			var backstage = $("#backstage").val();
			if (code == null || code == "") {
				layer.msg("验证码不能为空！", {
					time : 10000,
					offset : 0,
					shift : 6
				});
				return false;
			}
			if (code != backstage) {
				layer.msg("验证码错误！", {
					time : 10000,
					offset : 0,
					shift : 6
				});
				return false;
			}
		});
		/* $("#button").click(function(){
			if(a==1&&a1==1&&a2==1&&a3==1&&a4==1&&a5==1&&a6==1&&a7==1&&a8==1&&a9==1&&a10==1){
			$(".form").submit();
			}else{
				return false;
			}
		});	 */

	})
	function sub() {
		var taskType = $("#taskType").val();

		if (taskType == 2) {
			var taskTitle = $("#taskTitle").val();
			if (taskTitle == null || taskTitle == "") {
				layer.msg("标题不能为空！", {
					time : 5000,
					offset : 0,
					shift : 6
				});
				return false;
			}
		}
		if (taskType == 1) {
			var taskTitle2 = $("#taskTitle2").val();
			if (taskTitle2 == null || taskTitle2 == "") {
				layer.msg("标题不能为空！", {
					time : 5000,
					offset : 0,
					shift : 6
				});
				return false;
			}
		}
		if (taskType == 3) {
			var taskTitle1 = $("#taskTitle1").val();

			if (taskTitle1 == null || taskTitle1 == "") {
				layer.msg("标题不能为空！", {
					time : 5000,
					offset : 0,
					shift : 6
				});
				return false;
			}
		}
		var taskTotalNumber = $("#taskTotalNumber").val();
		if (taskTotalNumber == null || taskTotalNumber == ""
				|| isNaN(taskTotalNumber)) {
			layer.msg("总数为空或非数字！", {
				time : 5000,
				offset : 0,
				shift : 6
			});
			return false;
		}

		var taskBonus = $("#taskTotalBonus").val();
		if (taskBonus == null || taskBonus == "" || isNaN(taskBonus)) {
			layer.msg("金额为空或非数字！", {
				time : 5000,
				offset : 1,
				shift : 2
			});
			return false;
		}

		var taskBriefing = $("#taskBriefing").val();
		if (taskBriefing == null || taskBriefing == "") {
			layer.msg("简介不能为空！", {
				time : 5000,
				offset : 0,
				shift : 6
			});
			return false;
		}
		if (taskType == 3) {
			var appAndroidUrl = $("#appAndroidUrl").val();
			if (appAndroidUrl == null || appAndroidUrl == "") {
				layer.msg("路径不能为空！", {
					time : 5000,
					offset : 0,
					shift : 6
				});
				return false;
			}
			var appIOSUrl = $("#appIOSUrl").val();
			if (appIOSUrl == null || appIOSUrl == "") {
				layer.msg("路径不能为空！", {
					time : 5000,
					offset : 0,
					shift : 6
				});
				return false;
			}
		}
		var taskContacts = $("#taskContacts").val();
		if (taskContacts == null || taskContacts == "") {
			layer.msg("联系人不能为空！", {
				time : 5000,
				offset : 0,
				shift : 6
			});
			return false;
		}
		var Phone = $("#Phone").val();
		var reg = /^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
		if (Phone.trim().length == 0 || !reg.test(Phone)) {
			layer.msg("手机号为空或格式不对！", {
				time : 10000,
				offset : 0,
				shift : 6
			});
			return false;
		}
		var code = $("#code1").val();
		var backstage = $("#backstage").val();
		if (code == null || code == "") {
			layer.msg("验证码不能为空！", {
				time : 10000,
				offset : 0,
				shift : 6
			});
			return false;
		}
		if (code != backstage) {
			layer.msg("验证码错误！", {
				time : 10000,
				offset : 0,
				shift : 6
			});
			return false;
		}

		$(".form").submit();

	}
</script>
<script type="text/javascript">
	function getCheckCode(value) {

		var Phone = $("#Phone").val();
		var reg = /^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
		if (Phone.trim().length == 0 || !reg.test(Phone)) {
			layer.msg("手机号码为空或格式错误！", {
				time : 5000,
			});
			return false;

		}
		$.ajax({
			type : "GET",
			url : "TaskAction_getSMSCheckCode1",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
			data : {
				"mobile" : Phone
			},
			success : function(data) {
				$("#backstage").val(data.code);
			}

		});

	}
</script>



</head>
<body scroll="none">

	<!--头部-->

	<!--====================== 内容 ======================-->
	<form action="TaskAction_queryfaTask" method="post" class="form"
		enctype="multipart/form-data">
		<input type="hidden" name="user.id" value="${user.id}">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<div class="bs-example bs-example-form content" role="form">
						<div class="one">
							<div class="one-one mg">
								<label> 需求类型 </label> <select name="task.taskType" id="taskType"
									onchange="a(value)">
<!-- 									<option value="1" id="a1">关注公众号</option> -->
									<option value="2" id="a2">广告宣传</option>
<!-- 									<option value="3" id="a3">APP下载</option> -->
								</select>
							</div>
							<div class="div2" style="display: none">
								<hr class="fgx">
								<div class="one-two mg">
									<label> APP名称 </label> <input type="text" name="taskTitle1"
										id="taskTitle1" style="font-size:14px"
										placeholder="请输入标题，长度1~25" maxlength="25">
								</div>
							</div>
							<div class="div3" style="display: none">
								<hr class="fgx">
								<div class="one-two mg">
									<label> 公众号名称 </label> <input type="text" name="taskTitle2"
										id="taskTitle2" style="font-size:14px"
										placeholder="请输入标题，长度1~25" maxlength="25">
								</div>
							</div>
							<div class="div1">
<!-- 							<div class="div1" style="display: none"> -->
								<hr class="fgx">
								<div class="one-two mg">
									<label> 广告标题 </label> <input type="text" name="taskTitle"
										placeholder="请输入标题，长度1~25" style="font-size:14px"
										id="taskTitle" maxlength="25">
								</div>
							</div>
							<hr class="fgx">
							<div class="one-three mg">
								<label> 需求数量 </label> <input type="text"
									name="task.taskTotalNumber" id="taskTotalNumber"
									style="font-size:14px" placeholder="请输入数量" maxlength="9">
							</div>
							<hr class="fgx">
							<div class="one-three mg">
								<label> 需求佣金 </label> <input type="text"
									name="taskTotalBonus" id="taskTotalBonus"
									style="font-size:14px" placeholder="请输入佣金" maxlength="9">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row row2">
				<div class="col-xs-12">
					<div class="bs-example bs-example-form content" role="form">
						<div class="two">
							<div class="two-one mg">
								<label> 需求简介 </label>

								<!-- 替换地方 -->
								<textarea class="synopsis" name="task.taskBriefing"
									maxlength="30" id="taskBriefing"
									style="border:0px;font-size:14px" placeholder="请输入简介（30字以内）"></textarea>

							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row row3">
				<div class="col-xs-12">
					<div class="bs-example bs-example-form content" role="form">
						<div class="three">
							<div class="three-one mg">
								<label> LOGO </label> <span id="preview1"
									style="font-weight:normal;font-family:Microsoft YaHei;  font-size:12px; color:#CCCCCC">图片大小于2M</span>
								<!--   <input type="text" placeholder="图片尺寸,大小要求限制"> -->
								<a class="a-upload"> <input type="file" name="fileTest"
									onchange="preview(this)">选择上传图片
								</a>
								<script>
									function preview(file) {
										var prevDiv = document
												.getElementById('preview1');
										if (file.files && file.files[0]) {
											var reader = new FileReader();
											reader.onload = function(evt) {
												prevDiv.innerHTML = '<img src="' + evt.target.result + '" with="30" height="30"/>';
											}
											reader.readAsDataURL(file.files[0]);
										} else {
											prevDiv.innerHTML = '<div class="img1" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src=\''
													+ file.value
													+ '\')" ></div>';
										}
									}
								</script>
							</div>
<!-- 							<div class="div3"> -->
							<div class="div3" style="display: none">
								<hr class="fgx" style=" margin-bottom:0px;">
								<div class="three-one mg">
									<label>二维码</label> <span id="preview"
										style="font-weight:normal;font-family:Microsoft YaHei;  font-size:12px;color:#CCCCCC ">图片大小于2M</span>
									<!--   <input type="text" placeholder="图片尺寸,大小要求限制"> -->
									<a class="a-upload"> <input type="file" name="fileTest1"
										onchange="preview1(this)">选择上传图片
									</a>
									<script>
										function preview1(file) {
											var prevDiv = document
													.getElementById('preview');
											if (file.files && file.files[0]) {
												var reader = new FileReader();
												reader.onload = function(evt) {
													prevDiv.innerHTML = '<img src="' + evt.target.result + '" with="30" height="30"/>';
												}
												reader
														.readAsDataURL(file.files[0]);
											} else {
												prevDiv.innerHTML = '<div class="img1" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src=\''
														+ file.value
														+ '\')" ></div>';
											}
										}
									</script>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="div2" style="display: none">
				<label class="a-i"> APP下载地址 </label>
				<div class="row">
					<div class="col-xs-12">
						<div class="bs-example bs-example-form content" role="form">
							<div class="four">
								<div class="four-one mg">
									<label> <img src="img/Android.png"> 安卓
									</label> <input type="text" id="appAndroidUrl"
										name="task.appAndroidUrl" style="font-size:14px"
										placeholder="安卓下载地址">
								</div>
								<hr class="fgx">
								<div class="four-two mg">
									<label> <img src="img/iPhone.png"> 苹果
									</label> <input type="text" name="task.appIOSUrl" id="appIOSUrl"
										style="font-size:14px" placeholder="iOS下载地址">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- 广告地址 -->
			<div class="div1">
<!-- 			<div class="div1" style="display: none"> -->
				<label class="a-i"> 广告地址 </label>
				<div class="row">
					<div class="col-xs-12">
						<div class="bs-example bs-example-form content" role="form">
							<div class="four">
								<div class="four-one mg">
									<label> url </label> <input type="text" id="adUrl"
										name="task.adUrl" style="font-size:14px" placeholder="店铺地址">
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row row4">
				<div class="col-xs-12">
					<div class="bs-example bs-example-form content" role="form">
						<div class="five">
							<div class="five-one mg">
								<label> 联系人 </label> <input type="text" placeholder="联系名称"
									id="taskContacts" style="font-size:14px"
									name="task.taskContacts">
							</div>
							<input type="hidden" id="backstage" value="">
							<hr class="fgx">
							<div class="five-two mg">
								<label> 手机号 </label> <input type="text" placeholder="手机号码"
									style="font-size:14px" maxlength="11" name="task.mobilePhone"
									id="Phone">
							</div>
							<hr class="fgx">
							<div class="five-three mg" style="line-height:100%">
								<label> 验证码 </label> <input type="text" id="code1" name="code1"
									style="font-size:14px" placeholder="验证码"> <input
									type="button" value="  获 取 验 证 码    " class="a-upload"
									id="btnSendCode" onClick="getCheckCode(this),sendMessage()">


								<%-- <input type="button"  value="    获  取  验  证  码    " id="btnSendCode" onClick="getSMSCheckCode(${userId},this),sendMessage()"> --%>

							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="col-xs-12 btn" style="margin-bottom: 50px">
				<input type="button" id="button" onclick="sub()" value="确认">
			</div>
		</div>
</form>

		<!--======================尾部======================-->
		<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>
</body>
<script type="text/javascript">
	var InterValObj; //timer变量，控制时间
	var count = 60; //间隔函数，1秒执行
	var curCount;//当前剩余秒数

	function sendMessage() {
		curCount = count;
		var mobile = $("#Phone").val();
		if ($.trim(mobile) == "") {
			layer.msg("手机号不能为空！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}

		var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
		if (!myreg.test($("#Phone").val())) {
			layer.msg("请输入有效手机号！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}
		$("#btnSendCode").attr("disabled", "true");
		$("#btnSendCode").val("请在" + curCount + "内输入验证码");
		InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次

	}

	//timer处理函数
	function SetRemainTime() {
		if (curCount == 0) {
			window.clearInterval(InterValObj);//停止计时器
			$("#btnSendCode").removeAttr("disabled");//启用按钮
			$("#btnSendCode").val("重新发送验证码");
		} else {
			curCount--;
			$("#btnSendCode").val("请在" + curCount + "内输入验证码");
		}
	}
</script>
</html>