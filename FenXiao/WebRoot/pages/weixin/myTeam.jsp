<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title></title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/myteamCss.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/foot.css" />
<!-- <link rel="stylesheet" -->
<!-- 	href="${pageContext.request.contextPath}/css/foot.css" /> -->
<!-- <link rel="stylesheet" -->
<!-- 	href=" ${pageContext.request.contextPath}/css/font_size1.css" /> -->
</head>
<body style="background:#F0F0F0;">
	<div id="box">
		<div class="box-search">
			<input type="text" placeholder="请输入关键字..." id="search">
			<input type="hidden" name="search" value="${search }"  id="search2">
		</div>
		<div class="empty"></div>
		<!-- 中间-->
		<div class="box-content">
			<div class="box-content-top">
				<!--锯齿框-->
				<div class="box-content-gear">
					<div class="box-content-gear-my">
						<p>我的团队</p>
					</div>
					<!-- 				<table border="0" style="border:none;color:white;"> -->
					<!-- 						<tr> -->
					<!-- 							<td><select id="s1_text1_color"> -->
					<!-- 									<option selected="" value="0">全部会员</option> -->
					<!-- 									<option value="1">员工会员</option> -->
					<!-- 									<option value="2">VIP会员</option> -->
					<!-- 							</select></td> -->
					<!-- 						</tr> -->
					<!-- 					</table>  -->


				</div>
				<!--二维码  -->
				<div class="content-two">
					<div class="content-two-p">
						<label> <img
							src="${pageContext.request.contextPath}${user.userQr}">
						</label>
					</div>
					<div class="content-two-p2">
						<p>邀请二维码</p>
						<input type="hidden" value="${openId }" id="openId">
					</div>
				</div>
				<!--信息-->
				<div class="content-div3">
					<div class="content-div3-1">
						<div class="peonum">
							<img src="${pageContext.request.contextPath}/img/icon_1.png">
							<label>团队人数</label>
							<p>${size }</p>
						</div>
					</div>
					<div class="content-div3-2">
						<div class="peonum">
							<img src="${pageContext.request.contextPath}/img/icon_2.png">
							<label>普通会员</label>
							<p>${a }</p>
						</div>
					</div>
					<div class="content-div3-3">
						<div class="peonum">
							<img src="${pageContext.request.contextPath}/img/icon_3.png">
							<label>VIP会员</label>
							<p>${b }</p>
						</div>
					</div>
				</div>
			</div>
			<c:if test="${user.userLevel==3 }">
				<div class="member"
					style="width: 100%;
					height:80px;
					background-color: #F0F0F0;
					margin-top: -10px;">
					<div class="member_juzhon"
						style="width: 94%;
						height: 80px;
						margin: auto;
						line-height: 50px;
						border-radius: 10px;">
						<div class="member_juzhon_left"
							style="width: 50%;
							height: 50px;
							float: left;
							background-color:#12B7F7 ;
							border-radius: 10px;
							text-align: center;
							font-size: 20px;
							color: white;">
							直属团队(${size})</div>
						<div class="member_juzhon_right"
							style="width: 50%;
							height: 50px;
							float: left;
							background-color: white;
							border-radius: 10px;
							text-align: center;
							font-size: 20px;
							color: #9F9F9F;">
							全部会员(${d })</div>
					</div>
				</div>

			</c:if>
			<!-- 会员 -->
			<input type="hidden" name="Total" value="${Total }" id="Total">
			<input type="hidden" name="page" value="${page }" id="page">
			<div id="wodetuandui" style="position:relative; padding-bottom:150px;">
			<c:forEach var="user" items="${pageuser1}">
				<div class="content-box1 box1-top">
					<div class="content-box1-jibie-div">
						<img src="${user.userPhoto }" width="50px" height="45px">
					</div>
					<div class="content-box1-jibie-div2">
						<p>${user.nickName }</p>
						<p>
							<span>${user.regTime }</span>
						</p>
					</div>
					<div class="text-right  content-box1-jibie-div3">
						<div class="content-box1-jibie-div3-div">
							<p>会员级别</p>
						</div>
						<c:if test="${user.userLevel==1 }">
							<div class="content-box1-jibie-div3-div2 color-white">
								<p>普通会员</p>
							</div>
						</c:if>
						<c:if test="${user.userLevel==2 }">
							<div class="content-box1-jibie-div3-div2 color-white">
								<p>VIP会员</p>
							</div>
						</c:if>
						<c:if test="${user.userLevel==3 }">
							<div class="content-box1-jibie-div3-div2 color-white">
								<p>员工</p>
							</div>
						</c:if>
					</div>
				</div>
			</c:forEach>
			<div id="dianji" style="position:absolute; bottom:90px; left:0;font-size:14px; width:100%; height:40px; text-align:center; line-height:40px; color:#000; background:#fff;">点击加载更多</div>
			</div>
<!-- 			<div id="dianji" style=" font-size:15px; left:50%; bottom:80px; width:80%; height:50px; background:#ccc; margin-left:-40%; border-radius:10px; text-align:center; line-height:50px; color:#fff;">加载数据</div> -->
			<input type="hidden" name="total" value="${total }" id="total">
			<input type="hidden" name="page" value="${page }" id="page2">
			<!-- 全部成员 -->
			<c:if test="${user.userLevel==3 }">
				<c:if test="${pageuserlist!=null }">
				<div id="name1" style="display: none; position:relative; padding-bottom:150px;">
					<c:forEach var="user" items="${pageuserlist}">
						<div class="content-box1 box1-top" >
							<div class="content-box1-jibie-div">
								<img src="${user.userPhoto }" width="50px" height="45px">
							</div>
							<div class="content-box1-jibie-div2">
								<p>${user.nickName }</p>
								<p>
									<span>${user.regTime }</span>
								</p>
							</div>
							<div class="text-right  content-box1-jibie-div3">
								<div class="content-box1-jibie-div3-div">
									<p>会员级别</p>
								</div>
								<c:if test="${user.userLevel==1 }">
									<div class="content-box1-jibie-div3-div2 color-white">
										<p>普通会员</p>
									</div>
								</c:if>
								<c:if test="${user.userLevel==2 }">
									<div class="content-box1-jibie-div3-div2 color-white">
										<p>VIP会员</p>
									</div>
								</c:if>
								<c:if test="${user.userLevel==3 }">
									<div class="content-box1-jibie-div3-div2 color-white">
										<p>员工</p>
									</div>
								</c:if>
							</div>
						</div>
					</c:forEach>
					<div id="dianji2" style="position:absolute; bottom:90px; left:0;font-size:14px; width:100%; height:40px; text-align:center; line-height:40px; color:#000; background:#fff;">点击加载更多</div>
					</div>
				</c:if>
			</c:if>
		</div>

	</div>
	<!--======================尾部======================-->
	
	<div class="container-fluid all_foot">
	
		<div class="row all_foot_nav">
			<div class="col-xs-3 all_foot_nav_navs">
				<a
					class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
					href="${pageContext.request.contextPath}/UserAction_homepage">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">首页</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block"
					href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">任务</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block "
					href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">钱包</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block "
					href="${pageContext.request.contextPath}/UserAction_myPersonal">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/mine_true.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">我的</p>
				</a>
			</div>
		</div>
	</div>
	<input type="hidden" value="${userLevel }" id="userLevel">

	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
		<script type="text/javascript">
	var page = $("#page").val();
	var page2 = $("#page2").val();
	page++;
	page2++;
	var Total = $("#Total").val();
	var total = $("#total").val();
	if(page>Total){
		$("#dianji").css("display", "none");
	}
	if(page2>total){
		$("#dianji2").css("display", "none");
	}
</script>
<script type="text/javascript">
	$("#dianji")
			.click(
					function() {
						var page = $("#page").val();
						var   search2  = $("#search2").val();
						var opem = $("#openId").val();
						var dainji = $("#dianji").html();
						if (dainji == "点击加载更多") {
							$("#dianji").html("正在加载...");
							var commcontent = "";
							page++;
							var Total = $("#Total").val();
							$.ajax({
										type : "GET",
										url : "jsonAction_queryUserZhishu",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
										data : {
											"page" : page,
											"Total" : Total,
											"openId" : opem,
											"search":search2
										},
										//dataType:"json",
										success : function(result) {
											var jsonArray = eval("(" + result + ")");
											if (jsonArray != null) {
											
												for (var i = 0; i < jsonArray.length; i++) {
											   commcontent += "<div class='content-box1 box1-top'>";
											   commcontent += "<div class='content-box1-jibie-div'>";
											   commcontent += "<img src='"+jsonArray[i].Photo+"' width='50px' height='45px'>";
											   commcontent += "</div>";
											   commcontent += "<div class='content-box1-jibie-div2'>";
											   if(jsonArray[i].NickName!=null){
											   commcontent += "<p>'"+jsonArray[i].NickName+"'</p>";
											   }else {
												commcontent += "<p></p>";
											}
											if(jsonArray[i].RegTime!=null){
											   commcontent += "<p><span>"+jsonArray[i].RegTime+"</span></p>";
											}else {
												 commcontent += "<p><span></span></p>";
											}
											   commcontent += "</div>";
											   commcontent += "<div class='text-right  content-box1-jibie-div3'>";
											   commcontent += "<div class='content-box1-jibie-div3-div'>";
											   commcontent += "<p>会员级别</p>";
											   commcontent += "</div>";
											   if(jsonArray[i].Level==1){
											   commcontent += "<div class='content-box1-jibie-div3-div2 color-white'>";
											   commcontent += "<p>普通会员</p>";
											   commcontent += "</div>";
											   }else if(jsonArray[i].Level==2){
											   commcontent += "<div class='content-box1-jibie-div3-div2 color-white'>";
											   commcontent += "<p>VIP会员</p>";
											   commcontent += "</div>";
											   }else if(jsonArray[i].Level==1){
											   commcontent += "<div class='content-box1-jibie-div3-div2 color-white'>";
											   commcontent += "<p>员工</p>";
											   commcontent += "</div>";
											   }
											   commcontent += "</div>";
											   commcontent += "</div>";
												}
												$("#wodetuandui").append(commcontent);
												$("#page").val(page);
												if(page<Total){											
												$("#dianji").html("点击加载更多");
												}else {
												$("#dianji").css("display", "none");
												}
											} else {
												$("#dianji").attr({
													"disabled" : "disabled"
												});
												$("#dianji").css("display", "none");
											}
										}
									});
						}

					});
	$("#dianji2")
			.click(
					function() {
						var page = $("#page2").val();
						var opem = $("#openId").val();
						var dainji = $("#dianji2").html();
						var   search2  = $("#search2").val();
						if (dainji == "点击加载更多") {
							$("#dianji2").html("正在加载...");
							var commcontent = "";
							page++;
							var Total = $("#total").val();
							$
									.ajax({
										type : "GET",
										url : "jsonAction_queryUserquanbu",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
										data : {
											"page" : page,
											"Total" : Total,
											"openId" : opem,
											"search":search2
										},
										//dataType:"json",
										success : function(result) {
											var jsonArray = eval("(" + result
													+ ")");
											if (jsonArray != null) {
											
												for (var i = 0; i < jsonArray.length; i++) {
												commcontent += "<div class='content-box1 box1-top'>";
											   commcontent += "<div class='content-box1-jibie-div'>";
											   commcontent += "<img src='"+jsonArray[i].Photo+"' width='50px' height='45px'>";
											   commcontent += "</div>";
											   commcontent += "<div class='content-box1-jibie-div2'>";
											  if(jsonArray[i].NickName!=null){
											   commcontent += "<p>'"+jsonArray[i].NickName+"'</p>";
											   }else {
												commcontent += "<p></p>";
											}
											if(jsonArray[i].RegTime!=null){
											   commcontent += "<p><span>"+jsonArray[i].RegTime+"</span></p>";
											}else {
												 commcontent += "<p><span></span></p>";
											}
											   commcontent += "</div>";
											   commcontent += "<div class='text-right  content-box1-jibie-div3'>";
											   commcontent += "<div class='content-box1-jibie-div3-div'>";
											   commcontent += "<p>会员级别</p>";
											   commcontent += "</div>";
											   if(jsonArray[i].Level==1){
											   commcontent += "<div class='content-box1-jibie-div3-div2 color-white'>";
											   commcontent += "<p>普通会员</p>";
											   commcontent += "</div>";
											   }else if(jsonArray[i].Level==2){
											   commcontent += "<div class='content-box1-jibie-div3-div2 color-white'>";
											   commcontent += "<p>VIP会员</p>";
											   commcontent += "</div>";
											   }else if(jsonArray[i].Level==1){
											   commcontent += "<div class='content-box1-jibie-div3-div2 color-white'>";
											   commcontent += "<p>员工</p>";
											   commcontent += "</div>";
											   }
											   commcontent += "</div>";
											   commcontent += "</div>";
												}
												$("#name1")
														.append(commcontent);
												$("#page2").val(page);
												if(page<Total){											
												$("#dianji2").html("点击加载更多");
												}else {
													$("#dianji2").css("display", "none");
												}
											} else {
												$("#dianji2").css("display", "none");
											}
										}
									});
						}

					});
</script>
	<script type="text/javascript">
		$(function() {
			$("#s1_text1_color")
					.change(
							function() {
								// 		            alert($(this).val());
								var openId = $("#openId").val();
								window.location.href = "${pageContext.request.contextPath}/UserAction_myTeam?openId="
										+ openId
										+ "&userLevel="
										+ $(this).val();
							});

			$("#search")
					.blur(
							function() {
								/*  alert(123); */
								var value = $("#search").val();
								var openId = $("#openId").val();
								window.location.href = "${pageContext.request.contextPath}/UserAction_myTeam?openId="
										+ openId + "&search=" + value;

							});

			$("#s1_text1_color").val($("#userLevel").val());

			// 		$(".SY_cont_two_now").click(function() {
			// 		$(".SY_cont_two_well").removeClass("SY_cont_two_default");
			// 		$(this).addClass("SY_cont_two_default");
			// 		$("#SY_now").css("display", "none");
			// 		$("#agent1").css("display", "none");
			// 		$("#agent2").css("display", "none");
			// 		$("#SY_well").css("display", "block");
			// 	});
			// 	$(".SY_cont_two_well").click(function() {
			// 		$(".SY_cont_two_now").removeClass("SY_cont_two_default");
			// 		$(this).addClass("SY_cont_two_default");
			// 		$("#SY_now").css("display", "block");
			// 		$("#agent1").css("display", "block");
			// 		$("#agent2").css("display", "block");
			// 		$("#SY_well").css("display", "none");
			// 	});
			$(".member_juzhon_left").click(function(){
			$(".member_juzhon_left").css({
				backgroundColor:"#12B7F7",
				color:"white"
			})
			$("#name1").css("display", "none");
			$("#wodetuandui").css("display", "block");
			$(".member_juzhon_right").css({
				backgroundColor:"white",
				color:"#9F9F9F"
			})
		})
		$(".member_juzhon_right").click(function(){
			$(".member_juzhon_right").css({
				backgroundColor:"#12B7F7",
				color:"white"
			})
			$("#name1").css("display", "block");
			$("#wodetuandui").css("display", "none");
			$(".member_juzhon_left").css({
				backgroundColor:"white",
				color:"#9F9F9F"
			})
		})
		});
	</script>

		
</body>
</html>
