<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=10" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>更换银行卡</title>
<link href="bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">

<link href="${pageContext.request.contextPath}/css/foot.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/xiugaika.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
<script src="${pageContext.request.contextPath}/jsp/layer/layer.js"
	type="text/javascript"></script>
<style>
select {
	border: none;
	appearance: none;
	-moz-appearance: none;
	-webkit-appearance: none;
	margin: 0;
	padding: 0;
	outline: none;
	list-style: none;
	background: none;
	text-decoration: none;
	border: 0;
	font-weight: normal;
	font-family: "Microsoft YaHei";
	background: url("${pageContext.request.contextPath}/img/bottom-img.png")
		no-repeat scroll right center transparent;
	background-size: 14px 14px;
	-moz-background-size: 14px 14px;
	-webkit-background-size: 14px 14px;
	padding-right: 14px;
}

select {
	font-family: inherit;
	font-size: inherit;
	line-height: inherit;
}

select {
	text-transform: none;
	width: 70%
}

.se {
	height: 32px;
	outline: medium;
	margin-left: 2%;
}
</style>
</head>
<body>

	<!--====================== 内容 ======================-->

	<div class="container-fluid">
		<!-- 		<label class="title"> 修改银行卡 </label> -->
		<div class="row">
			<div class="col-xs-12">
				<form class="bs-example bs-example-form content"
					action="BankcardAction_updateBankcard2" role="form" id="form">
					<div class="one">
						<div class="one-one mg">
							<label> 开户行 </label> <select class="se" id="select_bank"
								name="bankcard.bankName">
								<option value="中国工商银行">中国工商银行</option>
								<option value="中国农业银行">中国农业银行</option>
								<option value="中国银行">中国银行</option>
								<option value="中国建设银行">中国建设银行</option>
								<option value="交通银行">交通银行</option>
								<option value="中信银行">中信银行</option>
								<option value="中国光大银行">中国光大银行</option>
								<option value="华夏银行">华夏银行</option>
								<option value="中国民生银行">中国民生银行</option>
								<option value="广发银行">广发银行</option>
								<option value="深圳发展银行">深圳发展银行</option>
								<option value="招商银行">招商银行</option>
								<option value="兴业银行">兴业银行</option>
								<option value="上海浦东发展银行">上海浦东发展银行</option>
								<option value="恒丰银行">恒丰银行</option>
								<option value="浙商银行">浙商银行</option>
								<option value="渤海银行">渤海银行</option>
								<option value="中国邮政储蓄银行">中国邮政储蓄银行</option>
								<option value="徽商银行">徽商银行</option>
								<option value="北京银行">北京银行</option>
								<option value="上海银行">上海银行</option>
							</select>
						</div>
						<!-- <hr class="fgx">
						<div class="one-two mg">
							<label> 支行 </label> <input type="text" placeholder="开户行支行名称"
								name="bankcard.partBankName" id="partBankName">
						</div> -->
					</div>
			</div>
		</div>
		<div class="row row2">
			<div class="col-xs-12">
				<div class="two">
					<div class="two-one mg">
						<label> 账号 </label> <input type="text" placeholder="银行卡号"
							name="bankcard.cardCode" id="cardCode">
					</div>
					<hr class="fgx">
					<div class="two-two mg">
						<label> 户名 </label> <input type="text" placeholder="开户名"
							name="bankcard.cardName" id="cardName">
					</div>
					<hr class="fgx">
					<div class="two-three mg">
						<label> 手机号码 </label> <input type="text" placeholder="手机号"
							 value="${lastFour }"
							readonly="readonly">
					</div>
					<input type="hidden" value="${userId }" name="bankcard.user.id">
					<input type="hidden" value="${cardModile }" name="bankcard.cardMobile"
						id="cardMobile">
					<hr class="fgx">
					<div class="two-four mg" style="position：relative;">
						<label> 验证码 </label> <input type="text" id="smsCheckCode" style="width:28%;">

						<span vertical-align:
							middle;  id="btnSendCode"
							style="background:#f5f5f5; font-size:15px; right;"
							onClick="getSMSCheckCode(${userId },this),sendMessage()">
					获 取 验 证 码 
						</span>
						<!-- <button type="button" class="btn btn-primary">（首选项）Primary</button> -->
						<input type="hidden" id="backstage" value="">
					</div>
					<hr class="fgx">
				</div>

			</div>
		</div>
		<div class="col-xs-12 btn">
			<input type="button" id="submit111" value="确认">
		</div>
	</div>
	</form>

	<!--======================尾部======================-->
	<div class="container-fluid all_foot">
		<div class="row all_foot_nav">
			<div class="col-xs-3 all_foot_nav_navs">
				<a
					class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
					href="${pageContext.request.contextPath}/UserAction_homepage">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">首页</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block"
					href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">任务</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block "
					href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/money_true.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">钱包</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block "
					href="${pageContext.request.contextPath}/UserAction_myPersonal">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">我的</p>
				</a>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	$(function() {
		$("#submit111").click(function() {
			/* var bankName=$("#bankName").val();
			if ($.trim(bankName) == "") {
				layer.msg("银行名不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
				return false;
			}
			var partBankName=$("#partBankName").val();
			if ($.trim(partBankName) == "") {
				layer.msg("支行名不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
				return false;
			} */
			var cardCode = $("#cardCode").val();
			if ($.trim(cardCode) == "") {
				layer.msg("银行卡号不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
				return false;
			}

			var myreg = /^(\d{16}|\d{19})$/;
			if (!myreg.test($("#cardCode").val())) {
				layer.msg("请输入有效卡号！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
				return false;
			}

			var cardName = $("#cardName").val();
			if ($.trim(cardName) == "") {
				layer.msg("开户名不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
				return false;
			}
			var cardMobile = $("#cardMobile").val();
			if ($.trim(cardMobile) == "") {
				layer.msg("手机号不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
				return false;
			}

			var smsCheckCode = $("#smsCheckCode").val();
			if ($.trim(smsCheckCode) == "") {
				layer.msg("验证码不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
				return false;
			}
			var backstage = $("#backstage").val();
			if ($.trim(backstage) == "") {
				layer.msg("请先获取验证码！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
				return false;
			}
			var smsCheckCode = $("#smsCheckCode").val();
			var backstage = $("#backstage").val();
			if (smsCheckCode != backstage && backstage != "") {
				layer.msg("验证码不正确！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
				return false;
			}

			$("#form").submit();
		});
	});
	function getSMSCheckCode(value, object) {
		var cardMobile = $("#cardMobile").val();
		if ($.trim(cardMobile) == "") {
			layer.msg("手机号不能为空！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}
		;
		$.ajax({
			type : "POST",
			url : "UserAction_checkOwnMobile",
			async : false,
			data : "mobile=" + cardMobile,
			success : function(data) {
				if ($.trim(data.code) == 0) {
					layer.msg("手机号码输入错误！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
				}
				return false;
			}
		});

		var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
		if (!myreg.test($("#cardMobile").val())) {
			layer.msg("请输入有效手机号码！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}
		$.ajax({
			type : "GET",
			url : "UserAction_getSMSCheckCode",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
			data : {
				"mobile" : cardMobile,
				"userId" : value
			},
			//dataType:"json",
			success : function(data) {
				if (data.codeT == 0) {
					alert(data.msg);
				} else {
					$("#backstage").val(data.code);
// 					$(object).html("<font color='green'>验证码已发送</font>");
				}
			}
		});
// 		$(object).html("<font color='green'>验证码已发送</font>");

	}
</script>
<script type="text/javascript">
	$(function() {
		/* $("#bankName").blur(function(){
			var bankName=$("#bankName").val();
			if ($.trim(bankName) == "") {
				layer.msg("银行名不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
			}
		});
		$("#partBankName").blur(function(){
			var partBankName=$("#partBankName").val();
			if ($.trim(partBankName) == "") {
				layer.msg("支行名不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
			}
		}); */
		$("#cardCode").blur(function() {
			var cardCode = $("#cardCode").val();
			if ($.trim(cardCode) == "") {
				layer.msg("银行卡号不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
			}

			var myreg = /^(\d{16}|\d{19})$/;
			if (!myreg.test($("#cardCode").val())) {
				layer.msg("请输入有效卡号！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
			}

		});
		$("#cardName").blur(function() {
			var cardName = $("#cardName").val();
			if ($.trim(cardName) == "") {
				layer.msg("开户名不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
			}
		});
		$("#cardMobile").blur(function() {
			var cardMobile = $("#cardMobile").val();
			if ($.trim(cardMobile) == "") {
				layer.msg("手机号不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
			}
			$.ajax({
				type : "POST",
				url : "UserAction_checkOwnMobile",
				async : false,
				data : "mobile=" + cardMobile,
				success : function(data) {
					if ($.trim(data.code) == 0) {
						layer.msg("手机号码输入错误！", {
							time : 3000,
							offset : 0,
							shift : 6
						});
					}
				}
			});
			return false;
		});
		$("#smsCheckCode").blur(function() {
			var smsCheckCode = $("#smsCheckCode").val();
			var backstage = $("#backstage").val();
			if ($.trim(smsCheckCode) == "") {
				layer.msg("验证码不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
			}
			if (smsCheckCode != backstage && backstage != "") {
				layer.msg("验证码不正确！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
			}

		});

	});
</script>
<script type="text/javascript">
	var InterValObj; //timer变量，控制时间
	var count = 60; //间隔函数，1秒执行
	var curCount;//当前剩余秒数

	function sendMessage() {
		curCount = count;
		var mobile = $("#cardMobile").val();
		if ($.trim(mobile) == "") {
			layer.msg("手机号不能为空！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}
		$.ajax({
			type : "POST",
			url : "UserAction_checkOwnMobile",
			async : false,
			data : "mobile=" + mobile,
			success : function(data) {
				if ($.trim(data.code) == 0) {
					layer.msg("手机号码输入错误！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
					return;
				}
			}
		});
		var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
		if (!myreg.test($("#cardMobile").val())) {
			layer.msg("请输入有效手机号！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}

		$("#btnSendCode").attr("disabled", "true");
		$("#btnSendCode").html("请在" + curCount + "秒内输入验证码");
		InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次

	}

	//timer处理函数
	function SetRemainTime() {
		if (curCount == 0) {
			window.clearInterval(InterValObj);//停止计时器
			$("#btnSendCode").removeAttr("disabled");//启用按钮
			$("#btnSendCode").html("重新发送验证码");
		} else {
			curCount--;
			$("#btnSendCode").html("请在" + curCount + "秒内输入验证码");
		}
	}
</script>

</html>
