<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
    <meta http-equiv="Content-Type" content="text ml; charset=UTF-8" >
    <title> 升级规则 </title>
    <link href="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/shengji.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/foot.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/js/bootstrap.min.js" type="text/javascript"></script>
    <style type="text/css">
        .all_foot{height:72px;line-height: 0px;}
        .all_foot_nav_navs{padding:12px 15px;}
    </style>
</head>
<body>


    <!--====================== 内容 ======================-->

    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12" >
                <div class="title">
                    <img src="${pageContext.request.contextPath}/img/iconbl.png">
                    <label> <h4> <strong> 升级规则 </strong> </h4> </label>
                </div>
                <div class="text">
                    <ul>
                        <li> 普通会员：新注册且任务佣金累计小于3000元 </li>
                        <li> VIP会员：任务佣金额大于等于3000元 </li>
                        <li> 普通会员：任务佣金额累计大于等于3000元自动升级VIP会员 </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <!--======================尾部======================-->
    <div class="container-fluid all_foot">
        <!-- <div class="row all_foot_nav">
            <div class="col-xs-3 all_foot_nav_navs">
                <a class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true" href="demand.html">
                    <div class="all_foot_nav_navs_block_icon"><img src="img/allIMG/index_true.png"></div>
                    <p class="all_foot_nav_navs_block_title font13">首页</p>
                </a>
            </div>
            <div class="col-xs-3 all_foot_nav_navs">
                <a class="text-center help-block all_foot_nav_navs_block " href="">
                    <div class="all_foot_nav_navs_block_icon"><img src="img/allIMG/renWu_f.png"></div>
                    <p class="all_foot_nav_navs_block_title font13">任务</p>
                </a>
            </div>
            <div class="col-xs-3 all_foot_nav_navs">
                <a class="text-center help-block all_foot_nav_navs_block " href="money.html">
                    <div class="all_foot_nav_navs_block_icon"><img src="img/allIMG/money_f.png"></div>
                    <p class="all_foot_nav_navs_block_title font13">钱包</p>
                </a>
            </div>
            <div class="col-xs-3 all_foot_nav_navs">
                <a class="text-center help-block all_foot_nav_navs_block " href="">
                    <div class="all_foot_nav_navs_block_icon"><img src="img/allIMG/mine_f.png"></div>
                    <p class="all_foot_nav_navs_block_title font13">我的</p>
                </a>
            </div>
        </div> -->
    </div>
</body>
</html>
