<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="java.util.Date"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>首页</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/index.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/foot.css" />
<link rel="stylesheet"
	href=" ${pageContext.request.contextPath}/css/font_size1.css" />
<!-- <script type="text/javascript" src="js/jquery-2.0.3.min.js"></script> -->
<script type='text/javascript'>
	document.querySelector('body').addEventListener('touchstart', function(ev) {
		event.preventDefault();
	});
</script>

</head>
<body>
	<div class="container-fluid SY_out">
		<div style="line-height:28px; font-size:14px">
			<span class=" pull-left"
				style=" padding-left:10px; width:50%;color:#999999; background-color:#fff;">已有<strong
				style="color:#ff9900">${cishu1.zhuche }</strong>个注册用户
			</span> <span
				style=" padding-right:10px; width:50%;color:#999999; background-color:#fff; text-align:right"
				class=" pull-right"><strong style="color:#ff9900">${cishu1.lioulang }
			</strong>人气</span>
		</div>
		<div id="myCarousel" class="carousel slide" data-ride="carousel"
			data-interval="2000">
			<!-- 轮播（Carousel）指标 -->
			<ol class="carousel-indicators sy_carousel_ol">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
				<li data-target="#myCarousel" data-slide-to="3"></li>
				<li data-target="#myCarousel" data-slide-to="4"></li>
			</ol>
			<!-- 轮播（Carousel）项目 -->
			<div class="carousel-inner">
				<div class="item active SY_LB_img">
					<img
						src="${pageContext.request.contextPath}/img/indexIMG/lunbo1.jpg"
						alt="First slide">
				</div>
				<div class="item">
					<img
						src="${pageContext.request.contextPath}/img/indexIMG/lunbo2.jpg"
						alt="Second slide">
				</div>
				<div class="item">
					<img
						src="${pageContext.request.contextPath}/img/indexIMG/lunbo3.jpg"
						alt="Third slide">
				</div>
				<div class="item">
					<img
						src="${pageContext.request.contextPath}/img/indexIMG/lunbo4.jpg"
						alt="Third slide">
				</div>
				<div class="item">
					<img
						src="${pageContext.request.contextPath}/img/indexIMG/lunbo5.jpg"
						alt="Third slide">
				</div>
			</div>
		</div>
		<input name="openId" value="${openId}" type="hidden" id="open">
		<div class="SY_cont">
			<div class="SY_cont_two clearfix" style="height: auto; ">
				<div class="SY_cont_two_now text-center SY_cont_two_default">
					<a href="#SY_well" class="help-block SY_cont_two_block font17">
						<p>每日优惠</p>
						<p>${b }</p>
					</a>
				</div>
				<div class="SY_cont_two_well text-center">

					<a href="#SY_now" class="help-block SY_cont_two_block font17">
						<p>点亮</p>
						<p>${a }</p>
					</a>
				</div>
			</div>
		</div>
		<div class="SY_nav clearfix">
			<input type="hidden" name="Total" value="${Total }" id="Total">
			<input type="hidden" name="page" value="${page }" id="page">
			<input type="hidden" name="userid" value="${userid }" id="userid">
			<!--================已完成================-->
			<div class="SY_nav_navs SY_nav_well" id="SY_well"
				style="display: block;" onclick="tiaozhuang">
				<c:forEach var="list" items="${pageli}">
					<input type="hidden" name="${list.taskId }" class="taskidname">
					<c:set var="nowDate">
						<fmt:formatDate value="<%=new Date()%>"
							pattern="yyyy-MM-dd HH:mm:ss" type="date" />
					</c:set>
					<c:set var="createDate">
						<%--  <fmt:formatDate value="${list.taskEndTime}" pattern="yyyy-MM-dd HH:mm:ss" type="date"/> --%>
						<fmt:parseDate value="${list.taskEndTime}" var="date"
							pattern="yyyy-MM-dd HH:mm:ss" />
						<fmt:formatDate value="${date}" pattern="yyyy-MM-dd HH:mm:ss" />
					</c:set>
					<c:if test="${nowDate>createDate||list.taskRemainderNumber==0}">

						<div class="SY_nav_now_nav" onclick="gettiao(${list.taskId})">
							<!--================-->
							<div class="SY_nav_now_nav_top clearfix">
								<div class="SY_nav_now_nav_top_img">
									<a class="help-block SY_nav_now_nav_top_img_block"
										onclick="gettiao(${list.taskId})"> <img
										src="${pageContext.request.contextPath}/${list.taskLogo }">
									</a>
								</div>
								<div class="SY_nav_now_nav_top_tittle">
									<div class="SY_nav_now_nav_top_tittle_tp_name">

										<a class="font17"  onclick="gettiao(${list.taskId})">${list.taskTitle }</a>
<!-- 										<span class="SY_nav_now_nav_top_tittle_tp_jiFen_fl font18">+${list.taskBonus }</span> -->
									</div>
									<div class="SY_nav_now_nav_top_tittle_bt">
										<a class="font14" onclick="gettiao(${list.taskId})" style="color:#F74C31;">${list.taskBriefing }</a>

									</div>
								</div>
							</div>
							<!--=============-->

							<div class="SY_nav_now_nav_cont clearfix">
								<span class=" SY_nav_now_nav_cont_time_fl font12"><img
									src="${pageContext.request.contextPath}/img/indexIMG/index_time_fl.png">已完成</span>
								<div class="text-center SY_nav_now_nav_cont_num font12">
									<span class="SY_nav_now_nav_cont_num_a">剩余数量:</span> <span>${list.taskRemainderNumber }</span>
								</div>
								<span class=" SY_nav_now_nav_cont_gz"> <a class="font13">
										<c:if test="${list.taskType==1 }">
									关注公众号
									</c:if> <c:if test="${list.taskType==2 }">
									广告宣传
									</c:if> <c:if test="${list.taskType==3 }">
									APP下载
									</c:if>
								</a>
								</span>
							</div>
							<!--==============-->
							<div class="SY_nav_now_nav_bt clearfix">
								<span class="SY_nav_now_nav_bt_id font12">任务编号： <span>${list.taskCode }</span></span>
								<span class="SY_nav_now_nav_bt_address_fl"> <a
									class="font13"><img
										src="${pageContext.request.contextPath}/img/indexIMG/index_map_bl.png">
										<c:if test="${list.city=='全国' }">
										${list.area }
										</c:if> <c:if test="${list.city!='全国' }">
										${list.city}${list.area }
										</c:if> </a>
								</span>
							</div>
						</div>
					</c:if>
				</c:forEach>



				<!-- </div> -->
				<div id="dianji" style="position:absolute; font-size:14px; left:0; bottom:-55px; width:100%; height:40px; text-align:center; line-height:40px; color:#000; background:#fff;">点击加载更多</div>
			</div>
			<!--================进行中================-->

			<div class="SY_nav_navs SY_nav_now" id="SY_now"
				style="display: none">
				<input type="hidden" name="toal" value="${toal }" id="toal">
				<input type="hidden" name="page" value="${page }" id="page2">
				<c:forEach var="list" items="${pagedls}" varStatus="status">
					<c:set var="nowDate">
						<fmt:formatDate value="<%=new Date()%>"
							pattern="yyyy-MM-dd HH:mm:ss" type="date" />
					</c:set>
					<c:set var="createDate">
						<%--  <fmt:formatDate value="${list.taskEndTime}" pattern="yyyy-MM-dd HH:mm:ss" type="date"/> --%>
						<fmt:parseDate value="${list.taskEndTime}" var="date"
							pattern="yyyy-MM-dd HH:mm:ss" />
						<fmt:formatDate value="${date}" pattern="yyyy-MM-dd HH:mm:ss" />
					</c:set>
					<c:if test="${nowDate<createDate&&list.taskRemainderNumber>0}">
						<div class="SY_nav_now_nav" onclick="gettiao(${list.taskId})">
							<!--================-->
							<div class="SY_nav_now_nav_top clearfix">
								<div class="SY_nav_now_nav_top_img">

									<a onclick="gettiao(${list.taskId});"
										class="help-block SY_nav_now_nav_top_img_block"> <img
										src="${pageContext.request.contextPath}/${list.taskLogo }">
									</a>
								</div>
								<div class="SY_nav_now_nav_top_tittle">
									<div class="SY_nav_now_nav_top_tittle_tp_name">
										<a onclick="gettiao(${list.taskId})" class="font17"
											style="
		max-width: 200px; width:200px;">${list.taskTitle }</a>
										<span class="SY_nav_now_nav_top_tittle_tp_jiFen font18">+${list.taskBonus }</span>
									</div>
									<div class="SY_nav_now_nav_top_tittle_bt">
										<a onclick="gettiao(${list.taskId});" class="font14">${list.taskBriefing }</a>
									</div>
								</div>
							</div>
							<!--=============-->
							<div class="SY_nav_now_nav_cont clearfix">
								<span class="SY_nav_now_nav_cont_time font12"><img
									src="${pageContext.request.contextPath}/img/indexIMG/index_time_true.png">

									<span class="settime" endTime="${list.taskEndTime}"
									id="${ status.index + 1}"></span> </span>
								<div class="text-center SY_nav_now_nav_cont_num font12">
									<span class="SY_nav_now_nav_cont_num_a">剩余数量:</span> <span>${list.taskRemainderNumber }</span>
								</div>
								<span class=" SY_nav_now_nav_cont_gz"> <a href=""
									class="font13" style="position:relative;top:6px;"> <c:if
											test="${list.taskType==1 }">
									关注公众号
									</c:if> <c:if test="${list.taskType==2 }">
									广告宣传
									</c:if> <c:if test="${list.taskType==3 }">
									APP下载
									</c:if>
								</a>
								</span>
							</div>
							<input type="hidden" value="${list.taskEndTime}"
								id="endTime${list.taskId }">
							<!--==============-->
							<div class="SY_nav_now_nav_bt clearfix">
								<span class="SY_nav_now_nav_bt_id font12">任务编号： <span>${list.taskCode }</span></span>
								<span class="SY_nav_now_nav_bt_address"> <a href=""
									class="font13"><img
										src="${pageContext.request.contextPath}/img/indexIMG/index_map_yl.png">
										<c:if test="${list.city=='全国' }">
										${list.area }
										</c:if> <c:if test="${list.city!='全国' }">
										${list.city}${list.area }
										</c:if> </a>
								</span>
							</div>
						</div>

					</c:if>
				</c:forEach>
				<div id="dianji2" style="position:absolute; font-size:14px; left:0; bottom:-55px; width:100%; height:40px; text-align:center; line-height:40px; color:#000; background:#fff;">点击加载更多</div>
			</div>
		</div>
	</div>

	<!--======================尾部======================-->
	<div class="container-fluid all_foot">
		<div class="row all_foot_nav">
			<div class="col-xs-3 all_foot_nav_navs">
				<a
					class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
					href="${pageContext.request.contextPath}/UserAction_homepage">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/index_true.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">首页</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block"
					href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">任务</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block "
					href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">钱包</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block "
					href="${pageContext.request.contextPath}/UserAction_myPersonal">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">我的</p>
				</a>
			</div>
		</div>
	</div>

</body>
<script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
<script
	src="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jgestures.min.js"></script>
<script type="text/javascript">
	var page = $("#page").val();
	var page2 = $("#page2").val();
	page++;
	page2++;
	var Total = $("#Total").val();
	var toal = $("#toal").val();
	if(page>Total){
		$("#dianji").css("display", "none");
	}
	if(page2>toal){
		$("#dianji2").css("display", "none");
	}
</script>
<script type="text/javascript">
	$("#dianji")
			.click(
					function() {
						var page = $("#page").val();
						var opem = $("#open").val();
						var dainji = $("#dianji").html();
						if (dainji == "点击加载更多") {
							$("#dianji").html("正在加载...");
							var commcontent = "";
							page++;
							var Total = $("#Total").val();
							$.ajax({
										type : "GET",
										url : "jsonAction_queryTaskAll",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
										data : {
											"page" : page,
											"Total" : Total,
											"openId" : opem
										},
										//dataType:"json",
										success : function(result) {
											var jsonArray = eval("(" + result + ")");
											if (jsonArray != null) {
											
												for (var i = 0; i < jsonArray.length; i++) {
											commcontent += "<input type='hidden' name='"+jsonArray[i].id+"' class='taskidname'>";
										commcontent += "<div class='SY_nav_now_nav' onclick='gettiao("+jsonArray[i].id+")'>";
													commcontent += "<div class='SY_nav_now_nav_top clearfix'>";
													commcontent += "<div class='SY_nav_now_nav_top_img'>";
													commcontent += "<a  class='help-block SY_nav_now_nav_top_img_block' onclick='gettiao("+jsonArray[i].id+")''>";
													commcontent += "<img src='${pageContext.request.contextPath}/"+jsonArray[i].Logo+"'>";
													commcontent += "</a>";
													commcontent += "</div>";
													commcontent += "<div class='SY_nav_now_nav_top_tittle'>";
													commcontent += "<div class='SY_nav_now_nav_top_tittle_tp_name'>";
													commcontent += "<a onclick='gettiao("+jsonArray[i].id+ ")' class='font17'>"+jsonArray[i].Title+"</a>";
// 													commcontent += "<span class='SY_nav_now_nav_top_tittle_tp_jiFen_fl font18'>+"+jsonArray[i].Bonus+"</span>";
													commcontent += "</div>";
													commcontent += "<div class='SY_nav_now_nav_top_tittle_bt'>";
													commcontent += "<a class='font14' onclick='gettiao("+jsonArray[i].id+ ")' style='color:#F74C31;'>"+jsonArray[i].Briefing+ "</a>";
													commcontent += "</div>";
													commcontent += "</div>";
													commcontent += "</div>";
													commcontent += "<div class='SY_nav_now_nav_cont clearfix'>";
													commcontent += "<span class='SY_nav_now_nav_cont_time_fl font12'><img src='${pageContext.request.contextPath}/img/indexIMG/index_time_fl.png'>已完成</span>";
													commcontent += "<div class='text-center SY_nav_now_nav_cont_num font12'>";
													commcontent += "<span class='SY_nav_now_nav_cont_num_a'>剩余数量:</span>";
													commcontent += "<span>"+jsonArray[i].Remainder+"</span>";
													commcontent += "</div>";
													commcontent += "<span class=' SY_nav_now_nav_cont_gz'>";
													if (jsonArray[i].Type == 1) {
														commcontent += "<a class='font13' style='position:relative;top:6px;'>关注公众号</a>";
													} else if (jsonArray[i].Type == 2) {
														commcontent += "<a class='font13' style='position:relative;top:6px;'>广告宣传</a>";
													} else {
														commcontent += "<a class='font13' style='position:relative;top:6px;'>APP下载</a>";
													}
													commcontent += "</span>";
													commcontent += "</div>";
													commcontent += "<div class='SY_nav_now_nav_bt clearfix'>";
													commcontent += "<span class='SY_nav_now_nav_bt_id font12'>任务编号： <span>"+jsonArray[i].Code+"</span></span>";
													commcontent += "<span class='SY_nav_now_nav_bt_address_fl'>";
													if (jsonArray[i].City == "全国") {
														commcontent += "<a class='font13'><img src='${pageContext.request.contextPath}/img/indexIMG/index_map_bl.png'>"+jsonArray[i].Area+ "</a>";
													} else {
														commcontent += "<a class='font13'><img src='${pageContext.request.contextPath}/img/indexIMG/index_map_bl.png'>"+jsonArray[i].City+jsonArray[i].Area+"</a>";
													}
													commcontent += "</span>";
													commcontent += "</div>";
													commcontent += "</div>";
												}
												$("#SY_well").append(commcontent);
												$("#page").val(page);
												if(page<Total){											
												$("#dianji").html("点击加载更多");
												}else {
												$("#dianji").css("display", "none");
												}
											} else {
												$("#dianji").attr({
													"disabled" : "disabled"
												});
												$("#dianji").css("display", "none");
											}
										}
									});
						}

					});
	$("#dianji2")
			.click(
					function() {
						var page = $("#page2").val();
						var opem = $("#open").val();
						var dainji = $("#dianji2").html();
						if (dainji == "点击加载更多") {
							$("#dianji2").html("正在加载...");
							var commcontent = "";
							page++;
							var Total = $("#toal").val();
							$
									.ajax({
										type : "GET",
										url : "jsonAction_queryTaskAlldl",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
										data : {
											"page" : page,
											"Total" : Total,
											"openId" : opem
										},
										//dataType:"json",
										success : function(result) {
											var jsonArray = eval("(" + result
													+ ")");
											if (jsonArray != null) {
											
												for (var i = 0; i < jsonArray.length; i++) {
													commcontent += "<input type='hidden' name='"+jsonArray[i].id+"' class='taskidname'>";
													commcontent += "<div class='SY_nav_now_nav' onclick='gettiao("
															+ jsonArray[i].id
															+ ")'>";
													commcontent += "<div class='SY_nav_now_nav_top clearfix'>";
													commcontent += "<div class='SY_nav_now_nav_top_img'>";
													commcontent += "<a  class='help-block SY_nav_now_nav_top_img_block' onclick='gettiao("
															+ jsonArray[i].id
															+ ")''>";
													commcontent += "<img src='${pageContext.request.contextPath}/"+jsonArray[i].Logo+"'>";
													commcontent += "</a>";
													commcontent += "</div>";
													commcontent += "<div class='SY_nav_now_nav_top_tittle'>";
													commcontent += "<div class='SY_nav_now_nav_top_tittle_tp_name'>";
													commcontent += "<a onclick='gettiao("
															+ jsonArray[i].id
															+ ")' class='font17'>"
															+ jsonArray[i].Title
															+ "</a>";
													commcontent += "<span class='SY_nav_now_nav_top_tittle_tp_jiFen font18'>+"
															+ jsonArray[i].Bonus
															+ "</span>";
													commcontent += "</div>";
													commcontent += "<div class='SY_nav_now_nav_top_tittle_bt'>";
													commcontent += "<a class='font14' onclick='gettiao("
															+ jsonArray[i].id
															+ ")'>"
															+ jsonArray[i].Briefing
															+ "</a>";
													commcontent += "</div>";
													commcontent += "</div>";
													commcontent += "</div>";
													commcontent += "<input type='hidden' value='"+jsonArray[i].EndTime+"' id='endTime"+jsonArray[i].id+"''>";
													commcontent += "<div class='SY_nav_now_nav_cont clearfix'>";
													commcontent += "<span class='SY_nav_now_nav_cont_time font12'><img src='${pageContext.request.contextPath}/img/indexIMG/index_time_true.png'><span class='settime' endTime='"+jsonArray[i].EndTime+"'></span> </span>";
// 													commcontent += "<span class='SY_nav_now_nav_cont_time_fl font12'><img src='${pageContext.request.contextPath}/img/indexIMG/index_time_true.png'><span class='settime' endTime='"+jsonArray[i].taskEndTime+"' id='"+${ status.index + 1}+"'></span> </span>";
													commcontent += "<div class='text-center SY_nav_now_nav_cont_num font12'>";
													commcontent += "<span class='SY_nav_now_nav_cont_num_a'>剩余数量:</span>";
													commcontent += "<span>"
															+ jsonArray[i].Remainder
															+ "</span>";
													commcontent += "</div>";
													commcontent += "<span class=' SY_nav_now_nav_cont_gz'>";
													if (jsonArray[i].Type == 1) {
														commcontent += "<a class='font13'>关注公众号</a>";
													} else if (jsonArray[i].Type == 2) {
														commcontent += "<a class='font13'>广告宣传</a>";
													} else {
														commcontent += "<a class='font13'>APP下载</a>";
													}
													commcontent += "</span>";
													commcontent += "</div>";
													commcontent += "<div class='SY_nav_now_nav_bt clearfix'>";
													commcontent += "<span class='SY_nav_now_nav_bt_id font12'>任务编号： <span>"
															+ jsonArray[i].Code
															+ "</span></span>";
													commcontent += "<span class='SY_nav_now_nav_bt_address'>";
													if (jsonArray[i].City == "全国") {
														commcontent += "<a class='font13'><img src='${pageContext.request.contextPath}/img/indexIMG/index_map_yl.png'>"
																+ jsonArray[i].Area
																+ "</a>";
													} else {
														commcontent += "<a class='font13'><img src='${pageContext.request.contextPath}/img/indexIMG/index_map_yl.png'>"
																+ jsonArray[i].City
																+ jsonArray[i].Area
																+ "</a>";
													}
													commcontent += "</span>";
													commcontent += "</div>";
													commcontent += "</div>";
												}
												$("#SY_now")
														.append(commcontent);
												$("#page2").val(page);
												if(page<Total){											
												$("#dianji2").html("点击加载更多");
												}else {
													$("#dianji2").css("display", "none");
												}
											} else {
												$("#dianji2").css("display", "none");
											}
										}
									});
						}

					});
</script>
<script>
	$(document).ready(
			function() {
				//手势右滑 回到上一个画面
				$('#myCarousel').bind('swiperight swiperightup swiperightdown',
						function() {
							$("#myCarousel").carousel('prev');
						});
				//手势左滑 进入下一个画面
				$('#myCarousel').bind('swipeleft swipeleftup swipeleftdown',
						function() {
							$("#myCarousel").carousel('next');
						});
			});

	$(".SY_cont_two_now").click(function() {
		$(".SY_cont_two_well").removeClass("SY_cont_two_default");
		$(this).addClass("SY_cont_two_default");
		$("#SY_now").css("display", "none");
		$("#SY_well").css("display", "block");
	});
	$(".SY_cont_two_well").click(function() {
		$(".SY_cont_two_now").removeClass("SY_cont_two_default");
		$(this).addClass("SY_cont_two_default");
		$("#SY_now").css("display", "block");
		$("#SY_well").css("display", "none");
	});
	// 	$(document).on('scroll',function(){
	//         var scrollTop = $(document).scrollTop();
	//         if($('.SY_out .SY_nav_now_nav').last().offset().top - $(window).scrollTop() - 592 <= 0){

	//         }
	//     });
	function remainedTime(value) {
		var time_start = new Date().getTime();//设定开始时间 
		var endTime = $("#endTime" + value).val();
		var endTime1 = endTime.replace("-", "/");
		var endTime2 = endTime1.replace("-", "/");
		/* alert(endTime2.length); */

		var s = endTime2.substring(0, endTime2.length - 2);
		console.log(s);
		var time_end = new Date(s).getTime(); //设定结束时间(等于系统当前时间) 
		//计算时间差 
		var time_distance = time_end - time_start;
		if (time_distance > 0) {
			// 天时分秒换算 
			var int_day = Math.floor(time_distance / 86400000)
			time_distance -= int_day * 86400000;

			var int_hour = Math.floor(time_distance / 3600000)
			time_distance -= int_hour * 3600000;

			var int_minute = Math.floor(time_distance / 60000)
			time_distance -= int_minute * 60000;

			var int_second = Math.floor(time_distance / 1000)
			// 时分秒为单数时、前面加零 
			if (int_day < 10) {
				int_day = "0" + int_day;
			}
			if (int_hour < 10) {
				int_hour = "0" + int_hour;
			}
			if (int_minute < 10) {
				int_minute = "0" + int_minute;
			}
			if (int_second < 10) {
				int_second = "0" + int_second;
			}
			// 显示时间 
			$("#time_d" + value).html(int_day + " 天");
			$("#time_h" + value).html(int_hour + " 时");
			$("#time_m" + value).html(int_minute + " 分");
			$("#time_s" + value).html(int_second + " 秒");

			setTimeout("remainedTime(" + value + ")", 1000);

		} else {
			$("#time_d" + value).html('时');
			$("#time_h" + value).html('间');
			$("#time_m" + value).html('结');
			$("#time_s" + value).html('束');

		}
		$("#button" + value).css("display", "none");
		$("#span" + value).css("display", "block");

	}
</script>
<script language="javascript">
	$(function() {
		updateEndTime();
	});
	//倒计时函数
	function updateEndTime() {
		var date = new Date();
		var time = date.getTime(); //当前时间距1970年1月1日之间的毫秒数

		$(".settime").each(
				function(i) {

					var endDate = this.getAttribute("endTime"); //结束时间字符串
					//转换为时间日期类型
					var endDate1 = eval('new Date('
							+ endDate.replace(/\d+(?=-[^-]+$)/, function(a) {
								return parseInt(a, 10) - 1;
							}).match(/\d+/g) + ')');

					var endTime = endDate1.getTime(); //结束时间毫秒数

					var lag = (endTime - time) / 1000; //当前时间和结束时间之间的秒数
					if (lag > 0) {
						var second = Math.floor(lag % 60);
						var minite = Math.floor((lag / 60) % 60);
						var hour = Math.floor((lag / 3600) % 24);
						var day = Math.floor((lag / 3600) / 24);
						$(this).html(
								day + "天" + hour + "小时" + minite + "分" + second
										+ "秒");
					} else {
						$(this).removeClass("settime");
						//alert($(this).attr("id"));
						$(this).html("任务已经结束啦！");
					}
				});
		setTimeout("updateEndTime()", 1000);
	}
</script>
<script type="text/javascript">
var opem = $("#open").val();
	function gettiao(value) {
		window.location.href = "TaskAction_weChatOfficialAccountsTaskDetails?taskId="
				+ value+"&openId="+opem;
	}
</script>


</html>
