<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
    <title> 提现密码 </title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/foot.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/jiaoyimima.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/js/bootstrap.min.js" type="text/javascript"></script>
    <style type="text/css">
        /*.all_foot{height:70px;line-height: 0px;}*/
        /*.all_foot_nav_navs{padding:12px 15px;}*/
    </style>
</head>
<body>

<!--头部-->
<!-- <header class="box-header">
    <div class="box-header-div1">
        <div class="box-header-div1_div1"><img src="img/a-1right.png"></div>
        <div class="box-header-div1-div2"><a href="#"> 返回 </a></div>
    </div>
    <div class="box-header-div2"> 交易密码 </div>
    <div class="box-header-div3"><a href="#"><img src="img/threeDian.png" ></a></div>
</header> -->
<!--====================== 内容 ======================-->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 All" >
            <div class="title">
               <img src="${pageContext.request.contextPath}/img/bindimg.png">
                <br>
                <label>
                    还没有绑定银行卡，请您在提现设置中绑定银行卡并设置提现密码
                </label>
            </div>
            <div class="col-xs-12 btn-setup">
                <a href="${pageContext.request.contextPath}/UserAction_bindingBankcardStep2?userId=${userId}"><input type="button" value="提现设置"></a>
            </div>
        </div>
    </div>
</div>


<!--======================尾部======================-->
<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_true.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>
</body>
</html>
