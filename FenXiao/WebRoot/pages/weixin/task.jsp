<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%
	request.setCharacterEncoding("UTF-8");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>任务列表</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/index_missionShare1.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/foot.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font_size1.css" />
<style>
.red {
	color: red;
}

select {
	border: none;
	appearance: none;
	-moz-appearance: none;
	-webkit-appearance: none;
	margin: 0;
	padding: 0;
	outline: none;
	list-style: none;
	background:
		url("${pageContext.request.contextPath}/img/indexMissionShare/sel.png")
		no-repeat right;
	text-decoration: none;
	border: 0;
	background-repeat: no-repeat;
	background-size: 10px;
	width: 80%;
	text-align: right;
	vertical-align: middle;
}
</style>
</head>
<body>

	<div class="container-fluid SY_out">

		<div class="SY_tit">
			<div class="row SY_tit_row">
				<div class="col-xs-4 SY_tit_row_nav">
					<!--<a href="" class="font14"><span>任务类型</span><img src="../img/indexMissionShare/sel.png"></a>-->
					<a id="drop1" class="dropdown-toggle font14 SY_tit_row_nav_a"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false"> <select id="taskType" name="taskType"
						onchange="a(value)">
							<option value="">任 务 类 型</option>
							<option value="1">关注公众号</option>
							<option value="2">广 告 宣 传</option>
							<option value="3">APP 下 载</option>
					</select> <%--     <img src="${pageContext.request.contextPath}/img/indexMissionShare/sel.png"> --%>
					</a>
				</div>
				<div class="col-xs-4 SY_tit_row_nav">
					<!--<a href="" class="font14"><span>按价格排序</span><img src="../img/indexMissionShare/sel.png"></a>-->
					<a id="drop2" class="dropdown-toggle font14 SY_tit_row_nav_a"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false"> <select id="bounts1"
						onchange="shaixuan2(value)">
							<option value="0">价格排序</option>
							<option value="1">价格最高</option>
							<option value="2">价格最低</option>
					</select> <%--    <img src="${pageContext.request.contextPath}/img/indexMissionShare/sel.png"> --%>
					</a>
					<!-- <ul id="menu2" class="dropdown-menu dm_wid" aria-labelledby="drop2">
                    <li><a href="javascript:;">按价格排序</a></li>
                    <li><a href="javascript:;" onClick="shaixuan2(1)">价格最高</a></li>
                    <li><a href="javascript:;"onClick="shaixuan2(2)">价格最低</a></li>
                </ul> -->
				</div>
				<div class="col-xs-4 SY_tit_row_nav">
					<!--<a href="" class="font14"><span>区域排序</span><img src="../img/indexMissionShare/sel.png"></a>-->
					<a id="drop3" class="dropdown-toggle font14 SY_tit_row_nav_a"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false"> <select id="area1"
						onchange="shaixuan3()">
							<option value="">区域</option>
							<option value="全国">全国</option>
							<option value="${user.area}">${user.area}</option>
					</select>
					</a>
				</div>
			</div>
		</div>

		<div class="SY_nav clearfix">
			<!--================进行中================-->
			<input type="hidden" name="Total" value="${Total }" id="Total">
			<input type="hidden" name="page" value="${page }" id="page">
			<div class="SY_nav_navs SY_nav_now" id="SY_now">

				<c:forEach items="${pagetask}" var="task" varStatus="s">
					<div class="SY_nav_now_nav" onclick="gettiao(${task.taskId})">

						<div class="SY_nav_now_nav_top clearfix">
							<div class="SY_nav_now_nav_top_img">
								<a
									onclick="gettiao(${task.taskId})"
									class="help-block SY_nav_now_nav_top_img_block"> <img
									src="${task.taskLogo}">
								</a>
							</div>
							<div class="SY_nav_now_nav_top_tittle">
								<div class="SY_nav_now_nav_top_tittle_tp_name">
									<a
										onclick="gettiao(${task.taskId})"
										class="font17">${task.taskTitle}</a> <span
										class="SY_nav_now_nav_top_tittle_tp_jiFen font18">+${task.taskBonus}</span>
								</div>
								<div class="SY_nav_now_nav_top_tittle_bt">
									<a
										onclick="gettiao(${task.taskId})"
										class="font14">${task.taskBriefing}</a>
								</div>
							</div>
						</div>
						<!--=============-->
						<div class="SY_nav_now_nav_cont clearfix">
							<span class=" SY_nav_now_nav_cont_time font12"> <img
								src="${pageContext.request.contextPath}/img/indexIMG/index_time_true.png"><span
								class="settime" endTime="${task.taskEndTime}"
								id="${s.index + 1}"></span>
							</span>
							<div class="text-center SY_nav_now_nav_cont_num font12">
								<span class="SY_nav_now_nav_cont_num_a">剩余数量:</span> <span>${task.taskRemainderNumber }</span>
							</div>

							<span class=" SY_nav_now_nav_cont_gz"> <a class="font13"
								href="javascript:;" style="position:relative;top:4px;"> <c:if
										test="${task.taskType==1}">关注公众号</c:if> <c:if
										test="${task.taskType==2}">广告宣传</c:if> <c:if
										test="${task.taskType==3}">APP下载</c:if>
							</a>
							</span>
						</div>


						<!--==============-->
						<div class="SY_nav_now_nav_bt clearfix">
							<span class="SY_nav_now_nav_bt_id font12">任务编号： <span>${task.taskCode}</span></span>
							<span class="SY_nav_now_nav_bt_address"> <a class="font13"><img
									src="${pageContext.request.contextPath}/img/indexIMG/index_map_yl.png">${task.area}</a>
							</span>
						</div>
					</div>
				</c:forEach>
			</div>
			<div id="dianji" style="position:absolute; font-size:14px; left:0; bottom:-55px; width:100%; height:40px; text-align:center; line-height:40px; color:#000; background:#fff;">点击加载更多</div>
		</div>
		<input type="hidden" value="${area}" id="area"> <input
			type="hidden" value="${bounts}" id="bounts"> <input
			type="hidden" value="${taskType}" id="taskType1" name="taskType">
		<input type="hidden" value="${user.id}" id="userId"> <input
			type="hidden" value="${openId}" id="openId" name="openId">
	</div>

	<!--======================尾部======================-->
	<div class="container-fluid all_foot">
		<div class="row all_foot_nav">
			<div class="col-xs-3 all_foot_nav_navs">
				<a
					class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
					href="${pageContext.request.contextPath}/UserAction_homepage">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">首页</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block"
					href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">任务</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block "
					href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">钱包</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block "
					href="${pageContext.request.contextPath}/UserAction_myPersonal">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">我的</p>
				</a>
			</div>
		</div>
	</div>

</body>
<script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
<script
	src="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
<script>
	function add(menu) {
		var menu = document.getElementById(menu).childNodes;
		var menuLi = $(menu);
		menuLi.click(function() {
			$(this).parent().siblings().children().text($(this).text());
		});
	}
</script>
<script type="text/javascript">
	var page = $("#page").val();
	page++;
	var Total = $("#Total").val();
	if(page>Total){
		$("#dianji").css("display", "none");
	}
</script>
<script type="text/javascript">
	$("#dianji")
			.click(
					function() {
						var page = $("#page").val();
						var opem = $("#openId").val();
						var area = $("#area").val();
						var bounts = $("#bounts").val();
						var taskType = $("#taskType").val();
						var dainji = $("#dianji").html();
						if (dainji == "点击加载更多") {
							$("#dianji").html("正在加载数据...");
							var commcontent = "";
							page++;
							var Total = $("#Total").val();
							$.ajax({
										type : "GET",
										url : "jsonAction_queryTask",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
										data : {
											"page" : page,
											"Total" : Total,
											"openId" : opem,
											"area" : area,
											"bounts" : bounts,
											"taskType" : taskType
										},
										//dataType:"json",
										success : function(result) {
											var jsonArray = eval("(" + result + ")");
											if (jsonArray != null) {
												for (var i = 0; i < jsonArray.length; i++) {
											commcontent += "<input type='hidden' name='"+jsonArray[i].id+"' class='taskidname'>";
													commcontent += "<div class='SY_nav_now_nav' onclick='gettiao("
															+ jsonArray[i].id
															+ ")'>";
													commcontent += "<div class='SY_nav_now_nav_top clearfix'>";
													commcontent += "<div class='SY_nav_now_nav_top_img'>";
													commcontent += "<a  class='help-block SY_nav_now_nav_top_img_block' onclick='gettiao("
															+ jsonArray[i].id
															+ ")''>";
													commcontent += "<img src='${pageContext.request.contextPath}/"+jsonArray[i].Logo+"'>";
													commcontent += "</a>";
													commcontent += "</div>";
													commcontent += "<div class='SY_nav_now_nav_top_tittle'>";
													commcontent += "<div class='SY_nav_now_nav_top_tittle_tp_name'>";
													commcontent += "<a onclick='gettiao("
															+ jsonArray[i].id
															+ ")' class='font17'>"
															+ jsonArray[i].Title
															+ "</a>";
													commcontent += "<span class='SY_nav_now_nav_top_tittle_tp_jiFen font18'>+"
															+ jsonArray[i].Bonus
															+ "</span>";
													commcontent += "</div>";
													commcontent += "<div class='SY_nav_now_nav_top_tittle_bt'>";
													commcontent += "<a class='font14' onclick='gettiao("
															+ jsonArray[i].id
															+ ")'>"
															+ jsonArray[i].Briefing
															+ "</a>";
													commcontent += "</div>";
													commcontent += "</div>";
													commcontent += "</div>";
													commcontent += "<input type='hidden' value='"+jsonArray[i].EndTime+"' id='endTime"+jsonArray[i].id+"''>";
													commcontent += "<div class='SY_nav_now_nav_cont clearfix'>";
													commcontent += "<span class='SY_nav_now_nav_cont_time font12'><img src='${pageContext.request.contextPath}/img/indexIMG/index_time_true.png'><span class='settime' endTime='"+jsonArray[i].EndTime+"'></span> </span>";
// 													commcontent += "<span class='SY_nav_now_nav_cont_time_fl font12'><img src='${pageContext.request.contextPath}/img/indexIMG/index_time_true.png'><span class='settime' endTime='"+jsonArray[i].taskEndTime+"' id='"+${ status.index + 1}+"'></span> </span>";
													commcontent += "<div class='text-center SY_nav_now_nav_cont_num font12'>";
													commcontent += "<span class='SY_nav_now_nav_cont_num_a'>剩余数量:</span>";
													commcontent += "<span>"
															+ jsonArray[i].Remainder
															+ "</span>";
													commcontent += "</div>";
													commcontent += "<span class=' SY_nav_now_nav_cont_gz'>";
													if (jsonArray[i].Type == 1) {
														commcontent += "<a class='font13' style='position:relative;top:6px;'>关注公众号</a>";
													} else if (jsonArray[i].Type == 2) {
														commcontent += "<a class='font13' style='position:relative;top:6px;'>广告宣传</a>";
													} else {
														commcontent += "<a class='font13' style='position:relative;top:6px;'>APP下载</a>";
													}
													commcontent += "</span>";
													commcontent += "</div>";
													commcontent += "<div class='SY_nav_now_nav_bt clearfix'>";
													commcontent += "<span class='SY_nav_now_nav_bt_id font12'>任务编号： <span>"
															+ jsonArray[i].Code
															+ "</span></span>";
													commcontent += "<span class='SY_nav_now_nav_bt_address'>";
													if (jsonArray[i].City == "全国") {
														commcontent += "<a class='font13'><img src='${pageContext.request.contextPath}/img/indexIMG/index_map_yl.png'>"
																+ jsonArray[i].Area
																+ "</a>";
													} else {
														commcontent += "<a class='font13'><img src='${pageContext.request.contextPath}/img/indexIMG/index_map_yl.png'>"
																+ jsonArray[i].City
																+ jsonArray[i].Area
																+ "</a>";
													}
													commcontent += "</span>";
													commcontent += "</div>";
													commcontent += "</div>";
												}
												$("#SY_now").append(commcontent);
												$("#page").val(page);
												if(page<Total){											
												$("#dianji").html("点击加载更多");
												}else {
												$("#dianji").css("display", "none");
												}
											} else {
												$("#dianji").attr({
													"disabled" : "disabled"
												});
												$("#dianji").css("display", "none");
											}
										}
									});
						}

					});
</script>
<script>
	$(document).ready(
			function() {
				//手势右滑 回到上一个画面
				$('#myCarousel').bind('swiperight swiperightup swiperightdown',
						function() {
							$("#myCarousel").carousel('prev');
						});
				//手势左滑 进入下一个画面
				$('#myCarousel').bind('swipeleft swipeleftup swipeleftdown',
						function() {
							$("#myCarousel").carousel('next');
						});
			});

	$(".SY_cont_two_now").click(function() {
		$(".SY_cont_two_well").removeClass("SY_cont_two_default");
		$(this).addClass("SY_cont_two_default");
		$("#SY_well").css("display", "none");
		$("#SY_now").css("display", "block");
	});
	$(".SY_cont_two_well").click(function() {
		$(".SY_cont_two_now").removeClass("SY_cont_two_default");
		$(this).addClass("SY_cont_two_default");
		$("#SY_now").css("display", "none");
		$("#SY_well").css("display", "block");
	});

	function remainedTime(value) {
		var time_start = new Date().getTime();//设定开始时间 
		var endTime = $("#endTime" + value).val();
		var endTime1 = endTime.replace("-", "/");
		var endTime2 = endTime1.replace("-", "/");
		/* alert(endTime2.length); */

		var s = endTime2.substring(0, endTime2.length - 2);
		console.log(s);
		var time_end = new Date(s).getTime(); //设定结束时间(等于系统当前时间) 
		//计算时间差 
		var time_distance = time_end - time_start;
		if (time_distance > 0) {
			// 天时分秒换算 
			var int_day = Math.floor(time_distance / 86400000)
			time_distance -= int_day * 86400000;

			var int_hour = Math.floor(time_distance / 3600000)
			time_distance -= int_hour * 3600000;

			var int_minute = Math.floor(time_distance / 60000)
			time_distance -= int_minute * 60000;

			var int_second = Math.floor(time_distance / 1000)
			// 时分秒为单数时、前面加零 
			if (int_day < 10) {
				int_day = "0" + int_day;
			}
			if (int_hour < 10) {
				int_hour = "0" + int_hour;
			}
			if (int_minute < 10) {
				int_minute = "0" + int_minute;
			}
			if (int_second < 10) {
				int_second = "0" + int_second;
			}
			// 显示时间 
			$("#time_d" + value).html(int_day + " 天");
			$("#time_h" + value).html(int_hour + " 时");
			$("#time_m" + value).html(int_minute + " 分");
			$("#time_s" + value).html(int_second + " 秒");

			setTimeout("remainedTime(" + value + ")", 1000);

		} else {
			$("#time_d" + value).html('时');
			$("#time_h" + value).html('间');
			$("#time_m" + value).html('结');
			$("#time_s" + value).html('束');

		}
		$("#button" + value).css("display", "none");
		$("#span" + value).css("display", "block");

	}
</script>
<script language="javascript">
	$(function() {
		updateEndTime();
	});
	//倒计时函数
	function updateEndTime() {
		var date = new Date();
		var time = date.getTime(); //当前时间距1970年1月1日之间的毫秒数

		$(".settime").each(
				function(i) {

					var endDate = this.getAttribute("endTime"); //结束时间字符串
					//转换为时间日期类型
					var endDate1 = eval('new Date('
							+ endDate.replace(/\d+(?=-[^-]+$)/, function(a) {
								return parseInt(a, 10) - 1;
							}).match(/\d+/g) + ')');

					var endTime = endDate1.getTime(); //结束时间毫秒数

					var lag = (endTime - time) / 1000; //当前时间和结束时间之间的秒数
					if (lag > 0) {
						var second = Math.floor(lag % 60);
						var minite = Math.floor((lag / 60) % 60);
						var hour = Math.floor((lag / 3600) % 24);
						var day = Math.floor((lag / 3600) / 24);
						$(this).html(
								day + "天" + hour + "小时" + minite + "分" + second
										+ "秒");
					} else {
						$(this).removeClass("settime");
						//alert($(this).attr("id"));
						$(this).html("任务已经结束啦！");
					}
				});
		setTimeout("updateEndTime()", 1000);
	}
</script>
<script type="text/javascript">
	function a(value) {
		var area = $("#area").val();
		var bounts = $("#bounts").val();
		/* var taskType=$("#taskType").val(); */
		var openId = $("#openId").val();

		window.location.href = "${pageContext.request.contextPath}/TaskAction_queryOnTask?openId="
				+ openId
				+ "&taskType="
				+ value
				+ "&bounts="
				+ bounts
				+ "&area=" + area;
	}

	/* function shaixuan1(value) {
	 var area=$("#area").val();
	 var bounts=$("#bounts").val();
	 var taskType=$("#taskType").val();
	 var userId=$("#userId").val();
	 if(value==1){
	 $("#taskType").html("关注公众号");
	 }
	 if(value==2){
	 $("#taskType").html("广告宣传");
	 }
	 if(value==3){
	 $("#taskType").html("APP下载");
	 }
	 window.location.href="${pageContext.request.contextPath}/TaskAction_queryOnTask?user.id="+userId+"&taskType="+value+"&bounts="+bounts+"&area="+area;
	
	
	 }
	 */
	function shaixuan2(value) {
		var area = $("#area").val();
		/* var bounts=$("#bounts").val(); */
		var taskType = $("#taskType1").val();
		var openId = $("#openId").val();

		window.location.href = "${pageContext.request.contextPath}/TaskAction_queryOnTask?openId="
				+ openId
				+ "&taskType="
				+ taskType
				+ "&bounts="
				+ value
				+ "&area=" + area;
	}

	function shaixuan3() {
		/* var area=$("#area").val(); */
		var bounts = $("#bounts").val();
		var taskType = $("#taskType1").val();
		var value = $("#area1").val();
		var openId = $("#openId").val();
		window.location.href = "${pageContext.request.contextPath}/TaskAction_queryOnTask?openId="
				+ openId
				+ "&taskType="
				+ taskType
				+ "&bounts="
				+ bounts
				+ "&area=" + value;
	}
</script>
<script type="text/javascript">
	function gettiao(value) {
	 var openId = $("#openId").val();
		window.location.href = "TaskAction_weChatOfficialAccountsTaskDetails?taskId="+value+"&openId="+openId;
	}
</script>
<script type="text/javascript">
	$(function() {
		var taskType = $("#taskType1").val();
		var area = $("#area").val();
		var bounts = $("#bounts").val();
		if (taskType != "") {
			$("#taskType").val(taskType);
		}

		if (area != "") {
			$("#area1").val(area);
		}
		if (bounts != "") {
			$("#bounts1").val(bounts);
		}
	});
</script>
</html>
