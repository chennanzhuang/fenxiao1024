<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
    <title> 提现设置 </title>
    <link href="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/foot.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/tixianshezhi.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>

<!--头部-->
<!-- <header class="box-header">
    <div class="box-header-div1">
        <div class="box-header-div1_div1"><img src="img/a-1right.png"></div>
        <div class="box-header-div1-div2" style="margin-left:10px;">
            <a href="#"> 返回 </a>
        </div>
    </div>
    <div class="box-header-div2"> 提现设置 </div>
    <div class="box-header-div3"><a href="#"><img src="img/threeDian.png" ></a></div>
</header> -->

<!--====================== 内容 ======================-->
<div class="container-fluid All">
    <div class="row">
        <div class="col-xs-12" >
            <form class="bs-example bs-example-form content" role="form">
                <div class="one">
                    <div class="left-text">
                        <label>
                            招商银行
                        </label>
                        <span> 储蓄卡 </span>
                    </div>
                    <div class="mantissa-zs right-text">
                        7806
                    </div>
                </div>
                <div class="two">
                    <div class="left-text">
                        <label>
                            中国民生用户
                        </label>
                        <span> 储蓄卡 </span>
                    </div>
                    <div class="mantissa-ms right-text">
                        8814
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="col-xs-12 add glyphicon glyphicon-plus" style="padding-left:10px;color: #ccc">
        <input type="button" value="添加银行卡">
    </div>
</div>


<!--======================尾部======================-->
<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_true.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>

</body>
</html>
