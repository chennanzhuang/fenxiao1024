<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title> 详情页面 </title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/detailpage.css" rel="stylesheet" >
    <link href="${pageContext.request.contextPath}/css/foot.css" rel="stylesheet">

</head>
<body>

    <!--头部-->
  <!--   <header class="box-header">
        <div class="box-header-div1">
            <div class="box-header-div1_div1"><img src="img/a-1right.png"></div>
            <div class="box-header-div1-div2"><a href="#"><strong>返回</strong></a></div>
        </div>
        <div class="box-header-div2"><strong> 详情页面 </strong></div>
        <div class="box-header-div3"><a href="#"><img src="img/threeDian.png" ></a></div>
    </header> -->

    <!-- 内容 -->
    <div class="All">
        <div class="content container-fluid">
            <!-- ===================================================== -->
            <div class="content_one">
                <div class="content_one_top">
                    <label>订单编号1232435435453</label>
                    <a href="#">任务已完成</a>
                </div>
                <div class="heti">
                    <div class="content_one_center">
                        <img src="${pageContext.request.contextPath}/img/index_tit.png" class="img-responsive" alt="hhhhh">
                    </div>
                    <div class="content_one_bottom">
                        <a href="#" class="content_one_bottom_font1">百年塑身堂百年塑身堂</a>
                        <br>
                        <div class="content_one_bottom_texta">
                            <a href="#" class="content_one_bottom_font2">想瘦吗?想瘦就关注想瘦吗?想瘦就关注想瘦吗?想瘦就关注想瘦吗?想瘦就关注想瘦吗?想瘦就关注想瘦吗?想瘦就关注想瘦吗?想瘦就关注想瘦吗?想瘦就关注</a>
                        </div>
                    </div>
                </div>
                <!-- 数量 -->
                <div class="number">
                    <div class="num_son1">
                        <label class="num_son_label1">
                            <span class="num_span1"> 剩余数量： </span>
                            <span class="num_span2"> 134312 </span>
                        </label>
                    </div>
                    <div class="num_son2">
                        <label class="num_son_label2">
                            <img src="${pageContext.request.contextPath}/img/index_time_true.png" >
                            <span class="num_span3"> 10天12小时59分 </span>
                        </label>
                    </div>
                </div>
                <!-- 任务 -->
                <div class="content_one_foot">
                    <div class="money_one_ber">
                        <label class="money_one">￥119.00</label>
                    </div>
                    <div class="money_one_com">
                        <a href="#">
                            <button class="download_one">做任务</button>
                        </a>
                    </div>
                </div>
            </div>

        </div>

        <!-- ===================================================== -->
        <div class="operatingsteps container-fluid">
            <div class="operatingsteps_left">
                <label class="operatingsteps_left_look">
                    任务操作步骤(图文)
                </label>
            </div>
            <div class="operatingsteps_right">
                <a href="${pageContext.request.contextPath}/pages/weixin/enter_detailpage.jsp" class="operatingsteps_right_do">
                    点击查看操作细则
                    <img src="${pageContext.request.contextPath}/img/content_rt.png">
                </a>
            </div>
        </div>

        <div class="steptext container-fluid">
            <label> 任务操作步骤 </label>
        </div>

        <div class="step container-fluid">
            <div class="stepimg">
                <img src="${pageContext.request.contextPath}/img/detailpage.png">
            </div>
        </div>

        <div class="steplabel">
            <a href="enter_detailpage.html" title="进入操作细则页面" alt="进入操作细则页面">
                <label>
                    点击查看操作细则
                </label>
            </a>
        </div>
    </div>



    <!--======================尾部======================-->
    <div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>
</body>
</html>
