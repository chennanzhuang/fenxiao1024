<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>提现</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/money_cash.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/foot.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font_size1.css"/>
    <script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
	<script src="${pageContext.request.contextPath}/jsp/layer/layer.js"
	type="text/javascript"></script>

</head>
<body>

<div class="container-fluid MY_cash_out">
    <form class="form" action="UserAction_moneyToCash" method="post">
        <div class="MY_cash_inner">
            <div class="row MY_cash_in_bank">
                <a href="">
                    <div class="col-xs-10 MY_cash_in_bank_nav MY_cash_in_bank_card">
                        <div class="MY_cash_in_bank_card_Icon">
                            <img src="${pageContext.request.contextPath}/img/moneyTiXianIMG/myTX_yhIcon.png" style="border:0px;">
                        </div>
                        <div class="MY_cash_in_bank_card_name">
                            <div class="font15 MY_cash_in_bank_card_name_tit">${bankcard.bankName }</div>
                            <div class="font13 MY_cash_in_bank_card_name_type"><span id="span">尾号${lastFour }</span> <span>储蓄卡</span></div>
                            <input type="hidden" id="cardNumber" value="${bankcard.cardCode }">
                        </div>
                    </div>
                    <input type="hidden" value="${msg}" id="msg" />
                    <div class="col-xs-2 MY_cash_in_bank_nav MY_cash_in_bank_IconImg">
                        <a href="" class="MY_cash_in_bank_IconImg_hr text-right">
                            <img src="${pageContext.request.contextPath}/img/moneyTiXianIMG/myTX_rt.png">
                        </a>
                    </div>
                </a>
            </div>

            <div class="MY_cash_in_sum">
                <div class="MY_cash_in_sum_tit font14">提现金额</div>
                <div class="MY_cash_in_sum_num clearfix">
                    <span>￥</span>
                    <input type="text" name="amount" id="amount" class="MY_cash_in_sum_num_text font13" placeholder="提现金额(整数))"/>
                    <span id="amountCheck" style="float:right;"></span>
                </div>
                <div class="MY_cash_in_sum_point font13">
                <b id="qiang">
                    可提现金额
         <c:if test="${bankcard.user.balance!=null }">      
                    ${bankcard.user.balance }
         </c:if> 
         <c:if test="${bankcard.user.balance==null }">      
                   0.0
         </c:if>   </b>      
                    
                     元
          	<span style="color: red;padding-left: 15px">手续费 <b style="padding-left: 3px;padding-right: 3px;" id="shouxu">0</b>元</span> 
                    <input type="hidden" id="balance" value="${bankcard.user.balance}">
                    <input type="hidden" id="userId" name="user.id" value="${bankcard.user.id}">
                	<input type="hidden" id="openId" name="openId" value="${openId }">
                </div>
            </div>

            <div class="MY_cash_in_info">
                <div class="MY_cash_in_info_password">
                    <ul class=" MY_cash_in_info_password_list clearfix">
                        <li class="font14 MY_cash_in_info_password_tit"><label for="my_password">提现密码</label></li>
                        <li class="font13 MY_cash_in_info_password_pass"><input type="password" name="user.presentPass"  placeholder="请输入提现密码" id="my_password"></li>
                        <!-- <li class="font13 MY_cash_in_info_password_info"><span>请输入交易密码</span></li> -->
                    </ul>
                </div>
                <input type="hidden" id="my_phone" value="${modil }"/>
                <div class="MY_cash_in_info_phone">
                    <ul class=" MY_cash_in_info_password_list clearfix">
                        <li class="font14 MY_cash_in_info_password_tit"><label for="my_phone">手机号</label></li>
                        <li class="font13 MY_cash_in_info_password_pass"><input type="text" placeholder="请输入手机号"  value="${lastmodile }"></li>
<!--                         <li class="font13 MY_cash_in_info_password_info"><span id="telephoneCheck">请输入手机号</span></li> -->
                         
                    </ul>
                </div>
                <div class="MY_cash_in_info_yanZ">
                    <ul class=" MY_cash_in_info_password_list clearfix">
                        <li class="font14 MY_cash_in_info_password_tit"><label for="my_yanZ">验证码</label></li>
                        <li class="font13 MY_cash_in_info_password_pass"><input type="text" id="my_yanZ"></li>
                        <li class="font13 MY_cash_in_info_password_info"><input type="button" id="btnSendCode" onclick="getCheckCode(${bankcard.user.id},this),sendMessage()" value="获取验证码" class="HQ_yanZ"/>
                        <input type="hidden" id="backstage" value="">
                        </li>
                        <li class="font13 MY_cash_in_info_password_info"><span id="mobileCheck"></span></li>
                    </ul>
                </div>
                <span id="SMSCodeCheck"></span>
            </div>

            <div class="MY_cash_in_foot">
                <div class="MY_cash_in_foot_tiXian">
                    <input type="button" id="submitButton" class="btn btn-block MY_cash_in_foot_tiXian_btn font14" value="提现">
                </div>
                <div class="MY_cash_in_foot_info">
                    <ul class="MY_cash_in_foot_info_list clearfix">
                        <li><a href="${pageContext.request.contextPath}/UserAction_forgetPassword?openId=${openId}" class="font12"><img src="${pageContext.request.contextPath}/img/moneyTiXianIMG/myTX_wj.png" class="myTX_wj">忘记交易密码</a></li>
                        <li><a href="${pageContext.request.contextPath}/pages/weixin/money_postalKnow.jsp" class="font12"><img src="${pageContext.request.contextPath}/img/moneyTiXianIMG/myTX_ti.png" class="myTX_ti">提现规则</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </form>
</div>

<!--======================尾部======================-->
<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_true.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>




</body>
<%-- <script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-1.4.4.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-2.1.1.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-2.1.1.min.js"></script>
<script src="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script> --%>
		
		
		
		<script type="text/javascript">
		$(function(){
		var xiaoshudian=$("#qiang").html();
		 var s = xiaoshudian + "";
  		var str = s.substring(0,s.indexOf(".") + 3);
    	$("#qiang").html(str);
		
		});
// 		  var b = $("#qiang").html();
// 			 var  a = b.toFixed(2);
//     		$("#qiang").html(a);
// 		$(function(){
// 		var msg = $("#msg").val();
// 			if ($.trim(msg) != "") {
// 				layer.msg("提现密码错误！", {
// 					time : 3000,
// 					offset : 0,
// 					shift : 6
// 				});
// 			}
// 			;
		
		var cardNumber=$("#cardNumber").val();
		str=cardNumber.substring(cardNumber.length-4,cardNumber.length);
		$("#span").html("尾号"+str);	
		
		var balance=$("#balance").val();
		$("#amount").on('input',function(){
			var amount=$("#amount").val();
			var asd=parseFloat(amount*(1+0.006));
			asd.toFixed(2);
			var shouxu = $("#shouxu").html();
			var amo=parseFloat(amount*0.006);
			 var  a = amo.toFixed(2);
    		$("#shouxu").html(a);
			if(parseFloat(asd)>parseFloat(balance)){
				layer.msg("余额不足！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
			}
		});
		
		$("#my_yanZ").blur(function(){
			var SMSCode=$("#my_yanZ").val();
			var backstage=$("#backstage").val();
			if(SMSCode!=backstage&&backstage!=""){
				layer.msg("您的验证码不对！", {
					time : 3000,
					offset : 0,
					shift : 6
				});	
			}
			if(SMSCode==backstage&&backstage!=""){
				$("#SMSCodeCheck").html("");	
			}
			
		});
		
		$("#submitButton").click(function(){
			var balance=$("#balance").val();
			var amount=$("#amount").val();
			if(parseFloat(amount)>parseFloat(balance)){
				layer.msg("余额不足！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
				return false;
			}
			
			
			var SMSCode=$("#my_yanZ").val();
			var backstage=$("#backstage").val();
			if(SMSCode!=backstage){
				layer.msg("您的验证码不对！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
				return false;
			}
			var mobile=$("#my_yanZ").val();
			if(mobile.trim().length==0){
				layer.msg("您的验证码不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
				return false;
			}
			
			$(".form").submit();
			$("#submitButton").attr({"disabled":"disabled"});
		});
		
		$("#my_phone").blur(function(){
			var cardMobile=$("#my_phone").val();
			/* var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/; 
			if(!myreg.test($("#my_phone").val())) 
			{ 
				layer.msg("请输入有效手机号！", {
					time : 3000,
					offset : 0,
					shift : 6
				}); 
			}  */
			
			if ($.trim(cardMobile) == "") {
				layer.msg("手机号不能为空！", {
				    time: 3000,
				    offset: 0,
				    shift: 6
				  });
				return false;
			}; 
		$("my_password").blur(function(){
			var password = $("#my_password").val();
								if ($.trim(password) == "") {
									layer.msg("密码不能为空！", {
										time : 3000,
										offset : 0,
										shift : 6
									});
									return false;
								}
								$.ajax({
        						type:"POST",
        						url : "BankcardAction_checkOwnMobile1",
        						data:"userId="+${bankcard.user.id}+"&password="+password,
        						success : function(data) {
        					if (data.code == 0) {
											layer.msg("你输入的密码错误！", {
												time : 3000,
												offset : 0,
												shift : 6
											});
											return false;
										}
        				}
        			
        		});
			
			
			
		});
		$("#my_yanZ").blur(function(){
			var mobile=$("#my_yanZ").val();
			if(mobile.trim().length!=0){
				$("#mobileCheck").html("");
			}
			
		});
		
		
		});
		
		function getCheckCode(value,object){
			/* alert(1); */
			var mobile=$("#my_phone").val();
			if(mobile.trim().length==0){
				layer.msg("您的手机号不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
				return false;
			}
			
			var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/; 
			if(!myreg.test($("#my_phone").val())) 
			{ 
				layer.msg("请输入有效手机号！", {
					time : 3000,
					offset : 0,
					shift : 6
				}); 
				return false;
			} 
			
			$.ajax({
				type:"GET",
				url:"TaskAction_getSMSCheckCode",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
				data:{"mobile":mobile,"userId":value},
				dataType:"json",
				success:function(data) {
					if(data.codeT==0){
						alert(data.msg);
					}else{
						$("#backstage").val(data.code);
					}
					
				}
				});
			
		}
		
		</script>
		<script type="text/javascript">

var InterValObj; //timer变量，控制时间
var count = 60; //间隔函数，1秒执行
var curCount;//当前剩余秒数

function sendMessage() {
     curCount = count;
     var mobile = $("#my_phone").val();
		if ($.trim(mobile) == "") {
			layer.msg("手机号不能为空！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}
		
		var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
		if (!myreg.test($("#my_phone").val())) {
			layer.msg("请输入有效手机号！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}
     $("#btnSendCode").attr("disabled", "true");
     $("#btnSendCode").val("请在" + curCount + "秒内输入验证码");
     InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次
    
}

//timer处理函数
function SetRemainTime() {
            if (curCount == 0) {                
                window.clearInterval(InterValObj);//停止计时器
                $("#btnSendCode").removeAttr("disabled");//启用按钮
                $("#btnSendCode").val("重新发送验证码");
            }
            else {
                curCount--;
                $("#btnSendCode").val("请在" + curCount + "秒内输入验证码");
            }
        }
</script>
<script type="text/javascript">
var msg="${msg}";
if(msg!=""){
	layer.msg(msg, {
		time : 3000,
		offset : 0,
		shift : 6
	});
}

</script>
</html>
