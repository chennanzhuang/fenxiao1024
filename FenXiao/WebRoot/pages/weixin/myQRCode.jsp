<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>我的二维码</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/index.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/foot.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font_size1.css" />

</head>
<body>

	<div class="container-fluid SY_out" style="margin: 0 auto;">
		<div class="SY_nav clearfix" style="margin:0 auto;text-align:center;">
		<img alt="" src="${pageContext.request.contextPath}/img/gongzhonghaoQRCode.jpg" style="margin:0 auto;">
		</div>
		<div class="SY_nav clearfix" style="text-align:center;">
		请长按二维码关注
		</div>
	</div>

	
</body>
<script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
<script
	src="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jgestures.min.js"></script>

</html>
