<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>冻结订单</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/money_freezeOders.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/foot.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font_size1.css"/>
</head>
<body>

<div class="container-fluid MY_know_out">
    <div class="MY_mission_cont">
        <div class="row MY_mission_cont_nav">
            <div class="col-xs-8 MY_mission_cont_nav_navs">
                <div class="MY_mission_cont_nav_navs_tittle"><a href="" class="font13">已完成《任务名称撒开恢复扩大》</a></div>
                <p class="font12 MY_mission_cont_nav_navs_time">
                    <span class="MY_mission_cont_nav_navs_time_name">关注任务</span>
                    <span class="MY_mission_cont_nav_navs_time_data">2016-06-20</span>
                </p>
            </div>
            <div class="col-xs-4 MY_mission_cont_nav_navs text-right">
                <span class="font18 MY_mission_cont_nav_navs_jiFen">+ 0.40</span>
            </div>
        </div>
        <div class="row MY_mission_cont_nav">
            <div class="col-xs-8 MY_mission_cont_nav_navs">
                <div class="MY_mission_cont_nav_navs_tittle"><a href="" class="font13">已完成《任务名称撒开恢复扩大》</a></div>
                <p class="font12 MY_mission_cont_nav_navs_time">
                    <span class="MY_mission_cont_nav_navs_time_name">关注任务</span>
                    <span class="MY_mission_cont_nav_navs_time_data">2016-06-20</span>
                </p>
            </div>
            <div class="col-xs-4 MY_mission_cont_nav_navs text-right">
                <span class="font18 MY_mission_cont_nav_navs_jiFen">+ 0.40</span>
            </div>
        </div>
        <div class="row MY_mission_cont_nav">
            <div class="col-xs-8 MY_mission_cont_nav_navs">
                <div class="MY_mission_cont_nav_navs_tittle"><a href="" class="font13">已完成《任务名称撒开恢复扩大》</a></div>
                <p class="font12 MY_mission_cont_nav_navs_time">
                    <span class="MY_mission_cont_nav_navs_time_name">关注任务</span>
                    <span class="MY_mission_cont_nav_navs_time_data">2016-06-20</span>
                </p>
            </div>
            <div class="col-xs-4 MY_mission_cont_nav_navs text-right">
                <span class="font18 MY_mission_cont_nav_navs_jiFen">+ 0.40</span>
            </div>
        </div>
        <div class="row MY_mission_cont_nav">
            <div class="col-xs-8 MY_mission_cont_nav_navs">
                <div class="MY_mission_cont_nav_navs_tittle"><a href="" class="font13">已完成《任务名称撒开恢复扩大》</a></div>
                <p class="font12 MY_mission_cont_nav_navs_time">
                    <span class="MY_mission_cont_nav_navs_time_name">下载APP</span>
                    <span class="MY_mission_cont_nav_navs_time_data">2016-06-20</span>
                </p>
            </div>
            <div class="col-xs-4 MY_mission_cont_nav_navs text-right">
                <span class="font18 MY_mission_cont_nav_navs_jiFen">+ 0.40</span>
            </div>
        </div>
        <div class="row MY_mission_cont_nav">
            <div class="col-xs-8 MY_mission_cont_nav_navs">
                <div class="MY_mission_cont_nav_navs_tittle"><a href="" class="font13">已完成《任务名称撒开恢复扩大》</a></div>
                <p class="font12 MY_mission_cont_nav_navs_time">
                    <span class="MY_mission_cont_nav_navs_time_name">关注任务</span>
                    <span class="MY_mission_cont_nav_navs_time_data">2016-06-20</span>
                </p>
            </div>
            <div class="col-xs-4 MY_mission_cont_nav_navs text-right">
                <span class="font18 MY_mission_cont_nav_navs_jiFen">+ 0.40</span>
            </div>
        </div>
    </div>
</div>




<!--======================尾部======================-->
<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_true.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>

</body>
<script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
<script src="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
</html>
