<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>提现记录</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/money_postalNotes.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/foot.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font_size1.css"/>

</head>
<body>

<div class="container-fluid MY_note_out">
    <div class="MY_note_out_cont" id="tixianjilu">
    <input type="hidden" name="openId" value="${openId }" id="open">
	<input type="hidden" name="Total" value="${Total }" id="Total">
	<input type="hidden" name="page" value="${page }" id="page">
    <c:forEach var="list" items="${pagetixia}">
<!--     <c:if test="${list.presentStatus==1}">已经审核</c:if> -->
<!--     <c:if test="${list.presentStatus==0}">正在审核</c:if> -->
        <div class="MY_note_out_cont_nav">
            <ul class=" MY_note_out_cont_nav_listTop clearfix">
                <li class="MY_note_out_cont_nav_list_left">
                    <span class="font15 MY_note_out_cont_nav_list_left_TX">提现</span>
                    <span class="font12 MY_note_out_cont_nav_list_left_time">${list.presentTime}</span>
                </li>
                <li class="MY_note_out_cont_nav_list_right">
                    <span class="font13 MY_note_out_cont_nav_list_left_999">手续费</span>
                    <span class="font12 MY_note_out_cont_nav_list_left_999">-<span class="MY_note_out_cont_nav_list_left_jianMoney">${list.presentFee }</span></span>
                </li>
            </ul>
            <ul class=" MY_note_out_cont_nav_listBt clearfix">
                <li class="MY_note_out_cont_nav_list_left">
                    <span class="font12 MY_note_out_cont_nav_list_left_yuE">账户余额</span>
                    <span class="font12 MY_note_out_cont_nav_list_left_yuE" id="remainingSum">${list.remainingSum}</span>
                </li>
                <li class="MY_note_out_cont_nav_list_right">
                <c:if test="${list.presentStatus==1}">
                <span class="font12 MY_note_out_cont_nav_list_left_yuE">(审核用过)</span>
                </c:if>
                <c:if test="${list.presentStatus==0}">
                <span class="font12 MY_note_out_cont_nav_list_left_yuE">(正在审核)</span>
                </c:if>
                <c:if test="${list.presentStatus==2}">
                <span class="font12 MY_note_out_cont_nav_list_left_yuE">(审核失败)</span>
                </c:if>
                    <span class="font16 MY_note_out_cont_nav_list_left_red">- <span class="MY_note_out_cont_nav_list_left_jianMoney">${list.presentMoney}</span></span>
                </li>
            </ul>
        </div>
        
    </c:forEach>
       
    </div>
    <div id="dianji" style="position:absolute; font-size:14px; left:0; width:100%; height:40px; text-align:center; line-height:40px; color:#000; background:#fff;">点击加载更多</div>
</div>

<!--======================尾部======================-->
<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_true.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>




</body>
<script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
<script src="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
<!-- 	<script type="text/javascript"> -->
<!-- 		$(function(){ -->
<!-- 		var xiaoshudian=$("#remainingSum").html(); -->
<!-- 		alert(message) -->
<!-- 		 var s = xiaoshudian + ""; -->
<!--   		var str = s.substring(0,s.indexOf(".") + 3); -->
<!--     	$("#remainingSum").html(str); -->
		
<!-- 		}); -->
<!-- 	</script> -->
<script type="text/javascript">
var page = $("#page").val();
	page++;
	var Total = $("#Total").val();
	if(page>Total){
		$("#dianji").css("display", "none");
	}
</script>
<script type="text/javascript">
	$("#dianji")
			.click(
					function() {
						var page = $("#page").val();
						var opem = $("#open").val();
						var dainji = $("#dianji").html();
						if (dainji == "点击加载更多") {
							$("#dianji").html("正在加载...");
							var commcontent = "";
							page++;
							var Total = $("#Total").val();
							$.ajax({
										type : "GET",
										url : "jsonAction_queryUsertixian",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
										data : {
											"page" : page,
											"Total" : Total,
											"openId" : opem
										},
										success : function(result) {
											var jsonArray = eval("(" + result + ")");
											if (jsonArray != null) {
											
												for (var i = 0; i < jsonArray.length; i++) {
												commcontent+="<div class='MY_note_out_cont_nav'>";
												commcontent+="<ul class='MY_note_out_cont_nav_listTop clearfix'>";
												commcontent+="<li class='MY_note_out_cont_nav_list_left'>";
												commcontent+="<span class='font15 MY_note_out_cont_nav_list_left_TX'>提现</span>";
												commcontent+="<span class='font12 MY_note_out_cont_nav_list_left_time'>"+jsonArray[i].PresentTime+"</span>";
												commcontent+="</li>";
												commcontent+="<li class='MY_note_out_cont_nav_list_right'>";
												commcontent+="<span class='font13 MY_note_out_cont_nav_list_left_999'>手续费</span>";
												commcontent+="<span class='font12 MY_note_out_cont_nav_list_left_999'>-<span class='MY_note_out_cont_nav_list_left_jianMoney'>"+jsonArray[i].Fee+"</span></span>";
												commcontent+="</li>";
												commcontent+=" </ul>";
												commcontent+="<ul class='MY_note_out_cont_nav_listBt clearfix'>";
												commcontent+="<li class='MY_note_out_cont_nav_list_left'>";
												commcontent+="<span class='font12 MY_note_out_cont_nav_list_left_yuE'>账户余额</span>";
												commcontent+="<span class='font12 MY_note_out_cont_nav_list_left_yuE' id='remainingSum'>"+jsonArray[i].Sum+"</span>";
												commcontent+="</li>";
												commcontent+="<li class='MY_note_out_cont_nav_list_right'>";
												if(jsonArray[i].Status==1){
												
												commcontent+="<span class='font12 MY_note_out_cont_nav_list_left_yuE'>(审核用过)</span>";
												}else if(jsonArray[i].Status==0){
												
												commcontent+="<span class='font12 MY_note_out_cont_nav_list_left_yuE'>(正在审核)</span>";
												}else if(jsonArray[i].Status==2){
												
												commcontent+="<span class='font12 MY_note_out_cont_nav_list_left_yuE'>(审核失败)</span>";
												}
												commcontent+="<span class='font16 MY_note_out_cont_nav_list_left_red'>- <span class='MY_note_out_cont_nav_list_left_jianMoney'>"+jsonArray[i].Money+"</span></span>";
												commcontent+="</li>";
												commcontent+="</ul>";
												commcontent+="</div>";
												}
												$("#tixianjilu").append(commcontent);
												$("#page").val(page);
												if(page<Total){											
												$("#dianji").html("点击加载更多");
												}else {
												$("#dianji").css("display", "none");
												}
											} else {
												$("#dianji").attr({
													"disabled" : "disabled"
												});
												$("#dianji").css("display", "none");
											}
										}
									});
						}

					});
			</script>
</html>
