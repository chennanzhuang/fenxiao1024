<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>我的需求</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
     <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/mytask2.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/foot.css" />

		
		<style type="text/css">
		#b{
		background-color:#F0F0F0;
		}
		
		</style>
	</head>
	<body id="b">
		<!--头部-->
		<%-- <header class="box-header">
			<div class="box-header-div1">
			    <div class="box-header-div1_div1"><img src="img/a-1right.png"></div>
			    <div class="box-header-div1-div2"><a href="#"><strong>返回</strong></a></div>
			</div>
			<div class="box-header-div2"><strong>我的团队</strong></div>
			<div class="box-header-div3"><a href="#"><img src="img/threeDian.png" ></a></div>
		</header> --%>
		<input type="hidden" name="page" value="${page }" id="page">
		<input type="hidden" name="Total" value="${Total }" id="Total">
		<input type="hidden" name="openId" value="${openId }" id="openId">
		<div class="box-content">
		
			<div class="box-content-border" id="tiaozhuangye">
			<c:forEach items="${pagexqlist}" var="task">
				<div class="box-content-border-img" >
					<img src="img/rounddot.png"><span>${task.pubTime}</span>
				</div>
				<div class="box-content-border-div">
					<div class="box-content-border-div-img">
						<div class="div-img"></div>
						<div class="div-p"><p><b><a style="font-size:1.5rem;" href="TaskAction_queryTaskinfo?task.taskId=${task.taskId}&user.id=${user.id}">${task.taskTitle}</a></b></p></div>
					</div>
					<div class="box-content-border-div-rewu">
						<p><img src="img/money_icon.png">任务总数:<span>${task.taskTotalNumber }</span>
						<c:if test="${task.taskStatus==1||task.taskStatus==2}">
						<span class="renwuing"  ><img src="img/rl_icon.png">任务状态&nbsp;:&nbsp;进行中...</span></c:if></p>
					</div>
					<div class="box-content-border-div-leixing"> 
						<p style="font-size:1.5rem;">需求类型:<button style="font-size:1.2rem;"class="box-button">
						<c:if test="${task.taskType==1}">关注公众号</c:if>
						<c:if test="${task.taskType==2}">广告宣传</c:if>
						<c:if test="${task.taskType==3}">APP下载</c:if>
						</button></p>
					</div>
					<div class="box-div">
						<div class="text-right box-content-border-div-price">
							<p>总价格 ¥ <span>${task.taskTotalBonus}</span></p>
						</div>
					</div>
					<c:if test="${task.taskStatus==0}">
					<div class="box-content-border-div-heji">
						<p>合计:${task.heji}(含手续费 ¥${task.shouxu})</p>
						<a href="TaskAction_payPage?user.id=${user.id}&task.taskId=${task.taskId}"><button class="box-zhifu" id="sub">去支付</button></a>
						
					</div>
					</c:if>
					<c:if test="${task.taskStatus==1}">
					<div class="box-content-border-div-heji">
						<p>合计:${task.heji}(含手续费 ¥${task.shouxu})</p>
						<button class="box-zhifu zhifusecc">进行中</button>
						
					</div>
					</c:if>
					<c:if test="${task.taskStatus==4}">
					<div class="box-content-border-div-heji">
						<p>合计:${task.heji}(含手续费 ¥${task.shouxu})</p>
						<button class="box-zhifu zhifusecc">待审核</button>
						
					</div>
					</c:if>
					<c:if test="${task.taskStatus==2}">
					<div class="box-content-border-div-heji">
						<p>退款金额:${task.taskSBonus}</p>
						<button class="box-zhifu tuikuan">已退款</button>
						
					</div>
					</c:if>
				</div>
				
				</c:forEach>
			</div>
			<div id="dianji" style="font-size:14px; left:0; width:100%; height:40px; text-align:center; line-height:40px; color:#000; background:#fff; margin-top:10px;">点击加载更多</div>
		</div>
		 <!--======================尾部======================-->
	<div style="z-index:-9999;height: 100px;width: 100%;"></div>
	<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>
<div style="widtn：100%; height:200px;"></div>
		<script type="text/javascript" src="js/jquery.js" ></script>
		<script type="text/javascript" src="js/bootstrap.js" ></script>
<script type="text/javascript">
		var page = $("#page").val();
		page++;
		var Total = $("#Total").val();
		if(page>Total|| total==1){
		$("#dianji").css("display", "none");
	}
		</script>
<script type="text/javascript">
	$("#dianji")
			.click(
					function() {
						var page = $("#page").val();
						alert(page)
						var opem = $("#openId").val();
						var dainji = $("#dianji").html();
						if (dainji == "点击加载更多") {
							$("#dianji").html("正在加载...");
							var commcontent = "";
							page++;
							var Total = $("#Total").val();
							alert(Total)
							$.ajax({
										type : "GET",
										url : "jsonAction_queryTaskxq",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
										data : {
											"page" : page,
											"Total" : Total,
											"openId" : opem
										},
										//dataType:"json",
										success : function(result) {
											var jsonArray = eval("(" + result + ")");
											if (jsonArray != null) {
												for (var i = 0; i < jsonArray.length; i++) {
												commcontent+="<div class='box-content-border-img' >";
												commcontent+="<img src='img/rounddot.png'><span>"+jsonArray[i].PubTime+"</span>";
												commcontent+="</div>";
												commcontent+="<div class='box-content-border-div'>";
												commcontent+="<div class='box-content-border-div-img'>";
												commcontent+="<div class='div-img'>";
												commcontent+="</div>";
												commcontent+="<div class='div-p'><p><b><a style='font-size:1.5rem;' href='TaskAction_queryTaskinfo?task.taskId="+jsonArray[i].id+"&user.id="+jsonArray[i].userid+"'>"+jsonArray[i].Title+"</a></b></p></div>";
												commcontent+="</div>";
												commcontent+="<div class='box-content-border-div-rewu'>";
												commcontent+="<p>";
												commcontent+="<img src='img/money_icon.png'>任务总数:<span>"+jsonArray[i].TotalNumber+"</span>";
												if(jsonArray[i].Status==1||jsonArray[i].Status==2){
												commcontent+="<span class='renwuing'><img src='img/rl_icon.png'>任务状态&nbsp;:&nbsp;进行中...</span>";
												}else {
												commcontent+="<span class='renwuing'></span>";	
												}
												commcontent+="</p>";
												commcontent+="</div>";
												
												
												commcontent+="<div class='box-content-border-div-leixing'>";
												if(jsonArray[i].Type=1){
												commcontent+="<p style='font-size:1.5rem;'>需求类型:<button style='font-size:1.2rem;'class='box-button'>关注公众号</button></p>";
												
												}else if(jsonArray[i].Type=2){
												commcontent+="<p style='font-size:1.5rem;'>需求类型:<button style='font-size:1.2rem;'class='box-button'>广告宣传</button></p>";
												}else if(jsonArray[i].Type=3){
												commcontent+="<p style='font-size:1.5rem;'>需求类型:<button style='font-size:1.2rem;'class='box-button'>App下载</button></p>";
												}
												commcontent+="</div>";
												commcontent+="<div class='box-div'>";
												commcontent+="<div class='text-right box-content-border-div-price'>";
												commcontent+="<p>总价格 ¥ <span>"+jsonArray[i].TotalBonus+"</span></p>";
												commcontent+="</div>";
												commcontent+="</div>";
												if(jsonArray[i].Status==0){
												commcontent+="<div class='box-content-border-div-heji'>";
												if(jsonArray[i].heji!=null&&jsonArray[i].Shouxu!=null){
												commcontent+="<p>合计:"+jsonArray[i].heji+"(含手续费 ¥"+jsonArray[i].Shouxu+")</p>";
												}else {
													commcontent+="<p>合计:0.00(含手续费 ¥0.00)</p>";
												}
												commcontent+="<a href='TaskAction_payPage?task.taskId="+jsonArray[i].id+"&user.id="+jsonArray[i].userid+"'><button class='box-zhifu' id='sub'>去支付</button></a>";
												commcontent+="</div>";
												}else if (jsonArray[i].Status==1) {
													commcontent+="<div class='box-content-border-div-heji'>";
												if(jsonArray[i].heji!=null&&jsonArray[i].Shouxu!=null){
												commcontent+="<p>合计:"+jsonArray[i].heji+"(含手续费 ¥"+jsonArray[i].Shouxu+")</p>";
												}else {
												commcontent+="<p>合计:0.00(含手续费 ¥0.00)</p>";	
												}
												commcontent+="<button class='box-zhifu zhifusecc'>进行中</button>";
												commcontent+="</div>";
												}else if (jsonArray[i].Status==4) {
													commcontent+="<div class='box-content-border-div-heji'>";
												if(jsonArray[i].heji!=null&&jsonArray[i].Shouxu!=null){
												commcontent+="<p>合计:"+jsonArray[i].heji+"(含手续费 ¥"+jsonArray[i].Shouxu+")</p>";
												}else {
												commcontent+="<p>合计:0.00(含手续费 ¥0.00)</p>";	
												}
												commcontent+="<button class='box-zhifu zhifusecc'>待审核</button>";
												commcontent+="</div>";	
												}else if (sonArray[i].Status==2) {
													commcontent+="<div class='box-content-border-div-heji'>";
												if(jsonArray[i].SBonus!=null){
												commcontent+="<p>退款金额:"+jsonArray[i].SBonus+"</p>";
												}else {
												commcontent+="<p>退款金额::0.00</p>";	
												}
												commcontent+="<button class='box-zhifu zhifusecc'>已退款<</button>";
												commcontent+="</div>";
												}
												commcontent+="</div>";
												
												}
												$("#tiaozhuangye").append(commcontent);
												$("#page").val(page);
												if(page<Total){											
												$("#dianji").html("点击加载更多");
												}else {
												$("#dianji").css("display", "none");
												}
											} else {
												$("#dianji").attr({
													"disabled" : "disabled"
												});
												$("#dianji").css("display", "none");
											}
										}
									});
						}

					});
</script>
	</body>
</html>
