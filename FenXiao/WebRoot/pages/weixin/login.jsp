<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>登录</title>
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/login.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/foot.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
<script src="${pageContext.request.contextPath}/jsp/layer/layer.js"
	type="text/javascript"></script>
<style type="text/css">
.login input[type=password] {
	margin-left: 2%;
}
</style>
</head>
<body>

	<!--头部-->
	<%-- <header class="box-header">
        <div class="box-header-div1">
            <div class="box-header-div1_div1"><img src="${pageContext.request.contextPath}/img/a-1right.png"></div>
            <div class="box-header-div1-div2"><a href="#"><strong>返回</strong></a></div>
        </div>
        <div class="box-header-div2"><strong> 登录 </strong></div>
        <div class="box-header-div3"><a href="#"><img src="${pageContext.request.contextPath}/img/threeDian.png" ></a></div>
    </header> --%>

	<form
		action="${pageContext.request.contextPath}/UserAction_weixinLogin"
		method="post" class="form">
		<div class="All">
			<div class="login container-fluid">
				<div class="phonenum mg">
					<img src="${pageContext.request.contextPath}/img/mine_f.png"
						class="mine"> <input type="text" name="user.mobile" style="color: #000;"
						placeholder="请输入手机号码" id="mobile">
				</div>
				<hr class="fgx">
				<div class="pwd mg">
					<img src="${pageContext.request.contextPath}/img/lock.png"
						class="lock"> <input type="password" style="color: #000;"
						name="user.userPassword" placeholder="请输入6-16位密码" id="password">
				</div>

			</div>


			<div class="login_enter container-fluid">
				<input class="loginenter_btn btn" type="button" id="submit111"
					value="登录">
			</div>
			<input type="hidden" value="${openId }" id="openId">

			<div class="xq_nav container">
				<a
					href="${pageContext.request.contextPath}/UserAction_registerAfterSubscribeCompletion?openId=${openId }"
					class="register"> 注册会员 </a> <a
					href="${pageContext.request.contextPath}/UserAction_forgetPasswordForloginEntering?openId=${openId }"
					class="forgotpwd"> 忘记密码 </a>
			</div>
		</div>
	</form>
	<!--======================尾部======================-->
	<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>
		<script type="text/javascript">
	 	var mobile = localStorage.getItem("demokey");
		$("#mobile").val(mobile);
		</script>
	<script type="text/javascript">
		$(function() {
			var flag = false;
			var msg = $("#msg").val();
			if ($.trim(msg) != "") {
				layer.msg("用户名或密码不正确！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
			}
			;

			$("#password").blur(function() {
				var password = $("#password").val();
				if ($.trim(password) == "") {
					layer.msg("密码不能为空！", {
						time : 3000,
						offset : 0,
						shift : 6
					});

				}
			});

			$("#mobile")
					.blur(
							function() {
								var mobile = $("#mobile").val();
								var openId = $("#openId").val();
								if ($.trim(mobile) == "") {
									layer.msg("手机号不能为空！", {
										time : 3000,
										offset : 0,
										shift : 6
									});

								}

								var myreg = /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
								if (!myreg.test($("#mobile").val())) {
									layer.msg("请输入有效手机号码！", {
										time : 3000,
										offset : 0,
										shift : 6
									});
								}

								$.ajax({
									type : "GET",
									url : "UserAction_checkOwnMobile",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
									data : {
										"mobile" : mobile,
// 										"openId" : openId,
									},
									dataType:"json",
									success : function(data) {
										if (data.code == 0) {
											layer.msg("请输入绑定手机号码！", {
												time : 3000,
												offset : 0,
												shift : 6
											});
										}
									}
								});

							});

			$("#submit111")
					.click(
							function() {
								var mobile = $("#mobile").val();
								var openId = $("#openId").val();
								localStorage.setItem("demokey", mobile);
								if ($.trim(mobile) == "") {
									layer.msg("手机号不能为空！", {
										time : 3000,
										offset : 0,
										shift : 6
									});
									return false;
								}

								var myreg = /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
								if (!myreg.test($("#mobile").val())) {
									layer.msg("请输入有效手机号码！", {
										time : 3000,
										offset : 0,
										shift : 6
									});
									return false;
								}
								var password = $("#password").val();
								if ($.trim(password) == "") {
									layer.msg("密码不能为空！", {
										time : 3000,
										offset : 0,
										shift : 6
									});
									return false;
								}
								$.ajax({
									type : "GET",
									url : "UserAction_checkOwnMobile",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
									async : false,
									data : {
										"mobile" : mobile,
										"openId" : openId,
									},
									success : function(data) {
										if (data.code == 1) {
											flag = true;
											return true;
										} else {
											layer.msg("请输入绑定手机号码！", {
												time : 3000,
												offset : 0,
												shift : 6
											});
											flag = false;
											return false;
										}
									}

								});
								if (flag) {
									$(".form").submit();
								}

							});

		});
	</script>
	<script type="text/javascript">
		function reminder() {
			layer.msg("请先登录！", {
				time : 3000,
				offset : 0,
				shift : 6
			});

		}
	</script>

</body>
</html>
