<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title> 详情页面 </title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/requirement.css" rel="stylesheet" >
    <link href="${pageContext.request.contextPath}/css/foot.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/jquery.min.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/bootstrap.js" type="text/javascript"></script>

</head>
<body>

    <!-- 内容 -->

    <div class="All">

        <!-- panel 1 -->
        <div class="panel panel-default">
            <div class="panel-body">
                <ul>
                    <li class="panel1-ul1-li1">
                        <img src="${pageContext.request.contextPath}/img/ingreat_icon.png">
                        <label> 任务编号：${task.taskCode } </label>
                        <span>
                任务状态： <c:if test="${task.taskStatus==1 }"> 进项中</c:if>   
                  <c:if test="${task.taskStatus==2}"> 已完成</c:if>    
                  <c:if test="${task.taskStatus==5}">提交审核中</c:if>     
                        
                         
                         
                         
                         
                         
                         </span>
                    </li>
                    <li class="panel1-ul1-li2">
                        <img src="${pageContext.request.contextPath}/img/money_icon.png">
                        <ul>
                            <li class="panel1-ul1-li2-yumon"> 剩余金额 </li>
                            <li> <b> 8724 </b> </li>
                        </ul>
                    </li>
                    <li class="panel1-ul1-li3">
                        <img src="${pageContext.request.contextPath}/img/rl_icon.png">
                        <ul>
                            <li class="panel1-ul1-li3-peonum"> 关注人数 </li>
                            <li> <b> 8724 </b> </li>
                        </ul>
                    </li>
                    <li class="panel1-ul1-li4">
                            <ul class="panel1-ul1-li4_son1 list-inline">
                                <li class="panel1-ul1-li4_img1"> <img src="${pageContext.request.contextPath}/img/lo.png" > </li>
                                <li class="panel1-ul1-li4_text1"> <a href="${pageContext.request.contextPath}/pages/weixin/focusonpeople.jsp"> 查看关注人数列表 </a> </li>
                            </ul>
                            <ul class="panel1-ul1-li4_son2 list-inline">
                                <li class="panel1-ul1-li4_img2"> <img src="${pageContext.request.contextPath}/img/biao.png" > </li>
                                <li class="panel1-ul1-li4_text2"> <a href="#"> 活动时间:06-22-06-30 </a> </li>
                            </ul>
                    </li>
                </ul>
            </div>
        </div>

        <!-- panel 2 -->
        <div class="pane2 pane2-default">
            <div class="panel-body">
                <ul>
                    <li class="panel2-ul1-li1">
                        <label> 需求编号：234927428734498327 </label>
                        <span> 下单时间：2014.7.17 </span>
                    </li>
                    <li class="panel2-ul1-li2">
                        <img src="${pageContext.request.contextPath}/img/index_tit.png" >
                        <ul class="panel2-ul1-li2_son">
                            <li class="panel2-ul1-li2_son_lititle"> <a href="${pageContext.request.contextPath}/TaskAction_taskDetail?taskId=${task.taskId }"> ${task.taskTitle }</a> </li>
                            <li class="panel2-ul1-li2_son_licon">
                                <a href="${pageContext.request.contextPath}/TaskAction_taskDetail?taskId=${task.taskId }">
                                    ${task.taskBriefing }
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="pane_price text-right">
                        <label> 总价格<span> ￥8274 </span> </label>
                    </li>

                    <li class="panel1-ul2-li3">
                        <ul class="list-inline panel1-ul2-li3_son1">
                            <li class="nr"> 需求类型: </li>
                            <li class="dw"> 
                            <c:if test="${task.taskType==1 }">关注公众号 </c:if>
                            
                            <c:if test="${task.taskType==2 }">APP下载 </c:if>
                            
                            </li>
                        </ul>
                        <ul class="list-inline panel1-ul2-li3_son2">
                            <img src="${pageContext.request.contextPath}/img/money_icon.png">
                            <li class="cotrnum"> 任务总数: </li>
                            <li class="snum"> ${task.taskNumber } </li>
                        </ul>
                    </li>
                    <li class="panel1-ul3-li4">
                        <ul class="panel1-ul3-li4_son1 list-inline">
                            <li class="panel3-ul1-li4_text1">
                                合计:129.00(含手续费￥0.00)
                            </li>
                            <li class="panel3-ul1-li4_text2">
                                付款成功
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>

        <div class="pane3_label">
            <label> 详情介绍 </label>
        </div>
        <div class="pane3 pane3-default">
            <div class="panel-body">
                <div class="pane3_contenttext">
                    <p>
                        香港塑身堂(国际)控股有限公司是集科研、生产、进出口贸易于一体的权威实业公司,总部
                        设在香港.在全球多个国家和地区有合作机构,其供应商在欧洲和台湾有自己的科研基地,拥有专
                        业国际级人才组件的卓越团队,顶尖的技术研发专家团,引入国际一流的时尚理念及科学的经营管
                        理模式,以其丰富的理疗经验和强大的实力博得业界尊崇!在中国及亚洲瘦身、纤体市场拥有很高
                        的占有率.
                    </p>
                </div>
                <div class="pane3_img">
                    <img src="${pageContext.request.contextPath}/img/Ad_Details_bo.png">
                </div>
            </div>
        </div>

    </div>


    <!--======================尾部======================-->
    <div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>

</body>
</html>
