<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>帮助中心</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/helpCenter.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/foot.css" />
<script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
<script src="${pageContext.request.contextPath}/jsp/layer/layer.js"
	type="text/javascript"></script>
</head>
<body>

	<div class="all col-xs-12">

	 <!-- 圆点 样式 -->
        <div class="icon" id="icon">
            <!-- <a class="round">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> -->
        </div>
		<!-- right -->
		<div class="right col-xs-11" id="col">
			<!-- content1 如何做任务 -->
			<div class="contenet-task">
				<h4>1、如何点亮优惠？</h4>
				<ul>
					<li><span>1</span> 进入首页点击“点亮”，进入任务详情页面。</li>
					<li><span>2</span> 按照任务详情页面的要求进行操作。</li>
				</ul>
			</div>

			<!-- content2 如何提现 -->
			<div class="content-right content-withdrawal">
				<h4>2、如何发布优惠？</h4>
				<ul>
					<li><span>1</span> 进入“我的”个人中心，点击“发布需求”</li>
					<li><span>2</span> 按各板块要求，填入相应内容或者上传照片。</li>
				</ul>
			</div>
			<div class="content-right content-withdrawal">
				<h4>3、郑重声明：</h4>
				<ul>
					<li><span>1</span> 会员在平台上做任务不收任何费用！</li>
					<li><span>2</span> 被邀请的会员再邀请会员，和邀请人没有任何关系！</li>
				</ul>
			</div>
			<div class="content-right content-withdrawal">
				<h4>4.所有任务在10:00-22:00每个整点发布</h4>
<!-- 				<ul> -->
<!-- 					<li><span>1</span> ①会员在平台上做任务不收任何费用！</li> -->
<!-- 					<li><span>2</span> ②被邀请的会员再邀请会员，和邀请人没有任何关系！</li> -->
<!-- 				</ul> -->
			</div>

			<!-- content3 提现须知 -->
			<div class="content-right content-withdrawalNotice"
				style="margin-bottom:20%;">
				<h4>5、如何提现：</h4>
				<ul>
					<li><span>1</span> </li>
					<p>提现手续费0.60%</p>
					<p>提现金额*0.6%≤2元，按照2元手续费收取</p>
					<p>2＜提现金额*0.6%＜25，按照提现金额*0.6%手续费收取</p>
					<p>提现金额*0.6%≥25元，按照25元手续费收取</p>
					<li><span>2</span> 单笔最低提现 不限</li>
					<p>第三方支付渠道（提现周期T＋3）</p>
					<li><span>3</span> 单笔最高提现5000元</li>
					<li><span>4</span> 单日最高提现20000元</li>
				</ul>
			</div>
		</div>

	</div>

	<!--======================尾部======================-->
	<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>

	<script type="text/javascript">
        var oCol = document.getElementById('col');
        var aDiv = oCol.getElementsByTagName('div');
        var oIcon = document.getElementById('icon');

        for(var i = 0; i < aDiv.length; i++){
            var oA = document.createElement("a");
            oIcon.appendChild(oA);
            oA.className = 'round';
        }
        var aA = oIcon.getElementsByTagName('a');
        for(var i = 0; i < aA.length; i++){
            ;(function(index){
                aA[index].style.top = index*aDiv[0].offsetHeight+'px';
            })(i);
        }
    </script>
</body>
</html>
