<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>忘记密码</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/retrieve.css" >
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font_size1.css" >
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/foot.css"/>
    <script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/jsp/layer/layer.js" type="text/javascript"></script>
    <!-- 用IE专用的注释，防止非IE浏览器下载。-->
    <!--[if lt IE 10]>
    <script type="text/javascript" src="PIE.js"></script>
    <![endif]-->

</head>
<body>
<form action="${pageContext.request.contextPath}/UserAction_resetPasword" method="post" class="form">
    <div class="container-fluid all">
        <!-- 交易 密码 -->
        <div class="content-pwd col-xs-12" style="vertical-align: middle">
            <!-- 交易密码 -->
            <div class="trade">
                <span class="font17 ">提现密码</span>
                <input type="password" placeholder="输入新密码" class="font12" name="password" id="cardPassword">
                <span id="cardPasswordCheckCheck"></span>
            </div>

            <!-- 重复密码 -->
            <div class="repeat">
                <span  class="font17 ">重复密码</span>
                <input type="password" placeholder="重复密码" class="font12" id="repeatCardPassword" >
                <span id="repeatCardPasswordCheckCheck"></span>
            </div>

        </div>

        <!-- 手机号 验证 -->
        <div class="content-phone col-xs-12">
            <!-- 手机号 -->
            <div class="number">
                <span class="font17">手机号&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <input type="text" placeholder="手机号码" class="font12"  value="${lastmodile }" readonly="readonly">
                
                <input type="hidden" value="${modil }" name="modil" id="cardMobile">
                <input type="hidden" value="${user.id }" name="userId">
            </div>
            <!-- 验证码 -->
            <div class="identifying-code" style="position: relative;">
                <span class="font17">验证码&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <input type="text" placeholder="手机验证码" class="font12" id="SMSCheckCode">
                <input type="hidden" id="backstage" value="">
                <input type="button" style="background-color:#12B7F5;height:25px;position: absolute;top: 10px;right: 2px" value="   获 取 验 证 码     " class="font12" id="btnSendCode"  onClick="getSMSCheckCode(${user.id },this),sendMessage()">
            </div>

        </div>

        <!-- 确认修改 按钮 -->
        <input type="button" value="确认修改" id="submit111" class="btn-submit col-xs-10 col-xs-offset-1">
    </div>
</form>

    <!--======================尾部======================-->
   <div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_true.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>
</body>
<script type="text/javascript">
function getSMSCheckCode(value, object) {
	var cardMobile = $("#cardMobile").val();
	/* if ($.trim(cardMobile) == "") {
		$("#cardMobileCheck")
				.html("<font color='red'>开户手机号不能为空</font>");
		return false;
	} */
	if ($.trim(cardMobile) == "") {
		layer.msg("手机号不能为空！", {
		    time: 3000,
		    offset: 0,
		    shift: 6
		  });
		return false;
	}; 
	
	var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
	if (!myreg.test($("#cardMobile").val())) {
		layer.msg("请输入有效手机号码！", {
		    time: 3000,
		    offset: 0,
		    shift: 6
		  });
		return false;
	} 
	var cardMobile = $("#cardMobile").val();
	$.ajax({
		type : "GET",
		url : "UserAction_getSMSCheckCode",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
		data : {
			"mobile" : cardMobile,
			"userId" : value
		},
		//dataType:"json",
		success : function(data) {
			if(data.codeT==0){
				alert(data.msg);
			}else{
				$("#backstage").val(data.code);
				$(object).html("<font color='green'>验证码已发送</font>");
			}
		}
	});
	
}

$(function(){
	
	$("#repeatCardPassword")
			.blur(
					function() {
						var repeatCardPassword = $("#repeatCardPassword").val();
						var cardPassword = $("#cardPassword").val();
						if (cardPassword != repeatCardPassword) {
							layer.msg("两次输入的密码不一致！", {
							    time: 3000,
							    offset: 0,
							    shift: 6
							  });
							
						} 
					});
	$("#SMSCheckCode").blur(function() {
				var SMSCheckCode = $("#SMSCheckCode").val();
				var backstage = $("#backstage").val();
					if ($.trim(SMSCheckCode) == "") {
						layer.msg("验证码不能为空！", {
						    time: 3000,
						    offset: 0,
						    shift: 6
						  });
				} 
				if (SMSCheckCode != backstage && backstage != "") {
							layer.msg("验证码不正确！", {
							    time: 3000,
							    offset: 0,
							    shift: 6
							  });
				}
			});
});
</script>
<script type="text/javascript">
$(function(){
	$("#cardPassword").blur(
			function() {
				var cardPassword = $("#cardPassword").val().trim();
				var repeatCardPassword=$("#repeatCardPassword").val();
				if ($.trim(cardPassword) == "") {
					layer.msg("密码不能为空！", {
					    time: 3000,
					    offset: 0,
					    shift: 6
					  });
				} 
				
				if(cardPassword.length<6||cardPassword.length>16){
	                layer.msg("注册密码长度6~16！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
	            }
				
				if(cardPassword!=repeatCardPassword&&repeatCardPassword!=""){
					layer.msg("两次输入的密码不一致！", {
					    time: 3000,
					    offset: 0,
					    shift: 6
					  });
				}
			});
	
	
	$("#submit111").click(function(){
		var cardPassword = $("#cardPassword").val().trim();
		var repeatCardPassword=$("#repeatCardPassword").val();
		if ($.trim(cardPassword) == "") {
			layer.msg("密码不能为空！", {
			    time: 3000,
			    offset: 0,
			    shift: 6
			  });
			return false;
		} 
		if(cardPassword.length<6||cardPassword.length>16){
            layer.msg("注册密码长度6~16！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
            return false;
        }
		
		if(cardPassword!=repeatCardPassword&&repeatCardPassword!=""){
			layer.msg("两次输入的密码不一致！", {
			    time: 3000,
			    offset: 0,
			    shift: 6
			  });
			return false;
		}
		
		var SMSCheckCode = $("#SMSCheckCode").val();
		var backstage = $("#backstage").val();
			if ($.trim(SMSCheckCode) == "") {
				layer.msg("验证码不能为空！", {
				    time: 3000,
				    offset: 0,
				    shift: 6
				  });
				return false;
		} 
			if ($.trim(backstage) == "") {
				layer.msg("请获取验证码！", {
				    time: 3000,
				    offset: 0,
				    shift: 6
				  });
				return false;
		} 
		if (SMSCheckCode != backstage && backstage != "") {
					layer.msg("验证码不正确！", {
					    time: 3000,
					    offset: 0,
					    shift: 6
					  });
					return false;
		}
		$(".form").submit();
	});
});
</script>
<script type="text/javascript">

var InterValObj; //timer变量，控制时间
var count = 60; //间隔函数，1秒执行
var curCount;//当前剩余秒数

function sendMessage() {
     curCount = count;
     var mobile = $("#cardMobile").val();
		if ($.trim(mobile) == "") {
			layer.msg("手机号不能为空！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}
		
		var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
		if (!myreg.test($("#cardMobile").val())) {
			layer.msg("请输入有效手机号！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}
     $("#btnSendCode").attr("disabled", "true");
     $("#btnSendCode").val("请在" + curCount + "秒内输入验证码");
     InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次
    
}

//timer处理函数
function SetRemainTime() {
            if (curCount == 0) {                
                window.clearInterval(InterValObj);//停止计时器
                $("#btnSendCode").removeAttr("disabled");//启用按钮
                $("#btnSendCode").val("重新发送验证码");
            }
            else {
                curCount--;
                $("#btnSendCode").val("请在" + curCount + "秒内输入验证码");
            }
        }
</script>
</html>
