<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="java.util.Date"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title></title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/foot.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/gongzhonghaoxiangqing03.css" />
</head>
<body>
	<div id="box">
		<!--头部-->
		<div class="top">
			<div class="top-top">
				<div class="top-top-div1">
					<img src="${pageContext.request.contextPath}/${task.taskLogo }"
						width="80px" height="60px">
				</div>
				<div class="top-top-div2">
					<p>
						<b>${task.taskTitle }</b>
					</p>
					<label>${task.taskBriefing }</label>
				</div>
			</div>
			<div class="top-content">
				<div class="text-left top-content-left">
					<p>
						<b>剩余数量:</b>&nbsp;${task.taskRemainderNumber }
					</p>
				</div>
				<div class="text-right top-content-right">
					<p>
						<img
							src="${pageContext.request.contextPath}/img/index_time_true.png"
							width="18px" height="18px"> <span
							class=" SY_nav_now_nav_cont_time font12" id="span"> <span
							id="time_d" class="time"> </span> <span id="time_h" class="time">
						</span> <span id="time_m" class="time"> </span> <span id="time_s"
							class="time"></span>
						</span> <input type="hidden" id="endTime" value="${task.taskEndTime }" />
						<input type="hidden" id="taskRemainderNumber" value="${task.taskRemainderNumber }" />
				</div>
			</div>
			<div class="top-bottom">
				<div class="text-left top-bottom-left">
					<p>
						<b id="money">¥${task.taskBonus }</b>
					</p>
				</div>
				<div class="text-right top-bottom-right">
					<div class="top-botton-right-right" id="statusRemainder">
					<c:set var="nowDate">
						    <fmt:formatDate value="<%=new Date()%>"  pattern="yyyy-MM-dd HH:mm:ss" type="date"/> 
						</c:set>  
						<c:set var="createDate">  
						   <%--  <fmt:formatDate value="${list.taskEndTime}" pattern="yyyy-MM-dd HH:mm:ss" type="date"/> --%> 
						     <fmt:parseDate value="${task.taskEndTime}" var="date" pattern="yyyy-MM-dd HH:mm:ss"/>  
                             <fmt:formatDate value="${date}" pattern="yyyy-MM-dd HH:mm:ss" />  
						</c:set> 
						<%-- <c:if test="${nowDate<createDate&&task.taskRemainderNumber>0}"> 
				 	  <input type="button" value="做任务" id="doTheTask"
							onClick="doTheTask(${task.taskId })">
				      </c:if> --%>
				      <c:if test="${task.taskRemainderNumber==0}"> 
				 	<div class="top-botton-right-right"><input type="button" value="该任务已完成" id=""></div>
				      </c:if>
				      <c:if test="${nowDate>createDate}"> 
				 	<div class="top-botton-right-right"><input type="button" value="该任务已结束" id=""></div>
				      </c:if>
					
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" value="${openId}" id="openId">
		<div class="content">
			<div class="content-top">
				<p>任务操作步骤</p>
			</div>
			<div class="button-dian">
				<div class="button-dian-left">
					<div class="button-dian-left-dian"></div>
					<div class="button-dian-left-dian1">
						<p>
							长按下方蓝色字符先点"选择",再点"复制" <br /> <span><font color="blue">
							
							<c:if test="${task.taskRemainderNumber!=0&&nowDate<createDate}"> 
							<c:if test="${randomCodeStained!=null }">
							${randomCodeStained }
							</c:if>
							<c:if test="${randomCode!=null }">
							${randomCode }
							</c:if>
							</c:if>
							
							
							</font></span>
						</p>
					</div>
				</div>
				<div class="button-dian-middle">
					<div class="button-dian-left-dian"></div>
					<div class="button-dian-left-dian1">
						<p>长按二维码,选择识别二维码,关注公众号</p>
					</div>
				</div>
				<div class="button-dian-bottom">
					<div class="button-dian-left-dian"></div>
					<div class="button-dian-left-dian1">
						<p>进入新公众号,在对话框内粘贴并发送已复制的字符编码,等待系统审核通过,即可获得佣金</p>
					</div>
				</div>
			</div>
			<div class="content-two">
				<p>
					<img src="${pageContext.request.contextPath}/${task.taskOr }">
				</p>
			</div>
		</div>
		<input type="hidden" value="${taskStatus}" id="taskStatus">
		<!--======================尾部======================-->
		<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
</body>
<script type="text/javascript">
	//function remainedTime(value) {
	$(function() {
		var time_start = new Date().getTime();//设定开始时间 
		var endTime = $("#endTime").val();
		var endTime1 = endTime.replace("-", "/");
		var endTime2 = endTime1.replace("-", "/");
		/* alert(endTime2.length); */

		var s = endTime2.substring(0, endTime2.length - 2);
		console.log(s);
		var time_end = new Date(s).getTime(); //设定结束时间(等于系统当前时间) 
		//计算时间差 
		var time_distance = time_end - time_start;
		if (time_distance > 0) {
			// 天时分秒换算 
			var int_day = Math.floor(time_distance / 86400000);
			time_distance -= int_day * 86400000;

			var int_hour = Math.floor(time_distance / 3600000);
			time_distance -= int_hour * 3600000;

			var int_minute = Math.floor(time_distance / 60000);
			time_distance -= int_minute * 60000;

			var int_second = Math.floor(time_distance / 1000);
			// 时分秒为单数时、前面加零 
			if (int_day < 10) {
				int_day = "0" + int_day;
			}
			if (int_hour < 10) {
				int_hour = "0" + int_hour;
			}
			if (int_minute < 10) {
				int_minute = "0" + int_minute;
			}
			if (int_second < 10) {
				int_second = "0" + int_second;
			}
			// 显示时间 
			$("#time_d").html("<font color='green'>" + int_day + " 天</font>");
			$("#time_h").html("<font color='green'>" + int_hour + " 时</font>");
			$("#time_m")
					.html("<font color='green'>" + int_minute + " 分</font>");
			$("#time_s")
					.html("<font color='green'>" + int_second + " 秒</font>");

			setTimeout("remainedTime()", 1000);

		} else {
			$("#time_d").html("<font color='red'>时");
			$("#time_h").html("<font color='red'>间");
			$("#time_m").html("<font color='red'>结");
			$("#time_s").html("<font color='red'>束");
		}
		/* $("#button" + value).css("display", "none");
		$("#span" + value).css("display", "block"); */

	});
	//}
	function remainedTime() {
		var time_start = new Date().getTime();//设定开始时间 
		var endTime = $("#endTime").val();
		var endTime1 = endTime.replace("-", "/");
		var endTime2 = endTime1.replace("-", "/");
		/* alert(endTime2.length); */

		var s = endTime2.substring(0, endTime2.length - 2);
		console.log(s);
		var time_end = new Date(s).getTime(); //设定结束时间(等于系统当前时间) 
		//计算时间差 
		var time_distance = time_end - time_start;
		if (time_distance > 0) {
			// 天时分秒换算 
			var int_day = Math.floor(time_distance / 86400000);
			time_distance -= int_day * 86400000;

			var int_hour = Math.floor(time_distance / 3600000);
			time_distance -= int_hour * 3600000;

			var int_minute = Math.floor(time_distance / 60000);
			time_distance -= int_minute * 60000;

			var int_second = Math.floor(time_distance / 1000);
			// 时分秒为单数时、前面加零 
			if (int_day < 10) {
				int_day = "0" + int_day;
			}
			if (int_hour < 10) {
				int_hour = "0" + int_hour;
			}
			if (int_minute < 10) {
				int_minute = "0" + int_minute;
			}
			if (int_second < 10) {
				int_second = "0" + int_second;
			}
			// 显示时间 
			$("#time_d").html("<font color='green'>" + int_day + " 天</font>");
			$("#time_h").html("<font color='green'>" + int_hour + " 时</font>");
			$("#time_m")
					.html("<font color='green'>" + int_minute + " 分</font>");
			$("#time_s")
					.html("<font color='green'>" + int_second + " 秒</font>");

			setTimeout("remainedTime()", 1000);

		} else {
			$("#time_d").html("<font color='red'>时");
			$("#time_h").html("<font color='red'>间");
			$("#time_m").html("<font color='red'>结");
			$("#time_s").html("<font color='red'>束");

		}

	}

	function doTheTask(value) {
		$.ajax({
			type : "POST",
			url : "TaskAction_doWeixinTask",
			data : "taskId=" + value + "&openId=" + $("#openId").val(),
			success : function(data) {
				if (data.msg == 1) {
					$("#doTheTask").val("任务已进行");
					$("#doTheTask").attr("disabled", true);
				}

			}
		});

	}
</script>
<script type="text/javascript">
	$(function() {
		var taskStatus = $("#taskStatus").val();
		var taskRemainderNumber=$("#taskRemainderNumber").val();
		var time_start = new Date();//设定开始时间 
		
		var endTime = $("#endTime").val().replace(/-/g,"/").replace(/-/g,"/");
		endTime=endTime.substr(0,endTime.lastIndexOf('.'));
		var end=new Date(endTime); 
		if (taskStatus !=2&&taskRemainderNumber!=0&&time_start<end&&taskStatus !=6) {
			$("#statusRemainder").html("<input type='button' value='该任务已进行'>");
		}
		if (taskRemainderNumber==0) {
			$("#statusRemainder").html("<input type='button' value='该任务已完成'>");
		}
		if (time_start>end) {
			$("#statusRemainder").html("<input type='button' value='该任务已结束'>");
		}
		if (taskStatus == 2&&time_start<end) {
			$("#statusRemainder").html("<input type='button' value='该任务已完成'>");
		}
		
		if (taskStatus == 2&&time_start>end) {
			$("#statusRemainder").html("<input type='button' value='该任务已结束'>");
		}
		
	});
</script>

</html>
