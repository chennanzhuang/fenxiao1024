<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>我的需求支付</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<meta
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
	name="viewport">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/pay.css" />
<link rel="stylesheet" href="css/foot.css" />
<script type="text/javascript" src="js/jquery-2.0.3.min.js"></script>


<script type="text/javascript">
	$(function() {
		$("#button").click(function() {
			$(".form").submit();
		});
	})
</script>
</head>
<body style="background-color:#F0F0F0;">
	<!--头部-->
	<%-- <header class="box-header">
				<div class="box-header-div1">
				    <div class="box-header-div1_div1"><img src="img/a-1right.png"></div>
				    <div class="box-header-div1-div2"><a href="#"><strong>返回</strong></a></div>
				</div>
				<div class="box-header-div2"><strong>我的需求支付</strong></div>
				<div class="box-header-div3"><a href="#"><img src="img/threeDian.png" ></a></div>
			</header> --%>
	<form action="TaskAction_payTask" method="post" class="form">
		<input type="hidden" name="user.id" value="${user.id}"> <input
			type="hidden" name="task.taskId" value="${task.taskId}">
		<!--信息-->
		<div class="centent">
			<div class="centent-Information">
				<div class="centent-infor-font">
					<p>
						<b>${task.taskTitle}</b>
					</p>
				</div>
				<div class="two">
					<div class="two-left">
						<p>
							<img src="img/fs_num_icon.png"><span><strong>需求总数:${task.taskTotalNumber}</strong></span>
						</p>
						<p class="two-left-two">
							<b>发布时间:${task.pubTime}</b>
						</p>
						<p class="two-left-two">
							<b style="font-size:11px">订单编号:${task.taskCode}</b>
						</p>
					</div>
					<div class="two-rigth" style="float: right;width: 35%;">
						<p>
							<b>￥${task.taskTotalBonus}</b>
						</p>
					</div>
				</div>
				<div class="content-time">
					<p>合计:￥${task.heji}(含手续费￥${task.shouxu })</p>
				</div>
			</div>
			<div class="pay">
				<div class="centent-pay">
					<p>支付方式:</p>
				</div>
				<div class="centent-weixin">
					<img
						style="vertical-align: middle; text-align: center;margin: auto;margin-top:80%"
						src="img/modeofpayment_icon.png" width="14px" height="14px">
				</div>
				<div class="centent-weixinpay">
					<p>微信支付</p>
				</div>
			</div>
			<div class="queren" style="padding-top: 10px">
				<!--<p>确认支付</p>-->
				<div style="text-align: center;">
					请添加社区部落客服（微信号：gyr136）为好友<br /> 
							提供订单编号<br /> 
						并通过微信转账功能完成付款。
				</div>
				<img src="${pageContext.request.contextPath}/img/kehu.jpg" width="100px" height="100px"
					style="display:block;margin: auto;padding-top: 10px">
				<div style="padding-top: 10px; text-align: center;width: 100%">
					长按识别二维码加好友</div>

				<!--<p>确认支付</p>-->
				<!-- 					<input type="button" id="button" value="确认支付"> -->
			</div>
		</div>
	</form>
	<!--======================尾部======================-->
	<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_true.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>

	<script src="js/jquery.js" type="text/javascript"></script>
	<script src="js/bootstrap.js" type="text/javascript"></script>
	<script type="text/javascript">
		var msg = "${msg}";
		if (msg != "") {
			alert(msg);
		}
	</script>
</body>
</html>