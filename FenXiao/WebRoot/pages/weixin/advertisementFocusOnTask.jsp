<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="java.util.Date"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>任务详情</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/foot.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/buzhou.css" />

<script type='text/javascript'>
	document.querySelector('body').addEventListener('touchstart', function(ev) {
		event.preventDefault();
	});
</script>
</head>
<body>
	<div id="box">
		<div id="myCarousel" class="carousel slide" data-ride="carousel"
			data-interval="2000">
			<!-- 轮播（Carousel）指标 -->
			<ol class="carousel-indicators sy_carousel_ol" id="lunbotuliebiao">
				<c:forEach var="taskImg" items="${list}" varStatus="status">
					<li data-target="#myCarousel" data-slide-to="${status.index}"></li>
				</c:forEach>
			</ol>
			<!-- 轮播（Carousel）项目 -->
			<div class="carousel-inner" id="lunbotudiv">
				<c:forEach var="taskImg" items="${list}" varStatus="status">
					<div class="item" style="width:100%;height:200px;">

						<img src="${pageContext.request.contextPath}/${taskImg.imgUrl}">

					</div>
				</c:forEach>
			</div>
		</div>


		<%-- <div id="myCarousel" class="carousel slide">
			<!-- 轮播（Carousel）指标 -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
				<li data-target="#myCarousel" data-slide-to="3"></li>
			</ol>
			<!-- 轮播（Carousel）项目 -->
			<div class="carousel-inner">
				<div class="item active">
					<img src="${pageContext.request.contextPath}/img/Ad_Details_lo.png"
						alt="First slide">
				</div>
				
			</div>
			<!-- 轮播（Carousel）导航 -->
			<a class="carousel-control left" href="#myCarousel" data-slide="prev"></a>
			<a class="carousel-control right" href="#myCarousel"
				data-slide="next"></a>
		</div> --%>


		<!--top-->
		<div class="top">
			<div class="top-top">
				<div class="top-top-div2">
					<p>
						<b>${task.taskTitle }</b>
					</p>
					<label>${task.taskBriefing }</label>
				</div>
			</div>
			<div class="top-content">
				<div class="text-left top-content-left" style="width:30%;">
					<p id="renwusl">
						<b>剩余数量:</b>&nbsp;${task.taskRemainderNumber }
					</p>
				</div>
				<div class="text-right top-content-right" style="width:60%;">
					<p>
						<img
							src="${pageContext.request.contextPath}/img/index_time_true.png"
							width="18px" height="18px"> <span
							class=" SY_nav_now_nav_cont_time font12" id="span"> <span
							id="time_d" class="time"> </span> <span id="time_h" class="time">
						</span> <span id="time_m" class="time"> </span> <span id="time_s"
							class="time"></span>
						</span> <input type="hidden" id="endTime" value="${task.taskEndTime }" />
						<input type="hidden" id="taskRemainderNumber"
							value="${task.taskRemainderNumber }" />
					</p>
				</div>
			</div>
			<div class="top-bottom">
				<div class="text-left top-bottom-left">
					<p>
						<b>¥${task.taskBonus }</b>
					</p>
				</div>
				<div class="text-right top-bottom-right" id="statusRemainder">
					<c:set var="nowDate">
						<fmt:formatDate value="<%=new Date()%>"
							pattern="yyyy-MM-dd HH:mm:ss" type="date" />
					</c:set>
					<c:set var="createDate">
						<%--  <fmt:formatDate value="${list.taskEndTime}" pattern="yyyy-MM-dd HH:mm:ss" type="date"/> --%>
						<fmt:parseDate value="${task.taskEndTime}" var="date"
							pattern="yyyy-MM-dd HH:mm:ss" />
						<fmt:formatDate value="${date}" pattern="yyyy-MM-dd HH:mm:ss" />
					</c:set>
					<c:if test="${nowDate<createDate&&task.taskRemainderNumber>0}">
						<div class="top-botton-right-right">
							<input type="button" value="做任务" id="doTheTask"
								onClick="gettwofunction(${task.taskId})" style="width:100%;">

						</div>
						<div class="top-botton-right-right" style="text-indent: 20px;display: none">
							<input type="button" value="任务进行中" id="TheTask"
								onClick="getfunction()" style="width:100%;">

						</div>
					</c:if>
					<c:if test="${task.taskRemainderNumber>0&&task.shengc!=1&&nowDate<createDate&&taskStatus==2}">
						<div class="top-botton-right-right">
							<input type="button" value="该任务已完成" id="" >
						</div>
					</c:if>
					<c:if test="${nowDate>createDate && task.shengc!=1}">
						<div class="top-botton-right-right">
							<input type="button" value="该任务已结束" id="" >
						</div>
					</c:if>
				</div>
				<c:if test="${nowDate<createDate&&task.taskRemainderNumber>0&&task.shengc==1&&taskStatus==1&&task.adUrl!=''}">
					<div class="text-right top-bottom-right" style="margin-top: -40px;margin-left: 20px;">
						<div class="top-botton-right-right" >
							<input type="button" value="进入店铺" id="" onClick="allfunction()" style="background-color: #D3D3D3;width: 100PX;margin-right: -10px;">
						</div>
						</div>
					</c:if>
			</div>
		</div>
		<!--========bottom===========-->
		<div class="button">
			<div class="button-box" style="position:relative;">
				<span id="s" style="width:50%; height:3px; background:#15a0f5; position:absolute; left:0; bottom:0;"></span>
				<div class="button-box-left">
					<p>
						<a href="#" id="xianqingTask" class="doTheTask1"
							style="width:100%;"> 活动详情 </a>
						<!-- 						<input type="button" -->
						<!-- 							style="border: none;outline:none;background-color: none;" -->
						<!-- 							value="详情介绍" id="doTheTask" class="doTheTask1" style="width:100%;"> -->
					</p>
				</div>
				<div class="button-box-right" style="border:none;">
					<p>
						<a href="#" id="xianxiTask" style="width:100%;"> 任务详细步骤 </a>
						<!-- 						<input type="button" -->
						<!-- 							style="border: none;outline:none;background-color: none;" -->
						<!-- 							value="任务详细步骤" id="TheTask" style="width:100%;"> -->
					</p>
				</div>
			</div>
			<div class="xiangqing" >
				<p>${task.taskInfo }</p>
			</div>
			<input type="hidden" id="adurl" class="TaskAdurl"
				value="${task.adUrl}">
			<div class="button-dian" style="text-indent: 20px;display: none">
				<div class="button-dian-left">
					<div class="button-dian-left-dian"></div>
					<div class="button-dian-left-dian1">
						<p>点击"做任务"进入店铺并截图</p>
					</div>
				</div>
				<div class="button-dian-middle">
					<div class="button-dian-left-dian"></div>
					<div class="button-dian-left-dian1">
						<p>去我的的我的任务,找到该任务并上传截图</p>
					</div>
				</div>
				<div class="button-dian-bottom">
					<div class="button-dian-left-dian"></div>
					<div class="button-dian-left-dian1">
						<p>点击完成</p>
					</div>
				</div>

			</div>

		</div>
		<!--======================尾部======================-->
		<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>

		<input type="hidden" value="${openId}" id="openId">
		 <input
			type="hidden" value="${taskStatus }" id="taskStatus">
	</div>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
</body>
<script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
<script
	src="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jgestures.min.js"></script>
<script>
	$(document).ready(
			function() {
				//手势右滑 回到上一个画面
				$('#myCarousel').bind('swiperight swiperightup swiperightdown',
						function() {
							$("#myCarousel").carousel('prev');
						});
				//手势左滑 进入下一个画面
				$('#myCarousel').bind('swipeleft swipeleftup swipeleftdown',
						function() {
							$("#myCarousel").carousel('next');
						});
				$("#xianxiTask").click(function() {
					$(".button-dian").css("display", "block");
					$(".xiangqing").css("display", "none");
					$(".xiangqing").css("display", "none");
					$("#xianqingTask").css("border", "none");
					$('#s').css('left','50%');
				});
				$("#xianqingTask").click(function() {
					$(".xiangqing").css("display", "block");
					$(".button-dian").css("display", "none");
					$("#xianxiTask").css("border", "none");
					
					$('#s').css('left','0')
				});
			});
</script>
<!--  -->
<script type="text/javascript">
	//function remainedTime(value) {
	$(function() {
		var time_start = new Date().getTime();//设定开始时间 
		var endTime = $("#endTime").val();
		var endTime1 = endTime.replace("-", "/");
		var endTime2 = endTime1.replace("-", "/");

		var s = endTime2.substring(0, endTime2.length - 2);
		console.log(s);
		var time_end = new Date(s).getTime(); //设定结束时间(等于系统当前时间) 
		//计算时间差 
		var time_distance = time_end - time_start;
		if (time_distance > 0) {
			// 天时分秒换算 
			var int_day = Math.floor(time_distance / 86400000);
			time_distance -= int_day * 86400000;

			var int_hour = Math.floor(time_distance / 3600000);
			time_distance -= int_hour * 3600000;

			var int_minute = Math.floor(time_distance / 60000);
			time_distance -= int_minute * 60000;

			var int_second = Math.floor(time_distance / 1000);
			// 时分秒为单数时、前面加零 
			if (int_day < 10) {
				int_day = "0" + int_day;
			}
			if (int_hour < 10) {
				int_hour = "0" + int_hour;
			}
			if (int_minute < 10) {
				int_minute = "0" + int_minute;
			}
			if (int_second < 10) {
				int_second = "0" + int_second;
			}
			// 显示时间 
			$("#time_d").html("<font color='green'>" + int_day + "天</font>");
			$("#time_h").html("<font color='green'>" + int_hour + "时</font>");
			$("#time_m").html("<font color='green'>" + int_minute + "分</font>");
			$("#time_s").html("<font color='green'>" + int_second + "秒</font>");

			setTimeout("remainedTime()", 1000);

		} else {
			$("#time_d").html("<font color='red'>时");
			$("#time_h").html("<font color='red'>间");
			$("#time_m").html("<font color='red'>结");
			$("#time_s").html("<font color='red'>束");
		}
		/* $("#button" + value).css("display", "none");
		$("#span" + value).css("display", "block"); */

	});
	//}
	function remainedTime() {
		var time_start = new Date().getTime();//设定开始时间 
		var endTime = $("#endTime").val();
		var endTime1 = endTime.replace("-", "/");
		var endTime2 = endTime1.replace("-", "/");
		/* alert(endTime2.length); */

		var s = endTime2.substring(0, endTime2.length - 2);
		console.log(s);
		var time_end = new Date(s).getTime(); //设定结束时间(等于系统当前时间) 
		//计算时间差 
		var time_distance = time_end - time_start;
		if (time_distance > 0) {
			// 天时分秒换算 
			var int_day = Math.floor(time_distance / 86400000);
			time_distance -= int_day * 86400000;

			var int_hour = Math.floor(time_distance / 3600000);
			time_distance -= int_hour * 3600000;

			var int_minute = Math.floor(time_distance / 60000);
			time_distance -= int_minute * 60000;

			var int_second = Math.floor(time_distance / 1000);
			// 时分秒为单数时、前面加零 
			if (int_day < 10) {
				int_day = "0" + int_day;
			}
			if (int_hour < 10) {
				int_hour = "0" + int_hour;
			}
			if (int_minute < 10) {
				int_minute = "0" + int_minute;
			}
			if (int_second < 10) {
				int_second = "0" + int_second;
			}
			// 显示时间 
			$("#time_d").html("<font color='green'>" + int_day + "天</font>");
			$("#time_h").html("<font color='green'>" + int_hour + "时</font>");
			$("#time_m").html("<font color='green'>" + int_minute + "分</font>");
			$("#time_s")
					.html("<font color='green'>" + int_second + "秒 </font>");

			setTimeout("remainedTime()", 1000);

		} else {
			$("#time_d").html("<font color='red'>时");
			$("#time_h").html("<font color='red'>间");
			$("#time_m").html("<font color='red'>结");
			$("#time_s").html("<font color='red'>束");
		}
	}
</script>

<script type="text/javascript">
	function getfunction(){
	tiao();
	
	
	}
	function allfunction(){
	tiao();
	
	
	}
	function over(){
	tiao();
	
	
	}
	  function tiao(){
	  var adurl = $("#adurl").val();
				/* alert(123); */
	window.location.href ="YeMianAction_tiaozhuang?url=" + adurl;
	  }
	 
	
	

	function gettwofunction(value) {
		//shengcheng(value);
		/* 	alert(123); */
		shengcheng(value);
		tiaozhuan();
	}
	function shengcheng(value) {
		/* alert(1234); */
		$.ajax({
			type : "POST",
			url : "TaskAction_doAdvertisementTask",
			async : false,
			data : "taskId=" + value + "&openId=" + $("#openId").val(),
			success : function(data) {
				if (data.msg == 1) {
					$("#doTheTask").val("任务已进行");
					$("#doTheTask").attr("disabled", true);
				}else if(data.msg == 2){
				$("#doTheTask").val("不要重复接任务");
				$("#doTheTask").attr("disabled", true);
				}else if(data.msg == 3){
				$("#doTheTask").val("数量不足");
				$("#doTheTask").attr("disabled", true);
				}
			}
		});
	}
	function getfunction(){
		var adurl = $("#adurl").val();
// 				alert(123);
				window.location.href ="YeMianAction_tiaozhuang?url=" + adurl;
	}
	// 	加判断数量
	function tiaozhuan() {
		var renwusul = $("#renwusl").text();
			if (renwusul == 0) {
				layer.msg("此任务数量不足！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
				return false;
			}else {		
				var adurl = $("#adurl").val();
				/* alert(123); */
				window.location.href ="YeMianAction_tiaozhuang?url=" + adurl;
			}
		}
</script>
<script type="text/javascript">
 function shangchuan(){
		window.location.href ="TaskAction_orderDetails?userTaskId="+${UserTaskId};
	  }
</script>
<script type="text/javascript">
	$(function() {
		var taskStatus = $("#taskStatus").val();
		var taskRemainderNumber = $("#taskRemainderNumber").val();
		var time_start = new Date();//设定开始时间 
		var endTime = $("#endTime").val().replace(/-/g, "/").replace(/-/g, "/");
		endTime = endTime.substr(0, endTime.lastIndexOf('.'));
		var end = new Date(endTime);
		if (taskStatus == 5 && taskRemainderNumber != 0 && time_start < end) {
			$("#statusRemainder").html("<input type='button'  value='该任务已进行' onClick='getfunction()'>");
		}
		if (taskStatus == 2 && taskRemainderNumber != 0 && time_start < end) {
			$("#statusRemainder").html("<input type='button' value='进入店铺' onClick='getfunction()'>");
		}
		if (taskRemainderNumber == 0) {
			$("#statusRemainder").html("<input type='button' value='进入店铺' onClick='getfunction()'>");
		}
		if (time_start > end) {
			$("#statusRemainder").html("<input type='button' value='进入店铺' onClick='over()'>");
		}
		if (taskStatus == 2 && time_start < end) {
			$("#statusRemainder").html("<input type='button' value='进入店铺' onClick='allfunction()'>");
		}
		if (taskStatus == 2 && time_start > end) {
			$("#statusRemainder").html("<input type='button' value='进入店铺' onClick='over()'>");
		}
		if (${task.shengc} == 1 && time_start < end && taskStatus == 1) {
			$("#statusRemainder").html("<input type='button' value='上传截图' onClick='shangchuan()'>");
		}
	});
</script>
<script type="text/javascript">
	$("#lunbotuliebiao li:first").addClass("active");
	$("#lunbotudiv div:first").attr("class", "item active SY_LB_img");
</script>
<script type="text/javascript">
	var InterValObj; //timer变量，控制时间
	var count = 1; //间隔函数，1秒执行
	var curCount;//当前剩余秒数

	$(function() {
		curCount = count;
		var taskStatus = $("#taskStatus").val();
		if (taskStatus == null || taskStatus == ""&&${task.shengc!=1}) {
			$("#doTheTask").attr("disabled", "true");
			$("#doTheTask").val("请在" + curCount + "后做任务");
			InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次
		}else if (${task.shengc==1}) {
			$("#doTheTask").val("做任务");
		}
	});

	//timer处理函数
	function SetRemainTime() {
		if (curCount == 0||curCount=='') {
			window.clearInterval(InterValObj);//停止计时器
			$("#doTheTask").removeAttr("disabled");//启用按钮
			$("#doTheTask").val("做任务");
		} else {
			curCount--;
			$("#doTheTask").val("请在" + curCount + "后做任务");
		}
	}
</script>

</html>
