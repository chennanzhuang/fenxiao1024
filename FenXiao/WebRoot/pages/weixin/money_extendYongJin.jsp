<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>推广佣金</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/money_extendYongJin.css" />
<link rel="stylesheet"
	href="" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/foot.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font_size1.css" />
</head>
<body>
			<input type="hidden" name="Total" value="${Total }" id="Total">
			<input type="hidden" name="page" value="${page }" id="page">
			<input name="openId" value="${openId}" type="hidden" id="open">
	<div class="container-fluid MY_know_out">
		<div class="MY_mission_cont" id="tuigu">
			<c:forEach items="${tuiguanglist}" var="userTask">
				<div class="row MY_mission_cont_nav">
					<div class="col-xs-8 MY_mission_cont_nav_navs">
						<div class="MY_mission_cont_nav_navs_tittle">
							<a href="" class="font13">已完成《${userTask.taskTitle }》</a>
						</div>
						<p class="font12 MY_mission_cont_nav_navs_time">
							<span class="MY_mission_cont_nav_navs_time_name">关注任务</span> <span
								class="MY_mission_cont_nav_navs_time_data">${userTask.taskOrderTime }</span>
<!-- 							<span class="MY_mission_cont_nav_navs_time_author">${userTask.user.nickName }</span> -->
						</p>
					</div>
					<div class="col-xs-4 MY_mission_cont_nav_navs text-right">
						<span class="font18 MY_mission_cont_nav_navs_jiFen" >+ ${userTask.tuiguang}</span>
					</div>
				</div>
			</c:forEach>
		</div>
	<div id="dianji" style="position:absolute; font-size:14px; left:0; width:100%; height:40px; text-align:center; line-height:40px; color:#000; background:#fff;">点击加载更多</div>	
	</div>
		




	<!--======================尾部======================-->
	<div class="container-fluid all_foot">
<!-- 	<div id="dianji" style="position:absolute; font-size:14px; left:0; top:-50px; width:100%; height:40px; text-align:center; line-height:40px; color:#000; background:#fff;">点击加载更多</div> -->
<!-- 				<div id="dianji" style="position:absolute; font-size:14px; left:0; top:-50px; width:100%; height:40px; text-align:center; line-height:40px; color:#000; background:#fff;">点击加载更多</div> -->
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_true.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>


</body>
<script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
<script
	src="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
<script type="text/javascript">
var page = $("#page").val();
	page++;
	var Total = $("#Total").val();
	if(page>Total){
		$("#dianji").css("display", "none");
	}
</script>
<script type="text/javascript">
	$("#dianji")
			.click(
					function() {
						var page = $("#page").val();
						var opem = $("#open").val();
						var dainji = $("#dianji").html();
						if (dainji == "点击加载更多") {
							$("#dianji").html("正在加载...");
							var commcontent = "";
							page++;
							var Total = $("#Total").val();
							$.ajax({
										type : "GET",
										url : "jsonAction_queryMyTasktuiguang",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
										data : {
											"page" : page,
											"Total" : Total,
											"openId" : opem
										},
										success : function(result) {
										   
											var jsonArray = eval("(" + result + ")");
											if (jsonArray != null) {
											
												for (var i = 0; i < jsonArray.length; i++) {
												commcontent+="<div class='row MY_mission_cont_nav'>";
												commcontent+="<div class='col-xs-8 MY_mission_cont_nav_navs'>";
												commcontent+="<div class='MY_mission_cont_nav_navs_tittle'>";
												commcontent+="<a href='' class='font13'>已完成《"+jsonArray[i].Title+"》</a>";
												commcontent+="</div>";
												commcontent+="<p class='font12 MY_mission_cont_nav_navs_time'>";
												commcontent+="<span class='MY_mission_cont_nav_navs_time_name'>关注任务</span>";
												commcontent+="<span class='MY_mission_cont_nav_navs_time_data'>"+jsonArray[i].OrderTime+"</span>";
// 												if(jsonArray[i].nickName!=null){
// 												commcontent+="<span class='MY_mission_cont_nav_navs_time_author'>"+jsonArray[i].nickName+"</span>";									
// 												}else {
// 													commcontent+="<span class='MY_mission_cont_nav_navs_time_author'></span>";	
// 												}
												commcontent+="</p>";
												commcontent+="</div>";
												commcontent+="<div class='col-xs-4 MY_mission_cont_nav_navs text-right'>";
												if(jsonArray[i].Type!=null){									
												commcontent+="<span class='font18 MY_mission_cont_nav_navs_jiFen' >+ "+jsonArray[i].Type+"</span>";
												}else {
													commcontent+="<span class='font18 MY_mission_cont_nav_navs_jiFen' >+0.00</span>";
												}
												commcontent+="</div>";
												commcontent+="</div>";
												}
												$("#tuigu").append(commcontent);
												$("#page").val(page);
												if(page<Total){											
												$("#dianji").html("点击加载更多");
												}else {
												$("#dianji").css("display", "none");
												}
											} else {
												$("#dianji").attr({
													"disabled" : "disabled"
												});
												$("#dianji").css("display", "none");
											}
										}
									});
						}

					});
			</script>
</html>
