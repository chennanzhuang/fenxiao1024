<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>任务订单详情</title>
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/orderdetails.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/foot.css"
	rel="stylesheet">

</head>
<body>

	<!--头部-->
	<%-- <header class="box-header">
        <div class="box-header-div1">
            <div class="box-header-div1_div1"><img src="${pageContext.request.contextPath}/img/a-1right.png"></div>
            <div class="box-header-div1-div2"><a href="#"><strong>返回</strong></a></div>
        </div>
        <div class="box-header-div2"><strong> 任务订单详情 </strong></div>
        <div class="box-header-div3"><a href="#"><img src="${pageContext.request.contextPath}/img/threeDian.png" ></a></div>
    </header> --%>


	<div class="All">
		<div class="content container">
			<!-- ===================================================== -->
			<div class="content_one">
				<div class="content_one_top">
					<label>订单编号${task.taskOrderCode}</label> <a href="#"> <c:if
							test="${task.taskStatus==2 }">任务已完成</c:if> <c:if
							test="${task.taskStatus==3 }">任务审核失败</c:if>
					</a>
				</div>
				<div class="heti">
					<div class="content_one_center">
						<img src="${pageContext.request.contextPath}/${task.taskLogo}"
							class="img-responsive" alt="hhhhh">
					</div>
					<div class="content_one_bottom">
						<a href="#" class="content_one_bottom_font1">${task.taskTitle}</a>
						<br>
						<div class="content_one_bottom_texta">
							<a href="#" class="content_one_bottom_font2">${task.taskBriefing }</a>
						</div>
					</div>
				</div>
				<div class="content_one_foot">
				<c:if test="${task.taskType==1 }">
					<a href="#" class="download_one">关注公众号</a> <label class="money_one">￥${task.taskBonus }</label>
				</c:if>
				<c:if test="${task.taskType==2 }">
					<a href="#" class="download_one">广告宣传</a> <label class="money_one">￥${task.taskBonus }</label>
				</c:if>
				<c:if test="${task.taskType==3 }">
					<a href="#" class="download_one">app下载</a> <label class="money_one">￥${task.taskBonus }</label>
				</c:if>
				</div>

				<hr class="fgx container-fluid">
				<!-- ===================================================== -->

				<div class="time">
					<!-- 下单时间 -->
					<div class="order_time_div">
						<p class="order_time_p">下单时间:${task.taskOrderTime }</p>
					</div>
					<!-- 完成时间 -->
					<div class="complete_time_div">
						<p class="complete_time_p">完成时间:${task.taskCompletionTime }</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--======================尾部======================-->
	<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>
</body>
</html>
