<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>注册</title>
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet">
<!--<link href="css/login.css" rel="stylesheet" >-->
<link href="${pageContext.request.contextPath}/css/register.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/foot.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/daohan.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-1.7.2.min.js"></script>
<script src="${pageContext.request.contextPath}/jsp/layer/layer.js"
	type="text/javascript"></script>
<script charset="utf-8"
	src="${pageContext.request.contextPath}/js/text.js"></script>
<script type="text/javascript"
	src="http://res.wx.qq.com/open/js/jweixin-1.1.0.js"></script>
<style>

select {
	border: none;
	appearance: none;
	-moz-appearance: none;
	-webkit-appearance: none;
	margin: 0;
	padding: 0;
	outline: none;
	list-style: none;
	background: none;
	text-decoration: none;
	border: 0;
	font-weight: normal;
	font-family: "Microsoft YaHei";
	background: url("${pageContext.request.contextPath}/img/bottom-img.png")
		no-repeat scroll right center transparent;
	background-size: 14px 14px;
	-moz-background-size: 14px 14px;
	-webkit-background-size: 14px 14px;
	padding-right: 14px;
}

select {
	font-family: inherit;
	font-size: inherit;
	line-height: inherit;
}

select {
	text-transform: none;
	width: 25%
}

.se {
	height: 32px;
	color: #a9a9a9;
	outline: medium;
	margin-left: 2%;
}
</style>
</head>

<body>
	<div class="All">
		<form action="UserAction_register" method="post" class="form">
			<div class="register container-fluid">
				<!-- 推荐人 -->
				<div class="referee mg">
					<label class="label">头像：</label>
					<div id="preview1">
	      	<c:if test="${admin.adminPhoto==null}">
	      	<img alt="" src="jsp/img/111.png" name="" with="100" height="100">
	      	</c:if>
	      	<c:if test="${admin.adminPhoto!=null}">
	      	<img alt="" src="${admin.adminPhoto}" name="admin.adminPhoto" with="100" height="100">
	      	</c:if>
	      	
	      	</div>
	      	<input type="file" name="fileTest" onchange="preview1(this)" /></td>
				</div>
             <script>  
       function preview1(file)  {
               var prevDiv = document.getElementById('preview1');
               if (file.files && file.files[0]){
                       var reader = new FileReader();
                         reader.onload = function(evt){
                        prevDiv.innerHTML = '<img src="' + evt.target.result + '" with="100" height="100"/>';
                             }  
                       reader.readAsDataURL(file.files[0]);
                         }else{
                  prevDiv.innerHTML = '<div class="img1" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src=\'' + file.value + '\')" ></div>';
    }
    }
 </script>	      
				<!-- 昵称 -->
				<hr class="fgx nickname_hr">
				<div class="nickname mg">
					<img src="${pageContext.request.contextPath}/img/mine_f.png"
						class="nickname_icon"> <input type="text"
						placeholder="请输入昵称" name="userName" id="username" value="${user.username }">
				</div>
				<!-- 密码 -->
				<hr class="fgx">
				<input name="Fruit" type="radio" value="1" />男
				
				<input name="Fruit" type="radio" value="2" />女
				
			</div>
			<div class="retister_enter container-fluid">
				<input class="registerenter_btn btn" type="button" id="submit111"
					value="注册">
			</div>
	</form>
	</div>
	<!--======================尾部======================-->

	<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>

</body>
<script type="text/javascript">
	
</script>
</html>
