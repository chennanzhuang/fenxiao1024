<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>提现须知</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/money_postalKnow.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/foot.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font_size1.css"/>

</head>
<body>

<div class="container-fluid MY_know_out">
    <div class="MY_know_out_inner">
        <div class="MY_know_out_in_one">
            <div class="MY_know_out_in_one_tit clearfix">
                <span class="MY_know_out_in_one_tit_imgs"><img src="${pageContext.request.contextPath}/img/moneyTXxuzhi/my_tx_xz_notes.png"></span>
                <span class="font16">提现说明</span>
            </div>
            <ul class="font13 MY_know_out_in_one_navs">
                <li>最低提现金额50元</li>
                <li>单笔提现最高金额5000元</li>
                <li>每日累计提现最高20000元</li>
            </ul>
        </div>
        <div class="MY_know_out_in_two">
            <div class="MY_know_out_in_one_tit ">
                 <span class="MY_know_out_in_one_tit_imgs"><img src="${pageContext.request.contextPath}/img/moneyTXxuzhi/my_tx_xz_time.png"></span>
                <span class="font16">提现周期</span>
            </div>
            <ul class="font13 MY_know_out_in_one_navs">
                <li>周一至周五0:00到24:00提现周期为</li>
                <li>T+3（T为当天）；如遇节假日顺延</li>
            </ul>
        </div>
        <div class="MY_know_out_in_three">
            <div class="MY_know_out_in_one_tit">
                 <span class="MY_know_out_in_one_tit_imgs"><img src="${pageContext.request.contextPath}/img/moneyTXxuzhi/my_tx_xz_shouxufei.png"></span>
                <span class="font16">提现手续费</span>
            </div>
            <ul class="font13 MY_know_out_in_one_navs">
                <li>单笔最低2元，最高25元</li>
                <li>超过2元且低于25元的区间，按0.6%计算</li>
            </ul>
        </div>
    </div>

</div>



<!--======================尾部======================-->
<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_true.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>

</body>
<script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
<script src="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
</html>
