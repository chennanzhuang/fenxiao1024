<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title></title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/foot.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/updatePassword.css" />
		<script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/jsp/layer/layer.js" type="text/javascript"></script>
	</head>
	<body style="background-color:#F0F0F0;">
		<form action="UserAction_resetPasswpord1" class="form">
		<div class="input2">
			<ul>
				<li>原密码<input type="password" placeholder="输入原密码" id="oldPresentPass"></li>
				<input type="hidden" value="${user.presentPass }" id="oldPresentPass1">
				<input type="hidden" value="${user. id}" name="userId" id="userId">
				<li class="input-li">交易密码<input type="password" placeholder="输入新密码" id="newPresentPass" name="password"></li>

				<li class="input-li">重复密码<input type="password" placeholder="重复密码" id="repeatPresentPass"></li>
			</ul>
		</div>
		<div class="input4">
			<ul>
				<li>手机号码<input type="text" placeholder="手机号码" class="input4-li three" value="${lastmodile }" readonly="readonly"></li>
				<li>验证码<input type="text" placeholder="手机验证码" class="input4-li botton" id="smsCheckCode"><input type="button" style="margin-left:-5%;background-color:#12B7F5;height:100%;font-size:10px;width:30%;" value="获取验证码" onClick="getSMSCheckCode(${user.id },this),sendMessage()" id="btnSendCode" class="input-botton"></li>
			<input type="hidden" id="backstage" value="">
			<input type="hidden" id="mobile" value="${modil }">
			</ul>
		</div>
		<div class="queren">
			<input type="button" id="submit111" value="确认修改">
		</div>
		</form>
		 <!--======================尾部======================-->
			<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>
		<script type="text/javascript">
		$(function(){
			$("#oldPresentPass").blur(function(){
			
				var oldPresentPass=$("#oldPresentPass").val();
				var userId=$("#userId").val();
				if ($.trim(oldPresentPass) == "") {
				
					layer.msg("原交易密码不能为空！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
				}
				$.ajax({ 
				 type: "POST",
	             url: "BankcardAction_checkOwnMobile1",
	             data: {
	             "password":oldPresentPass,
	             "userId":userId
	             },
	             dataType: "json",
				success: function(data){
				if(data.code==0){
				layer.msg("原交易密码不正确！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
				}
		      }
		      });
			});
			$("#newPresentPass").blur(function(){
				var newPresentPass=$("#newPresentPass").val();
				var repeatPresentPass=$("#repeatPresentPass").val();
				if ($.trim(newPresentPass) == "") {
					layer.msg("新密码不能为空！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
				}
				if (newPresentPass!=repeatPresentPass&&repeatPresentPass!="") {
					layer.msg("两次输入的密码不一致！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
				}
			});
			$("#repeatPresentPass").blur(function(){
				var newPresentPass=$("#newPresentPass").val();
				var repeatPresentPass=$("#repeatPresentPass").val();
				if ($.trim(repeatPresentPass) == "") {
					layer.msg("重复密码不能为空！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
				}
				if (newPresentPass!=repeatPresentPass&&repeatPresentPass!="") {
					layer.msg("两次输入的密码不一致！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
				}
			});
			$("#mobile").blur(function(){
				var mobile=$("#mobile").val();
				if ($.trim(mobile) == "") {
					layer.msg(" 手机号码不能为空！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
				}
				var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
				if (!myreg.test($("#mobile").val())) {
					layer.msg("手机号不能为空！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
				}
			});
     $("#smsCheckCode").blur(function(){
    	var smsCheckCode=$("#smsCheckCode").val();
    	var backstage = $("#backstage").val();
    	if ($.trim(smsCheckCode) == "") {
			layer.msg(" 验证码不能为空！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
		}
		
		if (smsCheckCode != backstage && backstage != "") {
			layer.msg("验证码不正确！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
		}
    	 
     });		
		});
		function getSMSCheckCode(value, object) {
			var mobile = $("#mobile").val();
			if ($.trim(mobile) == "") {
				layer.msg("手机号不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
				return false;
			};

			var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
			if (!myreg.test($("#mobile").val())) {
				layer.msg("请输入有效手机号！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
				return false;
			}
			$.ajax({
				type : "GET",
				url : "UserAction_getSMSCheckCode",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
				data : {
					"mobile" : mobile,
					"userId" : value
				},
				//dataType:"json",
				success : function(data) {
					if (data.codeT == 0) {
						alert(data.msg);
					} else {
						$("#backstage").val(data.code);
						$(object).val("验证码已发送");
					}
				}
			});

		}
		
		</script>
		<script type="text/javascript">
		$(function(){
			$("#submit111").click(function(){
				var oldPresentPass=$("#oldPresentPass").val();
				var userId=$("#userId").val();
				if ($.trim(oldPresentPass) == "") {
					layer.msg("原交易密码不能为空！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
					return false;
				}
			
				var newPresentPass=$("#newPresentPass").val();
				if ($.trim(newPresentPass) == "") {
					layer.msg("新密码不能为空！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
					return false;
				}
				var repeatPresentPass=$("#repeatPresentPass").val();
				if ($.trim(repeatPresentPass) == "") {
					layer.msg("重复密码不能为空！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
					return false;
				}
				var mobile=$("#mobile").val();
				if ($.trim(mobile) == "") {
					layer.msg(" 手机号码不能为空！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
					return false;
				}
				var smsCheckCode=$("#smsCheckCode").val();
		    	var backstage = $("#backstage").val();
		    	if ($.trim(smsCheckCode) == "") {
					layer.msg(" 验证码不能为空！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
					return false;
				}
		    	if ($.trim(backstage) == "") {
					layer.msg("请先获取验证码！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
					return false;
				}
				
				if (smsCheckCode != backstage && backstage != "") {
					layer.msg("验证码不正确！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
					return false;
				}
	$.ajax({ 
				 type: "POST",
	             url: "BankcardAction_checkOwnMobile1",
	             data: {
	             "password":oldPresentPass,
	             "userId":userId
	             },
	             dataType: "json",
				success: function(data){
				if(data.code==0){
				layer.msg("原交易密码不正确！", {
						time : 3000,
						offset : 0,
						shift : 6
					});
				return false;
				}else {
					$(".form").submit();
				}
		      }
		      });
				
			});
			
			
		});
		
		</script>
			<script type="text/javascript">

var InterValObj; //timer变量，控制时间
var count = 60; //间隔函数，1秒执行
var curCount;//当前剩余秒数

function sendMessage() {
     curCount = count;
     var mobile = $("#mobile").val();
		if ($.trim(mobile) == "") {
			layer.msg("手机号不能为空！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}
		
		var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
		if (!myreg.test($("#mobile").val())) {
			layer.msg("请输入有效手机号！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}
     $("#btnSendCode").attr("disabled", "true");
     $("#btnSendCode").val("请在" + curCount + "内输入验证码");
     InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次
    
}

//timer处理函数
function SetRemainTime() {
            if (curCount == 0) {                
                window.clearInterval(InterValObj);//停止计时器
                $("#btnSendCode").removeAttr("disabled");//启用按钮
                $("#btnSendCode").val("重新发送验证码");
            }
            else {
                curCount--;
                $("#btnSendCode").val("请在" + curCount + "内输入验证码");
            }
        }
</script>
	</body>
</html>
