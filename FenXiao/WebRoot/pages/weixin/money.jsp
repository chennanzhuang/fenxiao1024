<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>钱包</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/money.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/foot.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font_size.css" />

</head>
<body>

	<div class="container-fluid idx_out">
		<!--==================头部==================-->
		<div class="idx_head">
			<div class="idx_head_top">
				<div class="idx_head_top_tit">
					<p class="text-center idx_head_top_tit_font clearfix">
						<span class="idx_head_top_tit_font_img"> <img
							class="idx_head_top_tit_font_img"
							src="${pageContext.request.contextPath}/img/moneyIMG/m_tt.png">
							<span class="font24">可用余额&nbsp;(元)</span> <img
							class="idx_head_top_tit_font_img"
							src="${pageContext.request.contextPath}/img/moneyIMG/m_tt.png">
						</span>
						<!-- <a  class="idx_head_top_tit_font_xq font26">查看详情</a> -->
					</p>
				</div>
				<div class="text-center idx_head_top_num font84">
					<span id="keyong"> 
					<c:if test="${user.balance!=null }">      
                    ${user.balance }
         </c:if> <c:if test="${user.balance==null }">      
                   0.00
                 </c:if>
					</span>
				</div>
				 <div class="idx_head_top_nan text-center font24">
                <span class="">冻结金额&nbsp;(元)</span>
                <span class="idx_head_top_nan_num" id="xiaoshu">
                <c:if test="${user.freezingAmount==null }">      
                   0.00
                 </c:if>
                 <c:if test="${user.freezingAmount!=null }">      
                  ${user.freezingAmount}
                 </c:if>
                
                </span>
            </div>
			</div>

			<div class="idx_head_bt">
				<a
					href="${pageContext.request.contextPath}/TaskAction_queryBankcardOfUser?userId=${user.id}"
					class="help-block text-center idx_head_bt_block"> <img
					src="${pageContext.request.contextPath}/img/moneyIMG/m_tx.png"
					class="idx_head_bt_block_icon"> <span
					class="idx_head_bt_block_tit font30">提现</span>
				</a>
			</div>
		</div>

		<!--====================中心=================-->
		<div class="idx_cont">
			<div class="row idx_cont_nav">
				<div class="col-xs-4 idx_cont_nav_navs">
					<a class="text-center help-block idx_cont_nav_navs_block"
						href="${pageContext.request.contextPath}/UserTaskAction_queryThoseTaskCompleted?userId=${user.id}">
						<div class="idx_cont_nav_navs_block_img">
							<img
								src="${pageContext.request.contextPath}/img/moneyIMG/m_yj.png">
						</div>
						<p class="idx_cont_nav_navs_block_tit font26">任务佣金</p>
					</a>
				</div>
				<div class="col-xs-4 idx_cont_nav_navs">
					<a class="text-center help-block idx_cont_nav_navs_block"
						href="${pageContext.request.contextPath}/UserTaskAction_queryTeamTaskForTheMan2?userId=${user.id}">
						<div class="idx_cont_nav_navs_block_img">
							<img
								src="${pageContext.request.contextPath}/img/moneyIMG/m_tgyj.png">
						</div>
						<p class="idx_cont_nav_navs_block_tit font26">推广佣金</p>
					</a>
				</div>
				<div class="col-xs-4 idx_cont_nav_navs">
					<a class="text-center help-block idx_cont_nav_navs_block"
						href="${pageContext.request.contextPath}/UserTaskAction_queryTeamTaskForTheMan?userId=${user.id}">
						<div class="idx_cont_nav_navs_block_img">
							<img
								src="${pageContext.request.contextPath}/img/moneyIMG/m_jj.png">
						</div>
						<p class="idx_cont_nav_navs_block_tit font26">团队奖金</p>
					</a>
				</div>
				<div class="col-xs-4 idx_cont_nav_navs">
					<a class="text-center help-block idx_cont_nav_navs_block"
						href="${pageContext.request.contextPath}/pages/weixin/money_postalKnow.jsp">
						<div class="idx_cont_nav_navs_block_img">
							<img
								src="${pageContext.request.contextPath}/img/moneyIMG/m_gz.png">
						</div>
						<p class="idx_cont_nav_navs_block_tit font26">提现规则</p>
					</a>
				</div>
				<div class="col-xs-4 idx_cont_nav_navs">
					<a class="text-center help-block idx_cont_nav_navs_block"
						href="${pageContext.request.contextPath}/PresentAction_queryPresentList?userId=${user.id}">
						<div class="idx_cont_nav_navs_block_img">
							<img
								src="${pageContext.request.contextPath}/img/moneyIMG/m_zd.png">
						</div>
						<p class="idx_cont_nav_navs_block_tit font26">提现账单</p>
					</a>
				</div>
				<div class="col-xs-4 idx_cont_nav_navs">
					<a class="text-center help-block idx_cont_nav_navs_block"
						href="${pageContext.request.contextPath}/UserAction_moneyToCashSetUp?userId=${user.id}">
						<div class="idx_cont_nav_navs_block_img">
							<img
								src="${pageContext.request.contextPath}/img/moneyIMG/m_set.png">
						</div>
						<p class="idx_cont_nav_navs_block_tit font26">提现设置</p>
					</a>
				</div>
				<div class="col-xs-4 idx_cont_nav_navs idx_cont_nav_a_noneBt">
					<a class="text-center help-block idx_cont_nav_navs_block"
						href="${pageContext.request.contextPath}/UserAction_queryBankCardOfTheUser?userId=${user.id}">
						<div class="idx_cont_nav_navs_block_img">
							<img
								src="${pageContext.request.contextPath}/img/moneyIMG/m_password.png">
						</div>
						<p class="idx_cont_nav_navs_block_tit font26">提现密码</p>
					</a>
				</div>
				<div
					class="col-xs-4 idx_cont_nav_navs idx_cont_nav_a_noneBt idx_cont_nav_hidden">
					<a class="text-center help-block idx_cont_nav_navs_block" href="#">
						<div class="idx_cont_nav_navs_block_img"></div>
						<p class="idx_cont_nav_navs_block_tit font26">任</p>
					</a>
				</div>
			</div>
		</div>


	</div>

	<!--======================尾部======================-->
	<div class="container-fluid all_foot">
		<div class="row all_foot_nav">
			<%-- <div class="col-xs-3 all_foot_nav_navs">
				<a
					class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
					href="${pageContext.request.contextPath}/UserAction_homepage?openId=${openId}">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/index_true.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">首页</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block"
					href="${pageContext.request.contextPath}/UserTaskAction_queryTaskForTheMan?openId=${openId}">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">任务</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block "
					href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser?openId=${openId}">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">钱包</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block "
					href="${pageContext.request.contextPath}/UserAction_myPersonal?openId=${openId}">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">我的</p>
				</a>
			</div> --%>

			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block "
					href="${pageContext.request.contextPath}/UserAction_homepage">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font22">首页</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block "
					href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font22">任务</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a
					class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true "
					href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser?openId=${openId}">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/money_true.png">
					</div>
					<p class="all_foot_nav_navs_block_title font22">钱包</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block "
					href="${pageContext.request.contextPath}/UserAction_myPersonal">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font22">我的</p>
				</a>
			</div>
		</div>
	</div>



</body>
<script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
<script
	src="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		$(function(){
		var xiaoshudian=$("#keyong").html();
		 var s = xiaoshudian + "";
  		var str = s.substring(0,s.indexOf(".") + 3);
    	$("#keyong").html(str);
// 			
// 			 var a = "23.456322";
//     		var aNew;
//    			var re = /([0-9]+\.[0-9]{2})[0-9]*/;
//    			aNew = xiaoshudian.replace(re,"$2");
// 			alert(aNew)
// 		
		});
	</script>
</html>
