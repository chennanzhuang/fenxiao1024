<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<title>任务订单详情</title>
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/orderdetails2.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/foot.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/mypayCss.css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-1.7.2.min.js"></script>
<script src="${pageContext.request.contextPath}/js/uplo.js"></script>
<script src="${pageContext.request.contextPath}/jsp/layer/layer.js"
	type="text/javascript"></script>
</head>
<body>

	<!--头部-->
	<%-- <header class="box-header">
    <div class="box-header-div1">
        <div class="box-header-div1_div1"><img src="${pageContext.request.contextPath}/img/a-1right.png"></div>
        <div class="box-header-div1-div2"><a href="#"><strong>返回</strong></a></div>
    </div>
    <div class="box-header-div2"><strong> 任务订单详情 </strong></div>
    <div class="box-header-div3"><a href="#"><img src="${pageContext.request.contextPath}/img/threeDian.png" ></a></div>
</header> --%>


	<div class="All">
		<div class="content container-fluid">
			<!-- ===================================================== -->
			<div class="content_one">
				<div class="content_one_top">
					<label>订单编号1232435435453</label> <a href="#" id="taskStatys">
				<c:if test="${task.taskStatus==2}">任务已完成</c:if>
				<c:if test="${task.taskStatus!=2}">任务进行中</c:if>
					</a>
				</div>
				<div class="heti">
					<div class="content_one_center">
						<img src="${pageContext.request.contextPath}/${task.taskLogo}"
							class="img-responsive" alt="hhhhh">
					</div>
					<div class="content_one_bottom">
						<a href="#" class="content_one_bottom_font1">${task.taskTitle}</a>
						<br>
						<div class="content_one_bottom_texta">
							<a href="#" class="content_one_bottom_font2">${task.taskBriefing}</a>
						</div>
					</div>
				</div>
				<div class="content_one_foot">
					<a href="#" class="download_one">
					<c:if test="${task.taskType==3}">下载APP</c:if>
					<c:if test="${task.taskType==2}">广告宣传</c:if>
					
					</a> <label class="money_one">￥${task.taskBonus}</label>
				</div>

				<hr class="fgx container-fluid">
				<!-- ===================================================== -->

				<div class="time">
					<!-- 下单时间 -->
					<div class="order_time_div">
						<p class="order_time_p">下单时间:${task.taskOrderTime }</p>
					</div>
				</div>
				<hr class="fgx container-fluid">
				<!-- ===================================================== -->
					<div class="admin">
						<div class="admin_user">
						<c:if test="${task.taskType==2}">
							<label> 用户名 </label> <input type="text" placeholder="APP注册用户名" onBlur="appRegisterName(${task.userTaskId })" value="" id="appRegisterName" name="appRegisterName">
						</c:if>
						</div>
						<input type="hidden" value="${task.userTaskId}" name="taskId" id="taskId">
						<input type="hidden" value="${openId}" name="openId" id="openId">
						<input type="hidden" value="${task.uploadTime}" name="uploadTime" id="uploadTime">
						<div class="admin_pic" id="admin_pic">
							
							<div id="fileList" class="admin_pic_list" align="center"></div>

							<a class="file"> <input id="Button1" type="button"
								value="选择文件" />
							</a>
							<div class="centent" style="margin-bottom:70px;">
								<div class="queren">
									<input type="button" id="files" value="继续完成任务">
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>

	<!--======================尾部======================-->
	<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>
</body>
<script type="text/javascript">  

      /*   $(function () {  */ 
            //var btn = $("#Button1"); 
            /* $("#appRegisterName").blur(function(){
            	$.ajax({
        			type:"POST",
        			url : "TaskAction_doWeixinTask",
        			data:"taskId="+value+"&openId="+$("#openId").val(),
        			success : function(data) {
        				if(data.msg==1){
        					$("#doTheTask").val("任务已进行");
        					$("#doTheTask").attr("disabled", true);
        				}
        			}
        		});
            }); */
            function appRegisterName(value){
            	var name=$("#appRegisterName").val();
            	if(name.trim()==""){
            		alert("用户名不能为空");
            	}else{
            	
            	$.ajax({
        			type:"POST",
        			url : "TaskAction_appRegisterName",
        			data:"appRegisterName="+name+"&userTaskId="+value,
        			success : function(data) {
        				if(data.msg==1){
        					/* $("#doTheTask").val("任务已进行");
        					$("#doTheTask").attr("disabled", true); */
        					/* alert(231); */
        				}
        			}
        		});
            }
            }
            
            	var btn = $("#Button1").uploadFile({  
                	url:"${pageContext.request.contextPath}/servlet/Upload?taskId="+$("#taskId").val()+"&openId="+$("#openId").val(),  
                    fileSuffixs: ["jpg", "png", "gif", "jpeg"],  
                    maximumFilesUpload: 4,//最大文件上传数  
                    multi : false,
                    onComplete: function (msg) {  
                        $("#testdiv").append(msg + "<br/>");  
                    },  
                    onAllComplete: function () {  
                        layer.msg("全部上传完成！", {
             				time : 3000,
             				offset : 0,
             				shift : 6
             			});
                    },  
                    isGetFileSize: true,//是否获取上传文件大小，设置此项为true时，将在onChosen回调中返回文件fileSize和获取大小时的错误提示文本errorText  
      
                    perviewElementId: "fileList", //设置预览图片的元素id  
                    perviewImgStyle: { width: '80px', height: '58px', border: '1px solid #ebebeb' }//设置预览图片的样式  
                });  
      
                var upload = btn.data("uploadFileData");  
               
                $("#files").click(function () {  
                	 var uploadTime=$("#uploadTime").val();
                	 var taskId=$("#taskId").val();
                	 var openId=$("#openId").val();
                	 if(parseInt(uploadTime)>=1){
                		 layer.msg("任务正在审核中请不要重复提交", {
             				time : 3000,
             				offset : 0,
             				shift : 6
             			});
             			
             			return false;
                	 }
//                 	 if(${task.uploadTime}>1){
//              			alert(${task.uploadTime});
//              				layer.msg("请不要重复提交任务！", {
//              				time : 3000,
//              				offset : 0,
//              				shift : 6
//              			});
//              			return false;
//              			}
//                 	 var uploadTime=$("#uploadTime").val();
//                 	 var taskId=$("#taskId").val();
//                 	 var openId=$("#openId").val();
//                 	 if(parseInt(uploadTime)>=2){
//                 		 layer.msg("请不要重复提交任务！", {
//              				time : 3000,
//              				offset : 0,
//              				shift : 6
//              			});
//              			return false;
//                 	 }
                	 
                	 $.ajax({
             			type:"POST",
             			url : "UserTaskAction_uploadTime",
             			data:"taskId="+taskId+"&openId="+openId,
             			success : function(data) {
             				if(data.msg==1){
             					$("#uploadTime").val(parseInt(uploadTime)+1);
             				}
             			}
             		});
                	 
                    upload.submitUpload();  
                });  
            	
          /*   }); */
             
    </script>

</html>
