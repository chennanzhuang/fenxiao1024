<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>个人中心</title>
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/personal.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/foot.css"
	rel="stylesheet">
	 <script type="text/javascript" src="js/jquery-2.0.3.min.js"></script>
	<script type="text/javascript">
	$(function(){
		
		
		$("#fa").click(function(){
			var userid=$("#userId").val();
			window.location.href="TaskAction_releaseTaskPage?user.id="+userid;
		});
		$("#mytask").click(function(){
			var userid=$("#userId").val();
			window.location.href="TaskAction_queryTaskList?user.id="+userid;
		});
	});
	</script>
</head>
<body>

	<!--====================== 内容 ======================-->
	<div class="All container-fluid">
	<input type="hidden" name="user.id" id="userId" value="${user.id}">
		<div class="All_head">
			<div class="All_head_top">
				<div class="All_head_top_pic">
					<a href="${pageContext.request.contextPath}/UserAction_personalData?userId=${user.id}"><img src="${user.userPhoto }"></a>
				</div>
				<div class="All_head_top_pic_nickname">
					<p>${user.username }</p>
					<span> 会员级别 </span> <span class="pthy"> 
					<c:if  test="${user.userLevel==1}" >普通会员</c:if>
					<c:if  test="${user.userLevel==2}" >VIP会员</c:if>
					<c:if  test="${user.userLevel==3}" >员工</c:if>
					 </span>
				</div>
			</div>
			<!-- 昵称 -->
			<div class="All_head_bot">
				<div class="All_head_bot_left">
					<div class="All_head_bot_left_pic">
						<img src="${pageContext.request.contextPath}/img/idx_renWu.png">
					</div>
					<div class="All_head_bot_left_text">
						<span style="color:#333333; font-size:1.2rem"> 任务完成 </span><br> <span
							style="color:#333333;font-size:1.2rem">
							 <c:if test="${user.taskNum==null}">0</c:if>
							 <c:if test="${user.taskNum!=null}">${user.taskNum}</c:if>
							</span>
					</div>
				</div>
				<div class="All_head_bot_right">
					<div class="All_head_bot_right_pic">
						<img src="${pageContext.request.contextPath}/img/idx_ewm.png">
					</div>
					<div class="All_head_bot_right_text">
						<span><a
							href="${pageContext.request.contextPath}/pages/weixin/share_code.jsp?nickName=${user.nickName}&headImage=${user.userPhoto}&qrCode=${user.userQr}&city=${user.city}&area=${user.area}"
						 style="color:#333333;font-size:1.2rem"> 二维码 </a></span>
					</div>
				</div>
			</div>
		</div>

		<div class="All_center">
			<div class="All_center_myteam">
				<div class="All_center_myteam_pic">
					<img
						src="${pageContext.request.contextPath}/img/idx_my_tuanDui.png">
				</div>
				<div class="All_center_myteam_text">
					<a href="${pageContext.request.contextPath}/UserAction_myTeam?openId=${openId}"><div class="All_center_myteam_text_son">我的团队</div></a>
				</div>
				<div class="All_center_myteam_rimg">
					<img src="${pageContext.request.contextPath}/img/content_rt.png">
				</div>
			</div>
		</div>

		<!-- 中间 -->
		<div class="All_centertwo">
			<!-- ===== 我的钱包 ====== -->
			<div class="All_center_mywallet">
				<div class="All_center_myteam_pic">
					<img src="${pageContext.request.contextPath}/img/idx_my_money.png">
				</div>
				<div class="All_center_myteam_text">
<a href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser?openId=${openId}"><div class="All_center_myteam_text_son">我的钱包</div></a>
				</div>
				<div class="All_center_myteam_rimg">
					<img src="${pageContext.request.contextPath}/img/content_rt.png">
				</div>
			</div>
		</div>
		<hr class="fgx">
		<!-- ===== 我的任务 ====== -->
		<div class="All_center_mytask">
			<div class="All_center_myteam_pic">
				<img src="${pageContext.request.contextPath}/img/idx_my_renWu.png">
			</div>
			<div class="All_center_myteam_text">
				<a href="${pageContext.request.contextPath}/TaskAction_myTask?openId=${openId}"><div class="All_center_myteam_text_son">我的任务</div></a>
			</div>
			<div class="All_center_myteam_rimg">
				<img src="${pageContext.request.contextPath}/img/content_rt.png">
			</div>
		</div>
		<hr class="fgx">
		<!-- ===== 提现设置 ====== -->
		<div class="All_center_withdrawdeposit">
			<div class="All_center_myteam_pic">
				<img src="${pageContext.request.contextPath}/img/idx_my_set.png">
			</div>
			<div class="All_center_myteam_text">
				<a href="${pageContext.request.contextPath}/UserAction_moneyToCashSetUp?userId=${user.id}"><div class="All_center_myteam_text_son">提现设置</div></a>
			</div>
			<div class="All_center_myteam_rimg">
				<img src="${pageContext.request.contextPath}/img/content_rt.png">
			</div>
		</div>
		<!-- ===== 帮助中心 ====== -->
		<div class="All_center_helpcenter">
			<div class="All_center_myteam_pic">
				<img src="${pageContext.request.contextPath}/img/idx_my_help.png">
			</div>
			<div class="All_center_myteam_text">
				<a href="${pageContext.request.contextPath}/pages/weixin/helpCenter.jsp"><div class="All_center_myteam_text_son">帮助中心</div></a>
			</div>
			<div class="All_center_myteam_rimg">
				<img src="${pageContext.request.contextPath}/img/content_rt.png">
			</div>
		</div>
		<hr class="fgx">
		<!-- ===== 发布需求 ====== -->
		<div class="All_center_demand">
			<div class="All_center_myteam_pic">
				<img src="${pageContext.request.contextPath}/img/idx_my_xq.png">
			</div>
			<div class="All_center_myteam_text">
				<div
					class="All_center_myteam_text_son" id="fa" style="cursor: pointer;">
					发布需求</div>
			</div>
			<div class="All_center_myteam_rimg">
				<img src="${pageContext.request.contextPath}/img/content_rt.png">
			</div>
		</div>
		<hr class="fgx">
       <!-- ===== 我的需求 ====== -->
		<div class="All_center_demand">
			<div class="All_center_myteam_pic">
				<img src="${pageContext.request.contextPath}/img/idx_my_xq.png">
			</div>
			<div class="All_center_myteam_text">
				<div
					class="All_center_myteam_text_son" id="mytask" style="cursor: pointer;">
					我的需求</div>
			</div>
			<div class="All_center_myteam_rimg">
				<img src="${pageContext.request.contextPath}/img/content_rt.png">
			</div>
		</div>
		<hr class="fgx">
<!-- ===== 修改角色====== -->
<!-- 		<div class="All_center_withdrawdeposit"> -->
<!-- 			<div class="All_center_myteam_pic"> -->
<!-- 				<img src="${pageContext.request.contextPath}/img/idx_my_set.png"> -->
<!-- 			</div> -->
<!-- 			<div class="All_center_myteam_text"> -->
<!-- 				<a href="${pageContext.request.contextPath}/UserAction_Userjuese"><div class="All_center_myteam_text_son">个人信息</div></a> -->
<!-- 			</div> -->
<!-- 			<div class="All_center_myteam_rimg"> -->
<!-- 				<img src="${pageContext.request.contextPath}/img/content_rt.png"> -->
<!-- 			</div> -->
<!-- 		</div> -->
<!-- 		<hr class="fgx"> -->
<!-- ===== 退出应用====== -->
		<div class="All_center_withdrawdeposit">
			<div class="All_center_myteam_pic">
				<img src="${pageContext.request.contextPath}/img/idx_my_set.png">
			</div>
			<div class="All_center_myteam_text">
				<a href="${pageContext.request.contextPath}/UserAction_userOuot"><div class="All_center_myteam_text_son">退出应用</div></a>
			</div>
			<div class="All_center_myteam_rimg">
				<img src="${pageContext.request.contextPath}/img/content_rt.png">
			</div>
		</div>
	</div>
	<div style="z-index:-9999;height: 100px;width: 100%;"></div>
	<!--====================== 尾部 ======================-->
	<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_true.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>
</body>
</html>
