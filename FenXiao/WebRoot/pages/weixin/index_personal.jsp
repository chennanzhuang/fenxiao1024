<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>个人资料</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/index_personal.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/foot.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font_size1.css"/>

</head>
<body>

<div class="container-fluid IDX_out">
    <div class="IDX_inner">
        <div class="row IDX_inner_row">
            <div class="col-xs-6 IDX_inner_row_rowNav IDX_inner_row_rowNav_fontTit font15">头像</div>
            <div class="col-xs-6 IDX_inner_row_rowNav">
                <div class="ih_img">
                    <img src="${user.userPhoto }">
                </div>
            </div>
        </div>
        <div class="row IDX_inner_row">
            <div class="col-xs-6 IDX_inner_row_rowNav IDX_inner_row_rowNav_fontTit font15">昵称</div>
            <div class="col-xs-6 IDX_inner_row_rowNav text-right">
                <a href="" class="IDX_inner_row_rowNav_a">
                    <span class="font13 IDX_inner_row_rowNav_rtFont">${user.nickName }</span>
                    <img src="${pageContext.request.contextPath}/img/index_head/ih_rt.png" class="rt">
                </a>
            </div>
        </div>
        <div class="row IDX_inner_row">
            <div class="col-xs-6 IDX_inner_row_rowNav IDX_inner_row_rowNav_fontTit font15">登录号</div>
            <div class="col-xs-6 IDX_inner_row_rowNav text-right">
                <a href="" class="IDX_inner_row_rowNav_a">
                    <span class="font13 IDX_inner_row_rowNav_rtFont">${user.username }</span>
                    <img src="${pageContext.request.contextPath}/img/index_head/ih_rt.png" class="rt">
                </a>
            </div>
        </div>
        <div class="row IDX_inner_row">
            <div class="col-xs-6 IDX_inner_row_rowNav IDX_inner_row_rowNav_fontTit font15">二维码名片</div>
            <div class="col-xs-6 IDX_inner_row_rowNav text-right">
                <a href="" class="IDX_inner_row_rowNav_a">
                    <img class="my_erwm" src="${pageContext.request.contextPath}${user.userQr}">
                    <img src="${pageContext.request.contextPath}/img/index_head/ih_rt.png" class="rt">
                </a>
            </div>
        </div>
    </div>
    <div class="IDX_inner">
        <div class="row IDX_inner_row">
            <div class="col-xs-6 IDX_inner_row_rowNav IDX_inner_row_rowNav_fontTit font15">性别</div>
            <div class="col-xs-6 IDX_inner_row_rowNav text-right">
                <a href="" class="IDX_inner_row_rowNav_a">
                    <span class="font13 IDX_inner_row_rowNav_rtFont">
            <c:if test="${user.sex==0 }">        
                    女
          </c:if>  
            <c:if test="${user.sex==1 }">        
                    男
          </c:if> 
            <c:if test="${user.sex==2 }">        
                    未确定
          </c:if>       
                    </span>
                    <img src="${pageContext.request.contextPath}/img/index_head/ih_rt.png" class="rt">
                </a>
            </div>
        </div>
        <div class="row IDX_inner_row">
            <div class="col-xs-6 IDX_inner_row_rowNav IDX_inner_row_rowNav_fontTit font15">地区</div>
            <div class="col-xs-6 IDX_inner_row_rowNav text-right">
                <a href="" class="IDX_inner_row_rowNav_a">
                    <span class="font13 IDX_inner_row_rowNav_rtFont">${user.province }</span>
                    <span class="font13 IDX_inner_row_rowNav_rtFont">${user.city }</span>
                    <img src="${pageContext.request.contextPath}/img/index_head/ih_rt.png" class="rt">
                </a>
            </div>
        </div>
    </div>

</div>

<!--======================尾部======================-->
<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_true.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>

</body>
<script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
<script src="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
</html>
