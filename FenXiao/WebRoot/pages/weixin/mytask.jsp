<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>我的任务</title>
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/mytask.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/foot.css"
	rel="stylesheet">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
</head>
<body>

	<!--头部-->
	<%-- <header class="box-header">
        <div class="box-header-div1">
            <div class="box-header-div1_div1"><img src="${pageContext.request.contextPath}/img/a-1right.png"></div>
            <div class="box-header-div1-div2"><a href="#"><strong>返回</strong></a></div>
        </div>
        <div class="box-header-div2"><strong> 我的任务 </strong></div>
        <div class="box-header-div3"><a href="#"><img src="${pageContext.request.contextPath}/img/threeDian.png" ></a></div>
    </header> --%>

		<input type="hidden" name="Total" value="${Total }" id="Total">
		<input type="hidden" name="page" value="${page }" id="page"/>
	<!-- 下拉 -->
	<div class="SY_tit">
		<div class="row SY_tit_row">
			<div class="col-xs-4 SY_tit_row_nav">
				<!--<a href="" class="font14"><span>任务类型</span><img src="../img/indexMissionShare/sel.png"></a>-->
				<a id="drop1" href="javascript:;"
					class="dropdown-toggle font14 SY_tit_row_nav_a"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false" onclick="add('menu1');"> <span
					class="dropToggle_span">任务类型</span> <img
					src="${pageContext.request.contextPath}/img/indexMissionShare/sel.png">
				</a>
				<ul id="menu1" class="dropdown-menu dm_wid" aria-labelledby="drop1">
					<li><a href="#">任务类型</a></li>
					<li id="aaa3"><a href="#" onClick="shaixuan1(3)">app下载</a></li>
					<li id="aaa1"><a href="#" onClick="shaixuan1(1)">关注公众号</a></li>
					<li id="aaa2"><a href="#" onClick="shaixuan1(2)">广告宣传</a></li>
				</ul>
			</div>
			<div class="col-xs-4 SY_tit_row_nav">
				<!--<a href="" class="font14"><span>按价格排序</span><img src="../img/indexMissionShare/sel.png"></a>-->
				<a id="drop2" href="javascript:;"
					class="dropdown-toggle font14 SY_tit_row_nav_a"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false" onclick="add('menu2');"> <span
					class="dropToggle_span">价格查询</span> <img
					src="${pageContext.request.contextPath}/img/indexMissionShare/sel.png">
				</a>
				<ul id="menu2" class="dropdown-menu dm_wid" aria-labelledby="drop2">
					<li id="taskPrice00"><a href="javascript:;">价格排序</a></li>
					<li id="taskPrice1"><a href="javascript:;" onClick="shaixuan2(1)">价格最高</a></li>
					<li id="taskPrice3"><a href="javascript:;" onClick="shaixuan2(2)">价格最低</a></li>
				</ul>
			</div>
			<div class="col-xs-4 SY_tit_row_nav">
				<a id="drop3" href="javascript:;"
					class="dropdown-toggle font14 SY_tit_row_nav_a"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false" onclick="add('menu3');"> <span
					class="dropToggle_span">任务状态</span> <img
					src="${pageContext.request.contextPath}/img/indexMissionShare/sel.png">
				</a>
				<ul id="menu3" class="dropdown-menu dm_wid" aria-labelledby="drop3">
					<li id="menu31" ><a href="javascript:;">任务状态</a></li>
					<li id="taskStatus1"><a href="#" onClick="shaixuan3(1)">进行中</a></li>
					<li id="taskStatus5"><a href="#" onClick="shaixuan3(5)">审核中</a></li>
					<li id="taskStatus2"><a href="#" onClick="shaixuan3(2)">已完成</a></li>
					<li id="taskStatus3"><a href="#" onClick="shaixuan3(3)">审核失败</a></li>
				</ul>
			</div>
		</div>
	</div>


	<!-- =================================================================================== -->
	<div class="All container">
		<div class="content" id="fenye">
			<!-- 11111111111============================================== -->
			<c:forEach items="${pagetask}" var="task">
			<c:if test="${task.taskStatus!=6}">
				<div class="content_one" onclick="gettion(${task.userTaskId})">
					<div class="content_one_left">
						<img
							src="${pageContext.request.contextPath}/${task.taskLogo}">
					</div>
					<div class="content_one_center">
						<div class="content_one_center_son">
							<div class="title_p">
								<a  onclick="gettion(${task.userTaskId})"> 《${task.taskTitle }》</a>
							</div>
							<p class="title_bot_sm">
								<span>￥${task.taskBonus }</span> <label> ${task.taskCompletionTime} </label>
							</p>
						</div>
					</div>
					
					<div class="content_one_right">
						<c:if test="${task.taskStatus==2 }">
				任务已完成
				</c:if>
						<c:if test="${task.taskStatus==3 }">
				审核失败
				</c:if>
						<c:if test="${task.taskStatus==1 }">
				请上传截图&nbsp;&nbsp;&nbsp;&nbsp;
						</c:if>
						<c:if test="${task.taskStatus==5 }">
				任务待审核&nbsp;&nbsp;&nbsp;&nbsp;
						</c:if>
					</div>
				</div>
				<hr class="fgx">
				</c:if>
			</c:forEach>
		</div>
<div id="dianji" style="position:absolute; font-size:14px; left:0; width:100%; height:40px; text-align:center; line-height:40px; color:#000; background:#fff;">点击加载更多</div>
	</div>
<input type="hidden" value="${taskStatus }" id="taskStatus">
<input type="hidden" value="${taskPrice }" id="taskPrice">
<input type="hidden" value="${taskType }" id="taskType">
<input type="hidden" value="${openId }" id="openId">
	<!--======================尾部======================-->
	<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>
		<div style="widtn：100%; height:200px;"></div>
</body>


<script src="${pageContext.request.contextPath}/js/jquery-2.0.3.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script>
	function gettion(vale){
		window.location.href="${pageContext.request.contextPath}/TaskAction_orderDetails?userTaskId="+vale;
	}
	
	function add(menu) {
		var menu = document.getElementById(menu).childNodes;
		var menuLi = $(menu);
		menuLi.click(function() {
			$(this).parent().siblings().children().text($(this).text());
		});
	}

	function shaixuan3(value) {
		var openId=$("#openId").val();
		var taskType=$("#taskType").val();
		var taskPrice=$("#taskPrice").val();
		window.location.href="${pageContext.request.contextPath}/UserTaskAction_uploadTimeuploadTime?openId="+openId+"&taskStatus="+value+"&taskType="+taskType+"&taskPrice="+taskPrice;
	}
	
	function shaixuan2(value) {
		var openId=$("#openId").val();
		var taskType=$("#taskType").val();
		var taskStatus=$("#taskStatus").val();
		window.location.href="${pageContext.request.contextPath}/UserTaskAction_shaixuan?openId="+openId+"&taskStatus="+taskStatus+"&taskType="+taskType+"&taskPrice="+value;
	}
	
	function shaixuan1(value) {
		var openId=$("#openId").val();
		var taskStatus=$("#taskStatus").val();
		var taskPrice=$("#taskPrice").val();
		window.location.href="${pageContext.request.contextPath}/UserTaskAction_shaixuan?openId="+openId+"&taskStatus="+taskStatus+"&taskType="+value+"&taskPrice="+taskPrice;;
	}
</script>
<script type="text/javascript">
$(function(){
	var taskType=$("#taskType").val();
	var taskStatus=$("#taskStatus").val();
	var taskPrice=$("#taskPrice").val();
	$("#aaa"+taskType).css("background-color","red");
	$("#taskStatus"+taskStatus).css("background-color","red");
	$("#taskPrice"+taskPrice).css("background-color","red");
});


</script>
<script type="text/javascript">
var page = $("#page").val();
	page++;
	var Total = $("#Total").val();
	if(page>Total){
		$("#dianji").css("display", "none");
	}
</script>
<script type="text/javascript">
	$("#dianji")
			.click(
					function() {
						var page = $("#page").val();
						var opem = $("#openId").val();
						var taskStatus = $("#taskStatus").val();
						var taskPrice = $("#taskPrice").val();
						var taskType = $("#taskType").val();
						var dainji = $("#dianji").html();
						if (dainji == "点击加载更多") {
							$("#dianji").html("正在加载数据...");
							var commcontent = "";
							page++;
							var Total = $("#Total").val();
							$.ajax({
										type : "GET",
										url : "jsonAction_queryMyTask",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
										data : {
											"page" : page,
											"Total" : Total,
											"openId" : opem,
											"taskStatus" : taskStatus,
											"taskPrice" : taskPrice,
											"taskType" : taskType
										},
// 										dataType:"json",
										success : function(result) {
											var jsonArray = eval("(" + result + ")");
											if (jsonArray != null) {
												for (var i = 0; i < jsonArray.length; i++) {
											commcontent+="<div class='content_one' onclick='gettion("+jsonArray[i].UserId+")'>";
											commcontent+="<div class='content_one_left'>";
											commcontent+="<img src='${pageContext.request.contextPath}/"+jsonArray[i].Logo+"'>";
											commcontent+="</div>";
											commcontent+="<div class='content_one_center'>";
											commcontent+="<div class='content_one_center_son'>";
											commcontent+="<div class='title_p'>";
											commcontent+="<a  onclick='gettion("+jsonArray[i].UserId+")'> 《"+jsonArray[i].Title+"》</a>";
											commcontent+="</div>";
											commcontent+="<p class='title_bot_sm'>";
											commcontent+="<span>￥"+jsonArray[i].Bonus+"</span>";
											commcontent+="<label> "+jsonArray[i].CompTime+" </label>";
											commcontent+="</p>";
											commcontent+="</div>";
											commcontent+="</div>";
											commcontent+="<div class='content_one_right'>";
											if(jsonArray[i].Status==2){
											commcontent+="任务已完成&nbsp;&nbsp;&nbsp;&nbsp;";
											}else if(jsonArray[i].Status==3){
											commcontent+="审核失败&nbsp;&nbsp;&nbsp;&nbsp;";
											}else if (jsonArray[i].Status==5) {
											commcontent+="任务带审核&nbsp;&nbsp;&nbsp;&nbsp;";
											}
											commcontent+="</div>";
											commcontent+="</div>";
											commcontent+="<hr class='fgx'>";
												}
												$("#fenye").append(commcontent);
												$("#page").val(page);
												if(page<Total){											
												$("#dianji").html("点击加载更多");
												}else {
														$("#page").val(1);
												$("#dianji").css("display", "none");
												}
											} else {
												$("#dianji").attr({
													"disabled" : "disabled"
												});
												$("#dianji").css("display", "none");
											}
										}
									});
						}

					});
</script>



</html>
