<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>需求详情</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<meta
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
	name="viewport">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/requirement.css" rel="stylesheet">
<link href="css/foot.css" rel="stylesheet">
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>

<style type="text/css">
.pane3_contenttext {
	text-align: center;
	margin: 0px auto;
}

.pane3_contenttext img {
	width: 100%;
	float: left;
}
</style>
</head>


<body>

	<!--头部-->
	<!-- <header class="box-header">
        <div class="box-header-div1">
            <div class="box-header-div1_div1"><img src="img/a-1right.png"></div>
            <div class="box-header-div1-div2"><a href="#"><strong>返回</strong></a></div>
        </div>
        <div class="box-header-div2"><strong> 我的需求 </strong></div>
        <div class="box-header-div3"><a href="#"><img src="img/threeDian.png" ></a></div>
    </header> -->

	<!-- 内容 -->

	<div class="All">

		<!-- panel 1 -->
		<c:if test="${task.taskStatus==1}">
			<div class="panel panel-default">
				<div class="panel-body">
					<ul>
						<li class="panel1-ul1-li1"><label>
								任务编号：${task.taskCode} </label> <span style="font-size:1.2rem">
								任务状态：进行中 </span></li>
						<li class="panel1-ul1-li2"><img src="img/money_icon.png">
							<ul>
								<li class="panel1-ul1-li2-yumon">剩余金额</li>
								<li><b> ${task.taskSBonus} </b></li>
							</ul></li>
						<li class="panel1-ul1-li3"><img src="img/rl_icon.png">
							<ul>
								<li class="panel1-ul1-li3-peonum">关注人数</li>
								<li><b>
										${task.taskTotalNumber-task.taskRemainderNumber} </b></li>
							</ul></li>
						<li class="panel1-ul1-li4">
							<ul class="panel1-ul1-li4_son1 list-inline">
								<li class="panel1-ul1-li4_img1"><img src="img/lo.png">
								</li>
								<li class="panel1-ul1-li4_text1"><a
									href="TaskAction_findGuabzhuUser?openId=${openId}&task.taskId=${task.taskId}"> 查看关注人数列表 </a></li>
							</ul>
							<ul class="panel1-ul1-li4_son2 list-inline">
								<li class="panel1-ul1-li4_img2"><img src="img/biao.png">
								</li>
								<li class="panel1-ul1-li4_text2"><a href="javascript;">
										活动时间:<span class="settime" endTime="${task.taskEndTime}"
										id="${s.index + 1}"></span>
								</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</c:if>
		<!-- panel 2 -->
		<div class="pane2 pane2-default">
			<div class="panel-body">
				<ul>
					<li class="panel2-ul1-li1"><label>
							需求编号：${task.taskCode} </label> <span> 发布时间：${task.pubTime} </span></li>
					<li class="panel2-ul1-li2"><img src="${task.taskLogo}">
						<ul class="panel2-ul1-li2_son">
							<li class="panel2-ul1-li2_son_lititle"><a href="#">
									${task.taskTitle} </a></li>
							<li class="panel2-ul1-li2_son_licon"><a>
									${task.taskBriefing} </a></li>
						</ul></li>

					<li class="pane_price text-right"><label> 总价格<span>
								￥${task.taskTotalBonus} </span>
					</label></li>

					<li class="panel1-ul2-li3">
						<ul class="list-inline panel1-ul2-li3_son1">
							<li class="nr">需求类型:</li>
							<li class="dw"><c:if test="${task.taskType==1 }">
                                                                       关注公众号</c:if>
								<c:if test="${task.taskType==2 }">
                                                                      广告宣传</c:if>
								<c:if test="${task.taskType==3 }">
                             APP下载
                            </c:if></li>
						</ul>
						<ul class="list-inline panel1-ul2-li3_son2">
							<img src="img/money_icon.png">
							<li class="cotrnum">任务总数:</li>
							<li class="snum">${task.taskTotalNumber}</li>
						</ul>
					</li>
					<li class="panel1-ul3-li4"><c:if test="${task.taskStatus==0}">
							<ul class="panel1-ul3-li4_son1 list-inline">
								<li class="panel3-ul1-li4_text1">
									合计:${task.heji}(含手续费 ¥${task.shouxu})</li>
								<li class="panel3-ul1-li4_text2"><a
									href="TaskAction_payPage?user.id=${user.id}&task.taskId=${task.taskId}"><button
											class="box-zhifu" id="sub">去支付</button></a></li>
							</ul>
						</c:if> <c:if test="${task.taskStatus==1}">
							<ul class="panel1-ul3-li4_son1 list-inline">
								<li class="panel3-ul1-li4_text1">
									合计:${task.heji}(含手续费 ¥${task.shouxu})</li>
								<li class="panel3-ul1-li4_text2">进行中</li>
							</ul>
						</c:if> <c:if test="${task.taskStatus==2}">
							<ul class="panel1-ul3-li4_son1 list-inline">
								<li class="panel3-ul1-li4_text1">退款:${task.taskSBonus}</li>
								<li class="panel3-ul1-li4_text2">已退款</li>
							</ul>
						</c:if> <c:if test="${task.taskStatus==4}">
							<ul class="panel1-ul3-li4_son1 list-inline">
								<li class="panel3-ul1-li4_text1">
									合计:${task.heji}(含手续费 ¥${task.shouxu})</li>
								<li class="panel3-ul1-li4_text2">待审核</li>
							</ul>
						</c:if></li>
				</ul>
			</div>
		</div>
		<c:if test="${task.taskStatus==1||task.taskStatus==2}">
			<div class="pane3_label" style="margin-bottom: 50px">
				<label> 详情介绍 </label>
			</div>
			<div class="pane3 pane3-default">
				<div class="panel-body">
					<div class="pane3_contenttext" align="center">
						${task.taskInfo}</div>
					<!-- <div class="pane3_img">
                    <img src="img/Ad_Details_bo.png">
                </div> -->
				</div>
			</div>
		</c:if>
	</div>


	<!--======================尾部======================-->
	<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_true.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>

</body>
<script>
	$(document).ready(
			function() {
				//手势右滑 回到上一个画面
				$('#myCarousel').bind('swiperight swiperightup swiperightdown',
						function() {
							$("#myCarousel").carousel('prev');
						});
				//手势左滑 进入下一个画面
				$('#myCarousel').bind('swipeleft swipeleftup swipeleftdown',
						function() {
							$("#myCarousel").carousel('next');
						});
			});

	$(".SY_cont_two_now").click(function() {
		$(".SY_cont_two_well").removeClass("SY_cont_two_default");
		$(this).addClass("SY_cont_two_default");
		$("#SY_well").css("display", "none");
		$("#SY_now").css("display", "block");
	});
	$(".SY_cont_two_well").click(function() {
		$(".SY_cont_two_now").removeClass("SY_cont_two_default");
		$(this).addClass("SY_cont_two_default");
		$("#SY_now").css("display", "none");
		$("#SY_well").css("display", "block");
	});

	function remainedTime(value) {
		var time_start = new Date().getTime();//设定开始时间 
		var endTime = $("#endTime" + value).val();
		var endTime1 = endTime.replace("-", "/");
		var endTime2 = endTime1.replace("-", "/");
		/* alert(endTime2.length); */

		var s = endTime2.substring(0, endTime2.length - 2);
		console.log(s);
		var time_end = new Date(s).getTime(); //设定结束时间(等于系统当前时间) 
		//计算时间差 
		var time_distance = time_end - time_start;
		if (time_distance > 0) {
			// 天时分秒换算 
			var int_day = Math.floor(time_distance / 86400000)
			time_distance -= int_day * 86400000;

			var int_hour = Math.floor(time_distance / 3600000)
			time_distance -= int_hour * 3600000;

			var int_minute = Math.floor(time_distance / 60000)
			time_distance -= int_minute * 60000;

			var int_second = Math.floor(time_distance / 1000)
			// 时分秒为单数时、前面加零 
			if (int_day < 10) {
				int_day = "0" + int_day;
			}
			if (int_hour < 10) {
				int_hour = "0" + int_hour;
			}
			if (int_minute < 10) {
				int_minute = "0" + int_minute;
			}
			if (int_second < 10) {
				int_second = "0" + int_second;
			}
			// 显示时间 
			$("#time_d" + value).html(int_day + " 天");
			$("#time_h" + value).html(int_hour + " 时");
			$("#time_m" + value).html(int_minute + " 分");
			$("#time_s" + value).html(int_second + " 秒");

			setTimeout("remainedTime(" + value + ")", 1000);

		} else {
			$("#time_d" + value).html('时');
			$("#time_h" + value).html('间');
			$("#time_m" + value).html('结');
			$("#time_s" + value).html('束');

		}
		$("#button" + value).css("display", "none");
		$("#span" + value).css("display", "block");

	}
</script>
<script language="javascript">
	$(function() {
		updateEndTime();
	});
	//倒计时函数
	function updateEndTime() {
		var date = new Date();
		var time = date.getTime(); //当前时间距1970年1月1日之间的毫秒数

		$(".settime").each(
				function(i) {

					var endDate = this.getAttribute("endTime"); //结束时间字符串
					//转换为时间日期类型
					var endDate1 = eval('new Date('
							+ endDate.replace(/\d+(?=-[^-]+$)/, function(a) {
								return parseInt(a, 10) - 1;
							}).match(/\d+/g) + ')');

					var endTime = endDate1.getTime(); //结束时间毫秒数

					var lag = (endTime - time) / 1000; //当前时间和结束时间之间的秒数
					if (lag > 0) {
						var second = Math.floor(lag % 60);
						var minite = Math.floor((lag / 60) % 60);
						var hour = Math.floor((lag / 3600) % 24);
						var day = Math.floor((lag / 3600) / 24);
						$(this).html(
								day + "天" + hour + "小时" + minite + "分" + second
										+ "秒");
					} else {
						$(this).removeClass("settime");
						//alert($(this).attr("id"));
						$(this).html("任务已经结束啦！");
					}
				});
		setTimeout("updateEndTime()", 1000);
	}
</script>
</html>