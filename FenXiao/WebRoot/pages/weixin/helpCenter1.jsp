<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title> 帮助中心 </title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap-3.3.5-dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/helpCenter.css" >
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/foot.css"/>
</head>
<body>

    <div class="all col-xs-12">

        <!-- 圆点 样式 -->
        <div class="icon">
            <a class="round">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
            <hr class="hr1">

            <a class="round round-margintow">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
            <hr class="hr2">

            <a class="round round-marginthree">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
        </div>

        <!-- right -->
        <div class="right col-xs-11">
            <!-- content1 如何做任务 -->
            <div class="contenet-task">
                <h4>如何做任务?</h4>
                <ul>
                    <li> <span>1</span> 点击任务，进入任务详情页面。 </li>
                    <li> <span>2</span> 按照任务详情页面的任务操作步骤进行操作。 </li>
                    <li> <span>3</span> 完成任务。 </li>
                </ul>
            </div>

            <!-- content2 如何提现 -->
            <div class="content-right content-withdrawal">
                <h4>如何提现?</h4>
                <ul>
                    <li> <span>1</span> 打开我的钱包，进入钱包页面。 </li>
                    <li> <span>2</span> 点击提现按钮，进入提现页面，填写内容并点击确定。 </li>
                    <li> <span>3</span> 提现不成功，查看提现须知。 </li>
                </ul>
            </div>

            <!-- content3 提现须知 -->
            <div class="content-right content-withdrawalNotice" style="margin-bottom:20%;">
                <h4>提现须知?</h4>
                <ul>
                    <li> <span>1</span> .提现说明 </li>
                        <p> A最低提现金额50元 </p>
                        <p> B单笔提现最高金额5000元 </p>
                        <p> C每日累计提现最高20000元 </p>
                    <li> <span>2</span> .提现周期 </li>
                        <p> 周一至周五0:00到24:00 </p>
                        <p> 提现周期为T+3(T为当天)，如遇节假日顺延 </p>
                    <li> <span>3</span> .提现手续费 </li>
                        <p> A单笔最低2元，最高25元 </p>
                        <p> B超过2元且低于25元的区间，按0.6%计算 </p>
                </ul>
            </div>
        </div>

    </div>

    <!--======================尾部======================-->
    <div class="container-fluid all_foot">
        <%-- <div class="row all_foot_nav">
            <div class="col-xs-3 all_foot_nav_navs">
				<a
					class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
					href="${pageContext.request.contextPath}/UserAction_homepage?openId=${openId}">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">首页</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block"
					href="${pageContext.request.contextPath}/UserTaskAction_queryTaskForTheMan?openId=${openId}">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">任务</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block "
					href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser?openId=${openId}">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">钱包</p>
				</a>
			</div>
			<div class="col-xs-3 all_foot_nav_navs">
				<a class="text-center help-block all_foot_nav_navs_block "
					href="${pageContext.request.contextPath}/UserAction_myPersonal?openId=${openId}">
					<div class="all_foot_nav_navs_block_icon">
						<img
							src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
					</div>
					<p class="all_foot_nav_navs_block_title font13">我的</p>
				</a>
			</div>
        </div> --%>
    </div>

</body>
</html>
