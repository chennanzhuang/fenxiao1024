<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>注册</title>
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet">
<!--<link href="css/login.css" rel="stylesheet" >-->
<link href="${pageContext.request.contextPath}/css/register.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/foot.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/daohan.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-1.7.2.min.js"></script>
<script src="${pageContext.request.contextPath}/jsp/layer/layer.js"
	type="text/javascript"></script>
<script charset="utf-8"
	src="${pageContext.request.contextPath}/js/text.js"></script>
<script type="text/javascript"
	src="http://res.wx.qq.com/open/js/jweixin-1.1.0.js"></script>

<style>
select {
	border: none;
	appearance: none;
	-moz-appearance: none;
	-webkit-appearance: none;
	margin: 0;
	padding: 0;
	outline: none;
	list-style: none;
	background: none;
	text-decoration: none;
	border: 0;
	font-weight: normal;
	font-family: "Microsoft YaHei";
	background: url("${pageContext.request.contextPath}/img/bottom-img.png")
		no-repeat scroll right center transparent;
	background-size: 14px 14px;
	-moz-background-size: 14px 14px;
	-webkit-background-size: 14px 14px;
	padding-right: 14px;
}

select {
	font-family: inherit;
	font-size: inherit;
	line-height: inherit;
}

select {
	text-transform: none;
	width: 25%
}

.se {
	height: 32px;
	color: #a9a9a9;
	outline: medium;
	margin-left: 2%;
}
</style>
</head>

<body>
	<div class="All">
		<form action="UserAction_register" method="post" class="form">
			<div class="register container-fluid">
				<!-- 推荐人 -->
				<div class="referee mg">
					<img src="${pageContext.request.contextPath}/img/praise.png"
						class="mine"> <input type="text" id="code" placeholder="推荐人"
						class="tuijm" name="recomCode" value="rec20161017103054708"
						readonly="readonly">
					<!-- 						<input type="text"id="code" -->
					<!-- 						placeholder="推荐人"	class="tuijm" name="recomCode" value="${recomCode}" readonly="readonly">	 -->
				</div>
				<!-- 手机号码 -->
				<hr class="fgx">
				<div class="phone mg">
					<img src="${pageContext.request.contextPath}/img/phonenumber.png"
						class="phone_icon"> <input type="text"
						placeholder="请输入11位绑定手机号" name="mobile" id="mobile" value="">
					<input type="hidden" value="${openId }" name="WeChatId" /> <input
						type="hidden" value="${regTime }" id="regTime" /> <input
						type="hidden" value="${agentsId }" name="agentsId" />
						<input type="hidden" value="${sex }" name="sex" />
						<input type="hidden" value="${nickName }" name="nickName" id="nickName"/>
						<input type="hidden" value="${userPhoto }" name="userPhoto" />

				</div>
				<!-- 短信验证码 -->
				<hr class="fgx">
				<div class="phonemessage mg">
					<img src="${pageContext.request.contextPath}/img/message.png"
						class="phonemessage_icon"> <input type="text"
						placeholder="请输入短信验证码" id="smsCheckCode"> <input
						type="hidden" id="backstage" value=""> <input
						type="button" value="获取验证码" class="huoqucode" id="btnSendCode"
						style="height:30px;width:28%;font-size:10px;"
						onClick="getSMSCheckCode(this),sendMessage()" />
				</div>
				<!-- 昵称 -->
				<hr class="fgx nickname_hr">
				<div class="nickname mg">
					<img src="${pageContext.request.contextPath}/img/mine_f.png"
						class="nickname_icon"> <input type="text"
						placeholder="请输入昵称" name="userName" id="username">
				</div>
				<!-- 密码 -->
				<hr class="fgx">
				<div class="pwd mg">
					<img src="${pageContext.request.contextPath}/img/lock.png"
						class="lock"> <input type="password"
						placeholder="请设置密码(6-16个字符)" name="userPassword" id="password"
						value="">
				</div>
				<!-- 重复密码 -->
				<hr class="fgx">
				<div class="repeatpwd mg">
					<img src="${pageContext.request.contextPath}/img/lock.png"
						class="lock"> <input type="password"
						placeholder="请重复密码(6-16个字符)" id="repeatPassword" value="">
				</div>
				<hr class="fgx nickname_hr">
				<div class="nickname mg">
					<img src="${pageContext.request.contextPath}/img/mine_f.png"
						class="nickname_icon">
					<div class="se1" >
						<select class="se" id="select_province" name="province"
							style="margin-left:2%;"></select> <select class="se"
							id="select_city" name="city"></select> <select class="se"
							id="select_area" name="area"></select>

					</div>
				</div>
				<input id="mobileCheck111" value="" type="hidden">


			</div>
			</form>
			<div class="retister_enter container-fluid">
				<input class="registerenter_btn btn" type="button" id="submit111"
					value="注册">
			</div>
			<div
				style="width: 100%;text-align: center;font-size: 1.8rem;color:#ccc;margin-top: 20px;;margin-bottom: 150px;">
				<input type="checkbox" id="checbox" value="0" checked="checked"><a
					href="${pageContext.request.contextPath}/pages/weixin/tonyi.jsp">同意注册条款</a>
			</div>
	</div>
	<!--======================尾部======================-->

	<div class="container-fluid all_foot">
			<div class="row all_foot_nav">
				<div class="col-xs-3 all_foot_nav_navs">
					<a
						class="text-center help-block all_foot_nav_navs_block all_foot_nav_navs_block_true"
						href="${pageContext.request.contextPath}/UserAction_homepage">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/index_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">首页</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block"
						href="${pageContext.request.contextPath}/TaskAction_queryOnTask">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/renWu_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">任务</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/TaskAction_queryMoneyOfUser">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/money_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">钱包</p>
					</a>
				</div>
				<div class="col-xs-3 all_foot_nav_navs">
					<a class="text-center help-block all_foot_nav_navs_block "
						href="${pageContext.request.contextPath}/UserAction_myPersonal">
						<div class="all_foot_nav_navs_block_icon">
							<img
								src="${pageContext.request.contextPath}/img/allIMG/mine_f.png">
						</div>
						<p class="all_foot_nav_navs_block_title font13">我的</p>
					</a>
				</div>
			</div>
		</div>

</body>
<script type="text/javascript">
	region_init("select_province", "select_city", "select_area");
</script>
<script type="text/javascript">
	function getSMSCheckCode(object) {
		/* alert(132); */
		var mobile = $("#mobile").val();
		var mobileCheck = $("#mobileCheck111").val();

		if ($.trim(mobile) == "") {
			layer.msg("手机号不能为空！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}
		;

		var myreg = /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
		if (!myreg.test($("#mobile").val())) {
			layer.msg("请输入有效手机号！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}
		$
				.ajax({
					type : "POST",
					url : "UserAction_checkMobile",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
					data : {
						"mobile" : mobile
					},
					
					//dataType:"json",
					success : function(data) {
						if (data.code == 0) {
							layer.msg("手机号码已注册！", {
								time : 3000,
								offset : 0,
								shift : 6
							});
							$("#mobile").val("");
						} else if (data.code == 1) {
						
							$
									.ajax({
										type : "GET",
										url : "UserAction_getSMSCheckCode",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
										data : {
											"mobile" : mobile
										},
										//dataType:"json",
										success : function(data) {
											if (data.codeT == 0) {
												alert(data.msg);
											} else {
												$("#backstage").val(data.code);
												$(object)
														.html(
																"<font color='green' size='2'>验证码已发</font>");
											}
										}
									});

						}
					}
				});

	}
</script>
<script type="text/javascript">
	var regTime = $("#regTime").val();
	if (regTime != null) {
		$(".form")
				.attr("action",
						"${pageContext.request.contextPath}/UserAction_registerAfterSubscribe2");
	}
	if (regTime == null || regTime == "") {
		$(".form")
				.attr("action",
						"${pageContext.request.contextPath}/UserAction_register?dianji=1");
	}
	
</script>
<script type="text/javascript">
	$(function() {
		$("#mobile")
				.blur(
						function() {
							var mobile = $("#mobile").val();
							if ($.trim(mobile) == "") {
								layer.msg("手机号不能为空！", {
									time : 3000,
									offset : 0,
									shift : 6
								});
							}
							var myreg = /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
							if (!myreg.test($("#mobile").val())) {
								layer.msg("请输入有效手机号！", {
									time : 3000,
									offset : 0,
									shift : 6
								});
							}

							$.ajax({
								type : "GET",
								url : "UserAction_checkMobile",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
								data : {
									"mobile" : mobile
								},
								//dataType:"json",
								success : function(data) {
									if (data.code == 0) {
										layer.msg("手机号码已注册！", {
											time : 3000,
											offset : 0,
											shift : 6
										});
									}
								}
							});
						});

		$("#username").blur(function() {
			var username = $("#username").val();
			if ($.trim(username) == "") {
				layer.msg("昵称不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
			}

		});
		$("#code").blur(function() {
			var code = $("#code").val();
			if ($.trim(code) == "") {
				layer.msg("推荐码不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
			}

		});

// 				$("#smsCheckCode").blur(function() {
// 					var smsCheckCode = $("#smsCheckCode").val();
// 					var backstage = $("#backstage").val();
// 					if ($.trim(smsCheckCode) == "") {
// 						layer.msg("验证码不能为空！", {
// 							time : 3000,
// 							offset : 0,
// 							shift : 6
// 						});
// 					}
// 					if (smsCheckCode != backstage && backstage != "") {
// 						layer.msg("验证码不正确！", {
// 							time : 3000,
// 							offset : 0,
// 							shift : 6
// 						});
// 					}
// 				});

		$("#password").blur(function() {
			var password = $("#password").val().trim();
			var repeatPassword = $("#repeatPassword").val();
			if ($.trim(password) == "") {
				layer.msg("密码不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
			}
			if (password.length<6||password.length>16) {
				layer.msg("注册密码长度6~16！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
			}

			if (password != repeatPassword && repeatPassword != "") {
				layer.msg("两次输入的密码不一致！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
			}
		});

		$("#repeatPassword").blur(function() {
			var password = $("#password").val();
			var repeatPassword = $("#repeatPassword").val();
			if ($.trim(repeatPassword) == "") {
				layer.msg("密码不能为空！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
			}

			if (password != repeatPassword && password != "") {
				layer.msg("两次输入的密码不一致！", {
					time : 3000,
					offset : 0,
					shift : 6
				});
			}
		});

		$("#submit111")
				.click(
						function() {
							$("#submit111").attr({
								"disabled" : "disabled"
							});
							var flag = false;
							 var ranges = [        
       	 '\ud83c[\udf00-\udfff]', 
        	'\ud83d[\udc00-\ude4f]', 
       	 '\ud83d[\ude80-\udeff]'
    ];    
    var emojireg = $("#username").val();
    var emojireg1 = $("#nickName").val();
    emojireg = emojireg .replace(new RegExp(ranges.join('|'), 'g'), '');
    emojireg1 = emojireg1 .replace(new RegExp(ranges.join('|'), 'g'), '');
    $("#username").val(emojireg);
    $("#nickName").val(emojireg1);
							var mobile = $("#mobile").val();
							if ($.trim(mobile) == "") {
								layer.msg("手机号不能为空！", {
									time : 3000,
									offset : 0,
									shift : 6
								});
								$("#submit111").removeAttr("disabled");
								return false;
							}
							 
                           
							   var diqu = $(".se").val();
							   if ($.trim(diqu) == "省") {
								layer.msg("地区不能为空！", {
									time : 3000,
									offset : 0,
									shift : 6
								});
								$("#submit111").removeAttr("disabled");
								return false;
							}
							   
							var myreg = /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
							if (!myreg.test($("#mobile").val())) {
								layer.msg("请输入有效手机号！", {
									time : 3000,
									offset : 0,
									shift : 6
								});
								$("#submit111").removeAttr("disabled");
								return false;
							}
							$.ajax({
								type : "POST",
								url : "UserAction_checkMobile",//如果是主表，校验数据是否有重复，如果是子表，校验在上传之前需要主表有数据，并且不能重复。
								async : false,
								data : {
									"mobile" : mobile
								},
								//dataType:"json",
								success : function(data) {
									if (data.code == 0) {
										layer.msg("手机号码已注册！", {
											time : 3000,
											offset : 0,
											shift : 6
										});
										$("#mobile").val("");
										flag = false;
										$("#submit111").removeAttr("disabled");
										return false;
									} else {
										flag = true;
									}
								}
							});

// 														var smsCheckCode = $("#smsCheckCode").val();
// 														var backstage = $("#backstage").val();
// 														if ($.trim(smsCheckCode) == "") {
// 															layer.msg("验证码不能为空！", {
// 																time : 3000,
// 																offset : 0,
// 																shift : 6
// 															});
// 															$("#submit111").removeAttr("disabled");
// 															return false;
// 														}
							var username = $("#username").val();
							if ($.trim(username) == "") {
								layer.msg("昵称不能为空！", {
									time : 3000,
									offset : 0,
									shift : 6
								});
								$("#submit111").removeAttr("disabled");
								return false;
							}
							var code = $("#code").val();
							if ($.trim(code) == "") {
								layer.msg("推荐码不能为空！", {
									time : 3000,
									offset : 0,
									shift : 6
								});
								$("#submit111").removeAttr("disabled");
								return false;
							}
														var checbox = $("#checbox").val();
														if ($.trim(checbox) == "") {
															layer.msg("请选择同意注册条款单！", {
																time : 3000,
																offset : 0,
																shift : 6
															});
															$("#submit111").removeAttr("disabled");
															return false;
														}
// 														if ($.trim(backstage) == "") {
// 															layer.msg("请先获取验证码！", {
// 																time : 3000,
// 																offset : 0,
// 																shift : 6
// 															});
// 															$("#submit111").removeAttr("disabled");
// 															return false;
// 														}
// 														if (smsCheckCode != backstage && backstage != "") {
// 															layer.msg("验证码不正确！", {
// 																time : 3000,
// 																offset : 0,
// 																shift : 6
// 															});
// 															$("#submit111").removeAttr("disabled");
// 															return false;
// 														}

							var repeatPassword = $("#repeatPassword").val();
							var password = $("#password").val().trim();
							if (password.length<6||password.length>16) {
								layer.msg("注册密码长度6~16！", {
									time : 3000,
									offset : 0,
									shift : 6
								});
								$("#submit111").removeAttr("disabled");
								return false;
							}
							if ($.trim(password) == "") {
								layer.msg("密码不能为空！", {
									time : 3000,
									offset : 0,
									shift : 6
								});
								$("#submit111").removeAttr("disabled");
								return false;
							}

							if (password != repeatPassword
									&& repeatPassword != "") {
								layer.msg("两次输入的密码不一致！", {
									time : 3000,
									offset : 0,
									shift : 6
								});
								$("#submit111").removeAttr("disabled");
								return false;
							}
							if (flag) {
							alert("进入方法")
								$(".form").submit();
							} else {
								$("#submit111").removeAttr("disabled");
							}

						});
	});
</script>
<script type="text/javascript">
	var InterValObj; //timer变量，控制时间
	var count = 60; //间隔函数，1秒执行
	var curCount;//当前剩余秒数

	function sendMessage() {
		curCount = count;
		var mobile = $("#mobile").val();
		if ($.trim(mobile) == "") {
			layer.msg("手机号不能为空！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}

		var myreg = /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
		if (!myreg.test($("#mobile").val())) {
			layer.msg("请输入有效手机号！", {
				time : 3000,
				offset : 0,
				shift : 6
			});
			return false;
		}
		$("#btnSendCode").attr("disabled", "true");
		$("#btnSendCode").val("请在" + curCount + "秒内输入验证码");
		InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次

	}

	//timer处理函数
	function SetRemainTime() {
		if (curCount == 0) {
			window.clearInterval(InterValObj);//停止计时器
			$("#btnSendCode").removeAttr("disabled");//启用按钮
			$("#btnSendCode").val("重新发送验证码");
		} else {
			curCount--;
			$("#btnSendCode").val("请在" + curCount + "秒内输入验证码");
		}
	}
</script>
<script type="text/javascript">
	function reminder() {
		layer.msg("请先登录！", {
			time : 3000,
			offset : 0,
			shift : 6
		});

	}
</script>
<script type="text/javascript">
	var msg = "${msg}";
	if (msg != "") {
		layer.msg(msg, {
			time : 3000,
			offset : 0,
			shift : 6
		});
	}
</script>
</html>
